@echo off

if "%1" == "" (
	echo Usage: stop [BootFile]
	echo Stop default application...
)
java -jar bin/sgs-stop.jar %1
rmdir data /s /q
