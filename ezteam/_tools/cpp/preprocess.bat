
@echo off

REM ##################################################################
REM Parameters:
REM 	- CPP tool path: _tools/cpp
REM 	- Preprocess directory: .version/3-preprocess
REM 	- Add _DEBUG definition if compile mode is debug
REM ##################################################################

set DEFINITION=
set DEFINITION_FILE=

if "%COMPILE_MODE%"=="debug" (
	set DEFINITION=-D _DEBUG
)

if not "%PLATFORM%"=="" (
	set DEFINITION=%DEFINITION% -D %PLATFORM%
)

if not "%DEFINITION%"=="" (
	echo [CPP] Set definition: %DEFINITION%
)

if exist _DEFINES.H (
	set DEFINITION_FILE=-imacros _DEFINES.H
)

for %%i in (*.java) do (
REM for /f %%i in ('dir /b /s *.java') do (
	REM echo [CPP] Propressing file: %%i
	%1/cpp.exe -CC -P %DEFINITION% %DEFINITION_FILE% %%i %2/%%i
)

for %%i in (*.cs) do (
REM for /f %%i in ('dir /b /s *.cs') do (
	REM echo [CPP] Propressing file: %%i
	%1/cpp.exe -CC -P %DEFINITION% %DEFINITION_FILE% %%i %2/%%i
)
