
@echo off
REM Create a class from multiple *.h file in a directory.
REM Using: ClassCreater.bat file-data class-name inlucde-directory
REM Example: ClassCreater.bat DATA.cs DATA ./src
REM Thanh Vinh <thanh.vinh@hotmail.com>

set FILE_NAME=%1
set CLASS_NAME=%2
set INPUT_PATH=%3

echo Using ClassCreater to generate %CLASS_NAME% class

REM Class header
REM echo.	public static class %CLASS_NAME%>>			%FILE_NAME%
REM echo.	{>>											%FILE_NAME%
echo. >												%FILE_NAME%
echo public class %CLASS_NAME%>>					%FILE_NAME%
echo {>>											%FILE_NAME%

REM Include *.h file
for /f %%i in ('dir /b *.h') do (
	echo Including file: %%i
	more %%i >> 									%FILE_NAME%
)

REM Class footer
REM echo.	}>>											%FILE_NAME%
echo }>>											%FILE_NAME%

echo Completed!
