 SpriteEditor Alpha version 0.0.1 (revision 22)


 ---------------------------------------------------------------------
| HOW TO RUN                                                          |
 ---------------------------------------------------------------------
 Double click on SpriteEditor.exe to run it. :)
 Requirement: .NET Framework 3.0 (haven't test in other version yet)


 ---------------------------------------------------------------------
| HOW TO EXPORT SOURCE CODE                                           |
 ---------------------------------------------------------------------
 Command: SpriteEditor.exe input-sprite-file source-file-path
 
 Example:
 	SpriteEditor.exe ./template.sprite . .
