-This tool is used to convert xls into csv.
-Usage:
	ConvertXLS2CVS.exe sourceFilePath targetFolderPath [numSheet]
	* numSheet is a option. If numSheet = -1, convert all sheets. If don't have this option, only convert first sheet.
-Requirement: .net 2.0