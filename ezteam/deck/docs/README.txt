 ---------------------------------------------------------------------
| HOW TO BUILD WITH ANT                                               |
 ---------------------------------------------------------------------
 1/ Set JAVA_HOME & ANT_HOME variables in ./make/config.bat
 2/ Set WTK_PATH variable in ./make/build.properties
 3/ Call ./make/make [option]
		make data		to make the data
		make compile	to compile the source
		make debug		to make the debug version
		make release	to make the release version
		make clean		to clean

	Note:
		- debug is the default option, you can call "make.bat" to make debug version
		- to obfucaste, copy ./_tools/proguard.jar to WTK_PATH/bin


 ---------------------------------------------------------------------
| HOW TO DEBUG WITH ECLIPSE                                           |
 ---------------------------------------------------------------------
 1/ Use ./.eclipse as a workspace directory
 2/ Create new project and add link source to ./.version/4-preprocess and ./.version/2-dataPackage directories
 3/ Build & debug this project

 * Please don't commit any files from ./.eclipse directory


 ---------------------------------------------------------------------
| PROJECT MANAGER                                                     |
 ---------------------------------------------------------------------
 To open project manager file (*.POD), download OpenProject at http://openproj.org/, use it to open.
