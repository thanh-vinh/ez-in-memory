
@echo off

echo Export map layers...
for /f %%i in ('dir /b *.tmx') do (
	java -jar %TILED_MAP_EXPORTER% %%i %1
)
