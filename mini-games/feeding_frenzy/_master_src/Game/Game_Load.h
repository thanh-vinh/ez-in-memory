
	///*********************************************************************
	///* Game_Load.h
	///*********************************************************************

	private static void loadResource(int step)
	{
		#if _DEBUG
		printLoadingStepName(step);
		#endif
		switch (step) {
			case LOADING_RMS:
				setting = new Setting(RECORD_NAME, RECORD_SIZE);
				gameLevel = setting.readByte(RECORD_LEVEL);
				heroLevel = setting.readByte(RECORD_HERO_LEVEL);
				score = setting.readShort(RECORD_SCORE);
				break;

			case LOADING_PACK_OPEN_SPRITE:
				Package.open(PACK_SPRITE);
				break;

			case LOADING_PACK_OPEN_SPRITE2:
				Package.open(PACK_SPRITE2);
				break;

			case LOADING_PACK_OPEN_SPRITE3:
				Package.open(PACK_SPRITE3);
				break;

			case LOADING_PACK_OPEN_SPRITE4:
				Package.open(PACK_SPRITE4);
				break;

			case LOADING_PACK_OPEN_FONT:
				Package.open(PACK_FONT);
				break;

			case LOADING_PACK_OPEN_TEXT:
				Package.open(PACK_TEXT);
				break;

			case LOADING_PACK_CLOSE:
				Package.close();
				break;

			case LOADING_SPRITE_LOGO:
				sprites[SPRITE_LOGO] = Package.loadSprite(SPRITE_LOGO, false);
				break;

			case LOADING_FONT:
				fonts[FONT_NORMAL] = Package.loadFontSprite(FONT_NORMAL, FONT_NORMAL_MAPPING, true);
				// Cache all palettes
				for (int i = 0; i < fonts[FONT_NORMAL].getPaletteCount(); i++) {
					fonts[FONT_NORMAL].cache(i);
				}
				fonts[FONT_NORMAL].uncacheRGB();
				break;

			case LOADING_TEXT:
				if (setting.readByte(RECORD_LANGUAGE) == TEXT_EN) {
					Package.loadText(TEXT_EN);
				} else {
					Package.loadText(TEXT_VI);
				}
				break;

			case LOADING_SOUND:
				loadSoundPack();
				break;

			case LOADING_SPRITE_MENU:
				sprites[SPRITE_MENU] = Package.loadSprite(SPRITE_MENU, true);
				sprites[SPRITE_MENU].cache();
				sprites[SPRITE_MENU].uncacheRGB();
				break;

			case LOADING_SPRITE_SPLASH:
				sprites[SPRITE_SPLASH] = Package.loadSprite(SPRITE_SPLASH, false);
				break;

			case LOADING_SPRITE_HERO:
				sprites[SPRITE_HERO] = Package.loadSprite(SPRITE_HERO, true);
				sprites[SPRITE_HERO].cache();
				sprites[SPRITE_HERO].uncacheRGB();
				break;

			case LOADING_SPRITE_FISH:
				// TODO Check level to load right fish sprite
				int spriteId = SPRITE_FISH_LEVEL1 + heroLevel;
				for (int i = 0; i < LEVEL_MAX_SPRITE_FISH; i++) {
					sprites[spriteId] = Package.loadSprite(spriteId, true);
					sprites[spriteId].cache();
					sprites[spriteId].uncacheRGB();

					spriteId++;
				}
				break;

			case LOADING_INIT_ACTOR:
				// TODO Define number of each fist type for each level
				hero = new Actor(ACTOR_HERO, sprites[SPRITE_HERO]);

				int fishCount = LEVEL_ACTOR_FISH[gameLevel * 5];
				actors = new Actor[fishCount];

				spriteId = SPRITE_FISH_LEVEL1 + heroLevel;
				for (int i = 0; i < fishCount; i++) {
					actors[i] = new Actor(ACTOR_FISH_LEVEL1, sprites[spriteId]);
					for (int j = 0; j < 4; j++) {
						if (i == LEVEL_ACTOR_FISH[(gameLevel * 5) + j]) {
							spriteId++;
						}
					}
				}
				break;

			case LOADING_SPRITE_TILESET:
				sprites[SPRITE_TILE_SEA] = Package.loadSprite(SPRITE_TILE_SEA, true);
				sprites[SPRITE_TILE_SEA].cache();
				sprites[SPRITE_TILE_SEA].uncacheRGB();

				sprites[SPRITE_TILE_MAP1] = Package.loadSprite(SPRITE3_TILE_MAP1, true);
				sprites[SPRITE_TILE_MAP1].cache();
				sprites[SPRITE_TILE_MAP1].uncacheRGB();
				break;

			case LOADING_MAP_LAYER:
				GameLib.setTileLayer(MAP_MAX, TILE_WIDTH, TILE_HEIGHT);
				GameLib.loadTileset(MAP_SEA, PACK_MAP, MAP_SEA);
				GameLib.loadTileset(MAP_MAP1, PACK_MAP, MAP_MAP1);
				break;

			case LOADING_FREE_GAME_PLAY:
				sprites[SPRITE_HERO].finalize();
				sprites[SPRITE_HERO] = null;
				break;
		}
	}

	private static int[] loadingSteps;
	private static int loadStep;

	private static void updateLoading(Graphics g, int message, int type)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				if (sprites == null && fonts == null) {
					sprites = new GameSprite[SPRITE_MAX];
					fonts = new FontSprite[FONT_MAX];
				}

				if (type == LOADING_TYPE_INIT_GAME) {
					loadingSteps = LOADING_STEPS_INIT_GAME;
					currentTime = System.currentTimeMillis();
				} else if (type == LOADING_TYPE_GAME_PLAY) {
					loadingSteps = LOADING_STEPS_GAMEPLAY;
				} else if (type == LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU) {
					loadingSteps = LOADING_STEPS_GAMEPLAY_TO_MAIN_MENU;
				}
				loadStep = 0;

				stopAllSound();
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				if (loadStep < loadingSteps.length) {
					loadResource(loadingSteps[loadStep]);
				} else {
					if (type == LOADING_TYPE_INIT_GAME) {
						setGameState(STATE_LOGO_SCREEN);
					} else if (type == LOADING_TYPE_GAME_PLAY) {
						setGameState(STATE_GAME_PLAY);
					} else if (type == LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU) {
						setGameState(STATE_MAIN_MENU);
					}
				}

				loadStep++;
				break;

			case MESSAGE_PAINT:
				if (type == LOADING_TYPE_INIT_GAME && sprites[SPRITE_LOGO] != null) {
					fillRect(0xffffff);
					sprites[SPRITE_LOGO].paintFrame(g, LOGO_FRAME_LOGO,
						getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
				}
				break;

			case MESSAGE_DESTRUCTOR:
				SYSTEM_GC;
				break;
		}
	}
