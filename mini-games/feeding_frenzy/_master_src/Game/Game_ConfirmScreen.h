
	///*********************************************************************
	///* Game_ConfirmScreen.h
	///*********************************************************************

	//
	// Confirm screen types
	//
	private static final int CONFIRM_SCREEN_SOUND 				= 0;
	private static final int CONFIRM_SCREEN_EXIT_GAME 			= 1;
	private static final int CONFIRM_SCREEN_EXIT_TO_MAIN_MENU 	= 2;

	// Confirm screen question message
	private static final int[] CONFIRM_SCREEN_TITLES = {
		TEXT.SOUND_CONFIRM,
		TEXT.EXIT_GAME_CONFIRM,
		TEXT.EXIT_TO_MAIN_MENU_CONFIRM
	};

	private static void paintConfirmScreen(Graphics g, int type)
	{
		int x = getScreenWidth() >> 1;
		int y = ((getScreenHeight() - fonts[FONT_NORMAL].getCharHeight()) >> 1);

		fillRect(0x000000);
		if (type == CONFIRM_SCREEN_EXIT_GAME || type == CONFIRM_SCREEN_SOUND) {
			sprites[SPRITE_SPLASH].paintFrame(g, 0,
				getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
		} else if (type == CONFIRM_SCREEN_EXIT_TO_MAIN_MENU) {
			paintGamePlayBackGround(g);
		}
		fillRect(g, transparentImageMask, 0, 0, getScreenWidth(), getScreenHeight());

		// Background
		fillRect(g, CONFIRM_SCREEN_BG_COLOR, CONFIRM_SCREEN_BG_BORDER_COLOR,
			CONFIRM_SCREEN_BG_OFFSET_X, y - (CONFIRM_SCREEN_BG_HEIGHT >> 1),
			getScreenWidth() - (CONFIRM_SCREEN_BG_OFFSET_X << 1),
			CONFIRM_SCREEN_BG_HEIGHT);

		// Tile
		fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_DEFAULT);
		String title = Package.getText(CONFIRM_SCREEN_TITLES[type]);
		fonts[FONT_NORMAL].drawString(g, title, x, y, Graphics.HCENTER | Graphics.VCENTER);
	}

	private static void updateConfirmScreen(Graphics g, int message, int type)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				selectedMenuItem = TEXT.COMMON_NO;
				transparentImageMask = createTransparentImage(TRANSPARENT_IMAGE_MASK_SIZE,
					TRANSPARENT_IMAGE_MASK_SIZE, 0x000000, 0x80);
				setSoftKeys(TEXT.SOFTKEY_YES, TEXT.SOFTKEY_NO);
				break;

			case MESSAGE_UPDATE:
				if (LEFT_SOFT_KEY) {
					selectedMenuItem = TEXT.COMMON_YES;
				} else if (RIGHT_SOFT_KEY) {
					selectedMenuItem = TEXT.COMMON_NO;
				}

				if (LEFT_SOFT_KEY || RIGHT_SOFT_KEY) {
					switch (type) {
						case CONFIRM_SCREEN_SOUND:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setEnableSound(true);
							}
							setGameState(STATE_SPLASH_SCREEN);
							break;

						case CONFIRM_SCREEN_EXIT_GAME:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setGameState(STATE_EXIT);
							} else {
								setGameState(lastState);
							}
							break;

						case CONFIRM_SCREEN_EXIT_TO_MAIN_MENU:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setGameState(STATE_LOADING_TO_MAIN_MENU);
							} else {
								setGameState(lastState);
							}
							break;
					}	// switch(type)
				}	// if (LEFT_SOFT_KEY || RIGHT_SOFT_KEY)
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				paintConfirmScreen(g, type);
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}	//switch (message)
	}
