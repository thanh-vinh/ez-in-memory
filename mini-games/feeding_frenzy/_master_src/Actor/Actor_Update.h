
	///*********************************************************************
	///* Actor_Update.h
	///*********************************************************************

	public void update()
	{
		if ((this.flags & ACTOR_FLAG_NOT_ACTIVE) == 0) {
			switch (type) {
				case ACTOR_HERO:
					updateHero();
					break;

				case ACTOR_FISH_LEVEL1:
				case ACTOR_FISH_LEVEL2:
				case ACTOR_FISH_LEVEL3:
				case ACTOR_FISH_LEVEL4:
					updateFish();
					break;

				case ACTOR_BOSS:
					updateBoss();
					break;

				case ACTOR_PEARL:
					updatePearl();
					break;

				case ACTOR_BONUS:
					updateBonus();
					break;

				case ACTOR_OBJECT:
					updateObject();
					break;

				case ACTOR_FROTHY_WATER:
					udpateFrothyWater();
					break;

			}	//switch(type)

			this.updateRect();
		}	//if ((this.flags & ACTOR_FLAG_INVISIBLE) == 0)
	}
