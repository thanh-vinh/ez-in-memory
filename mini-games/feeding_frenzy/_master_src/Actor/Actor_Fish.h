
	///*********************************************************************
	///* Actor_Fish.h
	///*********************************************************************

	private void initializeFish()
	{
		this.x = (GameLib.getRandomNumber(2) == 0) ? -(Game.getScreenWidth() >> 1)
			: Game.getScreenWidth() + (Game.getScreenWidth() >> 1);
		this.x += Game.getCameraX();
		this.y = Game.getCameraY() + GameLib.getRandomNumber(Game.getScreenHeight());

		this.setAnimation(GameLib.getRandomNumber(this.sprite.getAnimationCount()));

		if (this.x == 0) {
			setFaceRight();
		} else {
			setFaceLeft();
		}
	}
	
	private int currentVx, currentVy;
	private int numFrameMoved = 0;

	public void updateFish()
	{
		if ((this.flags & ACTOR_FLAG_INVISIBLE) == 0) {
			// calculate range to MC
			int hx = Game.hero.x, hy = Game.hero.y;
			int range = (int)Math.sqrt((hx-this.x)*(hx-this.x) + (hy-this.y)*(hy-this.y));
			
			if (range<ACTOR_FISH_ALARM_RANGE) { // alarm -> run away from MC
				if (this.y<hy)
					currentVy = -ACTOR_FISH_MOVE_SPEED_MAX;
				else
					currentVy = ACTOR_FISH_MOVE_SPEED_MAX;
				currentVx = ACTOR_FISH_MOVE_SPEED_MAX;
				if (this.x<hx && Game.hero.isFaceLeft())
					setFaceLeft();
				else
					setFaceRight();
				numFrameMoved = 0;
			} else { // free move
				if (numFrameMoved<=0) {
					currentVx = GameLib.getRandomNumber(ACTOR_FISH_MOVE_SPEED_MAX - 3) + 1;
					currentVy = GameLib.getRandomNumber(ACTOR_FISH_MOVE_SPEED_MAX - 3) + 1;
					if (GameLib.getRandomNumber(2)==0) {
						currentVy *= -1;
					}
					numFrameMoved = 30;
					if (GameLib.getRandomNumber(2)==0)
						setFaceLeft();
					else
						setFaceRight();
				}
				numFrameMoved --;
			}
			
			if (isFaceLeft()) {
				this.x -= currentVx;
			} else {
				this.x += currentVx;
			}
			this.y += currentVy;
			// check coliding, TODO:
			if (this.x < 0)
				this.x = 0;
			else if (this.x > Game.levelMapWidth - 20)
				this.x = Game.levelMapWidth - 20;
			if (this.y < 0)
				this.y = 0;
			else if (this.y > Game.levelMapHeight - 20)
				this.y = Game.levelMapHeight - 20;
			
			// hoang.nv rem
			// if ((x < Game.getCameraX() - (Game.getScreenWidth() >> 1))
				// || (x > Game.getCameraY() + (Game.getScreenWidth() + (Game.getScreenWidth() >> 1)))) {
				// this.initializeFish();
			// }
				
		} else if (GameLib.getRandomNumber(50) % 25 == 0) {
				this.flags ^= ACTOR_FLAG_INVISIBLE;
		}
	}
