
public interface IActorType
{
	public static final int ACTOR_HERO					= 0;
	public static final int ACTOR_FISH_LEVEL1			= ACTOR_HERO + 1;
	public static final int ACTOR_FISH_LEVEL2			= ACTOR_FISH_LEVEL1 + 1;
	public static final int ACTOR_FISH_LEVEL3			= ACTOR_FISH_LEVEL2 + 1;
	public static final int ACTOR_FISH_LEVEL4			= ACTOR_FISH_LEVEL3 + 1;
	public static final int ACTOR_BOSS					= ACTOR_FISH_LEVEL4 + 1;
	public static final int ACTOR_PEARL					= ACTOR_BOSS + 1;
	public static final int ACTOR_BONUS					= ACTOR_PEARL + 1;
	public static final int ACTOR_OBJECT				= ACTOR_BONUS + 1;
	public static final int ACTOR_FROTHY_WATER			= ACTOR_OBJECT + 1;

	// public static final int ACTOR_MAX					= ACTOR_FROTHY_WATER + 1;
}
