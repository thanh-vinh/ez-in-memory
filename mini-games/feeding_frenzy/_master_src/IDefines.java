
public interface IDefines
{
	//
	// Palettes
	//
	public static final int PALETTE_FONT_NORMAL_DEFAULT 			= 0;
	public static final int PALETTE_FONT_NORMAL_HIGHT_LIGHT			= 1;
	public static final int PALETTE_SPLASH_NORMAL					= 0;
	public static final int PALETTE_SPLASH_MENU						= 1;

	//
	// Logo screen
	//
	public static final int TIME_LOGO_DELAY 						= 3000;	// Delay 3s for Logo screen

	//
	// Confirm screen
	//
	public static final int CONFIRM_SCREEN_BG_HEIGHT				= 70;
	public static final int CONFIRM_SCREEN_BG_COLOR					= 0x000000;
	public static final int CONFIRM_SCREEN_BG_BORDER_COLOR			= 0xffffff;
	public static final int CONFIRM_SCREEN_BG_OFFSET_X				= 4;

	//
	// Splash screen
	//
	public static final int SPLASH_PRESS5_OFFSET_Y					= 40;
	public static final int SPLASH_COPYRIGHT_OFFSET_Y				= 20;

	//
	// Menu
	//
	public static final int TRANSPARENT_IMAGE_MASK_SIZE				= 80;

	//
	// About
	//
	public static final int ABOUT_TITLE_TEXT_OFFSET_Y				= 20;
	public static final int ABOUT_TEXT_OFFSET_Y						= 50;
	public static final int ABOUT_SCROLL_SPEED						= 4;

	//
	// Math
	//
	public static final int FIXED_PRECISION							= 8;

	//
	// Tile
	//
	public static final int TILE_WIDTH								= 40;
	public static final int TILE_HEIGHT								= 40;

	//
	// Actors
	//
	public static final int ACTOR_HERO_MOVE_SPEED					= 8;
	public static final int ACTOR_FISH_MOVE_SPEED_MAX				= 6;

	public static final int ACTOR_FISH_ALARM_RANGE					= 50;

	//
	// Level
	//
	public static final int LEVEL_MAX_ACTOR							= 21;	// Actor in a level
	public static final int LEVEL_MAX_SPRITE_FISH					= 4;
	public static final byte[] LEVEL_ACTOR_FISH						= {
		10, 4, 3, 2, 1,		// Level 0: Total fish, fish level 1, fish level 2, fish level 3, fish level 4
		10, 4, 5, 2, 1,		// Level 1:
	};
}
