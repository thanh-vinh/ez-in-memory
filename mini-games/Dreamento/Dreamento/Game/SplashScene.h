#ifndef __SPLASH_SCENE_H__
#define __SPLASH_SCENE_H__

#include "cocos2d.h"

#include "SimpleAudioEngine.h"

class SplashScene : public cocos2d::CCLayerColor
{
public:
	SplashScene();
	~SplashScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::CCScene* scene();

	// implement the "static node()" method manually
	LAYER_NODE_FUNC(SplashScene);

	void spriteMoveFinished(CCNode* sender);

	void gameLogic(cocos2d::ccTime dt);

	void updateGame(cocos2d::ccTime dt);

	void registerWithTouchDispatcher();

protected:
	cocos2d::CCMutableArray<cocos2d::CCSprite*> *_targets;
	cocos2d::CCMutableArray<cocos2d::CCSprite*> *_projectiles;
	int _projectilesDestroyed;

private:
	void addTarget();

	void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event); 
};

#endif  // __SPLASH_SCENE_H__