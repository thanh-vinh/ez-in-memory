
	///*********************************************************************
	///* Actor_Update.h
	///*********************************************************************

	public void update()
	{
		if ((this.flags & ACTOR_FLAG_NOT_ACTIVE) == 0) {
			switch (type) {
				case ACTOR_HERO:
					updateHero();
					break;

				case ACTOR_COMPUTER:
					updateComputer();
					break;
			}	//switch(type)

			this.updateRect();
		}	//if ((this.flags & ACTOR_FLAG_INVISIBLE) == 0)
	}
