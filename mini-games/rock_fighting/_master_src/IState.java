
public interface IState
{
	//
	// Handle state messages
	//
	public static final int MESSAGE_CONSTRUCTOR 	= 0;
	public static final int MESSAGE_UPDATE 			= 1;
	public static final int MESSAGE_PAINT 			= 2;
	public static final int MESSAGE_DESTRUCTOR 		= 3;
	public static final int MESSAGE_SHOWNOTIFY 		= 4;
	public static final int MESSAGE_HIDENOTIFY 		= 5;

	//
	// States
	//
	public static final int STATE_EXIT							= -1;
	public static final int STATE_LOADING_INIT_GAME				= 0;
	public static final int STATE_LOGO_SCREEN					= STATE_LOADING_INIT_GAME + 1;
	public static final int STATE_SOUND_CONFIRM 				= STATE_LOGO_SCREEN + 1;
	public static final int STATE_SPLASH_SCREEN					= STATE_SOUND_CONFIRM + 1;
	public static final int STATE_MAIN_MENU 					= STATE_SPLASH_SCREEN + 1;
	public static final int STATE_OPTIONS 						= STATE_MAIN_MENU + 1;
	public static final int STATE_HELP 							= STATE_OPTIONS + 1;
	public static final int STATE_ABOUT 						= STATE_HELP + 1;
	public static final int STATE_LOADING_GAME_PLAY				= STATE_ABOUT + 1;
	public static final int STATE_GAME_PLAY 					= STATE_LOADING_GAME_PLAY + 1;
	public static final int STATE_INGAME_MENU 					= STATE_GAME_PLAY + 1;
	public static final int STATE_EXIT_CONFIRM					= STATE_INGAME_MENU + 1;
	public static final int STATE_EXIT_TO_MAIN_MENU				= STATE_EXIT_CONFIRM + 1;
	public static final int STATE_GAME_COMPLETE					= STATE_EXIT_TO_MAIN_MENU + 1;
	public static final int STATE_HIGHT_SCORE					= STATE_GAME_COMPLETE + 1;
	public static final int STATE_UPLOAD_SCORE					= STATE_HIGHT_SCORE + 1;
	public static final int STATE_LOADING_TO_MAIN_MENU			= STATE_UPLOAD_SCORE + 1;
}
