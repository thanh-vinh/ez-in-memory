
	///*********************************************************************
	///* Game_Debug.h
	///*********************************************************************

	private static final String[] STATE_NAMES = {
		"STATE_LOADING_INIT_GAME",
		"STATE_LOGO_SCREEN",
		"STATE_SOUND_CONFIRM",
		"STATE_SPLASH_SCREEN",
		"STATE_MAIN_MENU",
		"STATE_OPTIONS",
		"STATE_HELP",
		"STATE_ABOUT",
		"STATE_LOADING_GAME_PLAY",
		"STATE_GAME_PLAY",
		"STATE_INGAME_MENU",
		"STATE_EXIT_CONFIRM",
		"STATE_EXIT_TO_MAIN_MENU",
		"STATE_GAME_COMPLETE",
		"STATE_HIGHT_SCORE",
		"STATE_UPLOAD_SCORE",
		"STATE_LOADING_TO_MAIN_MENU",
		"STATE_DIALOG_DIE",
	};

	private static final String[] LOADING_STEP_NAMES = {
		"LOADING_PACK_CLOSE",
		"LOADING_PACK_OPEN_FONT",
		"LOADING_PACK_OPEN_SPRITE",
		"LOADING_PACK_OPEN_SPRITE2",
		"LOADING_PACK_OPEN_SPRITE3",
		"LOADING_PACK_OPEN_SPRITE4",
		"LOADING_PACK_OPEN_TEXT",
		// "LOADING_PACK_OPEN_SOUND",

		"LOADING_RMS",
		"LOADING_FONT",
		"LOADING_SPRITE_LOGO",
		"LOADING_TEXT",
		"LOADING_SPRITE_MENU",
		"LOADING_SOUND",
		"LOADING_SPRITE_SPLASH",
		"LOADING_SPRITE_ITEMS",
		"LOADING_SPRITE_HERO",
		"LOADING_SPRITE_FISH",
		"LOADING_SPRITE_BOSS",
		"LOADING_INIT_ACTOR",
		"LOADING_SPRITE_TILESET",
		"LOADING_MAP_LAYER",
		"LOADING_FREE_GAME_PLAY",
	};

	private static String[] SOUND_NAMES = {
		"SOUND_M_TITLE",
		"SOUND_SFX_JUMP",
		"SOUND_SFX_FAIL",
		"SOUND_SFX_COMFIRM",
	};

	private static void printStateName(int currentState, int nextState)
	{
		DBG("[SET GAME STATE] " + STATE_NAMES[currentState] + " -> " + STATE_NAMES[nextState]);
	}

	private static void printLoadingStepName(int step)
	{
		DBG("Loading resource: " + LOADING_STEP_NAMES[step]);
	}

	private static void printSoundName(int soundId)
	{
		DBG("Play sound: " + SOUND_NAMES[soundId]);
	}
