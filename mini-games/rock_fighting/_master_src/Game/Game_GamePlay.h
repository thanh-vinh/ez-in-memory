
	///*********************************************************************
	///* Game_GamePlay.h
	///*********************************************************************

	private static Actor hero;
	private static Actor computer;

	private static int cameraX;
	private static int cameraY;

	public static void setCameraXY(int x, int y)
	{
		cameraX = x;
		cameraY = y;
	}

	public static int getCameraX()
	{
		return cameraX;
	}

	public static void setCameraX(int x)
	{
		cameraX = x;
	}

	public static int getCameraY()
	{
		return cameraY;
	}

	public static void setCameraY(int y)
	{
		cameraY = y;
	}

	private static void paintGamePlayBackGround(Graphics g)
	{
		// fillRect(0x2e69f6);
		GameLib.paintTileset(g, sprites[SPRITE_TILE_SEA], MAP_SEA, cameraX, cameraY);
		GameLib.paintTileset(g, sprites[SPRITE_TILE_MAP1], MAP_MAP1, cameraX, cameraY);
	}

	private static void paintGamePlayHUD(Graphics g)
	{

	}

	private static void updateGameplay(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				setSoftKeys(TEXT.SOFTKEY_MENU, SOFTKEY_NONE);
				playSound(SOUND_M_TITLE);
				break;

			case MESSAGE_UPDATE:
				hero.update();
				computer.update();

				// Camera
				// cameraX = hero.getX() - (getScreenWidth() >> 1);
				// cameraY = hero.getY() - (getScreenHeight() >> 1);

				if (LEFT_SOFT_KEY) {
					setGameState(STATE_INGAME_MENU);
				}
				break;

			case MESSAGE_PAINT:
				setClip(0, 0, getScreenWidth(), getScreenHeight());
				paintGamePlayBackGround(g);					// Background

				hero.render(g);
				computer.render(g);

				paintGamePlayHUD(g);						// Interface
				break;

			case MESSAGE_DESTRUCTOR:
				break;

			case MESSAGE_SHOWNOTIFY:
				setGameState(STATE_INGAME_MENU);
				break;
		}
	}