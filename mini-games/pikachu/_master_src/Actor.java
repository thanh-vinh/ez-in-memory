
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * A very simple game actor.
 * @author EZ Solution Inc.
 */

public class Actor implements DATA, IDefines, IState, IActorType, IActorFlags
{
	//
	// Static fields
	//
	protected static int _cameraX;
    protected static int _cameraY;
    protected static int _frameCounter;

	protected int type;					// IActorType
	protected int x, y;					// Current position
	protected int vX, vY;				// Velocity
	protected int aX, aY;				// Accelerate
	protected int angle;				// Angle
	protected int zOrder;
	protected int flags;
	protected int[] rect;
	protected Actor[] links;
	protected short[] params;

	//
	// Animation
	//
	protected GameSprite sprite;		// Sprite
	protected int frame;				// Current frame
	protected int aframe;				// Current aframe
	protected int animation;			// Current animation	
	
	//
	//Mark Of Hero
	//	
	protected int isBloodCurrent;
	protected int isHightScore;
	protected static int MAX_HERO_BLOOD = 3;
	public int _state;
	public static int POSITION_BETWEEN_HUD_X = 5;
	
	public Actor(int type, GameSprite sprite, int animation, int x, int y)
	{
		this.type = type;
		this.sprite = sprite;
		this.animation = animation;
		this.x = x;
		this.y = y;
	}

	public Actor(int type, GameSprite sprite)
	{
		this(type, sprite, 0, 0, 0);
		this.rect = new int[4];				// x1, y1, x2, y2

		this.initialize();					// Initialize all actors
	}

	//
	// Static
	//
	public static boolean isRectCrossing(int[] rect1, int[] rect2)
	{
		if(rect1 == null || rect2 == null)
			return false;
		if (rect1[0] > rect2[2]) return false;
		if (rect1[2] < rect2[0]) return false;
		if (rect1[1] > rect2[3]) return false;
		if (rect1[3] < rect2[1]) return false;
		if (rect1[0] == rect1[2] && rect1[1] == rect1[3]) return false;
		if (rect2[0] == rect2[2] && rect2[1] == rect2[3]) return false;
		return true;
	}

	public static void setCameraXY(int x, int y)
	{
		_cameraX = x;
		_cameraY = y;
	}

	//
	// Properties
	//
	public void setSprite(GameSprite sprite)
	{
		this.sprite = sprite;
	}

	public void setPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}

	public void setFrame(int frame)
	{
		this.frame = frame;
		this.animation = -1;
	}

	public void setAnimation(int animation)
	{
		this.animation = animation;
		this.frame = -1;
		this.aframe = 0;
	}

	public boolean isEndAnimation()
	{
		if (this.aframe < this.sprite.getAFrameCount(this.animation)) {
			return false;
		} else {
			return true;
		}
	}

	public void setFlags(int mask)
    {
		this.flags |= mask ;
    }

	public int getFlags()
	{
		return this.flags;
	}
	
	public int getBlood()
	{
		return this.isBloodCurrent;
	}
	
	public void setBlood(int bloodCurrent) 
	{
		this.isBloodCurrent = bloodCurrent;
		if (bloodCurrent>MAX_HERO_BLOOD)
			this.isBloodCurrent = MAX_HERO_BLOOD;
	}
	
	public int getHightScore()
	{
		return this.isHightScore;
	}
	
	public void setHightScore(int hightScore)
	{
		this.isHightScore = hightScore;
	}

	//
	// Colliding
	//
	public void updateRect()
	{
		if (this.animation > -1) {
			this.sprite.getAFrameRect(this.rect, this.animation, this.aframe, this.x, this.y);
		} else if (this.frame > -1) {
			this.sprite.getFrameRect(this.rect, this.frame, this.x, this.y);
		}
		#if USE_SCALE_ACTOR_RECT
		scaleRect(this.rect);
		#endif
		if (type==ACTOR_OBJECT) {
			this.rect[3] += 50;
			if (Game.s_isWidthScreen)
				this.rect[0] -= 15;
		} else if (type==ACTOR_HERO) {
			this.rect[3] -= 30;
		} else if (type==ACTOR_NPC || type==ACTOR_NPC_2 || type==ACTOR_NPC_3) {
			if (this.animation==NPC_ANIMATION_JUMP_1)
				rect[2] += 10;
		}
	}

	#if USE_SCALE_ACTOR_RECT
	public void scaleRect(int rect[])
	{
		rect[0] += SCALE_X;
		rect[2] -= SCALE_X;
		rect[1] += SCALE_Y;
		rect[3] -= SCALE_Y;
	}
	#endif	//USE_SCALE_ACTOR_RECT

	public int[] getRect()
	{
		return this.rect;
	}

	public boolean isCollidingWith(Actor another)
	{
		int[] rect2 = another.getRect();
		return isRectCrossing(this.rect, rect2);
	}

	public void render(Graphics g)
	{
		if ((this.flags & ACTOR_FLAG_NOT_ACTIVE) == 0) {
			if (this.animation > -1) {
				this.sprite.paintAFrame(g, this.animation, this.aframe, this.x, this.y, 0);
				this.aframe++;
			} else if (this.frame > - 1) {
				this.sprite.paintFrame(g, this.frame, this.x, this.y, 0);
			}
		}

		#if USE_DEBUG_RECT
		Game.drawRect(g, 0xff00ff, this.rect[0], this.rect[1],
			this.rect[2] - this.rect[0], this.rect[3] - this.rect[1]);
		#endif
	}

	#include "Actor/Actor_Initialize.h"
	#include "Actor/Actor_Math.h"
	#include "Actor/Actor_Update.h"
}
