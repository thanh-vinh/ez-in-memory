    
    // gameplay static 
    static int game_play_frame_counter;

    static int game_globle_flag;
    public static boolean hasGlobleFlag(int flagMask)
    {
        return (game_globle_flag & flagMask) != 0;
    }

    public static void setGlobleFlag(int flagMask)
    {
        game_globle_flag |= flagMask ;
    }
    public static void removeGlobleFlag(int flagMask)
    {
        game_globle_flag &= ~flagMask ;
    }
	