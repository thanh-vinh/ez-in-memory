
	///*********************************************************************
	///* Actor_Initialize.h
	///*********************************************************************

	public void initialize()
	{
		switch (type) {
			case ACTOR_HERO:
				this.x = Game.getScreenWidth() >> 1;
				this.y = Game.getScreenHeight() - this.sprite.getFrameHeight(this.frame) - 20;
				this.setAnimation(CHARACTER_ANIMATION_STAND);
				break;

			// case ACTOR_NPC:
			// case ACTOR_NPC_2:
			// case ACTOR_NPC_3:
				// initAI();
				// break;

			case ACTOR_OBJECT:
				this.setAnimation(OBJECT_ANIMATION_LEAF);
				initObject();
				break;

			case ACTOR_BONUS:
				this.setFlags(ACTOR_ANIM_LOOP);		// Loop animation object -> don't need check isEndAnimation while update
				this.setAnimation(-1);
				this.setFrame(-1);				// TODO Call setAnimation method in update method
				break;
		}
	}
