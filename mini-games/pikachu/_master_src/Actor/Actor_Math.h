	///*********************************************************************
	///* Actor_Math.h
	///*********************************************************************
	#define M_PI 		3.14159265
	#define M_GRAVITY 	5.81

	private static int getTrajectoryX(int vX, int angle, int time, int x)
	{
		int ang = (int)(angle * M_PI / 180);
		return ((int)(vX * GameLib.getCos(ang) * time / 2) >> _FIXED_PRECISION) + x;
	}

	private static int getTrajectoryY(int vY, int angle, int time, int y)
	{
		int ang = (int)(angle * M_PI / 180);
		return y - (((int)(vY * GameLib.getSin(ang) * time / 2) >> _FIXED_PRECISION) - ((int)(M_GRAVITY * (time * time) / 4) >> 1));
	}
	
	// 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39
	
	private static int[] m_NPCJumpInfoList = {
		83,		6,		49,		15,		42,
		82,		9,		53,		12,		46,
		82,		15,		48,		15,		52,
		77,		21,		45,		12,		43,
		// 79,		18,		46,
		// 79,		15,		47,
		// 77,		15,		47,
		// 77,		12,		48,
		// 75,		18,		49,
	// angle	Vx		Vy		vx2		vy2
	};