
public interface GameLibConfig
{
#if USE_TOUCH_SCREEN
	public static boolean useTouchScreen			= false;
#else
	public static boolean useTouchScreen			= true;
#endif

	// Screen width & screen height
	public static boolean setScreenSize 			= false;	// Set true if want define screen size
	public static int screenWidth 					= 240;
	public static int screenHeight					= 320;

	public static boolean useServiceRepaints		= true;		// Call serviceRepaints() method after repaint() method

	public static boolean useSleepAfterEachFrame 	= true;
	public static int sleepTime 					= 20;

	// Sound
	public static boolean useCachedPlayers			= false;
	public static boolean usePrefetchedPlayers		= false;	// Set true when useCachedPlayers only (call realize() & prefetch() while cache)

	// Software double buffering
	public static boolean useDoubleBuffering		= false;

	// Transition effects
	#if USE_TRANSITION_EFFECT
	public static boolean useTransitionEffect		= true;		// Use transition effect
	#else
	public static boolean useTransitionEffect		= false;	// Use transition effect
	#endif
	public static int transitionSpeed				= 20;		// Speed, should be a even number
}