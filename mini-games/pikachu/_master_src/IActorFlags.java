
interface IActorFlags
{
	public static final int ACTOR_FLAG_FLIP_X 		= 1 ;
	public static final int ACTOR_FLAG_FLIP_Y 		= 1 << 1;
	public static final int ACTOR_FLAG_ROT_90 		= 1 << 2;

	public static final int ACTOR_FLAG_NOT_ACTIVE 	= 1 << 3; 	// Don't paint/update actor
	public static final int ACTOR_FLAG_NOT_UPDATE 	= 1 << 4; 	// Paint but not update
	public static final int ACTOR_FLAG_INVISIBLE 	= 1 << 5;	// Don't paint but update

	public static final int ACTOR_FLAG_STOP_AI 		= 1 << 6;
	public static final int ACTOR_FLAG_DESTROY 		= 1 << 7;

	public static final int ACTOR_FLAG_CONTROL_BY_CINEMATIC 	= 1 << 8;
	public static final int ACTOR_FLAG_REMOVE_WHEN_DESTORY 		= 1 << 9;
	public static final int ACTOR_ANIM_LOOP 					= 1 << 10;
};
