
public interface IRecordStore
{
	public static final int RECORD_LANGUAGE					= 0;
	public static final int RECORD_SOUND_ENABLED			= RECORD_LANGUAGE + 1;
	public static final int RECORD_MUSIC_ENABLED			= RECORD_SOUND_ENABLED + 1;
	public static final int RECORD_SOUND_LEVEL				= RECORD_MUSIC_ENABLED + 1;
	public static final int RECORD_VIBRATION_ENABLED		= RECORD_SOUND_LEVEL + 1;
	public static final int RECORD_ACCELEROMETER_ENABLED	= RECORD_VIBRATION_ENABLED + 1;
	public static final int RECORD_HIGHT_SCORE				= RECORD_ACCELEROMETER_ENABLED + 1;

	public static final int RECORD_HIGHT_SCORE_SIZE			= 2;	// short
	//public static final int RECORD_HIGHT_SCORE_LENGTH		= 10;	// Max item in score list

	public static final String RECORD_NAME					= "pikachu";
	public static final int RECORD_SIZE						= RECORD_HIGHT_SCORE + RECORD_HIGHT_SCORE_SIZE;
}
