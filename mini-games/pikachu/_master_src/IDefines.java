
public interface IDefines
{
	//
	// Palettes
	//
	public static final int PALETTE_FONT_NORMAL_DEFAULT 			= 0;
	public static final int PALETTE_FONT_NORMAL_HIGHT_LIGHT			= 1;
	public static final int PALETTE_SPLASH_NORMAL					= 0;
	public static final int PALETTE_SPLASH_MENU						= 1;

	//
	// Logo screen
	//
	public static final int TIME_LOGO_DELAY 						= 3000;	// Delay 3s for Logo screen

	//
	// Confirm screen
	//
	public static final int CONFIRM_SCREEN_OFFSET_Y					= 120;

	//
	// Splash screen
	//
	public static final int SPLASH_PRESS5_OFFSET_Y					= 40;
	public static final int SPLASH_COPYRIGHT_OFFSET_Y				= 20;

	//
	// Menu
	//
	public static final int TRANSPARENT_IMAGE_MASK_SIZE				= 80;

	//
	// About
	//
	public static final int ABOUT_TITLE_TEXT_OFFSET_Y				= 20;
	public static final int ABOUT_TEXT_OFFSET_Y						= 50;
	public static final int ABOUT_SCROLL_SPEED						= 4;

	//
	//Help
	//
	public static final int HELP_TITLE_TEXT_OFFSET_Y				= 20;
	public static final int HELP_TEXT_OFFSET_Y						= 40;
	
	//
	// Actors
	//
	public static final int ACTOR_MC_MOVE_SPEED						= 8;
	public static final int ACTOR_MC_MOVE_LIMIT_LEFT				= 30;
	public static final int ACTOR_MC_MOVE_LIMIT_RIGHT				= 110;
	public static final int ACTOR_NPC_MOVE_LIMIT_BOTTOM				= 85;// hoang.nv 95;


	public static final int _FIXED_PRECISION						= 8;
	
	//Dialog confirm
	public static final int DIALOG_TITLE_Y							= 140;
	
	//For HUD
	public static final int POSITION_SCORE_X_240					= 35;
	public static final int POSITION_SCORE_X_320					= 55;
}
