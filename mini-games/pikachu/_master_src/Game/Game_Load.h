
	///*********************************************************************
	///* Game_Load.h
	///*********************************************************************

	private static void loadResource(int step)
	{
		#if _DEBUG
		printLoadingStepName(step);
		#endif
		switch (step) {
			case LOADING_RMS:
				setting = new Setting(RECORD_NAME, RECORD_SIZE);
				scoreHero = setting.readShort(RECORD_HIGHT_SCORE);
				break;

			case LOADING_PACK_OPEN_SPRITE:
				Package.open(PACK_SPRITE);
				break;

			case LOADING_PACK_OPEN_SPRITE2:
				Package.open(PACK_SPRITE2);
				break;

			case LOADING_PACK_OPEN_FONT:
				Package.open(PACK_FONT);
				break;

			case LOADING_PACK_OPEN_TEXT:
				Package.open(PACK_TEXT);
				break;

			case LOADING_PACK_CLOSE:
				Package.close();
				break;

			case LOADING_SPRITE_LOGO:
				sprites[SPRITE_LOGO] = Package.loadSprite(SPRITE_LOGO, false);
				break;

			case LOADING_FONT:
				fonts[FONT_NORMAL] = Package.loadFontSprite(FONT_NORMAL, FONT_NORMAL_MAPPING, true);
				// Cache all palettes
				for (int i = 0; i < fonts[FONT_NORMAL].getPaletteCount(); i++) {
					fonts[FONT_NORMAL].cache(i);
				}
				fonts[FONT_NORMAL].uncacheRGB();
				break;

			case LOADING_TEXT:
				if (setting.readByte(RECORD_LANGUAGE) == TEXT_EN) {
					Package.loadText(TEXT_EN);
				} else {
					Package.loadText(TEXT_VI);
				}
				break;

			case LOADING_SOUND:
				loadSoundPack();
				break;

			case LOADING_SPRITE_MENU:
				sprites[SPRITE_MENU] = Package.loadSprite(SPRITE_MENU, true);
				sprites[SPRITE_MENU].cache();
				sprites[SPRITE_MENU].uncacheRGB();
				break;

			case LOADING_SPRITE_SPLASH:
				sprites[SPRITE_SPLASH] = Package.loadSprite(SPRITE_SPLASH, false);
				break;

			case LOADING_SPRITE_BACKGROUND:
				sprites[SPRITE_BACKGROUND] = Package.loadSprite(SPRITE_BACKGROUND, true);
				sprites[SPRITE_BACKGROUND].cache();
				sprites[SPRITE_BACKGROUND].uncacheRGB();
				break;

			case LOADING_SPRITE_ITEMS:
				sprites[SPRITE_OBJECT] = Package.loadSprite(SPRITE_OBJECT, true);
				sprites[SPRITE_OBJECT].cache();
				sprites[SPRITE_OBJECT].uncacheRGB();
				break;

			case LOADING_SPRITE_MC:
				sprites[SPRITE_CHARACTER] = Package.loadSprite(SPRITE_CHARACTER, true);
				sprites[SPRITE_CHARACTER].cache();
				sprites[SPRITE_CHARACTER].uncacheRGB();
				break;

			case LOADING_SPRITE_NPC:
				sprites[SPRITE_NPC] = Package.loadSprite(SPRITE_NPC, true);
				sprites[SPRITE_NPC].cache();
				sprites[SPRITE_NPC].uncacheRGB();
				break;

			case LOADING_INIT_ACTOR:
				actors = new Actor[ACTOR_MAX];
				actors[ACTOR_HERO] = new Actor(ACTOR_HERO, sprites[SPRITE_CHARACTER]);
				actors[ACTOR_NPC] = new Actor(ACTOR_NPC, sprites[SPRITE_NPC]);
				actors[ACTOR_NPC_2] = new Actor(ACTOR_NPC_2, sprites[SPRITE_NPC]);
				actors[ACTOR_NPC_3] = new Actor(ACTOR_NPC_3, sprites[SPRITE_NPC]);
				actors[ACTOR_OBJECT] = new Actor(ACTOR_OBJECT, sprites[SPRITE_OBJECT]);
				actors[ACTOR_BONUS] = new Actor(ACTOR_BONUS, sprites[SPRITE_OBJECT]);
				break;

			#if USE_BACK_BUFFER
			case LOADING_INIT_GAME_BUFFER:
				int frame = (getScreenWidth() == 240) ? BACKGROUND_FRAME_240X320 : BACKGROUND_FRAME_320X240;
				backgroundImage = sprites[SPRITE_BACKGROUND].getFrameImage(frame);

				frame = (getScreenWidth() == 240) ? OBJECT_FRAME_HUD_240 : OBJECT_FRAME_HUD_320;
				hudImage = sprites[SPRITE_OBJECT].getFrameImage(frame);

				dialogImage = sprites[SPRITE_MENU].getFrameImage(MENU_FRAME_DIALOG);

				sprites[SPRITE_BACKGROUND].finalize();
				sprites[SPRITE_BACKGROUND] = null;
				break;
			#endif	//USE_BACK_BUFFER

			case LOADING_FREE_GAME_PLAY:
			#if !USE_BACK_BUFFER
				sprites[SPRITE_BACKGROUND].finalize();
				sprites[SPRITE_BACKGROUND] = null;
			#endif
				sprites[SPRITE_OBJECT].finalize();
				sprites[SPRITE_OBJECT] = null;
				sprites[SPRITE_CHARACTER].finalize();
				sprites[SPRITE_CHARACTER] = null;
				sprites[SPRITE_NPC].finalize();
				sprites[SPRITE_NPC] = null;
				break;
		}
	}

	private static int[] loadingSteps;
	private static int loadStep;

	private static void updateLoading(Graphics g, int message, int type)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				if (sprites == null && fonts == null) {
					sprites = new GameSprite[SPRITE_MAX];
					fonts = new FontSprite[FONT_MAX];
				}

				if (type == LOADING_TYPE_INIT_GAME) {
					loadingSteps = LOADING_STEPS_INIT_GAME;
					currentTime = System.currentTimeMillis();
				} else if (type == LOADING_TYPE_GAME_PLAY) {
					loadingSteps = LOADING_STEPS_GAMEPLAY;
				} else if (type == LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU) {
					loadingSteps = LOADING_STEPS_GAMEPLAY_TO_MAIN_MENU;
				}
				loadStep = 0;

				stopAllSound();
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				if (loadStep < loadingSteps.length) {
					loadResource(loadingSteps[loadStep]);
				} else {
					if (type == LOADING_TYPE_INIT_GAME) {
						setGameState(STATE_LOGO_SCREEN);
					} else if (type == LOADING_TYPE_GAME_PLAY) {
						setGameState(STATE_GAME_PLAY);
					} else if (type == LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU) {
						setGameState(STATE_MAIN_MENU);
					}
				}

				loadStep++;
				break;

			case MESSAGE_PAINT:
				if (type == LOADING_TYPE_INIT_GAME && sprites[SPRITE_LOGO] != null) {
					fillRect(0xffffff);
					sprites[SPRITE_LOGO].paintFrame(g, LOGO_FRAME_LOGO,
						getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
				}
				break;

			case MESSAGE_DESTRUCTOR:
				SYSTEM_GC;
				break;
		}
	}
