
	///*********************************************************************
	///* Game_AboutScreen.h
	///*********************************************************************

	private static void updateAboutScreen(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				bufferText = splitString(Package.getText(TEXT.CREDIT), "\n");
				bufferTextOffset = getScreenHeight() - ABOUT_TEXT_OFFSET_Y;
				setSoftKeys(SOFTKEY_NONE, TEXT.SOFTKEY_BACK);
				break;

			case MESSAGE_UPDATE:
				if (RIGHT_SOFT_KEY) {
					setGameState(STATE_MAIN_MENU);
				}
				break;

			case MESSAGE_PAINT:
				fillRect(0x000000);

				fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_HIGHT_LIGHT);
				fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.ABOUT_TITLE), getScreenWidth() >> 1,
						ABOUT_TITLE_TEXT_OFFSET_Y, Graphics.HCENTER | Graphics.TOP);

				setClip(0, ABOUT_TEXT_OFFSET_Y, getScreenWidth(), getScreenHeight() - (ABOUT_TEXT_OFFSET_Y << 1));
				fonts[FONT_NORMAL].drawPage(g, bufferText, getScreenWidth() >> 1,
					bufferTextOffset, Graphics.HCENTER | Graphics.VCENTER);

				bufferTextOffset -= ABOUT_SCROLL_SPEED;
				if (bufferTextOffset + (fonts[FONT_NORMAL].getCharHeight() * bufferText.length - ABOUT_TEXT_OFFSET_Y) < 0) {
					bufferTextOffset = getScreenHeight() - ABOUT_TEXT_OFFSET_Y;	// Start at bottom screen
				}
				break;

			case MESSAGE_DESTRUCTOR:
				bufferText = null;
				break;
		}	//switch (message)
	}
