
	///*********************************************************************
	///* Game_GamePlay.h
	///*********************************************************************

	private static Image backgroundImage;
	private static Image hudImage;
	private static long s_lastTimeCreatBonusItem = -1;
	private static Image dialogImage;
	private static int currentNPCId = ACTOR_NPC_2;
	private static short scoreHero = 0;

	public static Actor getActor(int type) {
		return actors[type];
	}

	private static void paintGamePlayBackGround(Graphics g)
	{
		#if !USE_BACK_BUFFER
		sprites[SPRITE_BACKGROUND].paintFrame(g, (getScreenWidth() == 240) ? BACKGROUND_FRAME_240X320 : BACKGROUND_FRAME_320X240,
			0, getScreenHeight(), Graphics.LEFT | Graphics.BOTTOM);
		#else	//!USE_BACK_BUFFER
		g.drawImage(backgroundImage, 0, getScreenHeight(), Graphics.LEFT | Graphics.BOTTOM);
		#endif	//!USE_BACK_BUFFER
	}

	private static void paintGamePlayHUD(Graphics g)
	{
		#if !USE_BACK_BUFFER
		sprites[SPRITE_OBJECT].paintFrame(g, (getScreenWidth() == 240) ? OBJECT_FRAME_HUD_240 : OBJECT_FRAME_HUD_320,
			getScreenWidth() >> 1, 0, Graphics.HCENTER | Graphics.TOP);
		#else	//!USE_BACK_BUFFER
		g.drawImage(hudImage, getScreenWidth() >> 1, 0, Graphics.HCENTER | Graphics.TOP);
		#endif	//!USE_BACK_BUFFER

		// Blood
		for(int i = 0; i < Game.getActor(ACTOR_HERO).getBlood(); i++)
		sprites[SPRITE_OBJECT].paintFrame(g, OBJECT_FRAME_HUD_LIFE,
			(getScreenWidth() >> 1) + 20 - (i*20), Actor.POSITION_BETWEEN_HUD_X, Graphics.HCENTER | Graphics.TOP);

		// Score
		fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_DEFAULT);
		fonts[FONT_NORMAL].drawString(g, "" + (Game.getActor(ACTOR_HERO).getHightScore() * 100),
			getScreenWidth() - ((getScreenWidth() == 240) ? POSITION_SCORE_X_240 : POSITION_SCORE_X_320) , Actor.POSITION_BETWEEN_HUD_X, Graphics.HCENTER | Graphics.TOP);
		
		fonts[FONT_NORMAL].drawString(g, "" + (scoreHero * 100),
			((getScreenWidth() == 240) ? POSITION_SCORE_X_240 : POSITION_SCORE_X_320), Actor.POSITION_BETWEEN_HUD_X, Graphics.HCENTER | Graphics.TOP);
	}

	private static void updateGameplay(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				setSoftKeys(TEXT.SOFTKEY_MENU, SOFTKEY_NONE);
				if (!(lastState == STATE_INGAME_MENU)) {					
					actors[ACTOR_HERO].setBlood(Actor.MAX_HERO_BLOOD);
					actors[ACTOR_HERO].setHightScore(0);
					//hoang.nv add
					actors[ACTOR_NPC].cleanAI();
					actors[ACTOR_NPC_2].cleanAI();
					actors[ACTOR_NPC_3].cleanAI();
					//end hoang.nv
				}
				playSound(SOUND_M_TITLE);
				break;

			case MESSAGE_UPDATE:
				for (int i = 0; i < actors.length; i++) {
					actors[i].update();
				}
				
				// update bonus items
				long time = System.currentTimeMillis();
				if (s_lastTimeCreatBonusItem==-1)
					s_lastTimeCreatBonusItem = time;
				else if(time-s_lastTimeCreatBonusItem>30000) {
					actors[ACTOR_BONUS].initBonus();
					s_lastTimeCreatBonusItem = time;
				}
				
				// update 3 NPC
				int frames = 25;
				// fix for width screen (320x240)
				if (s_isWidthScreen)
					frames = 30;
					
				if (frameCounter%frames==0) {
					if (currentNPCId==ACTOR_NPC_3) {
						actors[ACTOR_NPC].initAI();
						currentNPCId = ACTOR_NPC;
					} else if (currentNPCId==ACTOR_NPC) {
						actors[ACTOR_NPC_2].initAI();
						currentNPCId = ACTOR_NPC_2;
					} else {
						actors[ACTOR_NPC_3].initAI();
						currentNPCId = ACTOR_NPC_3;
					}
				}

				if (LEFT_SOFT_KEY) {
					setGameState(STATE_INGAME_MENU);
				}
				if(Game.getActor(ACTOR_HERO).getBlood() == 0)
				{
					if(Game.getActor(ACTOR_HERO).getHightScore() > scoreHero)
					{
						scoreHero = (short)Game.getActor(ACTOR_HERO).getHightScore();
						setting.writeShort(RECORD_HIGHT_SCORE, scoreHero);
						setting.save();
					}
					// for(int i = RECORD_HIGHT_SCORE; i <= RECORD_HIGHT_SCORE + 4; i++) 
					// {
						// if(Game.getActor(ACTOR_HERO).getHightScore() > setting.readByte(i))
						// {
							// for(int j = RECORD_HIGHT_SCORE + 4; j > i; --j)
							// {
								// setting.writeByte((byte)j , setting.readByte(j - 1));
							// }
							// setting.writeByte(i, (byte)(Game.getActor(ACTOR_HERO).getHightScore()));
							// setting.save();
							// break;
						// }
					// }
					setGameState(STATE_DIALOG_DIE);
				}
				break;

			case MESSAGE_PAINT:
				setClip(0, 0, getScreenWidth(), getScreenHeight());
				paintGamePlayBackGround(g);					// Background

				for (int i = 0; i < actors.length; i++) {	// Actor
					actors[i].render(g);
				}
				paintGamePlayHUD(g);						// Interface
				break;

			case MESSAGE_DESTRUCTOR:
				break;

			case MESSAGE_SHOWNOTIFY:
				setGameState(STATE_INGAME_MENU);
				break;
		}
	}