
	///*********************************************************************
	///* Game_Effect.h
	///*********************************************************************

	//
	// Transition effects
	//
	public static final int TRANSITION_EFFECT_COLOR					= 0x000000;
	public static final byte[] TRANSITION_EFFECTS = {
		STATE_SOUND_CONFIRM,			TRANSITION_FILL_LEFT_TO_RIGHT,
		STATE_SPLASH_SCREEN,			TRANSITION_FILL_CENTER_TO_LEFT_RIGHT,
		STATE_MAIN_MENU,				TRANSITION_FILL_CENTER_TO_TOP_BOTTOM,
		// STATE_OPTIONS,					TRANSITION_FILL_LEFT_TO_RIGHT,
		// STATE_HELP,						TRANSITION_FILL_LEFT_TO_RIGHT,
		STATE_GAME_PLAY,				TRANSITION_FILL_CENTER_TO_LEFT_RIGHT,
		STATE_INGAME_MENU,				TRANSITION_FILL_CENTER_TO_TOP_BOTTOM,
		// STATE_EXIT_CONFIRM,				TRANSITION_FILL_LEFT_TO_RIGHT,
		// STATE_EXIT_TO_MAIN_MENU,		TRANSITION_FILL_LEFT_TO_RIGHT,
		STATE_GAME_COMPLETE,			TRANSITION_FILL_LEFT_TO_RIGHT,
	};

	private static void setTransitionByState(int state)
	{
		for (int i = 0; i < TRANSITION_EFFECTS.length >> 1; i++) {
			if (state == TRANSITION_EFFECTS[i << 1]) {
				setTransition(TRANSITION_EFFECTS[(i << 1) + 1], TRANSITION_EFFECT_COLOR);
				break;
			}
		}
	}
