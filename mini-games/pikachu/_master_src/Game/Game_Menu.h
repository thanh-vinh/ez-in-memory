
	///*********************************************************************
	///* Game_Menu.h
	///*********************************************************************

	//
	// Menu types
	//
	private static final int MENU_MAIN			 				= 0;
	private static final int MENU_INGAME 						= 1;

	//
	// Menu items
	//
	private static final int[] MENU_MAIN_ITEMS = {
		TEXT.MENU_PLAY,
		TEXT.MENU_OPTIONS,
		TEXT.MENU_HELP,
		TEXT.MENU_ABOUT,
		TEXT.MENU_EXIT,
	};

	private static final int[] MENU_INGAME_ITEMS = {
		TEXT.MENU_RESUME,
		TEXT.MENU_OPTIONS,
		TEXT.MENU_HELP,
		TEXT.MENU_EXIT_TO_MAIN_MENU,
	};

	private static final int[] MENU_OPTION_ITEMS = {
		TEXT.MENU_LANGUAGE,
		TEXT.MENU_SOUND,
	};

	private static int[] menuItems;

	private static void paintMenu(Graphics g, int type)
	{
		int dy = sprites[SPRITE_MENU].getFrameHeight(MENU_FRAME_BUTTON);
		dy += (dy >> 1);
		int x = getScreenWidth() >> 1;
		int y = getScreenHeight() - (dy * menuItems.length);
		if (getScreenWidth() == 240) {
			y -= dy;
		}

		fillRect(0x000000);
		if (type == MENU_MAIN) {
			sprites[SPRITE_SPLASH].paintFrame(g, SPLASH_FRAME_LARGE, getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.VCENTER | Graphics.HCENTER);
		} else {
			paintGamePlayBackGround(g);
		}
		fillRect(g, transparentImageMask, 0, 0, getScreenWidth(), getScreenHeight());

		for (int i = 0; i < menuItems.length; i++) {
			int palette = (selectedMenuIndex == i) ? PALETTE_FONT_NORMAL_HIGHT_LIGHT : PALETTE_FONT_NORMAL_DEFAULT;
			int frame = (selectedMenuIndex == i) ? MENU_FRAME_BUTTON : MENU_FRAME_BUTTON_SELECTED;
			sprites[SPRITE_MENU].paintFrame(g, frame, x, y + (dy * i), Graphics.HCENTER | Graphics.VCENTER);
			fonts[FONT_NORMAL].setPalette(palette);

			fonts[FONT_NORMAL].drawString(g, Package.getText(menuItems[i]),
				x, y + (dy * i), Graphics.HCENTER | Graphics.VCENTER);
		}
	}

	private static void paintMenuOption(Graphics g)
	{
		int dy = sprites[SPRITE_MENU].getFrameHeight(MENU_FRAME_BUTTON);
		dy += (dy >> 1);
		int x = getScreenWidth() >> 1;
		int y = getScreenHeight() - (dy * ((menuItems.length) + 1));
		y -= (y >> 2);

		fillRect(0x000000);
		if (lastState == STATE_MAIN_MENU) {
			sprites[SPRITE_SPLASH].paintFrame(g, SPLASH_FRAME_LARGE, getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.VCENTER | Graphics.HCENTER);
		} else if (lastState == STATE_INGAME_MENU) {
			paintGamePlayBackGround(g);
		}
		fillRect(g, transparentImageMask, 0, 0, getScreenWidth(), getScreenHeight());

		for (int i = 0; i < menuItems.length; i++) {
			int palette = (selectedMenuIndex == i) ? PALETTE_FONT_NORMAL_HIGHT_LIGHT : PALETTE_FONT_NORMAL_DEFAULT;
			int frame = (selectedMenuIndex == i) ? MENU_FRAME_BUTTON : MENU_FRAME_BUTTON_SELECTED;
			sprites[SPRITE_MENU].paintFrame(g, frame, x, y + (dy * i), Graphics.HCENTER | Graphics.VCENTER);
			fonts[FONT_NORMAL].setPalette(palette);

			String option;
			if (menuItems[i] == TEXT.MENU_SOUND) {
				option = (getEnableSound()) ?
					Package.getText(TEXT.MENU_OPTION_ON) : Package.getText(TEXT.MENU_OPTION_OFF);
			} else {
				option = (setting.readByte(RECORD_LANGUAGE) == 0x00) ?
					Package.getText(TEXT.MENU_OPTION_EN) : Package.getText(TEXT.MENU_OPTION_VI);
			}

			fonts[FONT_NORMAL].drawString(g, Package.getText(menuItems[i]) + option,
				x, y + (dy * i), Graphics.HCENTER | Graphics.VCENTER);
		}
	}

	private static void updateMenu(Graphics g, int message, int type)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				menuItems = (type == MENU_MAIN) ? MENU_MAIN_ITEMS : MENU_INGAME_ITEMS;
				selectedMenuIndex = 0;

				if (type == MENU_MAIN) {
					playSound(SOUND_M_TITLE, true);
				}
				setSoftKeys(TEXT.SOFTKEY_SELECT, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				if (GAME_KEY_UP_PRESSED) {
					if (selectedMenuIndex > 0) {
						selectedMenuIndex--;
					}
				} else if (GAME_KEY_DOWN_PRESSED) {
					if (selectedMenuIndex < menuItems.length - 1) {
						selectedMenuIndex++;
					}
				} else if (GAME_KEY_SELECT) {
					selectedMenuItem = menuItems[selectedMenuIndex];
					switch (selectedMenuItem) {
						case TEXT.MENU_PLAY:
							setGameState(STATE_LOADING_GAME_PLAY);
							break;

						case TEXT.MENU_OPTIONS:
							setGameState(STATE_OPTIONS);
							break;

						case TEXT.MENU_HELP:
							setGameState(STATE_HELP);
							break;

						case TEXT.MENU_ABOUT:
							setGameState(STATE_ABOUT);
							break;

						case TEXT.MENU_EXIT:
							setGameState(STATE_EXIT_CONFIRM);
							break;

						case TEXT.MENU_RESUME:
							setGameState(STATE_GAME_PLAY);
							break;

						case TEXT.MENU_EXIT_TO_MAIN_MENU:
							setGameState(STATE_EXIT_TO_MAIN_MENU);
							break;
					}
				}	//switch (selectedMenuItem)
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				paintMenu(g, type);
				break;

			case MESSAGE_DESTRUCTOR:
				if (nextState == STATE_LOADING_GAME_PLAY && sprites[SPRITE_SPLASH] != null) {
					sprites[SPRITE_SPLASH].finalize();
					sprites[SPRITE_SPLASH] = null;
				}
				break;
		}	//switch (message)
	}

	private static void updateMenuOption(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				selectedMenuIndex = 0;
				menuItems = MENU_OPTION_ITEMS;
				setSoftKeys(TEXT.SOFTKEY_SELECT, TEXT.SOFTKEY_BACK);
				break;

			case MESSAGE_UPDATE:
				if (GAME_KEY_UP_PRESSED) {
					if (selectedMenuIndex > 0) {
						selectedMenuIndex--;
					}
				} else if (GAME_KEY_DOWN_PRESSED) {
					if (selectedMenuIndex < menuItems.length - 1) {
						selectedMenuIndex++;
					}
				} else if (GAME_KEY_FIRE) {
					selectedMenuItem = menuItems[selectedMenuIndex];
					switch (selectedMenuItem) {
						case TEXT.MENU_LANGUAGE:
							if (setting.readByte(RECORD_LANGUAGE) == 0x00) {
								setting.writeByte(RECORD_LANGUAGE, (byte)0xff);
							} else {
								setting.writeByte(RECORD_LANGUAGE, (byte)0x00);
							}
							setting.save();

							loadResource(LOADING_PACK_OPEN_TEXT);
							loadResource(LOADING_TEXT);
							loadResource(LOADING_PACK_CLOSE);
							break;

						case TEXT.MENU_SOUND:
							if (!getEnableSound()) {
								setEnableSound(true);
								if (lastState == STATE_MAIN_MENU) {
									playSound(SOUND_M_TITLE, true);
								} else {
									playSound(SOUND_SFX_COMFIRM);
								}
							} else {
								stopSound();
								setEnableSound(false);
							}
							break;
					}
				} else if (RIGHT_SOFT_KEY) {
					setGameState(lastState);
				}
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				paintMenuOption(g);
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}	//switch (message)
	}
