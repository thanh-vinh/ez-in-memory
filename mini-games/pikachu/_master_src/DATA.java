
public interface DATA
{
	#include "package.h"

	//
	// Sprite module/frame/animation name
	//
	#include "logo.h"
	#include "splash.h"
	#include "menu.h"
	#include "background.h"
	#include "object.h"
	#include "character.h"
	#include "npc.h"
}
