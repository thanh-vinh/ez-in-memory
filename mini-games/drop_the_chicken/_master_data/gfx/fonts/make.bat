
@echo off

echo Export font sprite...
for /f %%i in ('dir /b *.sprite') do (
	echo Exporting file: %%i
	%SPRITE_EDITOR% %PLATFORM% ./%%i %1 %2
)
