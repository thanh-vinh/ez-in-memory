﻿
public static class State
{
	public const int EXIT 				= -1;
	public const int INITIALIZE 		= 0;
	public const int LOADING 			= 1;
	public const int LOGO 				= 2;

	public const int MAIN_MENU 			= 10;
	public const int OPTION 			= 11;
	public const int HELP 				= 12;
	public const int ABOUT 				= 13;
	public const int CHOOSE_MAP         = 14;

	public const int GAME_PLAY 			= 20;
	public const int IN_GAME_MENU 		= 21;
	public const int GAME_OVER 			= 22;
}
