﻿
public static class GameSubState
{
    public const int TUTORIAL = 0;
    public const int NORMAL = TUTORIAL + 1;
    public const int BONUS_MODE = NORMAL + 1;
}