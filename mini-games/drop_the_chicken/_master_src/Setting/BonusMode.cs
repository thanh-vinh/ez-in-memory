﻿
public static class BonusMode
{
    public const int FRAMES_TO_CHANGE_BONUS_MODE  = 800;
    public const int FRAMES_IN_BONUS_MODE         = 500;
    public const int FRAMES_TO_UPDATE_BONUS_MODE  = 300;
}