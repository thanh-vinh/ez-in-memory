
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

/// <summary>
/// This is the main type for your game.
/// </summary>
public class Game : GameLib.Game
{
	//public static Game instance;
	public static Sprite[] sprites;
	public static FontSprite[] fontSprites;

	private static int message;
	private static int currentState;
	private static int nextState;
	private static int lastState;
	private static bool isExitCurrentState;
	private static bool isEnterNextState;

	public static SpriteBatch spriteBatch;

	public static Actor[] actors;

	public Game()
		: base()
	{
		//instance = this;
	}

	#if ANDROID
	public Game(android.app.Activity activity)
	{
		super(activity);
	}
	#endif	//ANDROID

	public static void SetGameMessage(int value)
	{
		message = value;
	}

	public static void SetGameState(int state)
	{
		nextState = state;
		isExitCurrentState = true;

		message = Message.INITIALIZE;

		Log("Change state: " + currentState + " -> " + state);
	}

	public static int GetGameState()
	{
		return currentState;
	}

	/// <summary>
	/// Allows the game to perform any initialization it needs to before starting to run.
	/// This is where it can query for any required services and load any non-graphic
	/// related content.  Calling base.Initialize will enumerate through any components
	/// and initialize them as well.
	/// </summary>
	protected override void Initialize()
	{
		//
		// Set game configurations
		//
		#if DEBUG
		GameLibConfig.debug = true;
		#else
		GameLibConfig.debug = false;
		#endif

		#if WINDOWS
		GameLibConfig.useFullScreen = false;
		GameLibConfig.visibleMouse = true;
		#else
		GameLibConfig.useFullScreen = true;
		#endif
		GameLibConfig.screenWidth = 800;
		GameLibConfig.screenHeight = 480;
		// Apply game configurations
		base.Initialize();
	}

	/// <summary>
	/// LoadContent will be called once per game and is the place to load
	/// all of your content.
	/// </summary>
	protected override void LoadContent()
	{
		// Create a new SpriteBatch, which can be used to draw textures.
		#if !ANDROID
		spriteBatch = new SpriteBatch(GraphicsDevice);
		#endif

		// TODO: use this.Content to load your game content here
		//
		// Temporary load all data here
		// TODO Move to Loading state later
		//
		sprites = new Sprite[DATA.SPRITE_MAX];
		fontSprites = new FontSprite[DATA.FONT_MAX];

		Package.Open(DATA.PACK_FONT);
		Game.fontSprites[DATA.FONT_NORMAL] = Package.LoadFontSprite(DATA.SPRITE_SPLASH, DATA.FONT_NORMAL_MAPPING);
		Package.Close();

		Package.Open(DATA.PACK_TEXT);
		Package.LoadText(DATA.TEXT_EN);
		Package.Close();

		Sound.Load(DATA.PACK_SOUND);
		Sound.SetEnableSound(true);

		SetGameState(State.LOGO);
	}

	/// <summary>
	/// UnloadContent will be called once per game and is the place to unload
	/// all content.
	/// </summary>
	protected override void UnloadContent()
	{
		// TODO: Unload any non ContentManager content here
	}

	/// <summary>
	/// Allows the game to run logic such as updating the world,
	/// checking for collisions, gathering input, and playing audio.
	/// </summary>
	/// <param name="gameTime">Provides a snapshot of timing values.</param>
	protected override void Update(GameTime gameTime)
	{
		// Allows the game to exit
		#if !ANDROID
		if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
			this.Exit();
		#endif

		// TODO: Add your update logic here
		if (isExitCurrentState) {
			SetGameMessage(Message.INITIALIZE);
			isExitCurrentState = false;
			isEnterNextState = true;
			lastState = currentState;
			currentState = nextState;
		} else if (isEnterNextState) {
			SetGameMessage(Message.EXIT);
			isEnterNextState = false;
			frameCounter = 0;
		} else {
			SetGameMessage(Message.UPDATE);
		}

		switch (currentState) {
			case State.INITIALIZE:
				break;

			case State.LOADING:
				Loading.Update(gameTime, message);
				break;

			case State.LOGO:
				Logo.Update(gameTime, message);
				break;

			case State.MAIN_MENU:
				Menu.Update(gameTime, message);
				break;
			case State.GAME_PLAY:
				GamePlay.Update(gameTime, message);
				break;
			case State.CHOOSE_MAP:
				ChooseMap.Update(gameTime, message);
				break;
		}

		#if !ANDROID
		base.Update(gameTime);
		#endif
	}

	#if ANDROID
	protected void Update()
	{

	}
	#endif

	/// <summary>
	/// This is called when the game should draw itself.
	/// </summary>
	/// <param name="gameTime">Provides a snapshot of timing values.</param>
	protected override void Draw(GameTime gameTime)
	{
		#if !ANDROID
		//GraphicsDevice.Clear(Color.SkyBlue);
		//spriteBatch.Begin();
		spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
		#endif

		// TODO: Add your drawing code here
		switch (currentState) {
			case State.INITIALIZE:
				break;

			case State.LOADING:
				break;

			case State.LOGO:
				Logo.Draw(spriteBatch, gameTime);
				break;

			case State.MAIN_MENU:
				Menu.Draw(spriteBatch, gameTime);
				break;
			case State.GAME_PLAY:
				GamePlay.Draw(spriteBatch, gameTime);
				break;
			case State.CHOOSE_MAP:
				ChooseMap.Draw(spriteBatch, gameTime);
				break;
		}

		#if !ANDROID
		spriteBatch.End();
		base.Draw(gameTime);
		#endif
	}

	#if ANDROID
	protected override void Draw()
	{

	}
	#endif
}
