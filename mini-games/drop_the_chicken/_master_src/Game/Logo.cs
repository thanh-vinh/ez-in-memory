﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

public static class Logo
{
	public static void Initialize()
	{
		Game.Log("Logo->Initialize: Load splash sprite...");

		Package.Open(DATA.PACK_SPRITE);
		Game.sprites[DATA.SPRITE_SPLASH] = Package.LoadSprite(DATA.SPRITE_SPLASH);
		Package.Close();

		//Sound.PlayMusic(DATA.SOUND_M_TITLE);

		Game.SetGameMessage(Message.UPDATE);
	}

	public static void Update(GameTime gameTime, int message)
	{
		if (message == Message.INITIALIZE) {
			Initialize();
		} else if (message == Message.UPDATE) {
			if (Game.IsKeyPressed(Keys.Enter) || Game.IsTap(0, 0, Game.GetScreenWidth(), Game.GetScreenHeight())) {
				Exit();
				Game.SetGameState(State.MAIN_MENU);
			}
		}
	}

	public static void Draw(SpriteBatch spriteBatch, GameTime gameTime)
	{
		if (Game.sprites[DATA.SPRITE_SPLASH] != null) {
			Game.sprites[DATA.SPRITE_SPLASH].DrawFrame(spriteBatch, 0, 0, 0, 0);

			#if !ANDROID
			if (gameTime.TotalGameTime.Milliseconds % 1000 < 500) {
				Game.fontSprites[DATA.FONT_NORMAL].DrawString(spriteBatch, Package.GetText(TEXT.PRESS_5_TO_CONTINUE),
					Game.GetScreenWidth() >> 1, Game.GetScreenHeight() >> 1, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);
			}
			#endif	//!ANDROID
		}
	}

	public static void Exit()
	{
		Game.Log("Logo->Exit: Unload spash sprite...");
		// Game.sprites[DATA.SPRITE_SPLASH] = null;
	}
}
