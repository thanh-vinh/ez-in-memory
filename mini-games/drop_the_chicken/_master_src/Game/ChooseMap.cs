﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

public class ChooseMap
{
	//public static float RotationAngle;
	public static float a = -1.5f;
	public static float v0 = 40;
	public static int time = 0;
	public static bool countTime = false;
	public static int s = 400;
	public static int selection = 2;
	public static int MAX_SELECTION = 4;
	public static bool right = false;
	public static int selectionS = 0;
	public static int selectionSpeed = 1;

	public static void Initialize()
	{
		Game.Log("ChooseMap->Initialize: Load map sprites...");

		Package.Open(DATA.PACK_SPRITE);
		Game.sprites[DATA.SPRITE_MENU] = Package.LoadSprite(DATA.SPRITE_MENU);
		Game.sprites[DATA.SPRITE_MENUBGR] = Package.LoadSprite(DATA.SPRITE_MENUBGR);
		Package.Close();

		Game.SetGameMessage(Message.UPDATE);
	}

	public static void Update(GameTime gameTime, int message)
	{
		if (message == Message.INITIALIZE) {
			Initialize();
		} else if (message == Message.UPDATE) {
			// The time since Update was called last.
			#if !ANDROID
			float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
			#else
			float elapsed = 0;
			#endif

			if ((Game.IsKeyPressed(Keys.Left) || Game.IsTap(0, 0, (Game.GetScreenWidth() >> 1) - 100, Game.GetScreenHeight()))
				&& !countTime) {
				Game.Log("ChooseMap->Left");

				selection--;
				right = false;
				if (selection < 1) selection = MAX_SELECTION;
				countTime = true;
				time = 0;
			} else if ((Game.IsKeyPressed(Keys.Right) || Game.IsTap((Game.GetScreenWidth() >> 1) + 100, 0, Game.GetScreenWidth(), Game.GetScreenHeight()))
				&& !countTime)
			{
				Game.Log("ChooseMap->Right");

				selection++;
				right = true;
				if(selection > MAX_SELECTION) selection = 1;
				countTime = true;
				time = 0;
			} else if (Game.IsKeyPressed(Keys.Enter) || Game.IsTap((Game.GetScreenWidth() >> 1) - 100, 0, (Game.GetScreenWidth() >> 1) + 100, Game.GetScreenHeight()))
			{
				Sound.PlaySfx(DATA.SOUND_SFX_CONFIRM);

				Exit();
				Game.SetGameState(State.LOADING);
			}

			if (countTime && time <= 40)
			{
				s = (int)(v0 * time + (a * time * time)/2);
				if (right) s = -s + 800;
				time++;
			}
			else
			{
				countTime = false;
				//s = 0;
			}

			//update selectionSpeed
			if (selectionS > 20) selectionSpeed = -1;
			else if (selectionS < 0) selectionSpeed = 1;
			selectionS += selectionSpeed;

		}
	}

	public static void Draw(SpriteBatch spriteBatch, GameTime gameTime)
	{
		if (Game.sprites[DATA.SPRITE_MENUBGR] != null)
		{
			Game.sprites[DATA.SPRITE_MENUBGR].DrawFrame(spriteBatch, DATA.MENUBGR_FRAME_BGR_MAINMENU, 0, 0, 0);
		}

		if (Game.sprites[DATA.SPRITE_MENU] != null)
		{

			Game.sprites[DATA.SPRITE_MENU].DrawFrame(spriteBatch, ((selection - 2) < 1) ? MAX_SELECTION + selection - 2 : (selection - 2), -800 + s, Game.GetScreenHeight(), SpriteAnchor.HCENTER | SpriteAnchor.BOTTOM);
			Game.sprites[DATA.SPRITE_MENU].DrawFrame(spriteBatch, ((selection - 1) < 1) ? MAX_SELECTION : (selection - 1), -400 + s, Game.GetScreenHeight(), SpriteAnchor.HCENTER | SpriteAnchor.BOTTOM);
			Game.sprites[DATA.SPRITE_MENU].DrawFrame(spriteBatch, selection, s, Game.GetScreenHeight(), SpriteAnchor.HCENTER | SpriteAnchor.BOTTOM);
			Game.sprites[DATA.SPRITE_MENU].DrawFrame(spriteBatch, ((selection + 1) > MAX_SELECTION )?1:(selection+1), 400 + s, Game.GetScreenHeight(), SpriteAnchor.HCENTER | SpriteAnchor.BOTTOM);
			Game.sprites[DATA.SPRITE_MENU].DrawFrame(spriteBatch, ((selection + 2) > MAX_SELECTION) ? (selection + 2 - MAX_SELECTION) : (selection + 2), Game.GetScreenWidth() + s, Game.GetScreenHeight(), SpriteAnchor.HCENTER | SpriteAnchor.BOTTOM);


			#if !ANDROID
			//draw selection
			spriteBatch.Draw(Game.sprites[DATA.SPRITE_MENU].GetTexture(18), new Vector2(Game.GetScreenWidth() / 2 + 50 + selectionS, 200 - selectionS), null, Color.White, -45, new Vector2(Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Width / 2, Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Height / 2), 1.0f, SpriteEffects.None, 0f);
			spriteBatch.Draw(Game.sprites[DATA.SPRITE_MENU].GetTexture(18), new Vector2(Game.GetScreenWidth() / 2 - 50 - selectionS, 200 - selectionS), null, Color.White, -90, new Vector2(Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Width / 2, Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Height / 2), 1.0f, SpriteEffects.None, 0f);
			spriteBatch.Draw(Game.sprites[DATA.SPRITE_MENU].GetTexture(18), new Vector2(Game.GetScreenWidth() / 2 + 50 + selectionS, Game.GetScreenHeight() - 100 + selectionS), null, Color.White, 45, new Vector2(Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Width / 2, Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Height / 2), 1.0f, SpriteEffects.None, 0f);
			spriteBatch.Draw(Game.sprites[DATA.SPRITE_MENU].GetTexture(18), new Vector2(Game.GetScreenWidth() / 2 - 50 - selectionS, Game.GetScreenHeight() - 100 + selectionS), null, Color.White, 90, new Vector2(Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Width / 2, Game.sprites[DATA.SPRITE_MENU].GetTexture(18).Height / 2), 1.0f, SpriteEffects.None, 0f);
			#endif	//!ANDROID

		}

	}

	public static void Exit()
	{

	}
}
