﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameLib;

public class Loading
{
	public static void Initialize()
	{
		Game.Log("Loading->Initialize: Load game sprite...");

		Package.Open(DATA.PACK_SPRITE);
		Game.sprites[DATA.SPRITE_NINJA] = Package.LoadSprite(DATA.SPRITE_NINJA);
		Game.sprites[DATA.SPRITE_PAD] = Package.LoadSprite(DATA.SPRITE_PAD);
		Game.sprites[DATA.SPRITE_HUD] = Package.LoadSprite(DATA.SPRITE_HUD);
		Game.sprites[DATA.SPRITE_FARGROUND] = Package.LoadSprite(DATA.SPRITE_FARGROUND);
		Game.sprites[DATA.SPRITE_BACKGROUND] = Package.LoadSprite(DATA.SPRITE_BACKGROUND);
		Game.sprites[DATA.SPRITE_ITEM] = Package.LoadSprite(DATA.SPRITE_ITEM);
		Game.sprites[DATA.SPRITE_TUTORIAL] = Package.LoadSprite(DATA.SPRITE_TUTORIAL);
		Package.Close();

		Game.SetGameMessage(Message.UPDATE);
	}

	public static void Update(GameTime gameTime, int message)
	{
		if (message == Message.INITIALIZE) {
			Initialize();
		} else if (message == Message.UPDATE) {
			Exit();
			Game.SetGameState(State.GAME_PLAY);
		}
	}

	public static void Draw(SpriteBatch spriteBatch, GameTime gameTime)
	{
		if (Game.sprites[0] != null) {
			Game.sprites[0].DrawFrame(spriteBatch, 0, 0, 0, 0);
		}
	}

	public static void Exit()
	{

	}
}
