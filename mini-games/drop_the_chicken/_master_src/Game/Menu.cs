﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

public class Menu
{
	public static float RotationAngle;
	public static int animation = 0;
	public static int x = -400;
	public static int y = 0;

	public static void Initialize()
	{
		Game.Log("Menu->Initialize: Load menu sprites...");

		Package.Open(DATA.PACK_SPRITE);
		Game.sprites[DATA.SPRITE_MENU] = Package.LoadSprite(DATA.SPRITE_MENU);
		Game.sprites[DATA.SPRITE_MENUBGR] = Package.LoadSprite(DATA.SPRITE_MENUBGR);
		Package.Close();

		Sound.PlayMusic(DATA.SOUND_M_TITLE);

		Game.SetGameMessage(Message.UPDATE);
	}

	public static void Update(GameTime gameTime, int message)
	{
		if (message == Message.INITIALIZE)
		{
			Initialize();
		}
		else if (message == Message.UPDATE)
		{
			// The time since Update was called last.
			#if !ANDROID
			float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
			#else
			float elapsed = 0;
			#endif

			// TODO: Add your game logic here.
			RotationAngle += elapsed;
			float circle = Game.MATH_PI * 2;
			RotationAngle = RotationAngle % circle;

			if (Game.IsKeyPressed(Keys.Enter)
				|| Game.IsTap(0, 0, Game.GetScreenWidth(), Game.GetScreenHeight())
				)
			{
				Sound.PlaySfx(DATA.SOUND_SFX_CONFIRM);

				Exit();
				Game.SetGameState(State.CHOOSE_MAP);
			}
		}
	}

	public static void Draw(SpriteBatch spriteBatch, GameTime gameTime)
	{
		if (Game.sprites[DATA.SPRITE_MENUBGR] != null) {
			Game.sprites[DATA.SPRITE_MENUBGR].DrawFrame(spriteBatch, DATA.MENUBGR_FRAME_BGR_MAINMENU, 0, 0, 0);
		}

		if (Game.sprites[DATA.SPRITE_MENU] != null)	{
			Game.sprites[DATA.SPRITE_MENU].DrawFrame(spriteBatch, DATA.MENU_FRAME_MAIN_MENU, 0, 0, 0);
			Game.sprites[DATA.SPRITE_MENU].DrawFrame(spriteBatch, DATA.MENU_FRAME_MAP2, Game.GetScreenWidth() / 2, Game.GetScreenHeight(), SpriteAnchor.HCENTER | SpriteAnchor.BOTTOM);
		}

		//y = x2/400 +80
		x += 2;
		if (x > 400) x = -400;
		y = (int)(x * x / (float)400 + 80);

		int maxAnimation = Game.sprites[DATA.SPRITE_MENU].GetAFrameCount(DATA.MENU_ANIMATION_NINJA);
		animation++;
		if (animation/5 >= maxAnimation) animation = 0;
		Game.sprites[DATA.SPRITE_MENU].DrawAFrame(spriteBatch, DATA.MENU_ANIMATION_NINJA, animation / 5, x + 400, y, 0);

		#if !ANDROID
		spriteBatch.Draw(Game.sprites[DATA.SPRITE_MENU].GetTexture(3), new Vector2(Game.sprites[DATA.SPRITE_MENU].GetTexture(3).Width / 2 + 40, 340), null, Color.White, RotationAngle, new Vector2(70, 70), 1.0f, SpriteEffects.None, 0f);
		spriteBatch.Draw(Game.sprites[DATA.SPRITE_MENU].GetTexture(4), new Vector2(800 - Game.sprites[DATA.SPRITE_MENU].GetTexture(4).Width / 2 - 20, 340), null, Color.White, 360 - RotationAngle, new Vector2(91, 81), 1.0f, SpriteEffects.None, 0f);
		#endif

		Game.fontSprites[DATA.FONT_NORMAL].DrawString(spriteBatch, "ARCADE",
			100, Game.GetScreenHeight() - 40, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);

		Game.fontSprites[DATA.FONT_NORMAL].DrawString(spriteBatch, "MORE",
			Game.GetScreenWidth() >> 1, Game.GetScreenHeight() - 40, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);

		Game.fontSprites[DATA.FONT_NORMAL].DrawString(spriteBatch, "ENDLESS",
			Game.GetScreenWidth() - 120, Game.GetScreenHeight() - 40, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);
	}

	public static void Exit()
	{

	}
}
