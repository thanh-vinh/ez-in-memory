﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

	public static class GameOver
	{
        //const
        public const int MAX_STAR = 50;
        public const int MAX_LIGHT = 3;
        public const int MAX_ANGLE_ROTATE = 50;
        public static int LIGHT1_X = Game.GetScreenWidth() / 2 - 130;
        public static int LIGHT2_X = Game.GetScreenWidth() / 2;
        public static int LIGHT3_X = Game.GetScreenWidth() / 2 + 130;
        public static int LIGHT_Y = Game.GetScreenHeight() / 2 - 30;
        public static int OFFSET_GAMEOVER_STRING = 70;

        public static bool isBeginGameOver = false;
        public static int offset_gameover_string_value = 0;
        public static int runStep = 0;
        public static Star[] stars;
        public static Light[] lights;
        public static float scale = 1.0f;
        public static float valueScale = 0.01f;
		public static void Initialize()
		{
			Game.Log("Logo->Initialize: Load gameover sprite...");

			Package.Open(DATA.PACK_SPRITE);
            Game.sprites[DATA.SPRITE_GAMEOVER] = Package.LoadSprite(DATA.SPRITE_GAMEOVER);
            initStar();
            initLight();
            runStep = 0;
            offset_gameover_string_value = 30;
			Package.Close();

			Sound.PlayMusic(DATA.SOUND_M_LOSE);

			//Game.SetGameMessage(Message.UPDATE);
		}

        public static void initStar()
        {
            stars = new Star[MAX_STAR];
            for (int i = 0; i < MAX_STAR; i++)
            {
                stars[i] = new Star(ActorType.ACTOR_STAR, Game.sprites[DATA.SPRITE_GAMEOVER]);
                stars[i].Initialize(2, 10 * i, 0.07f, Game.GetScreenWidth() >> 1, Game.GetScreenHeight() >> 1,
					Game.GetRandomInt(-10, 10), Game.GetRandomInt(-10, 10), (float) Game.GetRandomDouble());
                stars[i].SetAnim(DATA.GAMEOVER_ANIMATION_BLUESTAR);
            }
        }

        public static void runStar(int pos)
        {
            for (int i = 0; i < MAX_STAR; i++)
            {
                stars[i].setStartPosition(LIGHT1_X + pos * 130, LIGHT_Y);
                stars[i].restart();
                stars[i].run();
            }
        }

        public static void initLight()
        {
            lights = new Light[MAX_LIGHT];
            for (int i = 0; i < MAX_LIGHT; i++)
            {
                lights[i] = new Light(ActorType.ACTOR_LIGHT, Game.sprites[DATA.SPRITE_GAMEOVER]);
                lights[i].Initialize(LIGHT1_X + i*130,LIGHT_Y, 0.07f);
                lights[i].SetAnim(DATA.GAMEOVER_ANIMATION_BLUESTAR);
            }
        }

		public static void Update(GameTime gameTime, int message)
		{
			if (isBeginGameOver) {
				Initialize();
                isBeginGameOver = false;
			} else if (message == Message.UPDATE) {

                for (int i = 0; i < MAX_STAR; i++)
                    stars[i].Update(gameTime);
                for (int i = 0; i < MAX_LIGHT; i++)
                    lights[i].Update(gameTime);
                if (Game.IsKeyPressed(Keys.Enter) || Game.IsTap(Game.GetScreenWidth() / 2 - 60, Game.GetScreenHeight()/2 + 100, Game.GetScreenWidth() / 2 + 60, Game.GetScreenHeight()/2 + 200))
                {
                    //for (int i = 0; i < MAX_STAR; i++)
                    //    stars[i].restart();
                    Exit();
                    Game.SetGameState(State.MAIN_MENU);
				}
                if (runStep == 0)
                {
                    lights[0].run();
                    runStar(0);
                }
                else if (runStep == 50)
                {
                    lights[1].run();
                    runStar(1);
                }
                else if (runStep == 100)
                {
                    lights[2].run();
                    runStar(2);
                }

                if(runStep <=100)
                    runStep++;
			}
		}

		public static void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			if (Game.sprites[DATA.SPRITE_GAMEOVER] != null) {
                Game.sprites[DATA.SPRITE_GAMEOVER].DrawFrame(spriteBatch, 0, Game.GetScreenWidth() / 2, Game.GetScreenHeight() / 2, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);

                if (offset_gameover_string_value > 0) offset_gameover_string_value--;
                Game.sprites[DATA.SPRITE_GAMEOVER].DrawFrame(spriteBatch, DATA.GAMEOVER_FRAME_GAMEOVER, Game.GetScreenWidth() / 2, Game.GetScreenHeight() / 2 - OFFSET_GAMEOVER_STRING - offset_gameover_string_value, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);

                int width = Game.sprites[DATA.SPRITE_GAMEOVER].GetTexture(18).Width;
                int height = Game.sprites[DATA.SPRITE_GAMEOVER].GetTexture(18).Height;

                if (scale <= 0.7f) valueScale = Math.Abs(valueScale);
                else if (scale >= 1.0f) valueScale = -Math.Abs(valueScale);

                scale += valueScale;
				
				#if !ANDROID
                if(runStep >= 100)
                    spriteBatch.Draw(Game.sprites[DATA.SPRITE_GAMEOVER].GetTexture(18), new Vector2(Game.GetScreenWidth() / 2 - width / 2 * scale, Game.GetScreenHeight() / 2 + 150 - height/2*scale), new Rectangle(0, 0, width, height), Color.White, 0.0f, new Vector2(0, 0), scale, SpriteAnchor.NONE, 0.0f);
				#endif	//!ANDROID
				
                DrawLights(spriteBatch, gameTime);
                DrawStars(spriteBatch, gameTime);
			}
		}

		public static void Exit()
		{
			Game.Log("Logo->Exit: Unload gameover sprite...");
            Game.sprites[DATA.SPRITE_GAMEOVER] = null;
		}

        public static void DrawStars(SpriteBatch spriteBatch, GameTime gameTime)
        {
            for (int i = 0; i < MAX_STAR; i++)
                stars[i].Draw(spriteBatch, gameTime);
        }

        public static void DrawLights(SpriteBatch spriteBatch, GameTime gameTime)
        {
            for (int i = 0; i < MAX_LIGHT; i++)
                lights[i].Draw(spriteBatch, gameTime);
        }
	}
