﻿
using GameLib;

public static class Sound
{
	private static bool enableSound;
	private static int currentMusic = -1;
	private static int currentSfx = -1;

	private static bool IsLoop(int index)
	{
		return (index == DATA.SOUND_M_TITLE);
	}

	public static void SetEnableSound(bool value)
	{
		enableSound = value;
	}

	public static bool GetEnableSound()
	{
		return enableSound;
	}

	public static void Load(string pack)
	{
		SoundPlayer.LoadSoundPack(pack);
	}

	public static void Unload()
	{
		SoundPlayer.Unload();
	}

	public static void PlayMusic(int index)
	{
		// TODO Stop current music before play another music
		if (index != currentMusic) {
			currentMusic = index;
			if (SoundPlayer.IsPlaying(DATA.SOUND_M_TITLE)) {
				SoundPlayer.Stop(DATA.SOUND_M_TITLE);
			}
			SoundPlayer.Play(index, IsLoop(index));
		}
	}

	public static void PlaySfx(int index)
	{
		currentSfx = index;
		SoundPlayer.Play(index);
	}
}
