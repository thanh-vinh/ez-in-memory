﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameLib;

public class Star : Actor
{
	private float scale = 1.0f;
    private bool isStop = true;

	public Star(int type, Sprite sprite)
		: base(type, sprite)
	{
	}

	public Star(int type, Animation animation)
		: base(type, animation)
	{
	}

    public void restart()
    {
        this.startTime = 0;
    }

    public void run()
    {
        this.isStop = false;
    }
    public void stop()
    {
        this.isStop = true;
    }

    public void setStartPosition(int start_X, int start_Y)
    {
        this.start_X = start_X;
        this.start_Y = start_Y;
        this.x = start_X;
        this.y = start_Y;
    }

	public void Initialize(int Velocity, int Angle, float timeChange,int x,int y,int vx, int vy, float scale)
	{
        isStop = true;
		this.startTime = 0;
		this.nVelocity  = Velocity;
		this.angle      = Angle;
		this.timeChange = timeChange;
        this.start_X = x;
        this.start_Y = y;
        this.vX = vx;
        this.vY = vy;
		this.x = x;
		this.y = y;
        this.scale = scale;
        //RotationAngle = (float)(1.5);
		base.Initialize();
	}

	public override void Update(GameTime gameTime)
	{
        if (!isStop)
        {
            if (startTime < 3)
            {
                this.startTime += this.timeChange;
                //this.x = GetTrajectoryX(nVelocity, angle, startTime, start_X);
                //this.y = GetTrajectoryY(nVelocity, angle, startTime, start_Y);
                this.x = this.x += vX;
                this.y = this.y += vY;
            }
        }

		base.Update(gameTime);
	}

    public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
    {
		#if !ANDROID
        if (!isStop)
        {
            if (startTime < 3)
            {
                int width = sprite.GetTexture(16).Width;
                int height = sprite.GetTexture(16).Height;
				
                spriteBatch.Draw(sprite.GetTexture(16), new Vector2(this.x, this.y), new Rectangle(0, 0, width, height), Color.White, 0.0f, new Vector2(0, 0), scale, SpriteAnchor.NONE, 0.0f);
            }
        }
		#endif	//!ANDROID
    }
}
