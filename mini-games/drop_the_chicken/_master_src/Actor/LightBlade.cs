﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

public class LightBlade : Actor
{
	//private const int PAD_SPEED = 10;			// Move speed
	//private const int PAD_LIMIT_X = 150;		// Litmit x and wall
	//private const int VALUE_CHANGE = 10;
	private const int MAXLENGHT = 100;  //lenght of lightBlade
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private float a;  //in y=ax+b
    private float b; //in y=ax+b

	#if !ANDROID
    Texture2D blank;
	#endif

	public bool isCollisionWithNinja = false;
	private bool isIncreaseY = true;
	private int valueChangeCurrent = 0;

    public LightBlade(int type, Sprite sprite)
		: base(type, sprite)
	{
	}

	public override void Initialize()
	{
		#if !ANDROID
        blank = new Texture2D(Game.GetGraphics(), 1, 1, false, SurfaceFormat.Color);
        blank.SetData(new[] { Color.White });
		#endif
		//this.x = GameLibConfig.screenWidth / 2;
		//this.y = MANUAL_VALUE_Y;
		base.Initialize();
	}

    public void setLightBlade(int x1, int y1, int x2, int y2)
    {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        a = (float)(y1 - y2) / (x1 - x2);
        b = y1 - a*x1;
        Game.Log("a=" + a + " b=" + b);
    }

    public int getYFromX(int x)
    {
        Game.Log("x = " + x + " y = " + (int)(a * x + b));
        return (int)(a * x + b);
    }

    public Boolean isColisionWithNinja(int x, int y)
    {
        if ((x >= x1) && (x <= x2))
            if (Math.Abs(y - getYFromX(x)) < 5)
            //if (y > getYFromX(x))
            {
                Game.Log("colision x="+x+ "  y="+y+"  getY="+getYFromX(x));
                return true;
            }
            else
                return false;
        return false;
    }

    public int getAngleOfReflection(int angleNinja)
    {
        //Console.Write("angleNinjja=" + angleNinja);
        float tam = -a;
        int gocphanxa = 0;
        int gocduongthang = (int)(180*Math.Atan((double)tam)/Game.MATH_PI);
        if (tam > 0)
            if (angleNinja + gocduongthang < 90)
                gocphanxa = (180 - angleNinja) - (90 - angleNinja + gocduongthang);
            else
                gocphanxa = gocduongthang + 90 + (angleNinja + gocduongthang - 90);
        else if (tam < 0)
            gocphanxa = -(Math.Abs(gocduongthang) - (angleNinja - Math.Abs(gocduongthang)));

        //gocphanxa = -176;
        Game.Log("goc phan xa=" + gocphanxa);
        return gocphanxa;
    }

	public override void Update(GameTime gameTime)
	{
		//
		// Move
		//
        //if (this.rect[RECT_X1] < PAD_LIMIT_X) {															// Limit x1
        //    this.x = PAD_LIMIT_X;
        //} else if (this.rect[RECT_X2] > (Game.GetScreenWidth() - PAD_LIMIT_X)) {						// Limit x2
        //    this.x = (Game.GetScreenWidth() - PAD_LIMIT_X) - (this.rect[RECT_X2] - this.rect[RECT_X1]);
        //} else {																						// Safe area, can move
        //    if (Game.IsKeyHold(Keys.Left)) {															// Press left
        //        this.x -= PAD_SPEED;
        //    } else if (Game.IsKeyHold(Keys.Right)) {													// Press right
        //        this.x += PAD_SPEED;
        //    } else if (Game.GetPointerY() > this.rect[RECT_Y1]) {											// Tap under pad
        //        int mouseX = Game.GetPointerX();
        //        if (mouseX < this.rect[RECT_X1]) {														// Tap left pad side
        //            this.x -= PAD_SPEED;
        //        } else if (mouseX > this.rect[RECT_X2]) {												// Tap right pad side
        //            this.x += PAD_SPEED;
        //        } else if (mouseX > this.rect[RECT_X1] && Game.GetPointerX() < this.rect[RECT_X2]) {		// Tap in pad
        //            this.x = mouseX - ((this.rect[RECT_X2] - this.rect[RECT_X1]) >> 1);
        //        }
        //    }
        //}

        ////
        //// Colliding
        //if (isCollisionWithNinja)
        //{
        //    Sound.PlaySfx(DATA.SOUND_SFX_HIT_PAD);

        //    if (isIncreaseY)
        //        valueChangeCurrent++;
        //    else
        //        valueChangeCurrent--;
        //    if (valueChangeCurrent > VALUE_CHANGE)
        //        isIncreaseY = false;
        //    if (valueChangeCurrent < 0)
        //    {
        //        isIncreaseY             = true;
        //        isCollisionWithNinja    = false;
        //        valueChangeCurrent      = 0;
        //    }
        //    this.y = MANUAL_VALUE_Y + valueChangeCurrent;
        //}

		// Call base.Update at here to update common properties
		base.Update(gameTime);
	}

    public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
    {
        //this.animation.Draw(spriteBatch, gameTime, this.x, this.y, SpriteAnchor.NONE);
		#if !ANDROID
        DrawLine(spriteBatch, blank, 1, Color.Black, new Vector2(x1, y1), new Vector2(x2, y2));
		#endif
        
    }

	#if !ANDROID
    void DrawLine(SpriteBatch batch, Texture2D blank,
              float width, Color color, Vector2 point1, Vector2 point2)
    {
        float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
        float length = Vector2.Distance(point1, point2);

        batch.Draw(blank, point1, null, color,
                   angle, Vector2.Zero, new Vector2(length, width),
                   SpriteEffects.None, 0);
    }
	#endif	//!ANDROID
}
