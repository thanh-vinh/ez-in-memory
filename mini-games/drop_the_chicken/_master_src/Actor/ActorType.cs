
public static class ActorType
{
	public const int ACTOR_NINJA			= 0;
	public const int ACTOR_PAD				= ACTOR_NINJA + 1;
    public const int ACTOR_ARROW            = ACTOR_PAD + 1;
    public const int ACTOR_GRENADE          = ACTOR_ARROW + 1;
    public const int ACTOR_SMOKE          = ACTOR_GRENADE + 1;
    public const int ACTOR_STAR             = ACTOR_SMOKE + 1;
    public const int ACTOR_LIGHT             = ACTOR_STAR + 1;

	// public const int ACTOR_MAX				= ACTOR_PAD + 1;
}
