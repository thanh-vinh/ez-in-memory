﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameLib;

public class Arrow : Actor
{
	private int POSITION_START_Y = 70;
    private float RotationAngle = 0;
    private float elapsed = 0;

	public Arrow(int type, Sprite sprite)
		: base(type, sprite)
	{
	}

	public Arrow(int type, Animation animation)
		: base(type, animation)
	{
	}

	public void Initialize(int Velocity, int Angle, float timeChange)
	{
		this.startTime = 0;
		this.nVelocity  = Velocity;
		this.angle      = Angle;
		this.timeChange = timeChange;
		this.x = this.start_X = GameLibConfig.screenWidth;
		this.y = this.start_Y = POSITION_START_Y;
        RotationAngle = (float)(1.5);
		base.Initialize();
	}

	public override void Update(GameTime gameTime)
	{
        if (RotationAngle <= 3.0f)
        {
			#if !ANDROID
            elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
			#else
            elapsed = 0;
			#endif
			
            RotationAngle += elapsed/(1.2f);
            float circle = Game.MATH_PI * 2;
            RotationAngle = RotationAngle % circle;
        }
		this.startTime += this.timeChange;

        if (GamePlay.pad != null)
        {
			this.x = GetTrajectoryX(nVelocity, angle, startTime, start_X);
            this.y = GetTrajectoryY(nVelocity, angle, startTime, start_Y);

			if (this.IsCollidingWith(GamePlay.pad)) {
				//this.y = GamePlay.pad.GetY();
			}
        }

		base.Update(gameTime);
	}

    public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
    {   
        //this.sprite.DrawModule(spriteBatch, 4, this.x, this.y + (this.rect[3] - this.rect[1] + 10), GameLib.SpriteFlag.NONE, 360 - RotationAngle, Vector2.Zero, SpriteAnchor.NONE);

	#if !ANDROID
        this.animation.Draw(spriteBatch, this.x, this.y, SpriteFlag.NONE, 360 - RotationAngle,
                new Vector2(this.sprite.GetTexture(0).Width / 2, this.sprite.GetTexture(0).Height / 2), SpriteAnchor.NONE);
        #if USE_DEBUG_RECT
                this._rectangle.X = this.rect[0];
                this._rectangle.Y = this.rect[1];
                this._rectangle.Width = this.rect[2] - this.rect[0];
                this._rectangle.Height = this.rect[3] - this.rect[1];
                spriteBatch.Draw(this._texture, this._rectangle, this._color);
        #endif
	#endif	//!ANDROID
    }

    public override void ComputeRect()
    {
        base.ComputeRect();

        this.rect[1] -= 15;
        this.rect[3] -= 10;
        this.rect[0] -= 20;
        this.rect[2] = this.rect[0];
    }
}
