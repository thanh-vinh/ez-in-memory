﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameLib;

public class Ninja : Actor
{
    public static int MAX_NUMBER_NINJA_DEAD = 5;
    public static int numberNinjaLive = MAX_NUMBER_NINJA_DEAD;
	public int POS_START_NINJA_X = 80;
	public int POS_END_NINJA_X = GameLibConfig.screenWidth - 80;
	public int DISTANCE_COLLITION = GameLibConfig.screenWidth / 8;
	public int DISTANCE_POSITION = 50;
    public int FIRST_POSITION_Y     = 172; // 100; //172;
	public int SECOND_POSITION_Y    = 252; // 180; //252;
    public int THIRST_POSITION_Y    = 352; // 285; //352;
	public int startPos;
	protected bool isDead = false;
	protected bool isEnableUpdate;
    public bool isColision = false;

	public Ninja(int type, Sprite sprite)
		: base(type, sprite)
	{
	}

	public void Initialize(int Velocity, int Angle, int Position, float timeChange)
	{
		this.startTime = 0;
		this.nVelocity = Velocity;
		this.angle = Angle;
		this.start_X = POS_START_NINJA_X;
		this.x = 0;
		startPos = Position;
		if (Position == 1)
		{
			this.y          = FIRST_POSITION_Y;
			this.start_Y    = FIRST_POSITION_Y;
		}
		else if (Position == 2)
		{
			this.y          = SECOND_POSITION_Y;
			this.start_Y    = SECOND_POSITION_Y;
		}
		else if (Position == 3)
		{
			this.y          = THIRST_POSITION_Y;
			this.start_Y    = THIRST_POSITION_Y;
		}
		this.timeChange = timeChange;
		setEnableUpdate(false);

		base.Initialize();
	}

	public override void Update(GameTime gameTime)
	{
		if (!isDead)
		{
			// Update Position Of Ninja
            //if (this.x < start_X)
            //    this.x = this.x + 2;
            //else if (this.x > POS_END_NINJA_X - 100)
            //{
            //    if ((this.y > FIRST_POSITION_Y - 10) && (this.y < FIRST_POSITION_Y) && (this.x > POS_END_NINJA_X - 60))
            //    {
            //        this.y = FIRST_POSITION_Y;
            //        this.x = this.x + 2;
            //    }
            //    else if ((this.y > SECOND_POSITION_Y - 10) && (this.y < SECOND_POSITION_Y) && (this.x > POS_END_NINJA_X - 60))
            //    {
            //        this.y = SECOND_POSITION_Y;
            //        this.x = this.x + 2;
            //    }
            //    else if ((this.y > THIRST_POSITION_Y - 10) && (this.y < THIRST_POSITION_Y))
            //    {
            //        this.y = THIRST_POSITION_Y;
            //        this.x = this.x + 2;
            //    }
            //    else if (this.y == FIRST_POSITION_Y || this.y == SECOND_POSITION_Y || this.y == THIRST_POSITION_Y)
            //    {
            //        this.x = this.x + 2;
            //    }
            //    else
            //    {
            //        this.startTime += this.timeChange;
            //        this.x = GetTrajectoryX(nVelocity, angle, startTime, start_X);
            //        this.y = GetTrajectoryY(nVelocity, angle, startTime, start_Y);
            //    }
			//}
			//else
			//{
				this.startTime += this.timeChange;
				this.x = GetTrajectoryX(nVelocity, angle, startTime, start_X);
				this.y = GetTrajectoryY(nVelocity, angle, startTime, start_Y);
			//}

            if (this.x > GameLibConfig.screenWidth - 10)
                GamePlay.numberNinjaPass++;

			if (GamePlay.pad != null) {
				if ((GamePlay.lightBlade.isColisionWithNinja(this.x + 20, this.y - 20)) && (!isColision)) {
                    isColision = true;
					resetNinjaWhenCollision();
					//GamePlay.addEffect(EffectType.NINJA_SMOKE, this.x + 30, GamePlay.pad.GetY() - 20);
                    //GamePlay.pad.isCollisionWithNinja = true;
				}
			}
			if (this.GetX() > GameLibConfig.screenWidth)
				setEnableUpdate(false);
			else if (this.GetY() > GameLibConfig.screenHeight) {
				this.isDead = true;
				this.SetAnim(DATA.NINJA_ANIMATION_GHOST);
                if(!GamePlay.isEndGame)
                    numberNinjaLive--;
			}
		}
		else
		{
			if (this.GetY() < 0)
				setEnableUpdate(false);
			this.y -= 4;
		}
		
		// Call base.Update at here to update common properties
		base.Update(gameTime);
	}

	// If not manualy draw at here, so don't need override
    public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
    {
        this.animation.Draw(spriteBatch, this.x, this.y - 90, SpriteAnchor.LEFT | SpriteAnchor.TOP);
        
#if USE_DEBUG_RECT
        this._rectangle.X = this.rect[0];
        this._rectangle.Y = this.rect[1];
        this._rectangle.Width = this.rect[2] - this.rect[0];
        this._rectangle.Height = this.rect[3] - this.rect[1];
        spriteBatch.Draw(this._texture, this._rectangle, this._color);        
#endif
    }

	//public int GetTrajectoryX(int nVelocity, int nAngle, float time, int nStartPosX)
	//{
	//    return (int)(nVelocity * Math.Cos(nAngle * Game.MATH_PI / 180) * time + nStartPosX);
	//}

	//public int GetTrajectoryY(int nVelocity, int nAngle, float time, int nStartPosY)
	//{
	//    return nStartPosY - (int)(nVelocity * Math.Sin(nAngle * Game.MATH_PI / 180) * time - (acceleration * (time * time)) / 2);
	//}

	public void ResetWhenCollision(float rateVelocity, float rateAngle)
	{
		this.startTime = 0;
		this.start_X = this.x;
		this.start_Y = this.y - 5;
		this.angle = (int)(this.angle * rateAngle);
		this.nVelocity = (int)(this.nVelocity * rateVelocity); // only cheat for test
	}

	public void ResetWhenCollision(int rateVelocity, int rateAngle, float timeChange)
	{
		this.timeChange = timeChange;
		this.startTime  = 0;
		this.start_X    = this.x;
		this.start_Y    = this.y - 5;
		this.angle      = rateAngle;
		this.nVelocity  = rateVelocity; // only cheat for test
	}

	public void resetNinjaWhenCollision()
	{
        this.ResetWhenCollision(80, GamePlay.lightBlade.getAngleOfReflection(this.angle), 0.07f);

        //if (this.x < 2 * DISTANCE_COLLITION)
        //    this.ResetWhenCollision(80, 45, 0.07f);
        //else if ((this.x > 2 * DISTANCE_COLLITION) && (this.x < 3 * DISTANCE_COLLITION))
        //    this.ResetWhenCollision(75, 55, 0.09f);
        //else if ((this.x > 3 * DISTANCE_COLLITION) && (this.x < 4 * DISTANCE_COLLITION))
        //    this.ResetWhenCollision(65, 60, 0.10f);
        //else if ((this.x > 4 * DISTANCE_COLLITION) && (this.x < 5 * DISTANCE_COLLITION))
        //    this.ResetWhenCollision(65, 60, 0.09f);
        //else if ((this.x > 5 * DISTANCE_COLLITION) && (this.x < 6 * DISTANCE_COLLITION))
        //    this.ResetWhenCollision(45, 60, 0.12f);
        //else if ((this.x > 6 * DISTANCE_COLLITION) && (this.x < 7 * DISTANCE_COLLITION))
        //    this.ResetWhenCollision(60, 40, 0.09f);
	}

	public void setEnableUpdate(bool enable)
	{
		this.isEnableUpdate = enable;
	}

	public bool getEnableUpdate()
	{
		return isEnableUpdate;
	}

    public override void ComputeRect()
    {
        base.ComputeRect();

        int height = this.rect[3] - this.rect[1];
        this.rect[3] = this.rect[3] - height - 5;
        this.rect[1] = this.rect[1] - 20;

        this.rect[2] = this.rect[2] - 20;
        this.rect[0] = this.rect[2] - 25;
    }
}