package ez.game;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import ez.gamelib.GameLib;

public class EzActivity extends Activity
{
	private Game game;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
		Log.d("Interrupt", "______________________________ onCreate");
    	Game game = new Game(this);
        super.onCreate(savedInstanceState);
        super.setContentView(game);
    }

	@Override
	protected void onPause() {
		// Log.d("Interrupt", "______________________________ onPause");
		super.onPause();
	}

	@Override
	protected void onResume() {
		// Log.d("Interrupt", "______________________________ onResume");
		super.onResume();
	}

	@Override
    protected void onStop() {
		// Log.d("Interrupt", "______________________________ onStop");
        super.onStop();
    }
	@Override
	protected void onDestroy() {
		// Log.d("Interrupt", "______________________________ onDestroy");
		super.onDestroy();
	}
}