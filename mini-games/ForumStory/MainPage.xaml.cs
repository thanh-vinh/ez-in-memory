﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Html;

namespace ForumStory
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

		private const string SAMPLE_PAGE_URL = "http://www.tangthuvien.com/forum/showthread.php?t=81201&page=2";

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }

			//
			// Test
			//
			WebPage page = new WebPage();
			page.Load(SAMPLE_PAGE_URL);
        }
    }
}