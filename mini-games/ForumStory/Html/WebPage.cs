﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using HtmlAgilityPack;

namespace Html
{
	public class WebPage
	{
		public WebPage()
		{
			this.Document = new HtmlDocument();
		}

		public string Url { get; private set; }
		public int Page { get; private set; }

		public HtmlDocument Document { get; private set; }
		public IEnumerable<HtmlNode> Title { get; private set; }
		public IEnumerable<HtmlNode> PageLinks { get; private set; }
		public IEnumerable<HtmlNode> PostLinks { get; private set; }
		public IEnumerable<HtmlNode> Messages { get; private set; }

		private HtmlNode DocumentNode { get; set; }

		protected void Parse()
		{
			this.DocumentNode = this.Document.DocumentNode;

			//
			// Title
			//
			this.Title = from node in this.DocumentNode.Descendants()
				where node.Name == Tag.NODE_TITLE
				select node;

			foreach (var node in this.Title)
			{
				Debug.WriteLine("Title: " + node.InnerText);
			}

			//
			// Page links
			//
			this.PageLinks = from node in this.DocumentNode.Descendants()
					where node.Name == Tag.PAGE_LINK_NODE
						&& node.Attributes["class"] != null
						&& node.Attributes["class"].Value == Tag.PAGE_LINK_CLASS
						&& node.Attributes["href"].Value.Contains("page")
					select node;

			foreach (var node in this.PageLinks)
			{
				Debug.WriteLine("Page link: " + node.Attributes["href"].Value);
			}

			//
			// Post links
			//
			this.PostLinks = from node in this.DocumentNode.Descendants()
							 where node.Name == Tag.POST_LINK_NODE
								&& node.Attributes["href"] != null
								 && node.Attributes["href"].Value.Contains(Tag.POST_LINK_HREF_CONTAINT)
							 select node;

			foreach (var node in this.PostLinks)
			{
				Debug.WriteLine("Post link: " + node.Attributes["href"].Value);
			}

			//
			// Message
			//
			//this.Messages =	from node in this.DocumentNode.Descendants()
			//    where node.Id.Contains(Tag.POST_ATTRIBUE_ID)
			//    select node;
		}

		public void LoadFile(string file)
		{
			this.Document.Load(file);
			this.Parse();

			//IEnumerable<HtmlNode> nodes =
			//    from divNode in doc.DocumentNode.Descendants()
			//    where divNode.Id.Contains("post_message_")
			//    select divNode;

			//foreach (HtmlNode div in nodes)
			//{
			//    //HtmlAttribute attribute = div.Attributes["name"];
			//    //if (attribute != null && attribute.Value == "description")
			//    {
			//        //Debug.WriteLine("{0}, line {1}", div.Attributes["content"].Value, attribute.Line);
			//        Debug.WriteLine("{0}", div.InnerHtml);

			//    }
			//}
		}

		public void LoadUrl(string url)
		{
			this.Url = url;

			//string source;
			//using (WebClient client = new WebClient())
			//{
			//    source = client.DownloadString(url);
			//}

			////Debug.WriteLine(source);

			//HtmlDocument doc = new HtmlDocument();
			//doc.OptionFixNestedTags = true;
			//doc.LoadHtml(source);
			//HtmlNode node = doc.DocumentNode;

			////string s = node.GetAttributeValue("description", null);
			////Debug.WriteLine(s);

			HtmlWeb web = new HtmlWeb();
			web.LoadAsync(this.Url);

			Debug.WriteLine(web.ToString());
		}


	}
}
