/*
 *
 * Copyright (c) 2007, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Sun Microsystems nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
import javax.microedition.rms.RecordStore;


/**
 * Demonstration MIDlet for File Connection API. This MIDlet implements simple
 * file browser for the filesystem available to the J2ME applications.
 *
 */
public class ImageRecordStore {
	private static RecordStore rs = null;

	public ImageRecordStore()
	{

	}

	public static boolean checkImageRecordExist(String fileName)
	{
		rs = null;
		try
		{
			rs = RecordStore.openRecordStore(fileName, false );
			if(rs != null)
				return true;
			else
				return false;
		}
		catch(Exception e) {System.out.println(e); return false;}
	}

	/*
	 * fileName 			: Name of file
	 */
	public static byte[] readImageRecord(String fileName)
	{
		byte buf[] = null;
		try {
			rs = RecordStore.openRecordStore(fileName, false );
			buf = rs.getRecord(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return buf;
	}

	/*
	 * fileName 			: Name of file
	 * array				: Byte array which contain value of file.	
	 */
	public static void writeImageRecord(String fileName, byte[] array)
	{
		try	{
			rs = RecordStore.openRecordStore(fileName, true );
			rs.addRecord(array, 0, array.length);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}
