
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * A very simple game actor.
 * @author EZ Solution Inc.
 */

public class Actor implements DATA, IDefines, IState, IActorType
{	
	//
	// Static fields
	//
	protected static int frameCounter;

	//
	// Fields
	//
	protected int type;					// IActorType
	protected int sex;					// IActorType
	protected int state;				// IActorType	
	protected int x, y;					// Current position
	protected int[] rect;

	//
	// Animation
	//
	protected GameSprite sprite;		// Sprite
	protected int frame;				// Current frame
	protected int aframe;				// Current aframe
	protected int animation;			// Current animation
	protected int flag;					// Current flag

	//
	// Image data
	//
	protected ImagePack pack;			// ImagePack
	
	public Actor(int type, GameSprite sprite, int animation, int x, int y)
	{
		this.type = type;
		this.sprite = sprite;
		this.animation = animation;
		this.x = x;
		this.y = y;
	}

	public Actor(int type, GameSprite sprite)
	{
		this(type, sprite, 0, 0, 0);
		this.rect = new int[4];				// x1, y1, x2, y2
	}

	public Actor(int type, ImagePack pack)
	{
		this.type = type;
		this.pack = pack;
	}

	public void setFrame(int frame)
	{
		this.frame = frame;
	}
	
	public void setSprite(GameSprite sprite)
	{
		this.sprite = sprite;
	}

	public void setPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}
	
	public void setFlag(int flag)
	{
		this.flag = flag;
	}
	
	public void setAnimation(int animation)
	{
		this.animation = animation;
	}
	
	public void setAnimation(int animID, int x, int y, int flag)
	{
		this.setAnimation(animID);
		this.setPosition(x, y);
		this.setFlag(Graphics.RIGHT | Graphics.BOTTOM);		
	}

	public void ResetAnimation()
	{
		this.frame = -1;
		this.aframe = 0;
	}

	public boolean isEndAnimation()
	{
		if (this.aframe < this.sprite.getAFrameCount(this.animation)) {
			return false;
		} else {
			return true;
		}
	}
	
	public void render(Graphics g)
	{
		if (this.animation > -1) {
			if (this.aframe < this.sprite.getAFrameCount(this.animation)) {
				this.sprite.paintAFrame(g, this.animation, this.aframe, this.x, this.y, this.flag);
				this.aframe++;
			} else {
				this.aframe = 0;
				this.sprite.paintAFrame(g, this.animation, this.aframe, this.x, this.y, this.flag);
			}
		} 
		// else if (this.frame > - 1) {
			// this.sprite.paintFrame(g, this.frame, this.x, this.y, 0);
		// }
	}

	#include "Actor/Actor_Update.h"
	#include "Actor/Actor_Hero.h"
	#include "Actor/Actor_Computer.h"
}
