
import javax.microedition.lcdui.Image;
import javax.microedition.io.*;
import java.io.*;
import java.util.*;
import javax.microedition.io.file.*;

public class ImagePack
{
	// Text encoding
	private static final String TEXT_ENCODING		= "UTF-8";
	// File extension
	private static final String FILE_INFO_EXT 		= "_nfo";
	private static final String FILE_DATA_EXT 		= "_dat";
	// File info
	private static String ROOT_PATH = "file:///";		// Where save data on SD card
	private static final int IMAGE_COUNT 			= 8;
	private static final int DESCRIPTION_INDEX 		= 0;
	private static final int THUMBNAIL_INDEX 		= 1;

	private String fileName;
	private String description;
	
	private String getRoots() {
	  String root="root";
      Enumeration drives = FileSystemRegistry.listRoots();
      System.out.println("The valid roots found are: ");
      while(drives.hasMoreElements()) {
         root = (String) drives.nextElement();
         System.out.println("\t"+root);
      }
      return root;
   }

	public ImagePack()
	{
	}
	public ImagePack(String fileName)
	{
		this.fileName = fileName;
	}

	public String getDescription()
	{
		return this.description;
	}

	public Image getThumbnail()
	{
		Image image = null;
		String name = this.fileName + FILE_INFO_EXT;
		if(fileName.equals("1") || fileName.equals("2") || fileName.equals("3"))
		{
			Package.open(name);
			byte[] buf = Package.getBytes(THUMBNAIL_INDEX);
			image = Image.createImage(buf, 0, buf.length);
			buf = Package.getBytes(DESCRIPTION_INDEX);
			try{
			this.description = new String(buf, TEXT_ENCODING);
			}catch(Exception e){e.printStackTrace();}
			buf = null;
			Package.close();
		}
		else
		{
			try
			{
				byte[] packageByteArray = ImageRecordStore.readImageRecord(name) ;
				if(packageByteArray != null)
				{
					InputStream input = new ByteArrayInputStream(packageByteArray);
					Package.open(input);
					input = new ByteArrayInputStream(packageByteArray);
					byte[] buf = Package.getBytes(input,THUMBNAIL_INDEX);
					image = Image.createImage(buf, 0, buf.length);
					input = new ByteArrayInputStream(packageByteArray);
					buf = Package.getBytes(input,DESCRIPTION_INDEX);
					this.description = new String(buf, TEXT_ENCODING);
					buf = null;
					Package.close();
				}
			}
			catch(Exception e) {e.printStackTrace();}
		}
		return image;
	}



	public void packageOpen()
	{
		String name = this.fileName + FILE_DATA_EXT;
		if(!this.fileName.equals("1"))
		{
			try
			{
				byte[] packageByteArray = ImageRecordStore.readImageRecord(this.fileName + FILE_DATA_EXT) ;		
				if (packageByteArray != null){
					InputStream input = new ByteArrayInputStream(packageByteArray);
					Package.open(input);
				}
			}
			catch(Exception e) {e.printStackTrace();}
		}
	}

	public Image getImage(int index)
	{
		Image image = null;
		if(this.fileName.equals("1")){
			String name = this.fileName + FILE_DATA_EXT;
			Package.open(name);
			byte[] buf = Package.getBytes(index);
			image = Image.createImage(buf, 0, buf.length);
			buf = null;

			Package.close();
		}
		else
		{
			try{
				byte[] packageByteArray = ImageRecordStore.readImageRecord(this.fileName + FILE_DATA_EXT) ;		
				if (packageByteArray != null){
					InputStream input = new ByteArrayInputStream(packageByteArray);
					byte[] buf = Package.getBytes(input,index);
					image = Image.createImage(buf, 0, buf.length); 
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}

		}
		return image;
	}
}
