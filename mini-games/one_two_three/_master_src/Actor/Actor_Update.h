
	///*********************************************************************
	///* Actor_Update.h
	///*********************************************************************

	public void update()
	{
		switch (type) {
			case ACTOR_HERO:
				updateHero();
				break;

			case ACTOR_COMPUTER:
				updateComputer();
				break;
		}	//switch(type)
	}
