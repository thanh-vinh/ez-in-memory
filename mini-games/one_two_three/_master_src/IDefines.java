
public interface IDefines
{
	//
	// Palettes
	//
	public static final int PALETTE_FONT_NORMAL_WHITE	 			= 0;
	public static final int PALETTE_FONT_NORMAL_BLACK				= 1;
	public static final int PALETTE_FONT_NORMAL_RED					= 2;

	//
	// Logo screen
	//
	public static final int TIME_LOGO_DELAY 						= 3000;	// Delay 3s for Logo screen

	//
	// Confirm screen
	//
	public static final int CONFIRM_SCREEN_OFFSET_Y					= 120;

	//
	// Splash screen
	//
	public static final int SPLASH_PRESS5_OFFSET_Y					= 40;
	public static final int SPLASH_COPYRIGHT_OFFSET_Y				= 20;

	//
	// Menu
	//
	public static final int TRANSPARENT_IMAGE_MASK_SIZE				= 80;

	//
	// About
	//
	public static final int ABOUT_TITLE_TEXT_OFFSET_Y				= 20;
	public static final int ABOUT_TEXT_OFFSET_Y						= 50;
	public static final int ABOUT_SCROLL_SPEED						= 4;
	
	//
	//Help
	//
	public static final int HELP_TITLE_TEXT_OFFSET_Y				= 20;
	public static final int HELP_TEXT_OFFSET_Y						= 40;
}
