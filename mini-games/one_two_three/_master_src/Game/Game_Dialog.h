
	///*********************************************************************
	///* Game_Dialog.h
	///*********************************************************************

	private static void paintGameDialog(Graphics g)
	{
		//#if !USE_BACK_BUFFER
		sprites[SPRITE_MENU].paintFrame(g, MENU_FRAME_DIALOG,
			getScreenWidth() >> 1, getScreenHeight()>>1, Graphics.HCENTER | Graphics.VCENTER);
		// #else	//!USE_BACK_BUFFER
		// g.drawImage(dialogImage, getScreenWidth() >> 1, getScreenHeight()>>1, Graphics.HCENTER | Graphics.VCENTER);
		// #endif	//!USE_BACK_BUFFER
	}
	
	private static void updateGameDialog(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				setSoftKeys(TEXT.SOFTKEY_BACK, TEXT.SOFTKEY_MENU);
				//playSound(SOUND_M_TITLE);
				break;

			case MESSAGE_UPDATE:
				
				if (LEFT_SOFT_KEY) {
					setGameState(STATE_GAME_PLAY);
				} else if(RIGHT_SOFT_KEY){
					setGameState(STATE_INGAME_MENU);
				}
				break;

			case MESSAGE_PAINT:
				setClip(0, 0, getScreenWidth(), getScreenHeight());
				// paintGamePlayBackGround(g);					// Background
				paintGameDialog(g);
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}
	}