
	///*********************************************************************
	///* Game_Sound.h
	///*********************************************************************

	private static byte[] SOUND_PRIORITY = {
		1,	//SOUND_M_TITLE
		3,	//SOUND_SFX_JUMP
		2,	//SOUND_SFX_FAIL
		2,	//SOUND_SFX_COMFIRM
	};

	private static SoundPlayer soundPlayer;
	private static int currentSoundId;

	public static void loadSoundPack()
	{
		soundPlayer.loadSoundPack(PACK_SOUND);
	}

	public static boolean getEnableSound()
	{
		return soundPlayer.getEnableSound();
	}

	public static void setEnableSound(boolean enable)
	{
		soundPlayer.setEnableSound(enable);
	}

	public static boolean isPlayingSound(int soundId)
	{
		if (soundPlayer.getCurrentSound() == soundId) {
			return true;
		} else {
			return false;
		}
	}

	public static void playSound(int soundId, boolean loop)
	{
		if (isPlayingSound(soundId) && SOUND_PRIORITY[soundId] < SOUND_PRIORITY[currentSoundId]) {
			// DBG("Ingore sound due low priority: " + soundId);
			return;
		}

		/* #if _DEBUG
		printSoundName(soundId);
		#endif */

		stopSound();
		soundPlayer.play(soundId, loop);
		currentSoundId = soundId;
	}

	public static void playSound(int soundId)
	{
		playSound(soundId, false);
	}

	public static void stopSound()
	{
		soundPlayer.stop();
	}

	public static void stopAllSound()
	{
		soundPlayer.stopAllSound();
	}
