
	///*********************************************************************
	///* Game_LogoScreen.h
	///*********************************************************************

	public void updateLogoScreen(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				// Display logo screen in TIME_LOGO_DELAY milliseconds
				if ((System.currentTimeMillis() - currentTime) > TIME_LOGO_DELAY) {
					setGameState(STATE_LOADING_TO_MAIN_MENU);
				}
				break;

			case MESSAGE_PAINT:
				fillRect(0xffffff);
				sprites[SPRITE_LOGO].paintFrame(g, LOGO_FRAME_LOGO,
					getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
				break;

			case MESSAGE_DESTRUCTOR:
				sprites[SPRITE_LOGO].finalize();
				sprites[SPRITE_LOGO] = null;
				break;
		}
	}

	public void updateSplashScreen(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				playSound(SOUND_M_TITLE, true);
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				fillRect(0x000000);
				sprites[SPRITE_SPLASH].paintFrame(g, 0,
					getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
				fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.COPYRIGHT),
					getScreenWidth() >> 1, getScreenHeight() - SPLASH_COPYRIGHT_OFFSET_Y, Graphics.HCENTER | Graphics.TOP);
				paintPress5Key(g, fonts[FONT_NORMAL], getScreenWidth() >> 1, getScreenHeight() - SPLASH_PRESS5_OFFSET_Y);

				if (GAME_KEY_SELECT) {
					setGameState(STATE_MAIN_MENU);
				}
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}
	}
