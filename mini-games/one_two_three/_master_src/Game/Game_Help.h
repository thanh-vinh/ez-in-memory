
	///*********************************************************************
	///* Game_Help.h
	///*********************************************************************
	
	private static void updateHelpScreen(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				bufferText = splitString(Package.getText(TEXT.HELP), "\n");
				bufferTextOffset = (getScreenHeight()>>1) - 60;
				setSoftKeys(SOFTKEY_NONE, TEXT.SOFTKEY_BACK);
				break;

			case MESSAGE_UPDATE:
				if (RIGHT_SOFT_KEY) {
					setGameState(lastState);
				}
				break;

			case MESSAGE_PAINT:
				fillRect(0x000000);

				fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_RED);
				fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.HELP_TITLE), getScreenWidth() >> 1,
						HELP_TITLE_TEXT_OFFSET_Y, Graphics.HCENTER | Graphics.TOP);

				//setClip(0, 0, getScreenWidth(), getScreenHeight());
				fonts[FONT_NORMAL].drawPage(g, bufferText, getScreenWidth() >> 1,
					(getScreenHeight()>>1) - HELP_TEXT_OFFSET_Y, Graphics.HCENTER | Graphics.VCENTER);
				break;

			case MESSAGE_DESTRUCTOR:
				bufferText = null;
				break;
		}	//switch (message)
	}