
	///*********************************************************************
	///* Game_Debug.h
	///*********************************************************************

	private static final String[] STATE_NAMES = {
		"STATE_EXIT",
		"STATE_LOADING_INIT_GAME",
		"STATE_LOGO_SCREEN",
		"STATE_LOADING_TO_MAIN_MENU",
		"STATE_SPLASH_SCREEN",
		"STATE_MAIN_MENU",
		"STATE_SETTINGS",
		"STATE_HELP",
		"STATE_ABOUT",
		"STATE_LOADING_GAME_PLAY",
		"STATE_GAME_PLAY",
		"STATE_GAME_DIALOG",
		"STATE_GAME_COMPLETE",
		"STATE_INGAME_MENU",
		"STATE_EXIT_GAME_QUESTION",
		"STATE_GO_TO_MAIN_MENU_QUESTION",
		"STATE_LOADING_SERVER_LIST",
		"STATE_SERVER_LIST",		
		"STATE_LOADING_GAMEPLAY_TO_MAIN_MENU",		
	};

	private static final String[] LOADING_STEP_NAMES = {
		"LOADING_PACK_CLOSE",
		"LOADING_PACK_OPEN_FONT",
		"LOADING_PACK_OPEN_SPRITE",
		"LOADING_PACK_OPEN_SPRITE2",
		"LOADING_PACK_OPEN_SPRITE3",
		"LOADING_PACK_OPEN_SPRITE4",
		"LOADING_PACK_OPEN_TEXT",
		// "LOADING_PACK_OPEN_SOUND",

		"LOADING_RMS",
		"LOADING_FONT",
		"LOADING_SPRITE_LOGO",
		"LOADING_SPRITE_SELECT_GIRL",
		"LOADING_TEXT",
		"LOADING_SPRITE_MENU",
		"LOADING_SOUND",
		"LOADING_SPRITE_SPLASH",
		"LOADING_SPRITE_OBJECT",
		"LOADING_SPRITE_INGAME_ICON",
		"LOADING_INIT_IMAGE_PACK",
		"LOADING_INIT_ACTOR",
		"LOADING_FREE_GAME_PLAY",
	};

	private static String[] SOUND_NAMES = {
		"SOUND_M_TITLE",
		"SOUND_SFX_JUMP",
		"SOUND_SFX_FAIL",
		"SOUND_SFX_COMFIRM",
	};

	private static void printStateName(int currentState, int nextState)
	{
		DBG("[SET GAME STATE] " + STATE_NAMES[currentState] + " -> " + STATE_NAMES[nextState]);
	}

	private static void printLoadingStepName(int step)
	{
		DBG("Loading resource: " + LOADING_STEP_NAMES[step]);
	}

	private static void printSoundName(int soundId)
	{
		DBG("Play sound: " + SOUND_NAMES[soundId]);
	}
