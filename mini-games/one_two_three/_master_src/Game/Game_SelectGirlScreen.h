
	///*********************************************************************
	///* Game_LogoScreen.h
	///*********************************************************************

	public static int indexSelect = 0;
	public static int indexStart = 0;
	public static int MAX_IMAGE_ARRAY = 12;
	public static int IMAGE_HEIGHT = 70;
	public static int SPACE_BETWEEN_IMAGES = 10;
	public static int currentNumberGirl = 0;
	public static int counterPaintDownloadAnim = 0;
	public static int NUMBER_DELAY_ANIM = 10; 
	public static boolean isDownloading = false;
	public static WebServiceClient wc = new WebServiceClient();
	Image image[] = null;
	String description[] = null;
	private String[] split(String original) 
	{
		Vector nodes = new Vector();
		String separator = "\n";
		// Parse nodes into vector
		int index = original.indexOf(separator);
		while(index>=0) {
		nodes.addElement( original.substring(0, index) );
		original = original.substring(index+separator.length());
		index = original.indexOf(separator);
		}
		// Get the last node
		nodes.addElement( original );

		// Create splitted string array
		String[] result = new String[ nodes.size() ];
		if( nodes.size()>0 ) {
			for(int loop=0; loop<nodes.size(); loop++)
			{
				result[loop] = (String)nodes.elementAt(loop);
			}
		}
	return result;
	}
	
	public void checkDataAvailable(){
		for(int i=4; i<= MAX_IMAGE_ARRAY; i++)
			if(ImageRecordStore.checkImageRecordExist(i+"_nfo"))
				currentNumberGirl = i;
	}
	
	public static void downloadFinish(){
		isDownloading = false;
		subState = SUBSTATE_SELECTGIRL_SCREEN;
		indexStart++;
		indexSelect++;
	}

	public void getThumbail(final int i){
		if(!ImageRecordStore.checkImageRecordExist(i+1+"_nfo"))
		{
			isDownloading = true;
			subState = SUBSTATE_DOWNLOADING;
			Thread t = new Thread() {
				public void run() {
					wc.getGirl(i);
				}
			};
			t.start();
		}
	}
	
	public static void sendSMSDownloadGirlSuccessful()
	{
		setGameState(STATE_LOADING_GAME_PLAY);
	}
	
	public void paintSelectGirlScreen(Graphics g)
	{
		fillRect(0xffffff);
		sprites[SPRITE_SPLASH].paintFrame(g, 0, getScreenWidth() >> 1,
		getScreenHeight() >> 1, Graphics.VCENTER | Graphics.HCENTER);
				
		g.fillRect(0, 20 + SPACE_BETWEEN_IMAGES + (indexSelect- indexStart)*(IMAGE_HEIGHT + SPACE_BETWEEN_IMAGES) - 5, getScreenWidth() , IMAGE_HEIGHT + SPACE_BETWEEN_IMAGES);

		try
		{
			byte[] b = null;

			for(int i = indexStart; i <= indexStart+2; i++)
			{
				if(image[i] == null)
				{
					if(i < 3){
						ImagePack ip = new ImagePack((i+1)+"");
						image[i] = ip.getThumbnail();
						description[i] = ip.getDescription();
					}
					else
					{
						if(ImageRecordStore.checkImageRecordExist(i+1+"_nfo")){
							ImagePack ip = new ImagePack((i+1)+"");
							image[i] = ip.getThumbnail();
							description[i] = ip.getDescription();
						}
					}
				}
				if(image[i] != null){
					g.drawImage(image[i], 10, 20+SPACE_BETWEEN_IMAGES + (IMAGE_HEIGHT + SPACE_BETWEEN_IMAGES)*(i-indexStart), Graphics.LEFT | Graphics.TOP);
					if((i != 0) && !ImageRecordStore.checkImageRecordExist(i+1+"_dat")){
					//fillRect(g, transparentImageMask, 10, 20+SPACE_BETWEEN_IMAGES + (IMAGE_HEIGHT + SPACE_BETWEEN_IMAGES)*(i-indexStart), 60, IMAGE_HEIGHT);
					sprites[SPRITE_MENU].paintFrame(g, MENU_FRAME_LOCK, 12, 20+SPACE_BETWEEN_IMAGES + (IMAGE_HEIGHT + SPACE_BETWEEN_IMAGES)*(i-indexStart) + IMAGE_HEIGHT, Graphics.LEFT | Graphics.BOTTOM);
					}
				}
				if(description[i] != null)
				{
					String tam[] = split(description[i]);
					for(int j=0; j<tam.length; j++)
					{
					int palette = PALETTE_FONT_NORMAL_BLACK;
					fonts[FONT_NORMAL].setPalette(palette);
					fonts[FONT_NORMAL].drawString(g, tam[j],
					10 + 80, 20+SPACE_BETWEEN_IMAGES + (IMAGE_HEIGHT + SPACE_BETWEEN_IMAGES)*(i-indexStart) + 10 + j *15, Graphics.LEFT | Graphics.VCENTER);
					}
				}
			}
					
			if(indexSelect == currentNumberGirl - 1 && indexSelect < MAX_IMAGE_ARRAY - 1){
				counterPaintDownloadAnim++;
				if(counterPaintDownloadAnim >= NUMBER_DELAY_ANIM*4) counterPaintDownloadAnim = 0;
				sprites[SPRITE_MENU].paintAFrame(g, MENU_ANIMATION_DOWNLOAD, counterPaintDownloadAnim/NUMBER_DELAY_ANIM, getScreenWidth() >> 1, getScreenHeight() - 10 , Graphics.HCENTER | Graphics.BOTTOM);
			}
		}
		catch(Exception e) {e.printStackTrace();}
	}
	public void updateSelectGirlScreen(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				subState = SUBSTATE_SELECTGIRL_SCREEN;
				image = new Image[MAX_IMAGE_ARRAY];
				description = new String[MAX_IMAGE_ARRAY];
				setSoftKeys(SOFTKEY_NONE, MENU_FRAME_SOFTKEY_SELECT);
				currentNumberGirl = 3;
				checkDataAvailable();
				transparentImageMask = createTransparentImage(TRANSPARENT_IMAGE_MASK_SIZE,
				TRANSPARENT_IMAGE_MASK_SIZE, 0x000000, 0x80);
				break;

			case MESSAGE_UPDATE:
				
				switch(subState)
				{
					case SUBSTATE_SELECTGIRL_SCREEN:
						if (GAME_KEY_UP_PRESSED) {
							if (indexSelect > 0) {
								indexSelect--;
								if(indexSelect == indexStart - 1)
								indexStart--;
							}
						} else if (GAME_KEY_DOWN_PRESSED) {
							if(indexSelect == currentNumberGirl - 1 && indexSelect < MAX_IMAGE_ARRAY - 1) {
								if(currentNumberGirl < MAX_IMAGE_ARRAY)
								{	
									currentNumberGirl++;
									getThumbail(currentNumberGirl);
								}
							}
							else if (indexSelect < currentNumberGirl - 1) {
								indexSelect++;
								if(indexSelect - indexStart == 3)
								indexStart++;
							}
						}
						else if (GAME_KEY_SELECT) {
							if(!ImageRecordStore.checkImageRecordExist(indexSelect+1+"_dat"))
								subState = SUBSTATE_SMSMESSAGE;
							else
								setGameState(STATE_LOADING_GAME_PLAY);
						} else if (RIGHT_SOFT_KEY) {
								setGameState(STATE_MAIN_MENU);
						}		
						break;
					case SUBSTATE_DOWNLOADING:
						paintLoading(g);
						break;
					case SUBSTATE_SMSMESSAGE:
						if(GAME_KEY_LEFT_PRESSED || GAME_KEY_RIGHT_PRESSED)
							isConfirmSendSMS = !isConfirmSendSMS;
						else if(GAME_KEY_SELECT){
							subState = SUBSTATE_SELECTGIRL_SCREEN;
							if(isConfirmSendSMS)
								sms.send(SMS_TYPE_DOWNLOADGIRL);
						}
						break;
				}
				break;
				
			case MESSAGE_PAINT:
				switch(subState)
				{
					case SUBSTATE_SELECTGIRL_SCREEN:
						paintSelectGirlScreen(g);
						break;
					case SUBSTATE_DOWNLOADING:
						paintLoading(g);
						break;
					case SUBSTATE_SMSMESSAGE:
						paintSMSMessage(g);
						break;
				}
				break;

			case MESSAGE_DESTRUCTOR:
				for(int i=0; i< MAX_IMAGE_ARRAY; i++)
					image[i] = null;
				image = null;
				break;
		}
	}

