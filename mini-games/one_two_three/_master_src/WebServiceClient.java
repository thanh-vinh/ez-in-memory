/*
 *
 * Copyright (c) 2007, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Sun Microsystems nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
import com.wingfoot.soap.*;
import com.wingfoot.soap.encoding.*;
import com.wingfoot.soap.transport.*;
import javax.microedition.lcdui.*;


/**
 * Demonstration MIDlet for File Connection API. This MIDlet implements simple
 * file browser for the filesystem available to the J2ME applications.
 *
 */
public class WebServiceClient {
    private static String url="http://lamgiahien.byethost13.com/MyWS/server/OneTwoThreeServices.php";
	private Image img;
	public WebServiceClient() {
	}
	
	public void getGirl(int i){
			try
			{
			Call c=new Call();
 
            // Set method name and its parameter
            c.setMethodName("getGirl");
            c.addParameter("girlId", new String(""+i));
 
            HTTPTransport transport=new HTTPTransport(url,null);
            transport.getResponse(true);
 
            Envelope res=c.invoke(transport);
 
            UntypedObject uo=(UntypedObject)res.getParameter(0);
 
            // If book found
            if(uo!=null){
                // Show the book information
               
 
                // Display the book cover
                // Create base64 object base on the returned base64 string
                //System.out.println("base64="+uo.getPropertyValue(5));
                Base64 b=new Base64(uo.getPropertyValue(1).toString());
 
                // Create the image
                System.out.println("size of image="+b.getBytes().length);
				//FileReader fr = new FileReader();
				//fr.writeFile(i+".nfo", b.getBytes());
				ImageRecordStore.writeImageRecord(i+"_nfo", b.getBytes());
            }
			Game.downloadFinish();
			}
			 catch(Exception ex){
			 Game.downloadFinish();
            System.out.println(ex.toString());
        }
	}
	
	
	public void getDataGirl(int i){
			try
			{
			Call c=new Call();
 
            // Set method name and its parameter
            c.setMethodName("getDataGirl");
            c.addParameter("girlId", new String(""+i));
 
            HTTPTransport transport=new HTTPTransport(url,null);
            transport.getResponse(true);
 
            Envelope res=c.invoke(transport);
 
            UntypedObject uo=(UntypedObject)res.getParameter(0);
 
            // If book found
            if(uo!=null){
      
                Base64 b=new Base64(uo.getPropertyValue(1).toString());
 
                // Create the image
                System.out.println("size of image="+b.getBytes().length);
				//FileReader fr = new FileReader();
				//fr.writeFile(i+".dat", b.getBytes());
                //img=Image.createImage(b.getBytes(), 0, b.getBytes().length);
				ImageRecordStore.writeImageRecord(i+"_dat", b.getBytes());
            }
			Game.downloadDataGirlFinish();
			}
			 catch(Exception ex){
			 Game.downloadDataGirlFinish();
            System.out.println(ex.toString());
        }
	}
}
