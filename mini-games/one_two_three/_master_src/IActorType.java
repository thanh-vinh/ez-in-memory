
public interface IActorType
{
	//
	// Type
	//
	public static final int ACTOR_HERO					= 0;
	public static final int ACTOR_COMPUTER				= 1;
	public static final int ACTOR_FACE					= 2;
	public static final int ACTOR_MAX					= 3;

	//
	// Sex
	//
	public static final int ACTOR_SEX_MALE				= 0;
	public static final int ACTOR_SEX_FERMALE			= 1;
}
