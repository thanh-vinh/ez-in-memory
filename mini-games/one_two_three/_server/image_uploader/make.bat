
@echo off
call ..\..\config.bat

REM set CURRENT_PATH=%~p0
set TEMPORATE_PATH=.temp
set OUTPUT_PATH=.output

echo Copy stock images...

if not exist %TEMPORATE_PATH% (
	md %TEMPORATE_PATH%
)

if not exist %OUTPUT_PATH% (
	md %OUTPUT_PATH%
)

for /d %%i in (*) do (
	if not "%%i" == "%TEMPORATE_PATH%" (
		if not "%%i" == "%OUTPUT_PATH%" (
			echo Process directory: %%i...
			del /q %TEMPORATE_PATH%\*.* > nul
			copy /d %%i\*.* %TEMPORATE_PATH% > nul

			echo Pakage data...
			java -jar %DATA_PACKAGE% %PLATFORM% ./package.xml %TEMPORATE_PATH% %OUTPUT_PATH% %OUTPUT_PATH%/package.h
			
			echo == Please copy %%i...
			pause
		)
	)
)
