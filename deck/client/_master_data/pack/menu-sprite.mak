#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=MENU_SPRITE_PACKAGE
# File name of package
PACKAGE_OUT_FILE=2
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=menu_sprite.png
# Source file
SOURCE_OUT_FILE=menu_sprite.h
