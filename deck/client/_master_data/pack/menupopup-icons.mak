#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=MENUPOPUP_ICONS_PACKAGE
# File name of package
PACKAGE_OUT_FILE=6
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=menupopup_icons.png
# Source file
SOURCE_OUT_FILE=menupopup_icons_sprite.h
