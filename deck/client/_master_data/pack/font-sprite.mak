#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=FONT_PACKAGE
# File name of package
PACKAGE_OUT_FILE=1
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=font_normal.png,font_bold.png,font_black.png,font_money.png,font_big.png
# Source file
SOURCE_OUT_FILE=font_sprite.h
