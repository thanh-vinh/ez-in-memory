#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=AVATAR_SPRITE_PACKAGE
# File name of package
PACKAGE_OUT_FILE=4
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=avatars_sprite.png
# Source file
SOURCE_OUT_FILE=avatars_sprite.h
