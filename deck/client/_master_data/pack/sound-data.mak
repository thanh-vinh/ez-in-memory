#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=SOUND_PACKAGE
# File name of package
PACKAGE_OUT_FILE=5
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=m_title.mid,m_intro.mid,m_win.mid,m_lose.mid,sfx_move_card.wav,sfx_select_card.wav
# Source file
SOURCE_OUT_FILE=sound.h
