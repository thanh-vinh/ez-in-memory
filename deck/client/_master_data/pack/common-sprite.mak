#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=COMMON_SPRITE_PACKAGE
# File name of package
PACKAGE_OUT_FILE=0
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=logo_image.png,splash_image.png,background_image.png,common_item_sprite.png,item_sprite.png,table_img.png,talk.png
# Source file
SOURCE_OUT_FILE=common_sprite.h
