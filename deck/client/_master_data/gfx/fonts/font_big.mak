#######################################
# font-exporter properties file
#######################################

# Font name: define the name for your font
FONT_NAME=FONT_BIG
# System font name: Arial, Times New Roman, Calibri...
# BASE_FONT_NAME=Segoe WP Black
BASE_FONT_NAME=Goudy Stout
# Font style: PLAIN, BOLD, ITALIC
STYLE=BOLD
# Font size
SIZE=16
# Color in hexa value
COLOR=FFFF00
# Transparent or not
TRANSPARENT=1
# Overwrite an existing image file if it exists
OVERWRITE=1
# Source file
SOURCE_OUT_FILE=font_big.h
# All characters which will be create
INPUT_STRING=CHỦMAINOK
