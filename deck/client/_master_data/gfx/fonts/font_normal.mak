#######################################
# font-exporter properties file
#######################################

# Font name: define the name for your font
FONT_NAME=FONT_NORMAL
# System font name: Arial, Times New Roman, Calibri...
BASE_FONT_NAME=Segoe UI
# Font style: PLAIN, BOLD, ITALIC
STYLE=PLAIN
# Font size
SIZE=11
# Color in hexa value
COLOR=FFFFFF
# Transparent or not
TRANSPARENT=1
# Overwrite an existing image file if it exists
OVERWRITE=1
# Source file
SOURCE_OUT_FILE=font_normal.h
# All characters which will be create
INPUT_STRING=!"#$%&'()*+,-_./0123456789:;<=>?@\~©®™ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝĂĐĨŨƠƯẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴỶỸabcdefghijklmnopqrstuvwxyzàáâãèéêìíòóôõùúýăđĩũơưạảấầẩẫậắằẳẵặẹẻẽếềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹ
