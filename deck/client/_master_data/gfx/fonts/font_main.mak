#######################################
# font-exporter properties file
#######################################

# Font name: define the name for your font
FONT_NAME=FONT_MAIN
# System font name: Arial, Times New Roman, Calibri...
BASE_FONT_NAME=VNI-Yahoo
# Font style: PLAIN, BOLD, ITALIC
STYLE=BOLD
# Font size
SIZE=18
# Color in hexa value
COLOR=FFFF00
# Transparent or not
TRANSPARENT=1
# Overwrite an existing image file if it exists
OVERWRITE=1
# Source file
SOURCE_OUT_FILE=font_main.h
# All characters which will be create
INPUT_STRING=CHỦMAINOK
