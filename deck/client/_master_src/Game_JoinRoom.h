
	///*********************************************************************
	///* Game_JoinRoom.h
	///*********************************************************************

	private static String room = "";
	private static ListView roomList;
	public static final String ROOM_LIST_HEADER = "ROOM LIST";
	public static final String STATUS_HEADER = "STATUS";

	public void updateJoinRoomScreen(byte message, Graphics g)
	{
		switch (message) {
		case MESSAGE_CONSTRUCTOR:
			userName = connection.sendRequest("Get userName");
			DBG("username = " + userName);
			createRoomInfo();
			break;

		case MESSAGE_UPDATE:
		#ifdef USE_REDWAF_SERVER
			if(GameClient.getRoomCount() > 0){
				String items[][] = new String[10][2];

				for(int i = 0; i < GameClient.getRoomCount(); i++) {
					items[i][0] = GameClient.getRoomNameAt((short)i);
					items[i][1] = "";
				}
				roomList.setItems(items);
			}
		#endif
			roomList.updateListView(keyCode);
			InfoPlaying.idRoom = roomList.getSelectedItemId();
			if(InfoPlaying.idRoom != -1){
				setGameState(STATE_TABLE_LIST);
			}
			if (RIGHT_SOFT_KEY) {
				setGameState(STATE_MAIN_MENU);
			}
			break;

		case MESSAGE_PAINT:
			this.paintJoinRoomScreen(g);
			break;

		case MESSAGE_DESTRUCTOR:
			roomList = null;
			break;
		}
	}

	public void createRoomInfo()
	{
	String headers[] = {ROOM_LIST_HEADER, STATUS_HEADER};
	#ifndef USE_REDWAF_SERVER
		room = "";
		room = connection.getRoomInfo();
		String row[] = GameLib.splitString(room, ";");
		String items[][] = new String[10][2];
		for(int i = 0; i < row.length; i++) {
			String column[] = GameLib.splitString(row[i] , ",");
			items[i][0] = "Room" + column[0];
			items[i][1] = "";
		}


		roomList = new ListView(headers, items, 0, 0, 240, 320);
	#else
		if(commmandClient != null){
		try{
			commmandClient.handleInputMessage("4");
		}catch(Exception e) {DBG(e);}
		}
		else {DBG("commmandClient null");}	
		String items[][] = new String[10][2];
		
		// TODO Should create a state for watting update from server
		// and continue after finish
		// remove lines below in the future
		while (!GameClient.isInitializeRoomList()) {
			;
		}
		
		for(int i = 0; i < GameClient.getRoomCount(); i++) {
			DBG(GameClient.getRoomNameAt((short)i));
			items[i][0] = GameClient.getRoomNameAt((short)i);
			items[i][1] = "";
		}
		roomList = new ListView(headers, items, 0, 0, 240, 320);
	#endif
		int[] columnPos = {0, 200};
		int[] aligns = {ListView.LEFT_ALIGN, ListView.RIGHT_ALIGN};
		roomList.setColumnsPos(columnPos, aligns);
		roomList.setColors(POPUP_INPUT_BOX_TITLE_COLOR, MENU_HOVER_BACKGROUND_COLOR);
		roomList.setFonts(fonts[FONT_BOLD],fonts[FONT_NORMAL]);
		Image[] icons = {
			commonItemSprite.getImage(COMMON_ITEM_ITEM_SPRITE_FRAME_ROOM),
			commonItemSprite.getImage(COMMON_ITEM_ITEM_SPRITE_FRAME_STATUS)
		};
		roomList.setIcons(icons);
		setSoftKeys(SOFTKEY_NONE, SOFTKEY_OK, SOFTKEY_BACK);
	}

	public void paintJoinRoomScreen(Graphics g)
	{
		g.drawImage(backgroundImage, 0, 0, 0);
		roomList.paintListView(g);
	}
