
	///*********************************************************************
	///* Game_Ingame.h
	///*********************************************************************

	private static final int MAX_PLAYERS			= 4;
	private static final int MAIN_PLAYER			= 0;	// Index in players array for MC
	public static final byte NUM_FRAME_CARD_WHEN_PLAY = 8;
	public static final int TIME_OF_EACH_TURN = 20;
	public static final int WAITING_PLAYER_PLAYING_CROSSBAR_WIDTH = 100;
	public static final int WAITING_PLAYER_PLAYING_CROSSBAR_HEIGHT = 10;
	public static final int WAITING_PLAYER_PLAYING_VERTICALBAR_WIDTH = 10;
	public static final int WAITING_PLAYER_PLAYING_VERTICALBAR_HEIGHT = 40;
	public static final int WAITING_PLAYER_PLAYING_CROSSBAR_BELOW_OFFSET = 104;
	public static final int WAITING_PLAYER_PLAYING_CROSSBAR_ABOVE_OFFSET = 267;
	
	public static GameBoard gameBoard;
	private static int selectedCardId;
	public static CardList lastCardListMoved;
	public static int stepFramePlay = NUM_FRAME_CARD_WHEN_PLAY;
	public static final int arrayMoveCard[] = {30,24,18,12,8,4,1,0};
	public static int s_playerTurnTimeCount = -1;
	public static long s_lastTimeCount = 0;
	public static boolean beginCountTime = false;
	public static boolean endTime = false;

	public static void setStepFramePlayToZero()
	{
		stepFramePlay = 0;
	}

	/**
	 * For testing only.
	 */
	private static void initPlayers()
	{
		Deck deck = new Deck();
		deck.shuffle();

		players = new Player[MAX_PLAYERS];
		for (int i = 0; i < MAX_PLAYERS; i++) {
			Vector cards = deck.getCards(i);
			players[i] = new Player(cards);
		}

		deck = null;

		#ifdef FOR_TEST_RULE
		//init last card list moved
		lastCardListMoved = new CardList();
		lastCardListMoved.add(new Card(ICard.RANK_3, ICard.SUIT_DIAMON));
		lastCardListMoved.add(new Card(ICard.RANK_4, ICard.SUIT_DIAMON));
		lastCardListMoved.add(new Card(ICard.RANK_5, ICard.SUIT_DIAMON));
		#endif
	}

	private int getRankFrameIndex(int rank, int suit)
	{
		if (suit < ICard.SUIT_DIAMON) {
			return rank - 3;
		} else {
			return (rank + 13) - 3;
		}
	}

	private int getSuitFrameIndex(int suit)
	{
		return (suit + 26);		// Frame 0 - 25: A 2 3 ... J Q K (13 x 2)
	}

	private void drawCard(int x, int y, ICard card, Graphics g)
	{
		cardSprite.paintFrame(CARD_SPRITE_WHITE_FRAME_ID, x, y, 0, g);

		int rankFrameId = getRankFrameIndex(card.getRank(), card.getSuit());
		int suitFrameId = getSuitFrameIndex(card.getSuit());

		rankSuitSprite.paintFrame(rankFrameId, x + 4, y + 4, 0, g);		// Hard code
		rankSuitSprite.paintFrame(suitFrameId, x + 4, y + 14, 0, g);	// Hard code for frame height !!!

		x += CARD_SPRITE_FRAMES_WIDTH;
		y += CARD_SPRITE_FRAMES_HEIGHT;

		// TRANS_ROT180
		rankSuitSprite.paintFrame(rankFrameId, x - 4, y - 4, Graphics.BOTTOM | Graphics.RIGHT, Sprite.TRANS_ROT180, g);
		rankSuitSprite.paintFrame(suitFrameId, x - 4, y - 14, Graphics.BOTTOM | Graphics.RIGHT, Sprite.TRANS_ROT180, g);
	}

	private void drawCardList(int x, int y, CardList cardList, Graphics g)
	{
		if (cardList == null)
			return;

		int cardListCount = cardList.cards.size();

		for (int i = 0; i < cardListCount; i++) {
			ICard card = cardList.getCardAt(i);
		#ifdef OPTIMIZE_DRAW_OVERLAP_CARD
			// Optimize for drawing card: don't draw overlaped part (undef OPTIMIZE_DRAW_OVERLAP_CARD to debug)
			if (cardList.wasSelected(i)) {
				g.setClip(x, y - CARD_SPACE, CARD_SPRITE_FRAMES_WIDTH, CARD_SPRITE_FRAMES_HEIGHT);
			} else if (cardList.wasSelected(i + 1) || (i == cardListCount - 1)) {
				g.setClip(x, y, CARD_SPRITE_FRAMES_WIDTH, CARD_SPRITE_FRAMES_HEIGHT);
			} else {
				g.setClip(x, y, CARD_SPACE, CARD_SPRITE_FRAMES_HEIGHT);
			}
		#endif	//OPTIMIZE_DRAW_OVERLAP_CARD

			if (cardList.wasSelected(i)) {
				this.drawCard(x, y - CARD_SPACE, card, g);
			} else {
				this.drawCard(x, y, card, g);
			}

		#ifdef USE_TOUCH_SCREEN
				if (GameLib.wasPointerPressedInRect(x, y, CARD_SPACE, CARD_SPRITE_FRAMES_HEIGHT) /* ||
				(GameLib.wasPointerPressedInRect(x, y, CARD_SPRITE_FRAMES_WIDTH, CARD_SPRITE_FRAMES_HEIGHT) && i == cardListCount -1)*/) {
					if (!GameBoard.mainPlayer.wasSelected(i)) {
						GameBoard.mainPlayer.select(i);
					} else {
						GameBoard.mainPlayer.deselect(i);
					}
					GameBoard.updateCanMoveCardAction = true;
				}
		#endif

			x += CARD_SPACE;
		}
	}

	private void drawLastCardListMoving(Graphics g)
	{
		int x = 0,y = 0;
		if(lastCardListMoved.cards.size() > 0) {
			if(InfoPlaying.lastPositionPlayerPlay == 0) {
				x = (getScreenWidth() -((lastCardListMoved.cards.size() - 1) * CARD_SPACE + CARD_SPRITE_FRAMES_WIDTH)) >> 1;
				y = (getScreenHeight() >> 1) - CARD_SPACE + arrayMoveCard[stepFramePlay];
			} else if(InfoPlaying.lastPositionPlayerPlay == 1) {
				x = (getScreenWidth() -((lastCardListMoved.cards.size() - 1) * CARD_SPACE + CARD_SPRITE_FRAMES_WIDTH)) >> 1;
				x = x + arrayMoveCard[stepFramePlay];
				y = (getScreenHeight() >> 1) - CARD_SPACE;
			} else if(InfoPlaying.lastPositionPlayerPlay == 2) {
				x = (getScreenWidth() -((lastCardListMoved.cards.size() - 1) * CARD_SPACE + CARD_SPRITE_FRAMES_WIDTH)) >> 1;
				y = (getScreenHeight() >> 1) - CARD_SPACE - arrayMoveCard[stepFramePlay];
			} else if(InfoPlaying.lastPositionPlayerPlay == 3) {
				x = (getScreenWidth() -((lastCardListMoved.cards.size() - 1) * CARD_SPACE + CARD_SPRITE_FRAMES_WIDTH)) >> 1;
				x = x - arrayMoveCard[stepFramePlay];
				y = (getScreenHeight() >> 1) - CARD_SPACE;
			}

			drawCardList(x, y, lastCardListMoved, g);
		}

		if (stepFramePlay < NUM_FRAME_CARD_WHEN_PLAY - 1) {
			stepFramePlay++;
			DBG("stepFramePlay = " + stepFramePlay);
		}
	}
	// TODO: draw waiting animation	
	private void drawWaitingForCurrentPlayerPlay(Graphics g)
	{
		int x = 0,y = 0;
		int width = 0;
		int height = 0;
		int screenWidth = getScreenWidth();
		int screenHeight = getScreenHeight();
		if(InfoPlaying.currentPositionPlayerPlay == -1) return;
		else if(InfoPlaying.currentPositionPlayerPlay == 0){
			x = (screenWidth - WAITING_PLAYER_PLAYING_CROSSBAR_WIDTH)/2;
			y = screenHeight - WAITING_PLAYER_PLAYING_CROSSBAR_BELOW_OFFSET;
			width = WAITING_PLAYER_PLAYING_CROSSBAR_WIDTH;
			height = WAITING_PLAYER_PLAYING_CROSSBAR_HEIGHT;
		}else if(InfoPlaying.currentPositionPlayerPlay == 1) {
			x = 200;
			y = 100;
			width = WAITING_PLAYER_PLAYING_VERTICALBAR_WIDTH;
			height = WAITING_PLAYER_PLAYING_VERTICALBAR_HEIGHT;
		} else if(InfoPlaying.currentPositionPlayerPlay == 2) {
			x = (screenWidth - WAITING_PLAYER_PLAYING_CROSSBAR_WIDTH)/2;
			y = screenHeight - WAITING_PLAYER_PLAYING_CROSSBAR_ABOVE_OFFSET;
			width = WAITING_PLAYER_PLAYING_CROSSBAR_WIDTH;
			height = WAITING_PLAYER_PLAYING_CROSSBAR_HEIGHT;
		} else if(InfoPlaying.currentPositionPlayerPlay == 3) {
			x = 40;
			y = 100;
			width = WAITING_PLAYER_PLAYING_VERTICALBAR_WIDTH;
			height = WAITING_PLAYER_PLAYING_VERTICALBAR_HEIGHT;
		}
		float percent = (float)s_playerTurnTimeCount / TIME_OF_EACH_TURN ;
		g.setColor(0xFFFFFF);
		g.fillRect(x, y, width, height);
		g.setColor(0);
		if((InfoPlaying.currentPositionPlayerPlay == 1) || (InfoPlaying.currentPositionPlayerPlay == 3))
			g.fillRect(x+1, y+1, width - 2, (int)(percent *(height - 2)));
		else
			g.fillRect(x+1, y+1, (int)(percent * (width - 2)),  (height - 2));
		// System.out.println(" >>>>  Hoang --  lastPositionPlayerPlay: " + InfoPlaying.lastPositionPlayerPlay + ", Name:" + InfoPlaying.arrayNamePlayer[InfoPlaying.lastPositionPlayerPlay]);
	}

	public void drawGameBoard(Graphics g)
	{
		drawGradientBackground(g);
		drawAvatarPlayer(g);
        drawLastCardListMoving(g);
		paintChatLogMessage(fonts[FONT_BLACK], g);
		// Draw all card of main player
		int x = 0, y = 0;

	#ifdef USE_DEAL_CARD_FROM_SERVER
		if(gameBoard.mainPlayer != null)
	#endif
		{
			x = PLAYERS_POS[MAIN_PLAYER][0];
			y = PLAYERS_POS[MAIN_PLAYER][1];
	#ifndef USE_DEAL_CARD_FROM_SERVER
			Player player = players[MAIN_PLAYER];
	#else
			Player player = gameBoard.mainPlayer;
	#endif
			drawCardList(x - 100, y, player, g);

			x = PLAYERS_POS[MAIN_PLAYER][0] + (selectedCardId * CARD_SPACE) + (CARD_SPACE >> 1);
			y = PLAYERS_POS[MAIN_PLAYER][1] + (CARD_SPRITE_FRAMES_HEIGHT >> 2);
			setFullScreenClip();	// Reset clip
			itemSprite.paintFrame(0, x - 100, y, 0, g);

			player = null;
		}
	}

	public void updateIngame(byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				if (gameBoard == null) {
					loadCardSprite();
					if (avatarSprite == null) {
						loadAvatarSprite();
					}
				#ifndef USE_DEAL_CARD_FROM_SERVER
					initPlayers();	// For testing only
					gameBoard = new GameBoard(players);
				#else
					// #ifdef FOR_TEST_RULE
						lastCardListMoved =new CardList();
						// lastCardListMoved.add(new Card(ICard.RANK_3, ICard.SUIT_DIAMON));
						// lastCardListMoved.add(new Card(ICard.RANK_3, ICard.SUIT_SPADE));
						// lastCardListMoved.add(new Card(ICard.RANK_5, ICard.SUIT_DIAMON));
					// #endif
					gameBoard = new GameBoard();
				#endif
				}

				// ???????????????????????????????????????????????????????????
				// @All: Sub states in game should be handle by Game or GameBoard
				// In this case, use Game to handle in game sub states
				// TODO Solve above question first
				// ???????????????????????????????????????????????????????????
				if (inputBox == null) {
					inputBox = new InputBox(Package.getText(TEXT.INGAME_ENTER_TO_CHAT), "", TextField.ANY, POPUP_INPUT_BOX_OFFSET_X,
							POPUP_INPUT_BOX_OFFSET_Y, getScreenWidth() - (POPUP_INPUT_BOX_OFFSET_X << 1), fonts[FONT_NORMAL].getHeight() + 8);
					inputBox.setFonts(fonts[FONT_BOLD], fonts[FONT_NORMAL]);
					inputBox.setColors(POPUP_INPUT_BOX_BACKGROUND_COLOR, POPUP_INPUT_BOX_TITLE_COLOR, TEXTFIELD_BACKGROUND_COLOR, TEXTFIELD_FOCUS_BACKGROUND_COLOR, TEXTFIELD_BORDER_COLOR);
				}
				subState = -1;
				soundPlayer.play(M_INTRO_ID);
				break;	// case MESSAGE_CONSTRUCTOR

			case MESSAGE_UPDATE:
		#ifdef USE_CHECK_CARDS_FROM_SERVER
				String updateInGameString = Game.connection.getUpdate();
				if(updateInGameString != null) {
					Game.connection.processUpdateInGameString(updateInGameString);
				}
		#endif
				if(InfoPlaying.isWin == 1){
					if(!InfoPlaying.isHostRoom) {InfoPlaying.isHostRoom = true; InfoPlaying.currentPlayerHostRoom = getPositionInTable();}
					setGameState(STATE_TABLE_WATTING_PLAYERS);
				}
				else if(InfoPlaying.isWin == 0) {
					setGameState(STATE_TABLE_WATTING_PLAYERS);
				}
				// left soft key
				if (LEFT_SOFT_KEY && subState==-1) {
					// lastState = currentState;
					// switch to subState InGame Menu
					subState = SUBSTATE_INGAME_MENU;
					// init for Menu Popup
					this.menuPopupItems = new byte[] {
						MENU_POPUP_ITEM_SETTING,
						MENU_POPUP_ITEM_FUNCTION,
						MENU_POPUP_ITEM_ADDFRIEND,
						MENU_POPUP_ITEM_LEAVETABLE,
					};
					if (this.menuPopup != null)
						this.menuPopup = null;
					this.menuPopup = new MenuPopup(this.menuPopupItems, fonts[FONT_NORMAL], getScreenHeight() - POPUP_MENU_OFFSET_Y,
							getScreenWidth(), getScreenHeight());
					setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_CLOSE);
				} else if (KEY_ENTER_MESSAGE_INGAME) {
					subState = STATE_INGAME_POPUP_ENTER_CHAT;
				#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
					offScreenTransparentImage = createTransparentImage(TRANSPARENT_IMAGE_WIDTH,
							TRANSPARENT_IMAGE_HEIGHT, TRANSPARENT_IMAGE_COLOR, TRANSPARENT_IMAGE_ALPHA);
				#endif
				}

				if (this.gameBoard.canMoveCardAction
						(
							Game.lastCardListMoved, InfoPlaying.isAllowPlay
				// #ifdef FIX_BUG_ALLOW_NEXT_PLAYER_IN_A_CIRCEL
							&& InfoPlaying.allowModify_isAllowPlay
				// #endif
						)) {
					setSoftKeys(SOFTKEY_MENU, SOFTKEY_MOVE, SOFTKEY_NONE);
				} else {
					setSoftKeys(SOFTKEY_MENU, SOFTKEY_NONE, SOFTKEY_IGNORE);
				}
				int selectedMenuPopupItem;
				switch(subState)
				{
					case -1:
						this.gameBoard.updateGameBoard(keyCode);
						selectedCardId = gameBoard.getSelectedCardId();
						break;
						
					case STATE_INGAME_POPUP_ENTER_CHAT:
						setSoftKeys(SOFTKEY_NONE, SOFTKEY_OK, SOFTKEY_NONE);

						if (ANY_KEY_PRESSED) {
							inputBox.updateInputBox(keyCode);
						}

						if (KEY_FIRE) {
							DBG("Input message: " + inputBox.getString());
							InfoPlaying.currentChatMessageString = inputBox.getString();
							startDisplayMessageChat = 0;
							InfoPlaying.currentPlayerTalkPos = 0;
							Game.connection.sendChatMessage(userName +":"+inputBox.getString());
							inputBox.setString("");
							subState = -1;
						}
						break;
						
					case SUBSTATE_INGAME_MENU:
						// update for Menu InGame Popup
						this.menuPopup.updateMenu(keyCode);
						selectedMenuPopupItem = this.menuPopup.getSelectedMenuItemId();
						switch (selectedMenuPopupItem) {
							
							case MENU_POPUP_ITEM_SETTING:
								// switch to subState Setting Menu Popup
								subState = SUBSTATE_SETTING_PP_MENU;
								this.menuPopupItems = new byte[] {
									MENU_POPUP_ITEM_SETTING_GOLD,
									MENU_POPUP_ITEM_SETTING_NUMBER_PLAYER,
									MENU_POPUP_ITEM_SETTING_PASSWORD,
								};
								if (this.menuPopup != null)
									this.menuPopup = null;
								this.menuPopup = new MenuPopup(this.menuPopupItems, fonts[FONT_NORMAL], getScreenHeight() - POPUP_MENU_OFFSET_Y,
										getScreenWidth(), getScreenHeight());
								setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_CLOSE);
								break;						
							
							case MENU_POPUP_ITEM_LEAVETABLE:
								connection.outTable();
								setGameState(STATE_TABLE_LIST);
								break;

							case MENU_POPUP_ITEM_FUNCTION:
								break;

							case MENU_POPUP_ITEM_ADDFRIEND:
								break;
						}	// switch (selectedMenuPopupItem)
						if (RIGHT_SOFT_KEY)
							subState = -1;
						break;
					case SUBSTATE_SETTING_PP_MENU:
						// update for Menu InGame Popup
						this.menuPopup.updateMenu(keyCode);
						selectedMenuPopupItem = this.menuPopup.getSelectedMenuItemId();
						switch (selectedMenuPopupItem) {				
							
							case MENU_POPUP_ITEM_SETTING_GOLD:
								setGameState(STATE_SETTING_POPUP_SETTING_GOLD);
								break;

							case MENU_POPUP_ITEM_SETTING_NUMBER_PLAYER:
								break;

							case MENU_POPUP_ITEM_SETTING_PASSWORD:
								break;
						}	// switch (selectedMenuPopupItem)
						if (RIGHT_SOFT_KEY) {
							subState = SUBSTATE_INGAME_MENU;
							this.menuPopupItems = new byte[] {
								MENU_POPUP_ITEM_SETTING,
								MENU_POPUP_ITEM_FUNCTION,
								MENU_POPUP_ITEM_ADDFRIEND,
								MENU_POPUP_ITEM_LEAVETABLE,
							};
							if (this.menuPopup != null)
								this.menuPopup = null;
							this.menuPopup = new MenuPopup(this.menuPopupItems, fonts[FONT_NORMAL], getScreenHeight() - POPUP_MENU_OFFSET_Y,
									getScreenWidth(), getScreenHeight());
							setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_CLOSE);
						}
						break;
				}
				// update time for player's turn
				// if (InfoPlaying.isAllowPlay) {
					if (beginCountTime) {
					DBG("set lai Time");
						s_playerTurnTimeCount = TIME_OF_EACH_TURN;
						s_lastTimeCount = System.currentTimeMillis();
						beginCountTime = false;
					} else {
						if (System.currentTimeMillis() - s_lastTimeCount >= 1000) {
							s_playerTurnTimeCount --;
							s_lastTimeCount = System.currentTimeMillis();
							if(s_playerTurnTimeCount == -1) endTime = true;
						}
						//next turn function
						if ((s_playerTurnTimeCount == -1) && (InfoPlaying.isAllowPlay) && (endTime == true)){
						//hien.phamhuu bug when one user must be play
							if(InfoPlaying.isCompleteCircle == false && (!InfoPlaying.isStartGame)){
								Game.connection.sendAllowNextPlayer();
								DBG("What the fuck");
								endTime = false;
								}
							else{
							//dai ca Vinh cho danh cay bai nho nhat o day coi, Ong lam cho nhanh.
							if (!GameBoard.mainPlayer.wasSelected(0)){
								gameBoard.mainPlayer.deselectAll();
								gameBoard.wasSelectedCard = true;
								GameBoard.mainPlayer.select(0);
								GameBoard.updateCanMoveCardAction = true;
								Game.soundPlayer.play(IData.SFX_SELECT_CARD_ID);
								DBG("[GameBoard] MC selected card id = " + 0);
								//GameBoard.updateCanMoveCardAction = true;
								}
								keyPressed(IKeyboard.FIRE);
								endTime = false;
								
							}
								
						}
					}
					
				break;	// case MESSAGE_UPDATE
			case MESSAGE_PAINT:
				// Paint game play
				drawGameBoard(g);
				// paintChatLogMessage(fonts[FONT_BOLD], fonts[FONT_NORMAL], g);
				if (subState == STATE_INGAME_POPUP_ENTER_CHAT) {
				#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
					fillRect(offScreenTransparentImage, 0, 0, getScreenWidth(), getScreenHeight(), g);
				#endif
					inputBox.paintInputBox(g);
				} else if (subState == SUBSTATE_INGAME_MENU 
						|| subState == SUBSTATE_SETTING_PP_MENU
				) {
					this.menuPopup.paintMenu(g);
				}
				// draw player's turn time remain
				// if (s_playerTurnTimeCount>=0) {
					// fonts[FONT_NORMAL].drawString("Your turn: " + s_playerTurnTimeCount + ".s", 5, 5, Graphics.LEFT | Graphics.TOP, g); // TODO: remove hard code string
				// } else {
					drawWaitingForCurrentPlayerPlay(g);
				// }
				break;

			case MESSAGE_DESTRUCTOR:
				gameBoard = null;
				inputBox = null;
				this.menuPopup = null;
				subState = -1;
			#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
				offScreenTransparentImage = null;
			#endif
				InfoPlaying.isWin = -1;
				break;
		}	// switch (message)
	}

