
	///*********************************************************************
	///* Game_MenuPopup.h
	///*********************************************************************

	/**
	 * Menu Popup items
	 */
	public static final byte MENU_POPUP_ITEM_COMMENT	= 0;
	public static final byte MENU_POPUP_ITEM_REGISTER	= 1;
	public static final byte MENU_POPUP_ITEM_FORGETPASS	= 2;
	public static final byte MENU_POPUP_ITEM_CHAGEPASS	= 3;
	public static final byte MENU_POPUP_ITEM_EXITGAME	= 4;
	public static final byte MENU_POPUP_ITEM_MESSAGE	= 5;
	public static final byte MENU_POPUP_ITEM_REFRESH	= 6;
	public static final byte MENU_POPUP_ITEM_GOTABLE	= 7; // go to the table that user input
	public static final byte MENU_POPUP_ITEM_SETTING	= 8; // set config of Table (Host only).
	public static final byte MENU_POPUP_ITEM_LEAVETABLE	= 9; // exit Table.
	public static final byte MENU_POPUP_ITEM_FUNCTION	= 10; // go to the functions of Host.
	public static final byte MENU_POPUP_ITEM_ADDFRIEND	= 11; //
	public static final byte MENU_POPUP_ITEM_SETTING_GOLD	= 12; //
	public static final byte MENU_POPUP_ITEM_SETTING_NUMBER_PLAYER	= 13; //
	public static final byte MENU_POPUP_ITEM_SETTING_PASSWORD	= 14; //
	public static final byte MENU_POPUP_ITEM_SETTING_GOLD_5000	= 15; //
	public static final byte MENU_POPUP_ITEM_SETTING_GOLD_10000	= 16; //
	public static final byte MENU_POPUP_ITEM_SETTING_GOLD_20000	= 17; //
	public static final byte MENU_POPUP_ITEM_LOGIN				= 18;

	private byte[] menuPopupItems;
	private MenuPopup menuPopup;

	/**
	 * Load menu items text.
	 */
	private void loadMenuPopupItems(byte menuId)
	{
		switch (menuId) {
			case STATE_LOGIN_POPUP_MENU:
				this.menuPopupItems = new byte[] {
					MENU_POPUP_ITEM_REGISTER,
					MENU_POPUP_ITEM_FORGETPASS,
					MENU_POPUP_ITEM_EXITGAME,
				};
				break;

			case STATE_REGISTER_POPUP_MENU:
				this.menuPopupItems = new byte[] {
					MENU_POPUP_ITEM_REGISTER,
					MENU_POPUP_ITEM_FORGETPASS,
					MENU_POPUP_ITEM_EXITGAME,
				};
				break;

			case STATE_JOIN_ROOM_POPUP_MENU:
				this.menuPopupItems = new byte[] {
					MENU_POPUP_ITEM_REFRESH,
					MENU_POPUP_ITEM_MESSAGE,
					MENU_POPUP_ITEM_EXITGAME,
				};
				break;

			case SUBSTATE_WAITING_PLAY_PP_MENU:
				this.menuPopupItems = new byte[] {
					MENU_POPUP_ITEM_SETTING,
					MENU_POPUP_ITEM_FUNCTION,
					MENU_POPUP_ITEM_ADDFRIEND,
					MENU_POPUP_ITEM_LEAVETABLE,
				};
				break;
			case SUBSTATE_SETTING_PP_MENU:
				this.menuPopupItems = new byte[] {
					MENU_POPUP_ITEM_SETTING_GOLD,
					MENU_POPUP_ITEM_SETTING_NUMBER_PLAYER,
					MENU_POPUP_ITEM_SETTING_PASSWORD,
				};
				break;
			// case STATE_INGAME_POPUP_MENU:
				// this.menuPopupItems = new byte[] {
					// MENU_POPUP_ITEM_SETTING,
					// MENU_POPUP_ITEM_FUNCTION,
					// MENU_POPUP_ITEM_ADDFRIEND,
					// MENU_POPUP_ITEM_LEAVETABLE,
				// };
				// break;

			// case STATE_SETTING_POPUP_MENU:
				// this.menuPopupItems = new byte[] {
					// MENU_POPUP_ITEM_SETTING_GOLD,
					// MENU_POPUP_ITEM_SETTING_NUMBER_PLAYER,
					// MENU_POPUP_ITEM_SETTING_PASSWORD,
				// };
				// break;

			case STATE_SETTING_POPUP_SETTING_GOLD:
				this.menuPopupItems = new byte[] {
					MENU_POPUP_ITEM_SETTING_GOLD_5000,
					MENU_POPUP_ITEM_SETTING_GOLD_10000,
					MENU_POPUP_ITEM_SETTING_GOLD_20000,
				};
				break;
		}
	}

	public void updateMenuPopup(byte menuId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				this.loadMenuPopupItems(menuId);		// Load menu text items

				if (this.menuPopup != null)
					this.menuPopup = null;
				this.menuPopup = new MenuPopup(this.menuPopupItems, fonts[FONT_NORMAL], getScreenHeight() - POPUP_MENU_OFFSET_Y,
						getScreenWidth(), getScreenHeight());
				// this.menuPopup.setColor(MENU_BACKGROUND_COLOR, MENU_HOVER_BACKGROUND_COLOR);
				// set soft keys
				setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_CLOSE);

				DBG("MESSAGE_CONSTRUCTOR menu popup");
				break;

			case MESSAGE_UPDATE:
				switch (menuId) {
					case STATE_LOGIN_POPUP_MENU:
						break;

					case STATE_MAIN_MENU_POPUP_MENU:
						break;

					case STATE_JOIN_ROOM_POPUP_MENU:
						break;

					// case STATE_WAITING_PLAY_POPUP_MENU:
					// case STATE_SETTING_POPUP_MENU:
					case STATE_SETTING_POPUP_SETTING_GOLD:
						paintWattingPlayersScreen(g);
						DBG("paintWattingPlayersScreen");
						break;

					// case STATE_INGAME_POPUP_MENU:
						// this.gameBoard.updateGameBoard(IKeyboard.NONE_VALUE);
						// drawGameBoard(g);
						// break;
				}	// switch (menuId)

				this.menuPopup.updateMenu(keyCode);
				int selectedMenuPopupItem = this.menuPopup.getSelectedMenuItemId();

				if (selectedMenuPopupItem != -1)
				switch (selectedMenuPopupItem) {
					case MENU_POPUP_ITEM_COMMENT:
						break;

					case MENU_POPUP_ITEM_REGISTER:
						setGameState(STATE_REGISTER_SCREEN);
						break;

					case MENU_POPUP_ITEM_FORGETPASS:
						break;

					case MENU_POPUP_ITEM_CHAGEPASS:
						break;

					case MENU_POPUP_ITEM_EXITGAME:
						setGameState(STATE_EXIT_CONFIRM);
						break;

					case MENU_POPUP_ITEM_MESSAGE:
						setGameState(STATE_CHAT_SCREEN);
						break;

					case MENU_POPUP_ITEM_REFRESH:
						if(lastState == STATE_TABLE_LIST)
							refreshTableInfo();
						setGameState(lastState);
						break;

					case MENU_POPUP_ITEM_GOTABLE:
						break;

					case MENU_POPUP_ITEM_SETTING:
						// setGameState(STATE_SETTING_POPUP_MENU);
						break;

					case MENU_POPUP_ITEM_SETTING_GOLD:
						setGameState(STATE_SETTING_POPUP_SETTING_GOLD);
						break;

					case MENU_POPUP_ITEM_SETTING_GOLD_5000:
						InfoPlaying.gold = "5000";
						connection.setGoldInTable(InfoPlaying.gold);
						setGameState(STATE_TABLE_WATTING_PLAYERS);
						break;

					case MENU_POPUP_ITEM_SETTING_GOLD_10000:
						InfoPlaying.gold = "10000";
						connection.setGoldInTable(InfoPlaying.gold);
						setGameState(STATE_TABLE_WATTING_PLAYERS);
						break;

					case MENU_POPUP_ITEM_SETTING_GOLD_20000:
						InfoPlaying.gold = "20000";
						connection.setGoldInTable(InfoPlaying.gold);
						setGameState(STATE_TABLE_WATTING_PLAYERS);
						break;

					// case MENU_POPUP_ITEM_LEAVETABLE:
						// connection.outTable();
						// setGameState(STATE_TABLE_LIST);
						// break;

					case MENU_POPUP_ITEM_FUNCTION:
						break;

					case MENU_POPUP_ITEM_ADDFRIEND:
						break;
				}	// switch (selectedMenuPopupItem) {

				// Go to previous state when press right soft key
				if (RIGHT_SOFT_KEY)
					setGameState(lastState);
				// updateInputMenuPopup(menuId);
				break;	//case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				this.menuPopup.paintMenu(g);
				break;

			case MESSAGE_DESTRUCTOR:
				this.menuPopupItems = null;
				this.menuPopup = null;
				/* if(lastState == nextState) {
					if(nextState < STATE_MAIN_MENU_POPUP_MENU || nextState > STATE_SETTING_POPUP_SETTING_GOLD)
						setSoftKeys(lastLeftSoftKey, lastMidSoftKey, lastRightSoftKey);
				} else {
					DBG("setGameState");
				} */
				break;
		}
	}

	public void updateInputMenuPopup(byte menuId)
	{
		switch(menuId) {
			case STATE_LOGIN_POPUP_MENU:
				if (RIGHT_SOFT_KEY) {
					setGameState(STATE_LOGIN_SCREEN);
				}
				break;

			/* case STATE_MAIN_MENU_POPUP_MENU:
				if (LEFT_SOFT_KEY) {
					setGameState(STATE_MAIN_MENU);
				}
				break; */

			// case STATE_WAITING_PLAY_POPUP_MENU:
				// if (LEFT_SOFT_KEY) {
					// setGameState(STATE_TABLE_WATTING_PLAYERS);
				// }
				// break;

			// case STATE_INGAME_POPUP_MENU:
				// if (LEFT_SOFT_KEY) {
					// setGameState(STATE_GAME_PLAY);
				// }
				// break;


			//hien.phamhuu add default for reduce code when the nextState is lastState. This need that you have a correct lastState.
			//When you get a error in here please check the vaule of lastState and see the example in MENU_POPUP_ITEM_SETTING. Thanks
			/* default:
				if (RIGHT_SOFT_KEY) {
					setGameState(lastState);
				}
				break; */
		}
	}
