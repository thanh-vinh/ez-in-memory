
public interface IGameConfig
{
	// Logo screen
	public static final int LOGO_IMAGE_OFFSET_X						= 65;
	public static final int LOGO_IMAGE_OFFSET_Y						= 90;
#ifndef _DEBUG
	public static final int TIME_LOGO_DELAY 						= 3000;	// Delay 3s for Logo screen
#else
	public static final int TIME_LOGO_DELAY 						= 0;
#endif

	// Sounds
	public static final int MUSIC_TITLE 							= 0;
	public static final int MUSIC_BACKGROUND 						= 1;
	public static final int SFX_CONFIRM 							= 2;

	// Fonts
	public static final int FONT_EXPANDED_SPACING					= 1;

	//
	// Game interface
	//

	// Confirm screen
	public static final int MESSAGE_BOX_SOUND_CONFIRM_OFFSET_Y		= 100;

	// Message box
	public static final int MESSAGE_BOX_OFFSET_X					= 4;
	public static final int MESSAGE_BOX_BACKGROUND_COLOR			= 0x000f30;
	public static final int MESSAGE_BOX_BUTTON_COLOR				= 0x000f30;
	public static final int MESSAGE_BOX_SELECTED_COLOR				= 0x0f64b7;
	public static final int MESSAGE_BOX_BORDER_COLOR				= 0xffffff;
	public static final int MESSAGE_BOX_COLOR_ALPHA					= 0xb3;

	public static final int MESSAGE_BOX_INFOMATION_OFFSET_Y			= 242;
	// public static final int POPUP_CONFIRM_SCREEN_BACKGROUND_COLOR	= 0x2a8ea7;
	// public static final int POPUP_CONFIRM_SCREEN_TITLE_COLOR		= 0xfe7e02;
	// public static final int POPUP_CONFIRM_SCREEN_BORDER_COLOR		= 0xffffff;

	// Splash screen
	public static final int SPLASH_IMAGE_OFFSET_X					= 14;
	public static final int SPLASH_IMAGE_OFFSET_Y					= 12;
	public static final int SPLASH_PRESS5_OFFSET_Y					= 245;

	// Menu
	public static final int MENU_BACKGROUND_COLOR 					= 0x0f1d3d; //0xffffff;
	public static final int MENU_HOVER_BACKGROUND_COLOR 			= 0x0f1d3d; //0xfe7e02;
	public static final int MENU_ROUND_RECT_ARC_WIDTH	 			= 8;

	public static final int TITLE_SCREEN_OFFSET_Y 					= 10;

	// Gradient fill color
	public static final int BACKGROUND_GRADIENT_COLOR1 				= 0x000000; // 0x302659;
	public static final int BACKGROUND_GRADIENT_COLOR2 				= 0x1a6dbf; //0x660000; // 0x0087c7;

	// TextField color
	public static final int TEXTFIELD_BACKGROUND_COLOR				= 0x302659;
	public static final int TEXTFIELD_FOCUS_BACKGROUND_COLOR		= 0xfe7e02;
	public static final int TEXTFIELD_BORDER_COLOR					= 0xffffff;

	// Popup input box screen
	public static final int POPUP_INPUT_BOX_OFFSET_X				= MESSAGE_BOX_OFFSET_X;
	public static final int POPUP_INPUT_BOX_OFFSET_Y				= 245;
	public static final int POPUP_INPUT_BOX_BACKGROUND_COLOR		= MESSAGE_BOX_BACKGROUND_COLOR;
	public static final int POPUP_INPUT_BOX_TITLE_COLOR				= MESSAGE_BOX_BACKGROUND_COLOR;

	// Login screen
	public static final int LOGIN_SCREEN_OFFSET_X					= 5;
	public static final int LOGIN_SCREEN_LABEL_OFFSET_Y				= 40;
	public static final int LOGIN_SCREEN_TEXTFIELD_OFFSET_Y			= 60;

	// About screen
	public static final int ABOUT_TITLE_TEXT_OFFSET_Y				= 20;
	public static final int ABOUT_TEXT_OFFSET_Y						= 50;
	public static final int ABOUT_SCROLL_SPEED						= 4;

#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
	// Transparent background while popup screen effect
	public static final int TRANSPARENT_IMAGE_WIDTH					= 48;
	public static final int TRANSPARENT_IMAGE_HEIGHT				= 48;
	public static final int TRANSPARENT_IMAGE_COLOR					= 0xffffff;
	public static final int TRANSPARENT_IMAGE_ALPHA					= 0x7f;
#endif

	// Pop up menu
	public static final int POPUP_MENU_OFFSET_Y						= 20;	// Soft key bar - 2

	// Game play
	public static final int[][] PLAYERS_POS = {{120, 230}, {228, 110}, {120, 17}, {13, 110}};
	public static final int CARD_SPACE								= 12;	// Space for every cards
}
