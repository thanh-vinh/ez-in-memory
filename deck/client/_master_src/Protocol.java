
import java.util.Vector;

public class Protocol implements IProtocol
{
	//private static final Logger logger = Logger.getLogger(Protocol.class.getName());

	private MessageBuffer input;
	private MessageBuffer output;

	/* public Protocol()
	{

	} */

	public static byte[] getLoginRequestMessage(String user, String password)
	{
		String s = user + "|" + password;	//Command, version, and user|password
		int capacity = BYTE_SIZE + BYTE_SIZE + MessageBuffer.getSize(s);

		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(LOGIN_REQUEST);
		message.putByte(VERSION);
		message.putString(s);

		return message.getBuffer();
	}

	public static byte[] getRoomListRequestMessage()
	{
		MessageBuffer message = new MessageBuffer(BYTE_SIZE);
		message.putByte(ROOM_LIST_REQUEST);

		return message.getBuffer();
	}

	public static byte[] getTableListRequestMessage(String roomName)
	{
		int capacity = BYTE_SIZE + MessageBuffer.getSize(roomName);

		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(TABLE_LIST_REQUEST);
		message.putString(roomName);

		return message.getBuffer();
	}

	public static byte[] getTableJoinRequestMessage(short roomIndex, short tableIndex)
	{
		int capacity = BYTE_SIZE + SHORT_SIZE * 2;

		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(TABLE_JOIN_REQUEST);
		message.putShort(roomIndex);
		message.putShort(tableIndex);

		return message.getBuffer();
	}
	
	public static byte[] getTableLeaveRequestMessage(short roomIndex, short tableIndex)
	{
		int capacity = BYTE_SIZE + SHORT_SIZE * 2;

		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(TABLE_LEAVE_REQUEST);
		message.putShort(roomIndex);
		message.putShort(tableIndex);

		return message.getBuffer();
	}

	/**
	 * Set the received message.
	 */
	public void setMessage(byte[] message)
	{
		//logger.log(Level.INFO, "Set message {0} ", message);

		this.input = new MessageBuffer(message);
	}

	/**
	 * Get the response message.
	 */
	public byte[] getMessage()
	{
		// Handle the received message
		//this.handleApplicationMessage();
		// Return ByteBuffer from MessageBuffer
		byte[] message = this.output.getBuffer();
		return message;
	}

	public void handleSessionMessage()
	{
		String name;
		short length;
		short index;
		byte status;

		try {
			// Get the client's command
			byte command = this.input.getByte();
			//logger.log(Level.INFO, "Handle command: {0}", command);
			DBG("Protocol handle command: " + command);

			switch (command) {
				case LOGIN_REQUEST:
					break;

				case ROOM_LIST:
					DBG("Parse room list...");
					length = this.input.getShort();
					GameClient.initializeRoomList(length);
					for (short i = 0; i < length; i++) {
						status = this.input.getByte();
						name = this.input.getString();

						GameClient.putRoom(i, status, name);
					}
					break;

				case TABLE_LIST:
					index = this.input.getShort();
					length = this.input.getShort();

					GameClient.setCurrentRoomIndex(index);
					GameClient.initializeTableList(length);

					for (short i = 0; i < length; i++) {
						status = this.input.getByte();
						name = this.input.getString();

						GameClient.putTable(i, status, name);
					}
					break;
				//hien rem: chua hieu, tu tu merge vo, commit da
				// case TABLE_JOIN:
					// int newUserId = this.input.getInt();
					// String newUserName = this.input.getString();

					// DBG("New user has joined...");
					// DBG("ID: " + newUserId);
					// DBG("Name: " + newUserName);
					// break;
				case TABLE_JOIN:
					// DBG("table join "+input);
					// for(int i=0; i< 4; i++){
						// InfoPlaying.arrayNamePlayer[i] = this.input.getString();
						// System.out.println(InfoPlaying.arrayNamePlayer[i]);
					// }
					break;

				case TABLE_LEAVE:
					int userId = this.input.getInt();

					DBG("A user has leave...");
					DBG("ID: " + userId);
					break;

				default:
					throw new Exception("Unknown session opcode: " + command);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void handleChannelMessage()
	{
		String name;
		short length;
		short index;
		byte status;

		try {
			// Get the client's command
			byte command = this.input.getByte();
			//logger.log(Level.INFO, "Handle command: {0}", command);
			DBG("Protocol handle command: " + command);

			switch (command) {
				case LOGIN_REQUEST:
					break;

				
				//hien rem: chua hieu, tu tu merge vo, commit da
				// case TABLE_JOIN:
					// int newUserId = this.input.getInt();
					// String newUserName = this.input.getString();

					// DBG("New user has joined...");
					// DBG("ID: " + newUserId);
					// DBG("Name: " + newUserName);
					// break;
				case TABLE_JOIN:
					DBG("table join "+input);
					for(int i=0; i< 4; i++){
						InfoPlaying.arrayNamePlayer[i] = this.input.getString();
						System.out.println(InfoPlaying.arrayNamePlayer[i]);
					}
					break;

				case TABLE_LEAVE:
					short positionLeave = this.input.getShort();
					InfoPlaying.arrayNamePlayer[positionLeave]="None";
					DBG("A user has leave..."+positionLeave);
					
					break;
					
				case CARD_LIST:
					int length_cardlist = this.input.getByte();					
					DBG("CARD_LIST length_cardlist = " + length_cardlist);
					Vector cards = new Vector();
					for(int i = 0; i < length_cardlist; i++){
						int value  = this.input.getShort();
						ICard cardReceive = new Card(value);
						cards.addElement(cardReceive);
					}
					Game.gameBoard.mainPlayer =  new Player(cards);
					Game.gameBoard.mainPlayer.sortByRank();

				default:
					throw new Exception("Unknown session opcode: " + command);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] getStartGameMessage()
	{
		int capacity = BYTE_SIZE;
		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(START_GAME_REQUEST);
		return message.getBuffer();
	}
}
