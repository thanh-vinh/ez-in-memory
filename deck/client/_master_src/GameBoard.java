
import java.util.Vector;
import javax.microedition.lcdui.Graphics;

/**
 * Last updated: March 03, 2011
 */
public class GameBoard 
{

	private int selectedCardId, lastSelectedCardId;
	public boolean wasSelectedCard;
#ifndef USE_DEAL_CARD_FROM_SERVER
	private Player players[], mainPlayer;
#else
	private Player players[];
	public static Player mainPlayer;
#endif
	private String[] currentMessage;	// Store current chat message for all users
	private long[] messageTimesCounter;	// Frames counter for each user, use for display chat message,...
	public static boolean updateCanMoveCardAction=true; //variable to optimize update can move card action
    public static boolean canMoveCardAction=true;

#ifndef USE_DEAL_CARD_FROM_SERVER
	public GameBoard(Player[] players) 
	{
		this.players = players;
		this.mainPlayer = this.players[0];
		this.currentMessage = new String[4];

		//Sort cards of Main Player
		this.mainPlayer.sortBySequencer();
	#ifdef _DEBUG
		DBG("[GameBoard] All cards of main player...");
		this.mainPlayer.print();
	#endif
	}
#else
	public GameBoard() 
	{
	}
#endif

	/**
	 * Check for can move current selected cards.
	 */
	public boolean canMoveCardAction(CardList lastCardListMoved, boolean isAllowPlay) 
	{

               // #ifndef FOR_TEST_RULE
               // CardList lastCardListMoved =new CardList();
               // #endif
		// TODO Check for game logic here...
            if(!updateCanMoveCardAction)
                return canMoveCardAction;

            if(this.mainPlayer == null)
                return canMoveCardAction;
				
				
            CardList listCardSelected = this.mainPlayer.getAllSelected();
            int cardListType = listCardSelected.getTypeOfCardList();
			if ((Game.s_playerTurnTimeCount == -1) && (InfoPlaying.isAllowPlay) && InfoPlaying.isCompleteCircle){
				canMoveCardAction = true;
			}
			else
			{
				if(cardListType > 0)
				{
					if(lastCardListMoved.cards.size() == 0)
					{
						#ifdef THREE_SUIT_SPADE_GO_FIRST
						if(listCardSelected.getCardAt(0).getRank() == Card.RANK_3 && listCardSelected.getCardAt(0).getSuit() == Card.SUIT_SPADE)
						#endif
						if(isAllowPlay)
							canMoveCardAction = true;
						else
							canMoveCardAction = false;
					}
					else
					{
						int lastCardListType=lastCardListMoved.getTypeOfCardList();

						if(cardListType != CardList.CARDLIST_TYPE_FOUR_CARDS
						  && cardListType != CardList.CARDLIST_TYPE_THREE_CONSECUTIVE_PAIRS
						  && cardListType != CardList.CARDLIST_TYPE_FOUR_CONSECUTIVE_PAIRS
						  && cardListType != lastCardListType)
							canMoveCardAction = false;
						else
						{
							if(cardListType != CardList.CARDLIST_TYPE_FOUR_CONSECUTIVE_PAIRS && !isAllowPlay)
								canMoveCardAction = false;
							else
							{
								switch(cardListType)
								{
									case CardList.CARDLIST_TYPE_SINGLE_CARD:
									case CardList.CARDLIST_TYPE_PAIR:
									case CardList.CARDLIST_TYPE_THREE_CARDS:
										if(listCardSelected.getCardAt(listCardSelected.getCardsCount() - 1).compareTo(lastCardListMoved.getCardAt(lastCardListMoved.getCardsCount() - 1)) == ICard.GREATER)
											canMoveCardAction = true;
										else
											canMoveCardAction = false;
										break;
									case CardList.CARDLIST_TYPE_SEQUENCER:
										if(listCardSelected.getCardsCount() == lastCardListMoved.getCardsCount()
										  && listCardSelected.getCardAt(listCardSelected.getCardsCount() - 1).compareTo(lastCardListMoved.getCardAt(lastCardListMoved.getCardsCount() - 1)) == ICard.GREATER)
											canMoveCardAction = true;
										else
											canMoveCardAction = false;
										break;
									case CardList.CARDLIST_TYPE_THREE_CONSECUTIVE_PAIRS:
										switch(lastCardListType)
										{
											case CardList.CARDLIST_TYPE_THREE_CONSECUTIVE_PAIRS:
												if(listCardSelected.getCardAt(listCardSelected.getCardsCount() - 1).compareTo(lastCardListMoved.getCardAt(lastCardListMoved.getCardsCount() - 1)) == ICard.GREATER)
													canMoveCardAction = true;
												else
													canMoveCardAction = false;
												break;
											case CardList.CARDLIST_TYPE_SINGLE_CARD:
												if(lastCardListMoved.getCardAt(0).getRank() == ICard.RANK_DEUCE)
													canMoveCardAction = true;
												else
													canMoveCardAction = false;
												break;
											default:
												canMoveCardAction = false;
												break;
										}
										break;
									case CardList.CARDLIST_TYPE_FOUR_CARDS:
										switch(lastCardListType)
										{
											case CardList.CARDLIST_TYPE_THREE_CONSECUTIVE_PAIRS:
												canMoveCardAction = true;
												break;
											case CardList.CARDLIST_TYPE_FOUR_CARDS:
												if(listCardSelected.getCardAt(listCardSelected.getCardsCount() - 1).compareTo(lastCardListMoved.getCardAt(lastCardListMoved.getCardsCount() - 1)) == ICard.GREATER)
													canMoveCardAction = true;
												else
													canMoveCardAction = false;
												break;
											case CardList.CARDLIST_TYPE_SINGLE_CARD:
											case CardList.CARDLIST_TYPE_PAIR:
												if(lastCardListMoved.getCardAt(0).getRank() == ICard.RANK_DEUCE)
													canMoveCardAction = true;
												else
													canMoveCardAction = false;
												break;
											default:
												canMoveCardAction = false;
												break;
										}
										break;

									case CardList.CARDLIST_TYPE_FOUR_CONSECUTIVE_PAIRS:
										switch(lastCardListType)
										{
											case CardList.CARDLIST_TYPE_THREE_CONSECUTIVE_PAIRS:
											case CardList.CARDLIST_TYPE_FOUR_CARDS:
												canMoveCardAction = true;
												break;
											case CardList.CARDLIST_TYPE_FOUR_CONSECUTIVE_PAIRS:
												if(listCardSelected.getCardAt(listCardSelected.getCardsCount() - 1).compareTo(lastCardListMoved.getCardAt(lastCardListMoved.getCardsCount() - 1)) == ICard.GREATER)
													canMoveCardAction = true;
												else
													canMoveCardAction = false;
												break;
											case CardList.CARDLIST_TYPE_PAIR:
											case CardList.CARDLIST_TYPE_SINGLE_CARD:
												if(lastCardListMoved.getCardAt(0).getRank() == ICard.RANK_DEUCE)
													canMoveCardAction = true;
												else
													canMoveCardAction = false;
												break;
											default:
												canMoveCardAction = false;
												break;
										}
										break;
								}
							}
						}
					}
				}
				else
					canMoveCardAction = false;
			}
			
            updateCanMoveCardAction = false;
            return canMoveCardAction;
	}

	public void setChatMessage(String message, int user) 
	{

		this.currentMessage[user] = message;
	}

	public int getSelectedCardId() 
	{

		return this.selectedCardId;
	}

	/**
	 * Update the the current selected cell in GameBoard.
	 */
	public void updateGameBoard(int keyCode) 
	{

		// Backup current position
		this.lastSelectedCardId = this.selectedCardId;

		// Update new position
		if (KEY_LEFT) {
			this.selectedCardId--;
			Game.soundPlayer.play(IData.SFX_SELECT_CARD_ID);
		} else if (KEY_RIGHT) {
			this.selectedCardId++;
			Game.soundPlayer.play(IData.SFX_SELECT_CARD_ID);
		}

		if (this.selectedCardId > this.mainPlayer.getCardsCount() - 1) {
			this.selectedCardId = 0;
		} else if (this.selectedCardId < 0) {
			this.selectedCardId = this.mainPlayer.getCardsCount() - 1;
		}

		if (KEY_UP) {
			if (!this.mainPlayer.wasSelected(this.selectedCardId)){
				this.wasSelectedCard = true;
				this.mainPlayer.select(this.selectedCardId);
				updateCanMoveCardAction = true;
				Game.soundPlayer.play(IData.SFX_SELECT_CARD_ID);
				DBG("[GameBoard] MC selected card id = " + this.selectedCardId) ;
			}
		} else if (KEY_DOWN) {
			if (this.mainPlayer.wasSelected(this.selectedCardId)) {
				this.wasSelectedCard = false;
				this.mainPlayer.deselect(this.selectedCardId);
				updateCanMoveCardAction = true;
				Game.soundPlayer.play(IData.SFX_SELECT_CARD_ID);
				DBG("[GameBoard] MC deselected card id = " + this.selectedCardId) ;
			}
		} else if (KEY_FIRE) {
			CardList cards = this.mainPlayer.getSelectedCards();

			DBG("[GameBoard] MC moves cards...");

			if (cards.getCardsCount() == 0) {
				DBG("[GameBoard] You must select a card!");
			}
		#ifdef USE_CHECK_CARDS_FROM_SERVER
			else{

				if (canMoveCardAction
						(
							Game.lastCardListMoved, InfoPlaying.isAllowPlay
					// #ifdef FIX_BUG_ALLOW_NEXT_PLAYER_IN_A_CIRCEL
							&& InfoPlaying.allowModify_isAllowPlay
					// #endif
                        )) {
                                updateCanMoveCardAction = true;
					String carListClientToServer = NetworkConnection.changeCardListToString(cards.cards);
					Game.connection.getResultCompareCard(carListClientToServer);

					Game.soundPlayer.play(IData.SFX_MOVE_CARD_ID);
				}
				else
				{
					System.out.println(">>>>>>>>>>>>>Khong danh dc<<<<<<<<<<<<<<<");
				}
			}
		#endif
			cards.print();
		}

		// Sort card - check for can do sort (can replace by check soft key id) !!!
		if (RIGHT_SOFT_KEY) {
			// DBG("[GameBoard] MC sort cards...");
			// this.mainPlayer.deselectAll();
			// this.mainPlayer.sort();
			if(InfoPlaying.isAllowPlay && (!InfoPlaying.isCompleteCircle)){
				DBG(">>>>>>>>>>>>>>>>>>>>>>Bo luot ");
				Game.connection.sendAllowNextPlayer();
				DBG(">>>>>>>>>>>>>>>>>>>>>>Bo luot end");
			}
			else{
				DBG(">>>>>>>>>>>>>>>>>>>>>>Khong duoc phep Bo luot");
			}
		}
	}
}
