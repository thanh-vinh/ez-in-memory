import java.util.Vector;

public class middleFunctions {

	public String changeCardListToString(Vector cards) {
		DBG("changeCardListToString begin");
		byte valueCard[] = new byte[cards.size()];
		String chuoiCardList = "";
		for (int i = 0; i < cards.size(); i++) {
			ICard card1 = (ICard) cards.elementAt(i);
			valueCard[i] = (byte)((card1.getRank())*4 + card1.getSuit());
			chuoiCardList += (char)(valueCard[i]);
			DBG("" + valueCard[i]);
		}
		DBG(chuoiCardList);
		DBG("public String changeCardListToValue() end");
		return chuoiCardList;
	}

	public byte[] changeStringToListCard(String resultFromServer) {
		byte[] result = new byte[resultFromServer.length()];
		for(int i = 0; i < resultFromServer.length(); i++) {
			result[i] = (byte)(resultFromServer.charAt(i));
		}
		return result;
	}
}