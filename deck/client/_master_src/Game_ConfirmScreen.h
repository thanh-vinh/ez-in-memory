
	///*********************************************************************
	///* Game_ConfirmScreen.h
	///*********************************************************************

	//
	// Values for confirmScreenId variables
	//
	private final byte SOUND_CONFIRM_SCREEN 				= 0;
	private final byte EXIT_GAME_CONFIRM_SCREEN 			= 1;
	private final byte EXIT_TO_MAIN_MENU_CONFIRM_SCREEN 	= 2;

	public void updateConfirmScreen(byte confirmScreenId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				if (confirmScreenId == SOUND_CONFIRM_SCREEN) {
					loadCommonItemSprite();
				}
				int title = TEXT.SOUND_TITLE;
				int question = TEXT.SOUND_CONFIRM;
				int button1 = TEXT.COMMON_YES;
				int button2 = TEXT.COMMON_NO;

				if (confirmScreenId == SOUND_CONFIRM_SCREEN) {						// SOUND_CONFIRM_SCREEN
					setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE);
				} else if (confirmScreenId == EXIT_GAME_CONFIRM_SCREEN) {			// EXIT_GAME_CONFIRM_SCREEN
					question = TEXT.EXIT_GAME_CONFIRM;
					setSoftKeys(SOFTKEY_SELECT, SOFTKEY_BACK);
				} else if (confirmScreenId == EXIT_TO_MAIN_MENU_CONFIRM_SCREEN) {	// EXIT_TO_MAIN_MENU_CONFIRM_SCREEN
					question = TEXT.EXIT_TO_MAIN_MENU_CONFIRM;
					setSoftKeys(SOFTKEY_SELECT, SOFTKEY_BACK);
				}

				messageBox = new MessageBox(title, question, button1, button2, MessageBox.BUTTON_2,	MESSAGE_BOX_OFFSET_X,
						MESSAGE_BOX_SOUND_CONFIRM_OFFSET_Y, getScreenWidth() - (MESSAGE_BOX_OFFSET_X << 1));
				messageBox.setFonts(fonts[FONT_BOLD], fonts[FONT_NORMAL]);
				messageBox.setColors(MESSAGE_BOX_BACKGROUND_COLOR, MESSAGE_BOX_BUTTON_COLOR,
						MESSAGE_BOX_SELECTED_COLOR, MESSAGE_BOX_BORDER_COLOR);
				messageBox.setAlpha(MESSAGE_BOX_COLOR_ALPHA);

				setTransition(TRANSITION_NEXT);
				break;

			case MESSAGE_UPDATE:
				if (GAME_KEY_RIGHT) {
					messageBox.nextButton();
				} else if (GAME_KEY_LEFT) {
					messageBox.previousButton();
				} else if (GAME_KEY_FIRE) {
					selectedItem = messageBox.getSelectedButton();
					switch (confirmScreenId) {
						case SOUND_CONFIRM_SCREEN:
							if (selectedItem == TEXT.COMMON_YES) {
								soundPlayer.setEnableSound(true);
								soundPlayer.loadSoundPack(SOUND_PACKAGE);
							} else {
								soundPlayer.setEnableSound(false);
							}
							setGameState(STATE_SPLASH_SCREEN);
							break;

						case EXIT_GAME_CONFIRM_SCREEN:
							if (selectedItem == TEXT.COMMON_YES) {
								super.exit();
							} else {
								setGameState(lastState);
							}
							break;

						case EXIT_TO_MAIN_MENU_CONFIRM_SCREEN:
							if (selectedItem == TEXT.COMMON_YES) {
								setGameState(STATE_MAIN_MENU);
							} else {
								setGameState(lastState);
							}
							break;
					}	// switch(confirmScreenId)
				}	// else if (GAME_KEY_FIRE)
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				setFullScreenClip();
				g.drawImage(backgroundImage, 0, 0, 0);
				messageBox.paint(g);
				break;

			case MESSAGE_DESTRUCTOR:
				messageBox = null;
				break;
		}	//switch (message)
	}
