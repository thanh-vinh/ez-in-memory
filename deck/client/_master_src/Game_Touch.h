
	///*********************************************************************
	///* Game_Touch.h
	///*********************************************************************

	private static int SOFT_KEY_BUTTON_WIDTH 		= 60;
	private static int SOFT_KEY_BUTTON_HEIGHT 		= 30;
// #ifdef SHOW_TOUCH_AREA
	// private static int TOUCH_RECT_COLOR				= 0x000a68;
// #endif

	public static boolean isPointerPressedMenu() {

		return (GameLib.wasPointerPressedInRect(0, getScreenHeight() - SOFT_KEY_BUTTON_HEIGHT,
				SOFT_KEY_BUTTON_WIDTH, SOFT_KEY_BUTTON_HEIGHT));
	}

	public static boolean isPointerPressedFire() {

		return (GameLib.wasPointerPressedInRect((getScreenWidth() >> 1) - (SOFT_KEY_BUTTON_WIDTH >> 1), getScreenHeight() - SOFT_KEY_BUTTON_HEIGHT,
				SOFT_KEY_BUTTON_WIDTH, SOFT_KEY_BUTTON_HEIGHT));
	}

	public static boolean isPointerPressedBack() {

		return (GameLib.wasPointerPressedInRect(getScreenWidth() - SOFT_KEY_BUTTON_WIDTH, getScreenHeight() - SOFT_KEY_BUTTON_HEIGHT,
				SOFT_KEY_BUTTON_WIDTH, SOFT_KEY_BUTTON_HEIGHT));
	}
