
	///*********************************************************************
	///*  Game_Paint.h
	///*********************************************************************

	/**
	 * Orientation for drawGradientBox() method.
	 */
	public static final int GRADIENT_VERTICAL 	= 0;
	public static final int GRADIENT_HORIZONTAL = 1;

	/**
	 * Soft key constants.
	 */
	public static final byte SOFTKEY_NONE		= 0;
	public static final byte SOFTKEY_OK			= 1;
	public static final byte SOFTKEY_SELECT		= 2;
	public static final byte SOFTKEY_MENU		= 3;
	public static final byte SOFTKEY_BACK		= 4;
	public static final byte SOFTKEY_CANCEL		= 5;
	public static final byte SOFTKEY_CLOSE		= 6;
	public static final byte SOFTKEY_DELETE		= 7;
	public static final byte SOFTKEY_EXIT		= 8;
	public static final byte SOFTKEY_REGISTER	= 9;
	public static final byte SOFTKEY_LOGIN		= 10;
	public static final byte SOFTKEY_MOVE		= 11;
	public static final byte SOFTKEY_IGNORE		= 12;

	private static byte leftSoftKey, rightSoftKey, midSoftKey;

	private static byte lastLeftSoftKey, lastRightSoftKey, lastMidSoftKey;

	public static String getSoftKeyLabel(byte softKeyId)
	{

		switch (softKeyId) {
		case SOFTKEY_OK:
			return Package.getText(TEXT.COMMON_OK);
		case SOFTKEY_SELECT:
			return Package.getText(TEXT.COMMON_SELECT);
		case SOFTKEY_MENU:
			return Package.getText(TEXT.COMMON_MENU);
		case SOFTKEY_BACK:
			return Package.getText(TEXT.COMMON_BACK);
		case SOFTKEY_CANCEL:
			return Package.getText(TEXT.COMMON_CANCEL);
		case SOFTKEY_CLOSE:
			return Package.getText(TEXT.COMMON_CLOSE);
		case SOFTKEY_DELETE:
			return Package.getText(TEXT.COMMON_DELETE);
		case SOFTKEY_EXIT:
			return Package.getText(TEXT.COMMON_EXIT);
		case SOFTKEY_REGISTER:
			return Package.getText(TEXT.COMMON_REGISTER);
		case SOFTKEY_LOGIN:
			return Package.getText(TEXT.COMMON_LOGIN);
		case SOFTKEY_MOVE:
			return Package.getText(TEXT.COMMON_MOVE);
		case SOFTKEY_IGNORE:
			return Package.getText(TEXT.COMMON_IGNORE);
		default:		// SOFTKEY_NONE or unknow id
			return null;
		}
	}

#ifdef USE_SOFTKEY_IMAGE
	public static int getSoftKeyFrame(byte softKeyId)
	{

		switch (softKeyId) {
		case SOFTKEY_OK:
		case SOFTKEY_SELECT:
		case SOFTKEY_LOGIN:
		case SOFTKEY_REGISTER:
		case SOFTKEY_MOVE:
			return COMMON_ITEM_ITEM_SPRITE_FRAME_FIRE;

		case SOFTKEY_MENU:
			return COMMON_ITEM_ITEM_SPRITE_FRAME_MENU;

		case SOFTKEY_BACK:
		case SOFTKEY_CANCEL:
		// case SOFTKEY_SORT_CARD:
		case SOFTKEY_DELETE:
		case SOFTKEY_EXIT:
		case SOFTKEY_IGNORE:
			return COMMON_ITEM_ITEM_SPRITE_FRAME_BACK;

		default:		// SOFTKEY_NONE or unknow id
			return -1;
		}
	}
#endif	//USE_SOFTKEY_IMAGE

	public static void setSoftKeys(byte left, byte mid, byte right)
	{
		lastLeftSoftKey = leftSoftKey;
		lastMidSoftKey = midSoftKey;
		lastRightSoftKey = rightSoftKey;
	#ifndef USE_INVERT_SOFTKEYS
		leftSoftKey = left;
		rightSoftKey = right;
	#else	//USE_INVERT_SOFTKEY
		leftSoftKey = right;
		rightSoftKey = left;
	#endif	//USE_INVERT_SOFTKEY
		midSoftKey = mid;
	}

	public static void setSoftKeys(byte left, byte right)
	{
	#ifndef USE_INVERT_SOFTKEYS
		leftSoftKey = left;
		rightSoftKey = right;
	#else	//USE_INVERT_SOFTKEY
		leftSoftKey = right;
		rightSoftKey = left;
	#endif	//USE_INVERT_SOFTKEY
		midSoftKey = SOFTKEY_NONE;
	}

	public static void paintSoftKeys(Graphics g)
	{
		setFullScreenClip();	// Avoid setClip in game can make soft key isn't painted

		if (leftSoftKey != SOFTKEY_NONE || midSoftKey != SOFTKEY_NONE || rightSoftKey != SOFTKEY_NONE) {
			if (commonItemSprite != null) {
				for (int i = 0; i < 12; i++) {
					commonItemSprite.paintFrame(0, COMMON_ITEM_ITEM_SPRITE_FRAMES_WIDTH * i,
							getScreenHeight() - COMMON_ITEM_ITEM_SPRITE_FRAMES_HEIGHT, 0, g);
				}
			}
		}

	#ifndef USE_SOFTKEY_IMAGE
		String label = null;

		// Paint left soft key
		label = getSoftKeyLabel(leftSoftKey);
		if (label != null) {
			fonts[FONT_BOLD].drawString(label, 2, getScreenHeight() - 2, Graphics.LEFT | Graphics.BOTTOM, g);
		}

		// Paint right soft key
		label = getSoftKeyLabel(rightSoftKey);
		if (label != null) {
			fonts[FONT_BOLD].drawString(label, getScreenWidth() - 2, getScreenHeight() - 2, Graphics.RIGHT | Graphics.BOTTOM, g);
		}

		// Paint mid soft key
		label = getSoftKeyLabel(midSoftKey);
		if (label != null) {
			fonts[FONT_BOLD].drawString(label, getScreenWidth() >> 1, getScreenHeight() - 2, Graphics.HCENTER | Graphics.BOTTOM, g);
		}
	#else	//#ifndef USE_SOFTKEY_IMAGE
		/* if (leftSoftKey == SOFTKEY_MENU) {
			softkeySprite.paintFrame(SOFTKEY_MENU_FRAME_ID, 2, getScreenHeight() - 2, Graphics.LEFT | Graphics.BOTTOM, g);
		} else if (leftSoftKey == SOFTKEY_OK || leftSoftKey == SOFTKEY_SELECT) {
			softkeySprite.paintFrame(SOFTKEY_SPRITE_OK_FRAME_ID, getScreenWidth() >> 1, getScreenHeight() - 2, Graphics.HCENTER | Graphics.BOTTOM, g);
		}

		if (rightSoftKey != SOFTKEY_NONE) {
			softkeySprite.paintFrame(SOFTKEY_BACK_FRAME_ID, getScreenWidth() -2, getScreenHeight() - 2, Graphics.RIGHT | Graphics.BOTTOM, g);
		} */
		int frameId = getSoftKeyFrame(leftSoftKey);
		if (frameId > -1)
			commonItemSprite.paintFrame(frameId, 2, getScreenHeight() - COMMON_ITEM_ITEM_SPRITE_FRAMES_HEIGHT, 0, g);

		frameId = getSoftKeyFrame(midSoftKey);
		if (frameId > - 1)
			commonItemSprite.paintFrame(frameId, getScreenWidth() >> 1, getScreenHeight() - COMMON_ITEM_ITEM_SPRITE_FRAMES_HEIGHT,
					Graphics.HCENTER | Graphics.TOP, g);

		frameId = getSoftKeyFrame(rightSoftKey);
		if (frameId > - 1)
			commonItemSprite.paintFrame(frameId, getScreenWidth() - 2, getScreenHeight() - COMMON_ITEM_ITEM_SPRITE_FRAMES_HEIGHT,
					Graphics.RIGHT | Graphics.TOP, g);
	#endif	//#ifndef USE_SOFTKEY_IMAGE
	}

	public void paintPress5Key(int x, int y, FontSprite font, Graphics g) {

		if (frameCounter % 10 < 4) {
			font.drawString(Package.getText(TEXT.PRESS_5_TO_CONTINUE), x, y, Graphics.HCENTER | Graphics.TOP, g);
		}
	}

	public void paintPress5Key(Graphics g) {

		paintPress5Key(getScreenWidth() >> 1, getScreenHeight() - 20, fonts[FONT_NORMAL], g);
	}

	public static int getMidColor(int color1, int color2, int prop, int max)
	{
		int red =
			(((color1 >> 16) & 0xff) * prop +
			((color2 >> 16) & 0xff) * (max - prop)) / max;

		int green =
			(((color1 >> 8) & 0xff) * prop +
			((color2 >> 8) & 0xff) * (max - prop)) / max;

		int blue =
			(((color1 >> 0) & 0xff) * prop +
			((color2 >> 0) & 0xff) * (max - prop)) / max;

		int color = red << 16 | green << 8 | blue;

		return color;
	}

	public static void drawGradientBox(int color1, int color2, int left, int top, int width, int height, int orientation, Graphics g)
	{
		int max = orientation == GRADIENT_VERTICAL ? height : width;

		for(int i = 0; i < max; i++)
		{
			int color = getMidColor(color1, color2, max * (max - 1 - i) / (max - 1), max);

			g.setColor(color);

			if(orientation == GRADIENT_VERTICAL)
				g.drawLine(left, top + i, left + width - 1, top + i);
			else
				g.drawLine(left + i, top, left + i, top + height - 1);
		}
	}

	public static void drawGradientBackground(Graphics g)
	{
		setFullScreenClip();
		drawGradientBox(BACKGROUND_GRADIENT_COLOR1, BACKGROUND_GRADIENT_COLOR2, 0, 0,
				getScreenWidth(), getScreenHeight(), GRADIENT_VERTICAL, g);
	}

#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
	public static void drawTransparentImage(Graphics g)
	{
		if (offScreenTransparentImage == null) {
			offScreenTransparentImage = createTransparentImage(TRANSPARENT_IMAGE_WIDTH,
					TRANSPARENT_IMAGE_HEIGHT, TRANSPARENT_IMAGE_COLOR, TRANSPARENT_IMAGE_ALPHA);
		}

		fillRect(offScreenTransparentImage, 0, 0, getScreenWidth(), getScreenHeight(), g);
	}
#endif
