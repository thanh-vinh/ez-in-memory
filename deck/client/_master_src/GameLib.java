
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDlet;
import java.util.Random;
import java.util.Vector;

/**
 * Last updated: May 07, 2011
 */

#ifndef USE_BLACKBERRY
public abstract class GameLib extends Canvas implements Runnable
#else
public abstract class GameLib extends BlackBerryMIDP implements Runnable
#endif
{
	//
	// Transition effects
	//
	public static final byte TRANSITION_NONE		= -1;
	public static final byte TRANSITION_NEXT		= 0;
	public static final byte TRANSITION_BACK		= 1;

	public static MIDlet midlet;
	public static Graphics graphics;

	public static Image offScreenBuffer, lastScreenBuffer;		// For double buffering
	public static Graphics bufferGraphic;						// For double buffering

	public static int screenWidth, screenHeight;				// Screen width, height

	public static boolean isPressed, isReleased, isRepeated;
	public static boolean isDisableKey;
	public static int keyCode;

	//
	// For touch screen
	//
	public static int pointerX, pointerY;
	public static boolean isPointerDragged, isPointerPressed, isPointerReleased;

	public static long frameCounter;			// Frame counter
	public static long lastTime, currentTime;	// Times
	public static Random random;				// Random function
	public static FontSprite[] fonts;			// Fonts list for drawString/drawText methods
	public static String[] pallets = {"\\0", "\\1", "\\2"};	// Support 3 pallets in drawText method

	private boolean isGamePlaying;				// Game is running or not?

	// Effects
	private byte transitionType;
	private boolean isPlayingTransitionEffect;
	private int offScreenBufferPosX;

	public abstract void gameUpdate() throws Exception;

	public GameLib(MIDlet _midlet)
	{
		super();
		super.setFullScreenMode(true);
		midlet = _midlet;
		initRandom();

		// Set screen width & screen height
		if (GameLibConfig.setScreenSize) {
			screenWidth = GameLibConfig.screenWidth;
			screenHeight = GameLibConfig.screenHeight;
		} else {
			screenWidth = super.getWidth();
			screenHeight = super.getHeight();
		}

		// Double buffering
		if (GameLibConfig.useDoubleBuffering) {
			offScreenBuffer = Image.createImage(screenWidth, screenHeight);
			bufferGraphic = offScreenBuffer.getGraphics();
		}
	}

	/**
	 * Call this method before use drawString/drawText methods.
	 */
	public static void initializeFonts(FontSprite[] value)
	{
		fonts = value;
	}

	public static int getScreenWidth()
	{
		return screenWidth;
	}

	public static int getScreenHeight()
	{
		return screenHeight;
	}

	public static void setClip(int left, int top, int width, int height)
	{
		graphics.setClip(left, top, width, height);
	}

	public static void setFullScreenClip()
	{
		graphics.setClip(0, 0, screenWidth, screenHeight);
	}

	public static void initRandom()
	{
		random = new Random(System.currentTimeMillis());
	}

	public static int getRandomNumber(int n)
	{
		return random.nextInt(n);
	}

	public static int getRandomNumber(int max, int exception)
	{
		int number = random.nextInt(max);
		while (number == exception) {
			number = random.nextInt(max);
		}

		return number;
	}

	public static int getMax(int number1, int number2)
	{
		return (number1 > number2 ? number1 : number2);
	}

	/**
	* A method for splitting a string in J2ME.
	* @param input 		The string to split.
	* @param separator 	The characters to use as delimiters.
	* @return 			An array of strings.
	*/
	public static String[] splitString(String input, String separator)
	{
		Vector nodes = new Vector();

		// Parse nodes into vector
		int index = input.indexOf(separator);
		while(index >= 0) {
			nodes.addElement(input.substring(0, index));
			input = input.substring(index + separator.length());
			index = input.indexOf(separator);
		}
		// Get the last node, ignore "" string
		if (!input.equals(""))
			nodes.addElement(input);

		// Convert the vector into an array
		String[] splitArray = new String[nodes.size()];
		for (int i = 0; i < splitArray.length; i++) {
			splitArray[i] = (String) nodes.elementAt(i);
		}

		return splitArray;
	}

	public static String replaceString(String original, String oldString, String newString)
	{
		String[] arrayString = splitString(original, oldString);
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < arrayString.length; i++) {
			if (!arrayString[i].equals("")) {
				buffer.append(arrayString[i]);
				if (i < arrayString.length - 1) {
					buffer.append(newString);
				}
			}
		}

		String result = buffer.toString();
		buffer = null;
		return result;
	}

	public static void sleep(long time)
	{
		try {
			Thread.sleep(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isPointerInRect(int x1, int y1, int x2, int y2)
	{
		if (pointerX > x1 && pointerY > y1 && pointerX < x2 && pointerY < y2) {
			DBG("[POINTER] Pointer in (" + x1 + ", " + y1 + ", " + x2 + ", " + y2 + ")");
			return true;
		} else {
			return false;
		}
	}

	public static boolean wasPointerPressedIn(int x1, int y1, int x2, int y2)
	{
		if (isPointerReleased && isPointerInRect(x1, y1, x2, y2)) {
			isPointerReleased = false;
			return true;
		} else {
			return false;
		}
	}

	public static boolean wasPointerPressedInRect(int x, int y, int width, int height)
	{
		return wasPointerPressedIn(x, y, x + width, y + height);
	}

	public static void drawString(int pallet, String text, int x, int y, int anchor, Graphics g)
	{
		fonts[pallet].drawString(text, x, y, anchor, g);
	}

	public static void drawText(int pallet, String[] lines, int x, int y, int anchor, Graphics g)
	{
		fonts[pallet].drawText(lines, x, y, anchor, g);
	}

	public static void drawText(String[] lines, int x, int y, int anchor, Graphics g)
	{
		if (lines == null) return;

		int pallet = 1;
		String currentLine;

		for (int i = 0; i < lines.length; i++) {
			// DBG("Process line " + i);
			currentLine = lines[i];
			for (int j = 0; j < pallets.length; j++) {
				if (currentLine.indexOf(pallets[j]) != -1) {
					// DBG("Set pallet: " + j + " for line " + i);
					pallet = j;
					currentLine = lines[i].substring(pallets[j].length());
					break;
				}
			}

			// DBG("Draw string for line " + i);
			fonts[pallet].drawString(currentLine, x, y, anchor, g);
			y += fonts[pallet].getHeight() + fonts[pallet].getLineSpacing();
		}
	}

	public static void drawLine(int color, int x1, int y1, int x2, int y2, Graphics g)
	{
		g.setColor(color);
		g.drawLine(x1, y1, x2, y2);
	}

	public static void drawRect(int color, int x, int y, int width, int height, int borderWidth, Graphics g)
	{
		g.setColor(color);
		for (int i = 0; i < borderWidth; i++) {
			g.drawRect(x - i, y - i, width + (i << 1), height + (i << 1));
		}
	}

	public static void drawRect(int color, int x, int y, int width, int height, Graphics g)
	{
		drawRect(color, x, y, width, height, 1, g);
	}

	public static void fillRect(int color, int x, int y, int width, int height, Graphics g)
	{
		g.setColor(color);
		g.fillRect(x, y, width, height);
	}

	public static void fillRect(int color)
	{
		setClip(0, 0, screenWidth, screenHeight);
		fillRect(color, 0, 0, screenWidth, screenHeight, graphics);
	}

	public static void fillRect(int backgroundColor, int borderColor, int x, int y,
		int width, int height, int borderWidth, Graphics g)
	{
		fillRect(backgroundColor, x, y, width, height, g);
		drawRect(borderColor, x, y, width, height, borderWidth, g);
	}

	public static void fillRect(int backgroundColor, int borderColor, int x, int y, int width, int height, Graphics g)
	{
		fillRect(backgroundColor, borderColor, x, y, width, height, 1, g);
	}

	public static void fillRect(Image image, int left, int top, int width, int height, Graphics g)
	{
		g.setClip(left, top, width, height);

		int imageWidth = image.getWidth();
		int imageHeight = image.getHeight();

		for (int row = top; row < (top + height); row += imageHeight) {
			for (int col = left; col < (left + width); col += imageWidth) {
				g.drawImage(image, col, row, 0);
			}
		}
	}

	public static void fillRoundRect(int backgroundColor, int borderColor, int x,
			int y, int width, int height, int arcWidth, Graphics g)
	{
		g.setColor(backgroundColor);
		g.fillRoundRect(x, y, width, height, arcWidth, arcWidth);
		g.setColor(borderColor);
		g.drawRoundRect(x, y, width, height, arcWidth, arcWidth);
	}

	public static Image createTransparentImage(int width, int height, int color, int alpha)
	{
		DBG("[GameLib] Create a transparent image with color: " + color + " and alpha: " + alpha);
		int pixel = (alpha << 24) | color;
		int[] rgbData = new int[width * height];
		for (int i = 0; i < rgbData.length; i++) {
			rgbData[i] = pixel;
		}

		Image image = Image.createRGBImage(rgbData, width, height, true);
		return image;
	}

	protected void setTransition(byte type)
	{
		this.transitionType = type;
		if (this.transitionType != TRANSITION_NONE) {
			this.isPlayingTransitionEffect = true;
			if (this.transitionType == TRANSITION_NEXT) {
				this.offScreenBufferPosX = screenWidth;
			} else if (this.transitionType == TRANSITION_BACK) {
				this.offScreenBufferPosX = -screenWidth;
			}
		}
	}

	// TODO Implements these functions
	protected void pause()
	{

	}

	protected void resume()
	{

	}

	protected void exit()
	{
		this.isGamePlaying = false;
	}

	/**
	 * Call this method while exit game.
	 */
	protected void freeAllObject()
	{
		graphics = null;
		bufferGraphic = null;
		offScreenBuffer = null;
	}

	public static boolean isAnyKeyPressed()
	{
		if (isPressed) {
			isPressed = false;
			return true;
		} else {
			return false;
		}
	}

	public static boolean isKeyPressed(int key)
	{
		if (key == keyCode && (isPressed || isRepeated)) {
			isPressed = false;
			return true;
		}
		return false;
	}

	public static boolean isKeyReleased(int key)
	{
		if (key == keyCode && isReleased) {
			isReleased = false;
			return true;
		}
		return false;
	}

	public static void resetKey()
	{
		isPressed = false;
		isReleased = false;
		isRepeated = false;
		keyCode = 0;
	}

	public static void resetPointer()
	{
		isPointerDragged = false;
		isPointerPressed = false;
		isPointerReleased = false;
	}

	/**
 	 * Game loop in a thread.
	 */
	// @Override
	public void run()
	{
		this.isGamePlaying = true;

		while (this.isGamePlaying) {
			super.repaint();
			if (GameLibConfig.useServiceRepaints) {
				super.serviceRepaints();
			}
			if (GameLibConfig.useSleepAfterEachFrame) {
				try {
					Thread.sleep(GameLibConfig.sleepTime);
				} catch (Exception e) {}
			}
		}

		// Exit game
		this.freeAllObject();
		System.gc();
		midlet.notifyDestroyed();
	}

	// @Override
	public void paint(Graphics g)
	{
		if (graphics == null) {
			if (!GameLibConfig.useDoubleBuffering) {			// Not use double buffering
				graphics = g;
			} else {											// Use double buffering
				graphics = bufferGraphic;
			}
		}

		if (GameLibConfig.useTransitionEffect && this.isPlayingTransitionEffect) {	// Use transition effects

			isDisableKey = true;

			if (!GameLibConfig.useDoubleBuffering) {			// Not use double buffering
				if (offScreenBuffer == null) {					// Create buffer
					offScreenBuffer = Image.createImage(screenWidth, screenHeight);
					bufferGraphic = offScreenBuffer.getGraphics();
					graphics = bufferGraphic;
				}
			}

			if (offScreenBuffer != null) {					// Use buffer to show transition effects
				if (this.transitionType == TRANSITION_NEXT) {
					if (this.offScreenBufferPosX > GameLibConfig.transitionSpeed) {
						this.offScreenBufferPosX -= GameLibConfig.transitionSpeed;
					} else {
						this.offScreenBufferPosX -= GameLibConfig.transitionSpeed >> 3;
					}
					if (this.offScreenBufferPosX < 0) {
						this.offScreenBufferPosX = 0;
						isDisableKey = false;
						this.isPlayingTransitionEffect = false;
					}
				} else if (this.transitionType == TRANSITION_BACK) {
					if (this.offScreenBufferPosX < -GameLibConfig.transitionSpeed) {
						this.offScreenBufferPosX += GameLibConfig.transitionSpeed;
					} else {
						this.offScreenBufferPosX += GameLibConfig.transitionSpeed >> 3;
					}
					if (this.offScreenBufferPosX > 0) {
						this.offScreenBufferPosX = 0;
						isDisableKey = false;
						this.isPlayingTransitionEffect = false;
					}
				}
			#ifdef USE_TWO_BUFFERS_TRANSITION
				if (lastScreenBuffer != null) {
					if (this.transitionType == TRANSITION_NEXT)
						g.drawImage(lastScreenBuffer, offScreenBufferPosX - screenWidth, 0, 0);
					else
						g.drawImage(lastScreenBuffer, offScreenBufferPosX + screenWidth, 0, 0);
				}
			#endif
				g.drawImage(offScreenBuffer, offScreenBufferPosX, 0, 0);
			}
		} else {												// Not use transition effects
			if (!GameLibConfig.useDoubleBuffering) {			// Not use double buffering
				if (offScreenBuffer != null) {					// Clear buffer when finish transition
					bufferGraphic = null;
					graphics = g;
					offScreenBuffer = null;
					lastScreenBuffer = null;
				}
			} else {											// Use double buffering
				g.drawImage(offScreenBuffer, 0, 0, 0);			// Draw buffer normal
			}
		}

		try {
			gameUpdate();
		} catch (Exception e) {
			DBG("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			DBG("[GameLib] ERROR IN paint() method");
			e.printStackTrace();
		}

		frameCounter++;
	}

	// @Override
	protected void keyPressed(int value)
	{
		if (!isDisableKey) {
			isPressed = true;
			keyCode = value;
			super.keyPressed(value);
		}
	}

	// @Override
	protected void keyReleased(int value)
	{
		if (!isDisableKey) {
			isReleased = true;
			if (isRepeated) {
				isRepeated = false;
			}
			keyCode = value;
			super.keyReleased(value);
		}
	}

	// @Override
	protected void keyRepeated(int value)
	{
		if (!isDisableKey) {
			isRepeated = true;
			keyCode = value;
			super.keyRepeated(value);
		}
	}

	// @Override
	protected void pointerDragged(int x, int y)
	{
		pointerX = x;
		pointerY = y;
		isPointerDragged = true;
		isPointerPressed = true;
	}

	// @Override
	protected void pointerPressed(int x, int y)
	{
		pointerX = x;
		pointerY = y;
		isPointerPressed = true;
	}

	// @Override
	protected void pointerReleased(int x, int y)
	{
		pointerX = x;
		pointerY = y;
		isPointerReleased = true;
		isPointerPressed = false;
		isPointerDragged = false;
	}
}
