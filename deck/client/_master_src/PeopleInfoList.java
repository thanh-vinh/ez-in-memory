
import javax.microedition.lcdui.Graphics;
/**
 * A object save the information of message from friends and other players.
 * Created: April 04, 2011
 */

public class PeopleInfoList {

	public static int PEOPLE_LIST_FRIENDS_MAX = 50;

	private final int ITEM_HEIGHT = 45;
	// define variable
	private PeopleInfo peopleInfo[];
	private int width, height, top, listLength;
	private CSprite avatarSprite;
	private FontSprite font;
	private int currentIndexSelected;

	private int backgroundSelectedColor = 0x3366FF;
	private int borderSelectedColor = 0xFFFFFF;
	private int onlineIconColor = 0xFF470A;
	private int offlineIconColor = 0xFFFFFF;
	private int titleBGColor = 0x8A00B8;

	// Constructer Function
	public PeopleInfoList (int top, int bottom, int width, CSprite avatarSprite, FontSprite font)
	{
		this.top = top;
		this.height = bottom - top;
		this.width = width;
		this.avatarSprite = avatarSprite;
		this.font = font;
		this.peopleInfo = new PeopleInfo[PEOPLE_LIST_FRIENDS_MAX];
		this.listLength = 0;
		this.currentIndexSelected = 0;
	}

	// get friend list information
	public void getPeopleInfoList()
	{
		// TODO: get Friends list and get state (Online or Offline)
		// hoang.nv hard code
		this.peopleInfo[0] = new PeopleInfo("hoang.IT", 0, true);
		this.peopleInfo[1] = new PeopleInfo("thaobenh", 0, true);
		this.peopleInfo[2] = new PeopleInfo("hienkudo", 0, false);
		this.peopleInfo[3] = new PeopleInfo("thah.vinh", 0, false);
		this.peopleInfo[4] = new PeopleInfo("SUsuCOnuOng", 0, true);
		this.listLength = 5;
	}

	public void refreshPeopleInfoList ()
	{
		for (int i=0; i<this.listLength; i++) {
			// TODO: update state of friends (Online or Offline)
			// hoang.nv hard code
			this.peopleInfo[i].setOnline(!this.peopleInfo[i].isOnline());
		}
	}

	public int getLength()
	{
		return this.listLength;
	}

	public void update (int keyCode)
	{
		if (GAME_KEY_UP)
			this.currentIndexSelected --;
		else if (GAME_KEY_DOWN)
			this.currentIndexSelected ++;

		if (this.currentIndexSelected<0)
			this.currentIndexSelected = this.listLength-1;
		else if (this.currentIndexSelected>=this.listLength)
			this.currentIndexSelected = 0;
	}

	public void paint (Graphics g)
	{
		int posY = this.top + 10;
		this.font.drawString(Package.getText(TEXT.MENU_FRIENDS) , this.width>>1, posY, Graphics.HCENTER | Graphics.VCENTER, g);
		posY += 15;
		g.setColor(this.titleBGColor);
		g.fillRect(0, posY-5, this.width, 25);
		this.font.drawString("Name" , this.width/8, posY+7, Graphics.LEFT | Graphics.VCENTER, g);
		this.font.drawString("Online" , this.width-50, posY+7, Graphics.LEFT | Graphics.VCENTER, g);
		posY += 35;
		Game.paintArrowAnimation(width>>1, posY+5, 0xFFFFFF, 8, Game.ARROW_TYPE_UP, Graphics.HCENTER | Graphics.VCENTER, g);
		// paint selected background
		g.setColor(this.borderSelectedColor);
		g.fillRect(1, posY + this.currentIndexSelected*ITEM_HEIGHT, this.width-2, ITEM_HEIGHT);
		g.setColor(this.backgroundSelectedColor);
		g.fillRect(3, posY + this.currentIndexSelected*ITEM_HEIGHT + 2, this.width-6, ITEM_HEIGHT-4);
		for (int i=0; i<this.listLength; i++)
		{
			this.avatarSprite.paintFrame(IData.AVATAR_SPRITE_FRAMES_COUNT-1, this.width/8, posY + (ITEM_HEIGHT>>1), Graphics.HCENTER | Graphics.VCENTER, g);
			this.font.drawString(this.peopleInfo[i].getName() , this.width/8 + 20, posY + (ITEM_HEIGHT>>1), Graphics.LEFT | Graphics.VCENTER, g);
			int color, w;
			if (peopleInfo[i].isOnline()) {
				color = this.onlineIconColor;
				w = 10;
			} else {
				color = this.offlineIconColor;
				w = 5;
			}
			g.setColor(color);
			// g.fillArc(this.width-70 , posY+(ITEM_HEIGHT>>1), w, w, 1, 1);
			g.fillRoundRect(this.width-30, posY+(ITEM_HEIGHT>>1), w, w, w, w);
			posY += ITEM_HEIGHT;
		}
		Game.paintArrowAnimation(width>>1, posY+5, 0xFFFFFF, 8, Game.ARROW_TYPE_DOWN, Graphics.HCENTER | Graphics.VCENTER, g);
	}
}