/**
 * A object save the information of each people that is friend of MainPlayer.
 * Created: April 03, 2011
 */
 
public class PeopleInfo {

	public static final byte PEOPLE_INFO_NAME 		= 0;
	public static final byte PEOPLE_INFO_ID			= 1;
	public static final byte PEOPLE_INFO_IS_ONLINE	= 2;
	
	// define variable
	private String name;
	private int ppID;
	private boolean isOnline;
	
	// Constructer Function
	public PeopleInfo (String name, int ID, boolean isOnline)
	{
		this.name = name;
		this.ppID = ID;
		this.isOnline = isOnline;
	}
	
	public String getName ()
	{
		return this.name;
	}
	
	public int getID ()
	{
		return this.ppID;
	}
	
	public boolean isOnline ()
	{
		return this.isOnline;
	}
	
	public void setOnline (boolean isOl)
	{
		this.isOnline = isOl;
	}
}