
import java.io.DataInputStream;
import java.io.InputStreamReader;

/**
 * For testing only.
 */
public class CommandClient
{
	private RedDwarfListener responseHandler;
	private GameClient client;
	private middleFunctions functionList;

	public CommandClient()
	{
		this.responseHandler = new GameListener();
		this.client = new GameClient(GameClient.HOST, GameClient.PORT, responseHandler);
	}

	public void joinRoom(int idRoom)
	{
		try {
			byte[] message = Protocol.getTableListRequestMessage("Room "+idRoom);
			this.client.sendToSession(message);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void joinTable(int idRoom, int idTable)
	{
		try{
		byte[] message = Protocol.getTableJoinRequestMessage((short) idRoom, (short) idTable);
			this.client.sendToSession(message);
		}
		catch(Exception e) {DBG("join table error"+e);}
	}
	public void leaveTable(int idRoom, int idTable)
	{
		try{
		byte[] message = Protocol.getTableLeaveRequestMessage((short) idRoom, (short) idTable);
			this.client.sendToChannel(message);
		}
		catch(Exception e) {DBG("leave table error"+e);}
	}
	public void handleInputMessage(String command) throws Exception
	{
		// System.out.println("CommandClient started!");
		// System.out.println("Command list:");
		// System.out.println("1. Connect to server \t2. Login \t3. Logout");
		// System.out.println("4. Get Room list \t5. Get table list \t6. Join table");
		// System.out.println("7. Join table another room, same table name");
		// System.out.println("8. Send message to channel");
		// System.out.println("0. Exit!");
		// System.out.println("Enter your commmand: ");

		//BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		//String command = "";//reader.readLine();

		System.out.println("Input command is: " + command);
		System.out.println("Iusername la: " + Game.userName);
		if (command.equals("1")) {
			this.client.connect();
			try {
				Thread.sleep(1000);
			} catch (Exception e) {}
			this.client.login(Game.userName, "password");
		} else if (command.equals("2")) {
			byte[] message = Protocol.getLoginRequestMessage(Game.userName, "password");
			this.client.sendToSession(message);
		} else if (command.equals("3")) {
			this.client.logout(true);
			this.client.disconnect();
		} else if (command.equals("4")) {
		DBG("input 4");
			byte[] message = Protocol.getRoomListRequestMessage();
			this.client.sendToSession(message);
		} else if (command.equals("5")) {
			byte[] message = Protocol.getTableListRequestMessage("Room 0");
			this.client.sendToSession(message);
		} else if (command.equals("6")) {
			byte[] message = Protocol.getTableJoinRequestMessage((short) 1, (short) 2);
			this.client.sendToSession(message);
		} else if (command.equals("7")) {
			byte[] message = Protocol.getTableJoinRequestMessage((short) 2, (short) 2);
			this.client.sendToSession(message);
		} else if (command.equals("8")) {
			byte[] message = (new String("Hello")).getBytes();
			this.client.sendToChannel(message);
		} else if (command.equals("0")) {
			this.client.logout(true);
			this.client.disconnect();
			System.exit(0);
		} else if (command.equals("" + IProtocol.START_GAME_REQUEST)) {
			byte[] message = Protocol.getStartGameMessage();
			this.client.sendToSession(message);
		}
	}

	// public static void main(String[] args)
	// {
		// CommandClient commmandClient = new CommandClient();
		// try {
			// while (true) {
				// commmandClient.handleInputMessage("1");
				// Thread.sleep(2000);
			// }
		// } catch (Exception e) {
			//TODO Auto-generated catch block
			// e.printStackTrace();
		// }

	// }
}