
import java.util.Vector;


public class Player extends CardList {

	private String userName, password;	// User name & password for this account
	private boolean isFirstMove;		// The person who move first, owner or last winner

	public Player() {

		super();
	}

	public Player(String userName, String password) {

		super();
		this.userName = userName;
		this.password = password;
	}

	public Player(Vector cards) {

		super(cards);
	}

	public void setName(String value) {

		this.userName = value;
	}

	public String getName() {

		return this.userName;
	}

	public void setPassword(String value) {

		this.password = value;
	}

	public String getPassword() {

		return this.password;
	}

	public void setFirstMove(boolean value) {

		this.isFirstMove = value;
	}

	public void go() {

	}
}
