
	///*********************************************************************
	///* Game_LoginScreen.h
	///*********************************************************************
	private static final byte LOGIN_SCREEN						= 0;
	private static final byte REGISTER_SCREEN					= 1;
	private static final byte CHECK_LOGIN_ACCOUNT_SCREEN		= 2;
	private static final byte CHECK_REGISTER_ACCOUNT_SCREEN		= 3;
	private static final byte LOGIN_MESSAGE_INFO_SCREEN			= 4;
	private static final byte REGISTER_MESSAGE_INFO_SCREEN		= 5;

	private static final int TEXTFIELD_USER_NAME				= 0;
	private static final int TEXTFIELD_PASSWORD					= 1;
	private static final int TEXTFIELD_PASSWORD_CONFIRM			= 2;
	private static final int TEXTFIELD_PHONE					= 3;

	private TextField[] textFields;
	#ifdef ADD_WAIT_PLAYER_STATE
	public static String userName, password;
	#else
	private static String userName = "", password = "";
	#endif
	private static int selectedTextFieldIndex;
	private boolean isLoginSuccess, isRegisterSuccess;
	private String 	resultLogin;

	private void createAccountTextFieldObjects(byte inputScreenId)
	{
		textFields = new TextField[TEXTFIELD_PHONE + 1];

		textFields[TEXTFIELD_USER_NAME] = new TextField(Package.getText(TEXT.LOGIN_USERNAME),
				userName, TextField.ANY, LOGIN_SCREEN_OFFSET_X, LOGIN_SCREEN_TEXTFIELD_OFFSET_Y,
				getScreenWidth() - (LOGIN_SCREEN_OFFSET_X << 1), fonts[FONT_NORMAL].getHeight() + 8);
		textFields[TEXTFIELD_USER_NAME].setFocus(true);

		textFields[TEXTFIELD_PASSWORD] = new TextField(Package.getText(TEXT.LOGIN_PASSWORD),
				password, TextField.ANY, LOGIN_SCREEN_OFFSET_X,	textFields[TEXTFIELD_USER_NAME].getBottomPositionY() + 5,
				getScreenWidth() - (LOGIN_SCREEN_OFFSET_X << 1), fonts[FONT_NORMAL].getHeight() + 8);

		textFields[TEXTFIELD_PASSWORD_CONFIRM] = new TextField(Package.getText(TEXT.CONFIRM_PASSWORD),
				"", TextField.ANY, LOGIN_SCREEN_OFFSET_X, textFields[TEXTFIELD_PASSWORD].getBottomPositionY() + 5,
				getScreenWidth() - (LOGIN_SCREEN_OFFSET_X << 1), fonts[FONT_NORMAL].getHeight() + 8);

		textFields[TEXTFIELD_PHONE] = new TextField(Package.getText(TEXT.PHONE_NUMBER), "",
				TextField.ANY, LOGIN_SCREEN_OFFSET_X, textFields[TEXTFIELD_PASSWORD_CONFIRM].getBottomPositionY() + 5,
				getScreenWidth() - (LOGIN_SCREEN_OFFSET_X << 1), fonts[FONT_NORMAL].getHeight() + 8);

		for (int i = 0; i < textFields.length; i++) {
			textFields[i].setFont(fonts[FONT_NORMAL]);
			textFields[i].setColors(TEXTFIELD_BACKGROUND_COLOR, TEXTFIELD_FOCUS_BACKGROUND_COLOR, TEXTFIELD_BORDER_COLOR);
		}
	}

	public void updateAccountInputScreen(byte screenId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				// Init TextField objects
				// if (screenId != LOGIN_SCREEN || textFields == null)
				createAccountTextFieldObjects(screenId);

				if (screenId == LOGIN_SCREEN) {
					setSoftKeys(SOFTKEY_MENU, SOFTKEY_LOGIN, SOFTKEY_DELETE);
				} else {
					setSoftKeys(SOFTKEY_MENU, SOFTKEY_REGISTER, SOFTKEY_DELETE);
				}

				if (lastState == STATE_SPLASH_SCREEN) {
					setTransition(TRANSITION_NEXT);
				} else {
					setTransition(TRANSITION_NONE);
				}
				break;	// case MESSAGE_CONSTRUCTOR

			case MESSAGE_UPDATE:
				if (KEY_UP) {
					selectedTextFieldIndex--;
				} else if (KEY_DOWN) {
					selectedTextFieldIndex++;
				} else if (KEY_FIRE) {
					userName = textFields[TEXTFIELD_USER_NAME].getString();
					userNameMainPlayer = userName;
					password = textFields[TEXTFIELD_PASSWORD].getString();

					if (screenId == LOGIN_SCREEN) {
						setGameState(STATE_LOGIN_CHECK_ACCOUNT);	// This state for checking valid account
					} else {
						setGameState(STATE_REGISTER_CHECK_ACCOUNT);
					}
				} else if (LEFT_SOFT_KEY) {
					if (screenId == LOGIN_SCREEN) {
						setGameState(STATE_LOGIN_POPUP_MENU);		// Poup menu
					} else {
						setGameState(STATE_REGISTER_POPUP_MENU);	// No popup menu
					}
				}

				// Set TextField focus
				if (screenId == LOGIN_SCREEN) {
					selectedTextFieldIndex %= 2;
				} else {
					selectedTextFieldIndex %= 4;
				}

				// Dectect & set focus base on selectedTextFieldIndex
				for (int i = 0; i < textFields.length; i++) {
					if (i == selectedTextFieldIndex) {
						textFields[i].setFocus(true);
						if (ANY_KEY_PRESSED) {
							textFields[i].updateTextField(keyCode);
						}
					} else {
						textFields[i].setFocus(false);
					}
				}
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				drawGradientBox(BACKGROUND_GRADIENT_COLOR1, BACKGROUND_GRADIENT_COLOR2, 0, 0,
						getScreenWidth(), getScreenHeight(), GRADIENT_HORIZONTAL, g);
				for (int i = 0; i < textFields.length; i++) {
					if (screenId == LOGIN_SCREEN) {
						if (i < TEXTFIELD_PASSWORD_CONFIRM)
							textFields[i].paintTextField(g);
					} else {
						textFields[i].paintTextField(g);
					}
				}

				// Draw hint message
				if (screenId == LOGIN_SCREEN) {
					fonts[FONT_NORMAL].drawString(Package.getText(TEXT.LOGIN_INVITE_REGISTER), LOGIN_SCREEN_OFFSET_X,
							textFields[TEXTFIELD_PASSWORD].getBottomPositionY() + 5, 0, g);
				} else {
					fonts[FONT_NORMAL].drawString(Package.getText(TEXT.AGREE_TERMS_OF_SERVICE), LOGIN_SCREEN_OFFSET_X,
							textFields[TEXTFIELD_PHONE].getBottomPositionY() + 5, 0, g);
				}
				break;	// case MESSAGE_PAINT

			case MESSAGE_DESTRUCTOR:
					textFields[TEXTFIELD_USER_NAME] = textFields[TEXTFIELD_PASSWORD] =
						textFields[TEXTFIELD_PASSWORD_CONFIRM] = textFields[TEXTFIELD_PHONE] = null;
				break;
		}	// switch (message)
	}

	/*
	 * After input your account information will go to this state for checking valid/invalid account.
	 */
	public void updateAccountCheckingScreen(byte screenId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				// Create a popup confirm screen
				int title, text;
				if (screenId == CHECK_LOGIN_ACCOUNT_SCREEN) {
					title = TEXT.LOGIN_TITLE;
					text = TEXT.CHECKING_ACCOUNT;
				} else {
					title = TEXT.REGISTER_TITLE;
					text = TEXT.CREATING_ACCOUNT;
				}

				messageBox = new MessageBox(title, text, MESSAGE_BOX_OFFSET_X,
						MESSAGE_BOX_INFOMATION_OFFSET_Y, getScreenWidth() - (MESSAGE_BOX_OFFSET_X << 1));
				messageBox.setFonts(fonts[FONT_BOLD], fonts[FONT_NORMAL]);
				messageBox.setColors(MESSAGE_BOX_BACKGROUND_COLOR, MESSAGE_BOX_BUTTON_COLOR,
						MESSAGE_BOX_SELECTED_COLOR, MESSAGE_BOX_BORDER_COLOR);
				messageBox.setAlpha(MESSAGE_BOX_COLOR_ALPHA);

			#ifndef _USE_LOGIN_WITH_DATABASE
				frameCounter = 0;
			#endif
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE, SOFTKEY_CANCEL);
				break;	// case MESSAGE_CONSTRUCTOR

			case MESSAGE_UPDATE:
				if (RIGHT_SOFT_KEY) {
					setGameState(STATE_LOGIN_SCREEN);
				}
				// Check for input account...
			#ifndef _USE_LOGIN_WITH_DATABASE
				if (frameCounter % 20 == 0) {	// For testing only
					if (screenId == CHECK_LOGIN_ACCOUNT_SCREEN) {
						isLoginSuccess = true;		// Always true
						#ifdef USE_REDWAF_SERVER
						DBG("username = "+userName);
						try{
						if(commmandClient == null) commmandClient = new CommandClient();
						commmandClient.handleInputMessage("1");
						// TODO Don't send message continual to avoid some errors
						// Should be call login() after init client
						// and send login information in login screen
						//commmandClient.handleInputMessage("2");
						}catch(Exception e) {DBG("loi khi ket noi server"+e);isLoginSuccess = false;}
						#endif
						nextState = STATE_LOGIN_INFO_MESSAGE;
						setGameState(STATE_LOGIN_INFO_MESSAGE);
					} else {
						isRegisterSuccess = true;	// Always true
						setGameState(STATE_REGISTER_INFO_MESSAGE);
					}

				}
			#else	// #ifndef _USE_LOGIN_WITH_DATABASE
				if (screenId == CHECK_LOGIN_ACCOUNT_SCREEN) {
					resultLogin = connection.getLoginInfo(userName + "," + password);
					if(resultLogin.equals(connection.RESULT_LOGIN_OK)) {
						isLoginSuccess = true;
					} else {
						isLoginSuccess = false;
					}
					DBG(">>>>>>>>>>>>>>>>>isLoginSuccess = " + isLoginSuccess);
					subState = STATE_LOGIN_INFO_MESSAGE;
				}
			#endif	// #ifndef _USE_LOGIN_WITH_DATABASE
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				messageBox.paint(g);
				commonItemSprite.paintAnimation(getScreenWidth() - (MESSAGE_BOX_OFFSET_X << 1) - 2,
						MESSAGE_BOX_INFOMATION_OFFSET_Y + 2, true, Graphics.TOP | Graphics.RIGHT, g);
				break;

			case MESSAGE_DESTRUCTOR:
				messageBox = null;
				break;
		}	// switch (message)
	}

	/*
	 * Message box for login/register information.
	 */
	public void updateAccountCheckingInfoScreen(byte screenId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				// Create a popup confirm screen
				int title, text;

				if (screenId == LOGIN_MESSAGE_INFO_SCREEN) {
					title = TEXT.LOGIN_TITLE;
				} else {
					title = TEXT.REGISTER_TITLE;
				}

				if (screenId == LOGIN_MESSAGE_INFO_SCREEN) {
					if (isLoginSuccess) {
						text = TEXT.LOGIN_SUCCESSFUL;
					} else {
						text = TEXT.LOGIN_FAILURE;
					}
				} else {
					if (isRegisterSuccess) {
						text = TEXT.REGISTER_SUCCESSFUL;
					} else {
						text = TEXT.REGISTER_SUCCESSFUL;
					}
				}

				messageBox = new MessageBox(title, text, MESSAGE_BOX_OFFSET_X,
						MESSAGE_BOX_INFOMATION_OFFSET_Y, getScreenWidth() - (MESSAGE_BOX_OFFSET_X << 1));
				messageBox.setFonts(fonts[FONT_BOLD], fonts[FONT_NORMAL]);
				messageBox.setColors(MESSAGE_BOX_BACKGROUND_COLOR, MESSAGE_BOX_BUTTON_COLOR,
						MESSAGE_BOX_SELECTED_COLOR, MESSAGE_BOX_BORDER_COLOR);
				messageBox.setAlpha(MESSAGE_BOX_COLOR_ALPHA);

				setSoftKeys(SOFTKEY_NONE, SOFTKEY_OK, SOFTKEY_NONE);
				break;	// case MESSAGE_CONSTRUCTOR

			case MESSAGE_UPDATE:
				if (KEY_FIRE) {
					if (isLoginSuccess) {
						// use login in this to add client to hashmap, it maybe remove in furture
						try{
						commmandClient.handleInputMessage("2");
						}
						catch(Exception e) {}
						//end
						setGameState(STATE_MAIN_MENU);
					} else if (isRegisterSuccess) {
						setGameState(STATE_LOGIN_SCREEN);
					} else {
						if (screenId == LOGIN_MESSAGE_INFO_SCREEN) {
							setGameState(STATE_LOGIN_SCREEN);
						} else {
							setGameState(STATE_REGISTER_SCREEN);
						}
					}
				}
				break;

			case MESSAGE_PAINT:
				messageBox.paint(g);
				break;

			case MESSAGE_DESTRUCTOR:
				messageBox = null;
				break;
		}	// switch (message)
	}
