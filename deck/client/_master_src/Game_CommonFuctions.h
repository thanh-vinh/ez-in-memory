
	/*********************************************************************
	 * Game_CommonFuctions.h
	 *********************************************************************/
	public static final byte ARROW_TYPE_UP 		= 0;
	public static final byte ARROW_TYPE_DOWN 	= 1;
	public static final byte ARROW_TYPE_LEFT 	= 2;
	public static final byte ARROW_TYPE_RIGHT 	= 3;
	
	private static byte arrowUpCount = 0;
	private static byte arrowDownCount = 0;
	private static byte arrowLeftCount = 0;
	private static byte arrowRightCount = 0;
	
	public static void paintArrowAnimation(int x, int y, int color, int height, byte type, int anchor, Graphics g)
	{
		g.setColor(color);
		int len = height<<1, posX = x, posY = y;
		
		if ((anchor & Graphics.HCENTER) != 0) {		// HCENTER
			posX -= height;
		} else if ((anchor & Graphics.RIGHT) != 0) {		// RIGHT
			posX -= len;
		}
		
		if ((anchor & Graphics.VCENTER) != 0) {		// VCENTER
			posY -= height;
		} else if ((anchor & Graphics.BOTTOM) != 0) {		// BOTTOM
			posY -= len;
		}
		
		switch(type)
		{
			case ARROW_TYPE_UP: 
				arrowUpCount++;arrowUpCount %= 8;
				posY -= (arrowUpCount>>1);
				for (int i=0; i<height; i++) {
					g.drawLine(posX, posY, posX + len, posY);
					posY--;
					posX++;
					len -= 2;
				}
				break;
				
			case ARROW_TYPE_DOWN: 
				arrowDownCount++; arrowDownCount %= 8;
				posY += (arrowUpCount>>1);
				for (int i=0; i<height; i++) {
					g.drawLine(posX, posY, posX + len, posY);
					posY++;
					posX++;
					len -= 2;
				}
				break;
				
			case ARROW_TYPE_LEFT: 
				
				break;
				
			case ARROW_TYPE_RIGHT: 
				
				break;
				
		}
	}