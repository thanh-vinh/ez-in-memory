
	///*********************************************************************
	///* Game_LogoScreen.h
	///*********************************************************************

	private static Image logoImage;
	private static Image splashImage;

	public void updateLogoScreen(byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				loadFontSprites();
				loadText();
				loadLogoImage();
				soundPlayer = new SoundPlayer();
				g.setColor(0xffffff);

				// init MultiChatBox variable
				if (multiChatBox == null)
					multiChatBox = new MultiChatBox(fonts[FONT_NORMAL], getScreenWidth(), getScreenHeight() - 22, 0);

				currentTime = System.currentTimeMillis();
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE, SOFTKEY_NONE);
			#ifdef DEBUG_CHATTING
				multiChatBox.addMessage("hoang", "Hi Hoang", false);
				multiChatBox.addMessage("hoang", "How are you?", false);

				multiChatBox.addTableMessage("hoang", "win roi yeye");
				multiChatBox.addTableMessage("vinh", "2222222222");
				multiChatBox.addTableMessage("trong", "11111111111 ");
				multiChatBox.addTableMessage("hoang", " hoang hoang hoang ");
				multiChatBox.addTableMessage("hien", "33333333333");
				multiChatBox.addTableMessage("Thao", "44444444444");
				multiChatBox.addTableMessage("hoang", "win roi yeye");
				multiChatBox.addTableMessage("vinh", "2222222222");
				multiChatBox.addTableMessage("trong", "11111111111 ");
				multiChatBox.addTableMessage("hoang", " hoang hoang hoang ");
				multiChatBox.addTableMessage("hien", "33333333333");
				multiChatBox.addTableMessage("Thao", "44444444444");
				multiChatBox.addTableMessage("vinh", "2222222222");
				multiChatBox.addTableMessage("vinh", "2222222222");
				multiChatBox.addTableMessage("trong", "11111111111 ");
				multiChatBox.addTableMessage("hoang", " hoang hoang hoang ");
				multiChatBox.addTableMessage("hien", "33333333333");
				multiChatBox.addTableMessage("trong", "11111111111 ");
				multiChatBox.addTableMessage("hoang", " hoang hoang hoang ");
				multiChatBox.addTableMessage("hien", "33333333333");
				multiChatBox.addTableMessage("Thao", "44444444444");
				multiChatBox.addTableMessage("vinh", "2222222222");
				multiChatBox.addTableMessage("trong", "11111111111 ");
				multiChatBox.addTableMessage("hoang", " hoang hoang hoang ");
				multiChatBox.addTableMessage("hien", "33333333333");
				multiChatBox.addTableMessage("Thao", "44444444444");

				multiChatBox.addMessage("hoang", "I am fine", true);
				multiChatBox.addMessage("hoang", "Thank you", true);

				multiChatBox.addMessage("dungdd", "Thank you", true);
				multiChatBox.addMessage("dungdd", "Thank you", true);
				multiChatBox.addMessage("dungdd", "Thank you", false);
				multiChatBox.addMessage("dungdd", "Thank you", false);

				multiChatBox.addMessage("vinhnt", "Thank you", true);
				multiChatBox.addMessage("vinhnt", "Thank you", true);
				multiChatBox.addMessage("vinhnt", "Thank you", false);
				multiChatBox.addMessage("vinhnt", "Thank you", false);

				multiChatBox.addMessage("ThaoGa", "Thank you", true);
				multiChatBox.addMessage("ThaoGa", "Thank you", true);
				multiChatBox.addMessage("ThaoGa", "Thank you", false);
				multiChatBox.addMessage("ThaoGa", "Thank you", false);
			#endif // DEBUG_CHATTING
				break;

			case MESSAGE_UPDATE:
				// Display logo screen in TIME_LOGO_DELAY milliseconds
				if ((System.currentTimeMillis() - currentTime) > TIME_LOGO_DELAY) {
					setGameState(STATE_SOUND_CONFIRM);
				}
				break;

			case MESSAGE_PAINT:
				setFullScreenClip();
				g.fillRect(0, 0, getScreenWidth(), getScreenHeight());
				g.drawImage(logoImage, LOGO_IMAGE_OFFSET_X, LOGO_IMAGE_OFFSET_Y, 0);
				break;

			case MESSAGE_DESTRUCTOR:
				logoImage = null;
				break;
		}
	}

	public void updateSplashScreen(byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				loadSplashImage();
				loadMenuPopupIconsSprite();
				g.setColor(0xffffff);
				soundPlayer.play(M_TITLE_ID, true);
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				setFullScreenClip();
				g.fillRect(0, 0, getScreenWidth(), getScreenHeight());
				g.drawImage(splashImage, SPLASH_IMAGE_OFFSET_X, SPLASH_IMAGE_OFFSET_Y, 0);
				fonts[FONT_BLACK].drawString(Package.getText(TEXT.COPYRIGHT), getScreenWidth() >> 1, getScreenHeight() - 20, Graphics.HCENTER | Graphics.TOP, g);
				paintPress5Key(getScreenWidth() >> 1, SPLASH_PRESS5_OFFSET_Y, fonts[FONT_BLACK], g);

			#ifndef USE_BLACKBERRY
				if (GAME_KEY_SELECT) {
			#else
				if (GAME_KEY_FIRE) {
			#endif
					setGameState(STATE_LOGIN_SCREEN);
				}

			#ifdef USE_TOUCH_SCREEN
				if (wasPointerPressedIn(0, 0, getScreenWidth(), getScreenHeight())) {
					setGameState(STATE_LOGIN_SCREEN);
				}
			#endif

				break;

			case MESSAGE_DESTRUCTOR:
				splashImage = null;
				break;
		}
	}
