
import javax.microedition.lcdui.Graphics;

public class MenuPopup
{
	private final int UNSELECTED_ID 		= -1;
	// define for menu popup
	// #ifdef SCROLL_MENU_POPUP
	// private final int MAX_MENU_ITEM_SHOW = 2;
	// #else
        private final int MAX_MENU_ITEM_SHOW = 5;
	// #endif
	private final int MENU_BORDER_WIDTH_HEIGHT = 2;
	private final int MENU_WIDTH = 110;
	private final int MENU_TEXT_OFF_X = 10;

	private FontSprite font;
	private byte[] itemsID;
	private int bottom, top, width, height;
	private int backgroundColor = 0x3366FF;
	private int borderColor = 0xFFFFFF;
	private int selectedItemBackgroundColor = 0xFF470A;

	private int currentItemId, selectedItemId;
#ifdef SCROLL_MENU_POPUP
	private int currentStartItem;
#endif
	private int ITEM_HEIGHT = IData.MAIN_MENU_ICONS_FRAMES_WIDTH + 25;
	private int MENU_POPUP_ITEM_HEIGHT;
	private int ITEM_WIDTH = ITEM_HEIGHT;

	public MenuPopup(byte[] itemsID, FontSprite font, int bottom, int width, int height)
	{
		this.font = font;
		this.bottom = bottom;
		this.width = width;
		this.height = height;
		this.currentItemId = 0;
	#ifdef SCROLL_MENU_POPUP
		this.currentStartItem = 0;
	#endif
		this.selectedItemId = UNSELECTED_ID;
		this.MENU_POPUP_ITEM_HEIGHT = font.getHeight() + 10;
		this.itemsID = itemsID;
		int length = this.itemsID.length;
		this.top = this.bottom - (MENU_POPUP_ITEM_HEIGHT * ((length<MAX_MENU_ITEM_SHOW)?length:MAX_MENU_ITEM_SHOW)) - 2*MENU_BORDER_WIDTH_HEIGHT;
	}

	public void setColor(int backgroundColor, int selectedItemBackgroundColor)
	{
		this.backgroundColor = backgroundColor;
		this.selectedItemBackgroundColor = selectedItemBackgroundColor;
	}

	/**
	 * If not set, use fillRect() method only.
	 * MENU_STYLE_HORIZON menu: frame 0 is normal item, frame 1 is selected item.
	 * MENU_STYLE_VERTICAL menu: normal items list, and selected items list.
	 */

	public void paintMenu(Graphics g)
	{
		g.setClip(0, this.top, this.MENU_WIDTH, this.bottom - this.top);
		int numItems = (this.itemsID.length<MAX_MENU_ITEM_SHOW)?this.itemsID.length:MAX_MENU_ITEM_SHOW;
		g.setColor(this.borderColor);
		g.fillRect(0, this.top, this.MENU_WIDTH, this.bottom - this.top + 1);
		g.setColor(this.backgroundColor);
		g.fillRect(0 + MENU_BORDER_WIDTH_HEIGHT, this.top + MENU_BORDER_WIDTH_HEIGHT, this.MENU_WIDTH - MENU_BORDER_WIDTH_HEIGHT*2, this.bottom - this.top - MENU_BORDER_WIDTH_HEIGHT*2 + 1);
		// paint current select item
		g.setColor(this.selectedItemBackgroundColor);

	#ifdef SCROLL_MENU_POPUP
		g.fillRect(MENU_BORDER_WIDTH_HEIGHT + 1, this.top + (currentItemId - currentStartItem)*MENU_POPUP_ITEM_HEIGHT + MENU_BORDER_WIDTH_HEIGHT + 1, this.MENU_WIDTH - MENU_BORDER_WIDTH_HEIGHT*3, MENU_POPUP_ITEM_HEIGHT);
		// paint all item
		for (int i = currentStartItem; i < currentStartItem + numItems; i++) {
			// Paint menu button if use menu sprite
			this.font.drawString(Package.getText(TEXT.POPUP_MENU_COMMENT + itemsID[i]), MENU_BORDER_WIDTH_HEIGHT + MENU_TEXT_OFF_X, this.top + (i - currentStartItem)*MENU_POPUP_ITEM_HEIGHT + MENU_BORDER_WIDTH_HEIGHT + MENU_POPUP_ITEM_HEIGHT/2, Graphics.LEFT | Graphics.VCENTER, g);
		}
	#else
		g.fillRect(MENU_BORDER_WIDTH_HEIGHT + 1, this.top + currentItemId*MENU_POPUP_ITEM_HEIGHT + MENU_BORDER_WIDTH_HEIGHT + 1, this.MENU_WIDTH - MENU_BORDER_WIDTH_HEIGHT*3, MENU_POPUP_ITEM_HEIGHT);
		// paint all item
		for (int i = 0; i < this.itemsID.length; i++) {
			// Paint menu button if use menu sprite
			this.font.drawString(Package.getText(TEXT.POPUP_MENU_COMMENT + itemsID[i]), MENU_BORDER_WIDTH_HEIGHT + MENU_TEXT_OFF_X, this.top + i*MENU_POPUP_ITEM_HEIGHT + MENU_BORDER_WIDTH_HEIGHT + MENU_POPUP_ITEM_HEIGHT/2, Graphics.LEFT | Graphics.VCENTER, g);
		}
	#endif
		// DBG(">>>>>>>>>>>> hoang   top: "+ this.top + "; bottom: " + this.bottom);
	}

	public void updateMenu(int keyCode)
	{
		int length = this.itemsID.length;

	#ifdef SCROLL_MENU_POPUP
		int numItems = (this.itemsID.length<MAX_MENU_ITEM_SHOW)?this.itemsID.length:MAX_MENU_ITEM_SHOW;
	#endif
		if (GAME_KEY_UP)
			this.currentItemId--;
		else if (GAME_KEY_DOWN)
			this.currentItemId++;

		if (this.currentItemId < 0)	{
			this.currentItemId = length - 1;
			#ifdef SCROLL_MENU_POPUP
			this.currentStartItem = this.currentItemId - numItems;
			#endif
		}

		if (this.currentItemId >= length) {
		this.currentItemId = 0;
			#ifdef SCROLL_MENU_POPUP
			this.currentStartItem = 0;
			#endif
		}

	#ifdef SCROLL_MENU_POPUP
		if (this.currentItemId < this.currentStartItem)
			this.currentStartItem = this.currentItemId;
		if (this.currentItemId >= this.currentStartItem + numItems)
			this.currentStartItem = this.currentItemId - numItems + 1;
	#endif
		if (GAME_KEY_SELECT)
			this.selectedItemId = this.currentItemId;
	}

	/**
	 * Get the selected menu item index.
	 * The selected index will reset to 0 after call this method.
	 */
	public int getSelectedMenuItemId()
	{
		// Reset select item index after get
		int index = -1;
		if (selectedItemId != -1) {
			index = itemsID[this.selectedItemId];
			this.selectedItemId = UNSELECTED_ID;
		}
		return index;
	}	
	
	/**
	 * Get the current menu item index.
	 * 
	 */
	public int getCurrentMenuItemId()
	{
		// Reset select item index after get
		if (this.currentItemId<0) return -1;
		return itemsID[this.currentItemId];
	}
	
}
