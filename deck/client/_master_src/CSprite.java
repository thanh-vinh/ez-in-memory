
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * A sprite have a image with frame y offset is always equal 0.
 * @author Thanh Vinh
 * Last updated: May 07, 2011
 */

// TODO		Change sprite offsets type to short
public class CSprite {

	private Image image;				// Single image
	private Image[] frameImages;		// Not use single image, cache all frames
	private int width, height;
	private int[] frameXOffsets;
	private int[] framesHeight;
	private int framesCount;			// Total frames
	private boolean useCacheImages;		// Cache images or use single image

	// The properties of current frame which is painted
	private int framePosX;	// Position to to draw at x (a part of large image), y always equal 0.
	private int frameWidth;
	private int frameHeight;

	// Animations
	private int[] animFrames;		// The frame index
	private int[] animDelays;		// Delay for each frame (fps)
	private int currentFrame;		// Current frame in a animation
	private int frameCounter;		// Number of frame in game which drawed for currenFrame
	private int animPosX, animPosY;	// Position for a animation

	public CSprite(Image image) {

		this.image = image;
		this.width = this.image.getWidth();
		this.height = this.image.getHeight();
	}

	public CSprite(Image image, int[] frameXOffsets, int[] frameYOffsets) {

		this(image);

		this.frameXOffsets = frameXOffsets;
		this.framesCount = frameXOffsets.length;
		this.framesHeight = frameYOffsets;
	}

	public CSprite(Image image, int[] frameXOffsets) {

		this(image);

		this.frameXOffsets = frameXOffsets;
		// Auto set frame height as same as image height (example: font sprite)
		this.framesCount = frameXOffsets.length;
		this.setFramesHeight(this.height);
	}

	public void buildCache() {

		this.frameImages = new Image[this.framesCount];	// Initialize first

		for (int i = 0; i < this.framesCount; i++) {
			int width = this.getFrameWidth(i);
			int height = this.getFrameHeight(i);
			int[] rgbData = new int[width * height];

			this.image.getRGB(rgbData, 0, width, this.frameXOffsets[i], 0, width, height);
			this.frameImages[i] = Image.createRGBImage(rgbData, width, height, true);
		}

		this.image = null; 	// Clear single image
		this.useCacheImages = true;
	}

	public void setFrameXOffsets(int[] offsets) {

		this.frameXOffsets = offsets;
		this.framesCount = this.frameXOffsets.length;
	}

	public void setFramesWidth(int width, int framesCount) {

		this.framesCount = framesCount;

		this.frameXOffsets = new int[framesCount];
		for (int i = 0; i < framesCount; i++) {
			this.frameXOffsets[i] = width * i;
		}
	}

	/**
	 * For the sprite have all frame which same size.
	 */
	public void setFramesHeight(int[] heights) {

		this.framesHeight = heights;
	}

	/**
	 * Set all height width in the sprite (same as height).
	 * Call setFrameXOffsets() method before call this method.
	 */
	public void setFramesHeight(int height) {

		this.framesHeight = new int[this.framesCount];
		for (int i = 0; i < this.framesCount; i++)
			this.framesHeight[i] = height;
	}

	/**
	 * For the sprite have all frame which same size.
	 */
	public void setFrameSize(int width, int height, int framesCount) {

		this.frameXOffsets = new int[framesCount];
		this.framesHeight = new int[framesCount];
		this.framesCount = framesCount;

		for (int i = 0; i < framesCount; i++) {
			this.frameXOffsets[i] = i * width;
			this.framesHeight[i] = height;
		}
	}

	public void setAnimation(int[] animFrames, int[] animDelays) {

			this.animFrames = animFrames;
			this.animDelays = animDelays;

			this.currentFrame = 0; 	// Reset currenFrame
			this.frameCounter = 0;	// Reset frameCounter
	}

	public void setAnimationPos(int x, int y) {

		this.animPosX = x;
		this.animPosY = y;
	}

	public int getWidth() {

		return this.width;
	}

	public int getHeight() {

		return this.height;
	}

	public int getFrameXOffset(int frameIndex) {

		return this.frameXOffsets[frameIndex];
	}

	public int getFrameWidth(int frameIndex) {

		if (frameIndex < this.framesCount - 1)
			return (this.frameXOffsets[frameIndex + 1] - this.frameXOffsets[frameIndex]);
		else
			return (this.getWidth() - this.frameXOffsets[frameIndex]);
	}

	public int getFrameHeight(int frameIndex) {

		return this.framesHeight[frameIndex];
	}

	public Image getImage(int frameIndex) {

		Image image = null;

		if (this.useCacheImages) {
			image = this.frameImages[frameIndex];
		} else {
			int width = this.getFrameWidth(frameIndex);
			int height = this.getFrameHeight(frameIndex);
			int[] rgbData = new int[width * height];

			this.image.getRGB(rgbData, 0, width, this.frameXOffsets[frameIndex], 0, width, height);
			image = Image.createRGBImage(rgbData, width, height, true);
		}

		return image;
	}

	public void paintFrame(int frameIndex, int x, int y, int anchor, int transform, Graphics g) {

		//
		// TODO Implement for all anchors
		//
		/*if ((anchor & Graphics.LEFT) != 0) {				// LEFT
			// Ignore, default is TOP | LEFT
		} else */
		if ((anchor & Graphics.HCENTER) != 0) {				// HCENTER
			x -= this.getFrameWidth(frameIndex) >> 1;
		} else if ((anchor & Graphics.RIGHT) != 0) {		// RIGHT
			x -= this.getFrameWidth(frameIndex);
		}

		/*if ((anchor & Graphics.TOP) != 0) {				// TOP
			// Ignore, default is TOP | LEFT
		} else */
		if ((anchor & Graphics.VCENTER) != 0) {				// VCENTER
			y -= this.getFrameHeight(frameIndex) >> 1;
		} else if ((anchor & Graphics.BOTTOM) != 0) {
			y -= this.getFrameHeight(frameIndex);
		}

		if (this.useCacheImages) {										// Cache images
			if (transform == 0) {
				g.drawImage(this.frameImages[frameIndex], x, y, 0);
			} else {
				g.drawRegion(this.frameImages[frameIndex], 0, 0, this.frameImages[frameIndex].getWidth(),
					this.frameImages[frameIndex].getHeight(), transform, x, y, 0);
			}
		} else {														// Use single image
			this.framePosX = this.frameXOffsets[frameIndex];
			this.frameWidth = this.getFrameWidth(frameIndex);
			this.frameHeight = this.framesHeight[frameIndex];

			g.drawRegion(this.image, this.framePosX, 0, this.frameWidth, this.frameHeight, transform, x, y, 0);
		}
	}

	public void paintFrame(int frameIndex, int x, int y, int anchor, Graphics g) {

		this.paintFrame(frameIndex, x, y, anchor, javax.microedition.lcdui.game.Sprite.TRANS_NONE, g);
	}

	public void paintFrame(int frameIndex, int x, int y, Graphics g) {

		this.paintFrame(frameIndex, x, y, 0, g);
	}

	public void paintAnimation(int x, int y, boolean loop, int anchor, Graphics g) {

		#ifdef _DEBUG
		try {
		#endif
		if (this.currentFrame < this.animFrames.length) {
			this.paintFrame(this.animFrames[currentFrame], x, y, anchor, g);

			this.frameCounter++;	// Increase frame counter (fps)
			// Check for delay this frame in the animation: 1, 2 or more frame
			// to draw to next frame in the animation
			if (this.frameCounter == this.animDelays[currentFrame]) {
				this.currentFrame++;
				this.frameCounter = 0;
			}

			// Loop the animation - paint from the first frame in the animation
			if (loop) {
				if (this.currentFrame >= this.animFrames.length) {
					this.currentFrame = 0;
				}
			}
		}
		#ifdef _DEBUG
		} catch (Exception e) {
			e.printStackTrace();
		}
		#endif
	}

	public void paintAnimation(boolean loop, int anchor, Graphics g) {

		this.paintAnimation(this.animPosX, this.animPosY, loop, anchor, g);
	}

	public boolean isAnimationStop() {

		if (this.currentFrame != this.animFrames.length)
			return false;
		else
			return true;
	}

	/**
	 * Destructor for CSprite.
	 * I sometimes, set CSprite = null but system still keeps image.
	 * Call this method for release them.
	 */
	protected void finalize() {	//throws Exception {

		// super.finalize();
		if (this.image != null) {
			this.image = null;
		}
		if (this.frameImages != null) {
			for (int i = 0; i < this.frameImages.length; i++) {
				if (this.frameImages[i] != null) {
					this.frameImages[i] = null;
				}
			}
		}
	}
}