
import javax.microedition.lcdui.Graphics;

/**
 * A object save the information of message from friends and other players.
 * Created: March 05, 2011
 */
 
public class MessageList {

	// define for box chat 
	public static final int MESSAGE_LIST_MAX_SENTENCE = 50;
	// public static final int MESSAGE_MAX_CHARACTER = 20;
	// private final int PLAYER_ID = 0;
	// private final int FRIEND_ID = 1;
	
	// define variable
	private Message message[];
	private String messageTitle;
	private String playerName;
	private int firstMessage_index = 0;
	private int lastMessage_index = -1;
	private int messagesLenght = 0;
	
	// Constructer Function
	public MessageList (String messageTitle) {
		
		this.message = new Message[MESSAGE_LIST_MAX_SENTENCE];
		this.messageTitle = new String(messageTitle);
		this.playerName = "You";// Package.getText(TEXT.MESSAGE_TEXT_YOU);
		this.firstMessage_index = 0;
		this.messagesLenght = 0;
	}
	
	public String getMessageTitle ()
	{
		return this.messageTitle;
	}
	
	public boolean isEqualMessageTitle (String name)
	{
		return this.messageTitle.equals(name);
	}
	
	public String getFullMessage_AtIndex (int index)
	{
		int i = (this.firstMessage_index + index) % MESSAGE_LIST_MAX_SENTENCE;
		// String ppName = (message[i].getPeopleID() == PLAYER_ID)?(this.playerName):(this.messageTitle);
		return message[i].getPeopleName() + ": " + message[i].getMessage();
	}
	
	public void addMessage (String name, String messageText)
	{
		this.messagesLenght ++;
		int index = 0;
		if (this.messagesLenght<=MESSAGE_LIST_MAX_SENTENCE) {
			index = this.messagesLenght - 1;
		} else {
			index = this.firstMessage_index;
			this.firstMessage_index ++;
			this.firstMessage_index %= MESSAGE_LIST_MAX_SENTENCE;
			messagesLenght = MESSAGE_LIST_MAX_SENTENCE;
		}
		
		if (this.message[index] != null)
			this.message[index] = null;
		this.message[index] = new Message(name, messageText);
	}
	
	public int getLength()
	{
		// if (firstMessage_index < lastMessage_index)
			// return lastMessage_index - firstMessage_index;
		// else
			// return MESSAGE_LIST_MAX_SENTENCE;
		return this.messagesLenght;
	}
}