
import javax.microedition.lcdui.Graphics;

/**
 * A popup screen object for display all message from friends and other players.
 * Created: March 05, 2011
 */
 
public class MultiChatBox {

	public static final int CHATBOX_MAX_PLAYER = 10;
	// define for box chat 
	private final int BOX_OFFSET_X = 5;
	private final int BOX_OFFSET_Y = 5;
	private final int BOX_BORDER_WIDTH = 1;
	private final int BOX_TITLE_HEIGHT = 20;
	private final int TAB_NOT_SELECTED_WIDTH = 15;
	private final int BOX_LINE_HEIGHT = 15;
	private final int CHATBOX_MAX_LINE = 14;
	private final int SCROLL_BAR_X = 225;
	private final int SCROLL_BAR_Y = 100;
	// define variable
	private MessageList messageList[];
	private FontSprite textFont;
	private int playerMessageCount = 0;
	private int indexViewMessages;
	private int top, width, height;
	private int indexSelected = 0;
	private TextField chatTextField;
	// default values
	private int backgroundColor1 = 0x660000;
	private int backgroundColor2 = 0xE00000;
	private int backgroundColor3 = 0x00E0E0;
	private int borderColor = 0xFFFFFF;
	
	// Constructer Function
	public MultiChatBox (FontSprite font, int boxWidth, int boxHeight, int top) {
		
		this.textFont = font;
		this.width = boxWidth;
		this.height = boxHeight;
		this.top = top;
		this.playerMessageCount = 0;
		this.messageList = new MessageList[CHATBOX_MAX_PLAYER];
		this.chatTextField = new TextField(null, new String(), TextField.ANY, BOX_OFFSET_X*2, this.top + this.height - 36,
				this.width - BOX_OFFSET_X*4, this.textFont.getHeight() + 8);
		this.chatTextField.setFont(this.textFont);
		this.chatTextField.setColors(IGameConfig.TEXTFIELD_BACKGROUND_COLOR, IGameConfig.TEXTFIELD_FOCUS_BACKGROUND_COLOR, IGameConfig.TEXTFIELD_BORDER_COLOR);
		this.chatTextField.setFocus(true);
		this.indexViewMessages = CHATBOX_MAX_LINE - 1;
		// create tab for In Game
		messageList[playerMessageCount] = new MessageList("Tin Nhắn"/* TODO Package.getText(TEXT.MESSAGE_TEXT_YOU)*/);
		playerMessageCount ++;
	}
	
	public void setFont (FontSprite font) {
	
		this.textFont = font;
	}
	
	public void setColors (int backgroundColor1, int backgroundColor2, int backgroundColor3, int borderColor) {
		
		this.backgroundColor1 = backgroundColor1;
		this.backgroundColor2 = backgroundColor2;
		this.backgroundColor3 = backgroundColor3;
		this.borderColor = borderColor;
	}
	
	public void addMessage (String friendName, String messageText, boolean isMessageOfMainPlayer)
	{
		// DBG(" -------------------  add Message  ---------------------");
		String name;
		for (int i=0; i<playerMessageCount; i++) {
			if (messageList[i] != null) {
				if (messageList[i].isEqualMessageTitle(friendName)) {
					name = isMessageOfMainPlayer?("You"/*Package.getText(TEXT.MESSAGE_TEXT_YOU)*/):friendName;
					messageList[i].addMessage(name, messageText);
					if (indexSelected == i)
						this.indexViewMessages ++;
					return;
				}
			}
		}
		messageList[playerMessageCount] = new MessageList(friendName);
		name = isMessageOfMainPlayer?("You"/*Package.getText(TEXT.MESSAGE_TEXT_YOU)*/):friendName;
		messageList[playerMessageCount].addMessage(name, messageText);
		if (indexSelected == playerMessageCount)
			this.indexViewMessages ++;
		playerMessageCount ++;
	}
	
	public void addTableMessage (String name, String messageText)
	{
		messageList[0].addMessage(name, messageText);
		if (indexSelected == 0)
			this.indexViewMessages ++;
	}
	
	public void update (int keyCode)
	{
		if (KEY_LEFT) {
			indexSelected--;
			// this.indexViewMessages = this.messageList[indexSelected].getLength() - 1;
		}
		if (KEY_RIGHT) {
			indexSelected++;
			// this.indexViewMessages = this.messageList[indexSelected].getLength() - 1;
		}
		this.indexViewMessages = this.messageList[indexSelected].getLength() - 1;
		if (KEY_UP) {
			if (this.indexViewMessages>=CHATBOX_MAX_LINE)
				this.indexViewMessages --;
		}
		if (KEY_DOWN) {
			if (this.indexViewMessages<this.messageList[indexSelected].getLength()-1)
				this.indexViewMessages ++;
		}
		
		if (KEY_FIRE && indexSelected>0) {
			if (chatTextField.getTextLength()>0) {
				// TODO: sent message to server here
				
				this.addMessage(messageList[indexSelected].getMessageTitle(), chatTextField.getString(), true);
				chatTextField.setString("");
			}
		}
		
		if (indexSelected < 0) 
			indexSelected = playerMessageCount;
		if (indexSelected >= playerMessageCount)
			indexSelected = 0;
		
		if (this.indexSelected > 0 && ANY_KEY_PRESSED)
			chatTextField.updateTextField(keyCode);
			
		if (this.indexViewMessages >= MessageList.MESSAGE_LIST_MAX_SENTENCE)
			this.indexViewMessages = MessageList.MESSAGE_LIST_MAX_SENTENCE-1;
	}
	
	public void paint (Graphics g)
	{
		
		g.setColor(0);
		g.fillRect(BOX_OFFSET_X, this.top + BOX_OFFSET_Y, this.width - 2*BOX_OFFSET_X + 1, this.height - 2*BOX_OFFSET_Y + 1);
		g.setColor(backgroundColor3);
		g.fillRect(BOX_OFFSET_X, this.top + BOX_OFFSET_Y, this.width - 2*BOX_OFFSET_X, this.height - 2*BOX_OFFSET_Y);
		g.setColor(borderColor);
		g.fillRect(BOX_OFFSET_X + 1, this.top + BOX_OFFSET_Y + BOX_TITLE_HEIGHT - 1, this.width - 2*BOX_OFFSET_X - 2, this.height - (this.top + BOX_OFFSET_Y + BOX_TITLE_HEIGHT) - 2*BOX_OFFSET_Y + 3);
		int posX = BOX_OFFSET_X + 1, tWidth, offsetY;
		String temp = new String();
		for (int i=0; i<playerMessageCount; i++) {
			if (i==indexSelected) {
				tWidth = this.textFont.getWidth(messageList[i].getMessageTitle()) + 8;
				temp = messageList[i].getMessageTitle();
				offsetY = 2;
			} else {
				temp = "...";
				tWidth = TAB_NOT_SELECTED_WIDTH;
				offsetY = 4;
			}
			g.setColor(borderColor);
			g.fillRect(posX, this.top + BOX_OFFSET_Y + offsetY, tWidth, BOX_TITLE_HEIGHT + offsetY + 2);
			if (i==indexSelected)
				g.setColor(backgroundColor1);
			else
				g.setColor(backgroundColor2);
			g.fillRect(posX + 1, this.top + BOX_OFFSET_Y + offsetY + 1, tWidth - 2, BOX_TITLE_HEIGHT + offsetY + 1);
			this.textFont.drawString( temp, posX + 4, this.top + BOX_OFFSET_Y + offsetY + 2, Graphics.LEFT | Graphics.TOP, g);
			posX += tWidth;
		}
		g.setColor(backgroundColor1);
		g.fillRect(BOX_OFFSET_X + 2, this.top + BOX_OFFSET_Y + BOX_TITLE_HEIGHT, this.width - 2*BOX_OFFSET_X - 4, this.height - (this.top + BOX_OFFSET_Y + BOX_TITLE_HEIGHT) - 2*BOX_OFFSET_Y);
		
		// draw all message off player indexSelected
		int posY = this.top + BOX_OFFSET_Y + BOX_TITLE_HEIGHT + BOX_LINE_HEIGHT;
		posX = BOX_OFFSET_X + 4;
		int start, end, length = messageList[indexSelected].getLength();
		if (length<=CHATBOX_MAX_LINE) {
			start = 0;
			end = length - 1;
		} else {
			start = (this.indexViewMessages<CHATBOX_MAX_LINE)?0:(this.indexViewMessages-CHATBOX_MAX_LINE+1);
			end = (this.indexViewMessages<CHATBOX_MAX_LINE)?(CHATBOX_MAX_LINE-1):(this.indexViewMessages);
		}
		for (int i=start; i<=end; i++) {
			this.textFont.drawString( messageList[indexSelected].getFullMessage_AtIndex(i), posX, posY, Graphics.LEFT | Graphics.TOP, g);
			posY += BOX_LINE_HEIGHT;
		}
		// draw scroll bar
		int height;
		if (length>CHATBOX_MAX_LINE) {
			height = (CHATBOX_MAX_LINE-1)*100/length;
			g.setColor(0xFFFFFF);
			g.fillRect(SCROLL_BAR_X, SCROLL_BAR_Y, 2, 100);
			g.setColor(backgroundColor3);
			g.fillRect(SCROLL_BAR_X, SCROLL_BAR_Y + (this.indexViewMessages*100/length) - height, 2, height + 5);
		}
		// draw text field
		if (this.indexSelected > 0)
			chatTextField.paintTextField(g);
	}
}