import java.util.Vector;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.SocketConnection;

public class NetworkConnection {
	public static DataInputStream inputStream;
	public static DataOutputStream outputStream;
	public static SocketConnection socket;
	public final static String CONNECTION_STRING = "socket://127.0.0.1:3330";
	// public final static String CONNECTION_STRING = "socket://conruacodon.no-ip.org:3330";
	public final static String ERROR_MESSAGE = "Error";

	//define parameter for request server
	public static String BEGIN_LOGIN_STRING	 		= "login,";
	public static String START_GAME					= "start_Game";
	public static String DEAL_CARD					= "deal_Card";
	public static String ATTACH_CARD				= "attach_Card";
	public static String ALLOW_NEXT_PLAYER			= "allow_next_player";
	public static String RESULT_ATTACH_TRUE			= "attach_True";
	public static String RESULT_ATTACH_FALSE		= "attach_False";
	public static String LAST_CARD					= "LAST_CARD";
	public static String IS_ALLOW_PLAY				= "allow_play";
	public static String IS_NOT_ALLOW_PLAY			= "not_allow_play";
	public static String IS_RE_ALLOW_PLAY			= "re_allow_play";
	public static String IS_COMPLETE_CIRCEL			= "IS_COMPLETE_CIRCEL";
	public static String IS_WIN						= "is_win";
	public static String IS_LOSE					= "is_lose";
	public static String RESULT_LOGIN_OK	 		= "login_Success";
	public static String RESULT_LOGIN_ERROR			= "login_Error";
	public static String MESSAGE_GET_ROOM			= "Get Room";
	public static String MESSAGE_GET_TABLE_IN_ROOM	= "Get Table in Room ";
	public static String MESSAGE_JOIN_TABLE			= "Join Table ";
	public static String MESSAGE_OUT_TABLE			= "Out Table ";
	public static String CHAT_MESSAGE				= "Message:";
	public static String MESSAGE_SET_GOLD			= "Set gold:";
	//end define

	public NetworkConnection(){
		try{
			socket = (SocketConnection) Connector.open(CONNECTION_STRING, Connector.READ_WRITE );
			DBG("Client: dang khoi tao");
			inputStream = socket.openDataInputStream();
			outputStream = socket.openDataOutputStream();
		}
		catch(Exception e) {DBG(ERROR_MESSAGE + e);}
	}

	public static String sendRequest(String request){
		String result = null;
		try
		{
			while(inputStream.available() != 0) inputStream.readUTF();
			outputStream.writeUTF(request);
			while(true){
				result = inputStream.readUTF();
				if(result != null) break;
			}
		} catch(Exception e){DBG(e);}
		return result;
	}

	//funtion send a message and receive a byte[]
	public static byte[] sendRequestByte(String request){
		byte[] result = new byte[20];
		try
		{
			outputStream.writeUTF(request);
			while(true){
				inputStream.read(result, 0, result.length);
				if(result != null)
				{
					outputStream.flush();
					break;
				}
			}
		} catch(Exception e){DBG(e);}
		return result;
	}


	public static void sendMessageToServer(String message){
		try{
			outputStream.writeUTF(message);
		} catch(Exception e) {DBG("sendMessageToServer Error "+e);}
	}

	public String getRoomInfo(){
		String room=null;
		try{
			room = sendRequest(MESSAGE_GET_ROOM);
		} catch(Exception e) {DBG(e);}
		return room;
	}

	public String getTableInfo(int idRoom){
		String table=null;
		try{
			table = sendRequest(MESSAGE_GET_TABLE_IN_ROOM + idRoom);
		} catch(Exception e) {DBG(e);}
		return table;
	}

	public String joinTable(int idTable){
		String result="";
		try{
			result = sendRequest(MESSAGE_JOIN_TABLE + idTable);
		} catch(Exception e) {DBG(e);}
		return result;
	}

	public String getUpdate(){
		String updateString = null;
		try{
		if(inputStream.available() != 0)
		updateString = inputStream.readUTF();
		} catch(Exception e) {DBG(e);}
		return updateString;
	}

	public String getLoginInfo(String userPass){
		String loginInfo = null;
		try{
			loginInfo = sendRequest(BEGIN_LOGIN_STRING + userPass);
			DBG("login");
		}
		catch(Exception e) {DBG(e.toString());}
		return loginInfo;
	}

	public void getResultCompareCard(String listCard){
		InfoPlaying.isStartGame = false;
		sendMessageToServer(ATTACH_CARD + listCard);
	}

	public void sendAllowNextPlayer()
	{
		sendMessageToServer(ALLOW_NEXT_PLAYER);
	}

	public void outTable(){
		sendMessageToServer(MESSAGE_OUT_TABLE);
		#ifdef ADD_WAIT_PLAYER_STATE
		InfoPlaying.isHostRoom = false;
		InfoPlaying.isAllowPlay = InfoPlaying.isHostRoom;
		#endif
	}

	public void sendChatMessage(String message){
		sendMessageToServer(CHAT_MESSAGE+message);
	}

	public void setGoldInTable(String gold){
		sendMessageToServer(MESSAGE_SET_GOLD+gold);
		DBG("send set gold");
	}

	public byte[] sentStartGame()
	{
		DBG("Client public byte[] sentStartGame() begin  ");
		byte[] cardListFromServer = null;
		String result = null;
		try
		{
			result = sendRequest(START_GAME);
			if(result != null)
			{
				DBG("000000000000 sentStartGame : result = " + result);
				result = result.substring(DEAL_CARD.length());
				DBG("sentStartGame : result = " + result);
				cardListFromServer = this.changeStringToListCard(result);
				InfoPlaying.currentPositionPlayerPlay = 0;
				Game.beginCountTime = true;
				InfoPlaying.isStartGame = true;
			}
		}
		catch(Exception ex)
		{
			DBG("sentStartGame error : "  + ex.toString());
		}
		if(cardListFromServer == null)
		{
			DBG(">>>>>>>>>>>>>>>>> cardListFromServer = null" );
		}
		return cardListFromServer;
	}

	#ifdef ADD_WAIT_PLAYER_STATE
	public void processUpdateString(String updateString){
		DBG("updateString="+updateString);
		if(updateString.startsWith("User Out Room")){
			int positionRemove = Integer.parseInt(updateString.substring(updateString.length()-1));
			InfoPlaying.arrayNamePlayer[positionRemove] = "None";
		}
		else if (updateString.equals("Host")) {
			InfoPlaying.isHostRoom = true;
			InfoPlaying.isAllowPlay = InfoPlaying.isHostRoom;
			InfoPlaying.currentPlayerHostRoom = Game.getPositionInTable();
		}
		else if (updateString.startsWith("Host:")){
			InfoPlaying.currentPlayerHostRoom = Integer.parseInt(updateString.substring(updateString.length()-1));
			if(InfoPlaying.currentPlayerHostRoom ==Game.getPositionInTable()) InfoPlaying.isHostRoom = true;
			else InfoPlaying.isHostRoom = false;
			InfoPlaying.isAllowPlay = InfoPlaying.isHostRoom;
			InfoPlaying.currentPositionPlayerPlay = Game.getPositionWithMainPlayer(InfoPlaying.arrayNamePlayer[InfoPlaying.currentPlayerHostRoom]);
		}
		else if(updateString.startsWith(CHAT_MESSAGE)){
			//Game.addMessageToChatLog(updateString.substring(CHAT_MESSAGE.length()));
			updateString = updateString.substring(CHAT_MESSAGE.length());
			String usernameCurrentPlayer = updateString.substring(0,updateString.indexOf(":"));
			DBG("Game.getPositionWithMainPlayer(usernameCurrentPlayer)="+Game.getPositionWithMainPlayer(usernameCurrentPlayer));
			InfoPlaying.currentPlayerTalkPos = Game.getPositionWithMainPlayer(usernameCurrentPlayer);
			InfoPlaying.currentChatMessageString = updateString.substring(updateString.indexOf(":"));
			Game.startDisplayMessageChat = 0;

		}
		else if(updateString.startsWith(DEAL_CARD))
		{
			Game.beginCountTime = true;
		}
		else{
			String a[] = GameLib.splitString(updateString, ",");
			InfoPlaying.arrayNamePlayer[Integer.parseInt(a[0])] = a[1];
		}
	}
	#endif


	#ifdef USE_CHECK_CARDS_FROM_SERVER
	public void processUpdateInGameString(String updateString){
		DBG("updateString="+updateString);
		
		if(updateString.startsWith("User Out Room")){
			int positionRemove = Integer.parseInt(updateString.substring(updateString.length()-1));
			InfoPlaying.arrayNamePlayer[positionRemove] = "None";
		}
		else if(updateString.startsWith(LAST_CARD)){
			DBG("lastCardListMoved : ");
			//InfoPlaying.isCompleteCircle = false;
			Game.lastCardListMoved.print();
			String listCard = updateString.substring(LAST_CARD.length());
			String userNamePlaying = listCard.substring(0,listCard.indexOf(":"));
			InfoPlaying.lastPositionPlayerPlay = Game.getPositionWithMainPlayer(userNamePlaying);
			listCard = listCard.substring(listCard.indexOf(":") + 1);
			DBG("listCard="+listCard);
			byte[] lastCardList = changeStringToListCard(listCard);
			Game.lastCardListMoved = new CardList(Deck.getCards(lastCardList));
			Game.setStepFramePlayToZero();
		}
		else if(updateString.equals(RESULT_ATTACH_TRUE))
		{
			InfoPlaying.isCompleteCircle = false;
			Game.lastCardListMoved = GameBoard.mainPlayer.getSelectedCards();
			GameBoard.mainPlayer.removeCards(Game.lastCardListMoved.cards);
			GameBoard.mainPlayer.deselectAll();
			InfoPlaying.lastPositionPlayerPlay = 0;
			Game.setStepFramePlayToZero();
		}
		else if(updateString.equals(RESULT_ATTACH_FALSE))
		{
			System.out.println("Co loi Xay ra trong qua trinh truyen du lieu");
		}
		else if(updateString.startsWith(IS_ALLOW_PLAY))
		{
			String usernameCurrentPlayer = updateString.substring(IS_ALLOW_PLAY.length());
			DBG("usernameCurrentPlayer="+usernameCurrentPlayer);
			if(usernameCurrentPlayer.equals(Game.userName)){
				if(!InfoPlaying.allowModify_isAllowPlay)
					sendAllowNextPlayer();
				else{
					InfoPlaying.isAllowPlay = true;
					InfoPlaying.currentPositionPlayerPlay = 0;
				}
			}
			else{
				DBG("isAllowPlay=false");
				InfoPlaying.isAllowPlay = false;
				InfoPlaying.currentPositionPlayerPlay = Game.getPositionWithMainPlayer(usernameCurrentPlayer);
			}
			Game.beginCountTime = true;
		}
		else if(updateString.equals(IS_NOT_ALLOW_PLAY))
		{
			InfoPlaying.isAllowPlay = false;
		}
	// #ifdef FIX_BUG_ALLOW_NEXT_PLAYER_IN_A_CIRCEL
		else if(updateString.equals(ALLOW_NEXT_PLAYER))
		{
			InfoPlaying.isAllowPlay = false;
			InfoPlaying.allowModify_isAllowPlay = false;
			DBG(">>>>>>>> Bo Luot <<<<<<<<<<");
		}
	// #endif
		else if(updateString.equals(IS_WIN))
		{
			InfoPlaying.allowModify_isAllowPlay = true;
			InfoPlaying.isWin 		 = 1;
		}
		else if(updateString.equals(IS_LOSE))
		{
			InfoPlaying.allowModify_isAllowPlay = true;
			InfoPlaying.isWin 		 = 0;
		}
		else if (updateString.startsWith("Host:")){
			InfoPlaying.currentPlayerHostRoom = Integer.parseInt(updateString.substring(updateString.length()-1));
			if(InfoPlaying.currentPlayerHostRoom == Game.getPositionInTable()) InfoPlaying.isHostRoom = true;
			else InfoPlaying.isHostRoom = false;
			InfoPlaying.isAllowPlay = InfoPlaying.isHostRoom;
		}
		else if(updateString.equals(IS_RE_ALLOW_PLAY))
		{
			Game.lastCardListMoved = new CardList();
			InfoPlaying.isAllowPlay = true;
		}
	// #ifdef FIX_BUG_ALLOW_NEXT_PLAYER_IN_A_CIRCEL
		else if(updateString.equals(IS_COMPLETE_CIRCEL))
		{
			InfoPlaying.allowModify_isAllowPlay = true;
			if(InfoPlaying.isAllowPlay)
				InfoPlaying.isCompleteCircle = true;
			System.out.println("InfoPlaying.isCompleteCircle = " + InfoPlaying.isCompleteCircle );
		}
		else if(updateString.startsWith(CHAT_MESSAGE)){
			//Game.addMessageToChatLog(updateString.substring(CHAT_MESSAGE.length()));
			updateString = updateString.substring(CHAT_MESSAGE.length());
			String usernameCurrentPlayer = updateString.substring(0,updateString.indexOf(":"));
			InfoPlaying.currentPlayerTalkPos = Game.getPositionWithMainPlayer(usernameCurrentPlayer);
			InfoPlaying.currentChatMessageString = updateString.substring(updateString.indexOf(":"));
			Game.startDisplayMessageChat = 0;

		}
	// #endif
	}
	#endif



	public static String changeCardListToString(Vector cards)
	{
		DBG("changeCardListToString begin");
		byte valueCard[] = new byte[cards.size()];
		String chuoiCardList = "";

		for (int i = 0; i < cards.size(); i++) {
			ICard card1 = (ICard) cards.elementAt(i);
			valueCard[i] = (byte)((card1.getRank())*4 + card1.getSuit());
			chuoiCardList += (char)(valueCard[i]);
			DBG("" + valueCard[i]);
		}
		DBG(chuoiCardList);
		DBG("public String changeCardListToValue() end");
		return chuoiCardList;
	}

	public static byte[] changeStringToListCard(String resultFromServer)
	{
		byte[] result = new byte[resultFromServer.length()];
		for(int i = 0; i < resultFromServer.length(); i++)
		{
			result[i] = (byte)(resultFromServer.charAt(i));
		}
		return result;
	}

}
