
	///*********************************************************************
	///* Game_Menu.h
	///*********************************************************************

	public static final byte MENU_MAIN 				= 0;
	public static final byte MENU_OPTIONS 			= 1;
	public static final byte MENU_ACCOUNT 			= 2;
	public static final byte MENU_INGAME 			= 3;

	public final int MAIN_MENU_ITEM_IN_ROW	= 4;
	public final int MAIN_MENU_START_Y		= 75;
	private int[] menuItems;
	private Menu menu;
	private CSprite menuSprite;

	/**
	 * Load menu items text.
	 */
	private void loadMenuItems(byte menuId)
	{
		switch (menuId) {
			case MENU_MAIN:
				this.menuItems = new int[] {
					TEXT.MENU_PLAY,
					TEXT.MENU_GET_COINS,
					TEXT.MENU_MESSAGE,
					TEXT.MENU_FRIENDS,
					TEXT.MENU_ACCOUNT,
					TEXT.MENU_ABOUT,
					TEXT.MENU_HELP,
					TEXT.MENU_OPTIONS,
					TEXT.MENU_EXIT,
				};
				break;

			case MENU_OPTIONS:
				this.menuItems = new int[] {
					(Settings.getSelectedLanguage() == 0) ? TEXT.MENU_OPTION_LANGUAGE_EN : TEXT.MENU_OPTION_LANGUAGE_VI,
					soundPlayer.getEnableSound() ? TEXT.MENU_OPTION_SOUND_ON : TEXT.MENU_OPTION_SOUND_OFF,
				};
				break;

			case MENU_ACCOUNT:
				this.menuItems = new int[] {
					TEXT.MENU_RESUME,
					TEXT.MENU_INGAME_OPTIONS,
					TEXT.MENU_EXIT_TO_MAIN_MENU,
				};
				break;

			case MENU_INGAME:
				this.menuItems = new int[] {
					TEXT.MENU_RESUME,
					TEXT.MENU_INGAME_OPTIONS,
					TEXT.MENU_EXIT_TO_MAIN_MENU,
				};
				break;
		}
	}

	public void updateMenu(byte menuId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				this.loadMenuItems(menuId);		// Load menu text items

				switch(menuId) {
					case MENU_MAIN:
						if (this.menu == null) {
							this.menu = new Menu(this.menuItems, fonts[FONT_NORMAL], 0, MAIN_MENU_START_Y,
								getScreenWidth(), getScreenHeight(), MAIN_MENU_ITEM_IN_ROW, Menu.MENU_STYLE_HORIZON);
							this.menu.setSelectedItemColor(MENU_HOVER_BACKGROUND_COLOR);
						}
						// Main menu icons sprite
						if (this.menuSprite == null) {
							Package.open(MENU_SPRITE_PACKAGE);
							this.menuSprite = Package.loadSprite(MENU_SPRITE_ID);
							this.menuSprite.setFrameSize(MAIN_MENU_ICONS_FRAMES_WIDTH, MAIN_MENU_ICONS_FRAMES_HEIGHT, MAIN_MENU_ICONS_FRAMES_COUNT);
							this.menuSprite.buildCache();
							Package.close();
							// Set sprite to MainMenu object
							this.menu.setMenuSprite(this.menuSprite);
						}
						setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_NONE);
						break;	//case MENU_MAIN

					default:
						if (this.menu == null) {
							this.menu = new Menu(this.menuItems, fonts[FONT_BOLD], 0, 0, getScreenWidth(),
									getScreenHeight(), 1, Menu.MENU_STYLE_VERTICAL);
							this.menu.setSelectedItemColor(MENU_HOVER_BACKGROUND_COLOR);
						}
						setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_BACK);
						break;	//default
				}

				// Play title sound
				if (menuId == MENU_MAIN) {
					soundPlayer.play(M_TITLE_ID, true);
				}

				if (lastState != STATE_LOGIN_INFO_MESSAGE && menuId == MENU_MAIN) {
					setTransition(TRANSITION_BACK);
				} else {
					setTransition(TRANSITION_NEXT);
				}
				break;	//case MESSAGE_CONSTRUCTOR

			case MESSAGE_UPDATE:
				this.menu.updateMenu(keyCode);
				selectedItem = this.menu.getSelectedMenuItem();
				// selectedItem += MENU_ITEM_STEP * menuId;

				switch (selectedItem) {
					/// Main menu
					case TEXT.MENU_PLAY:
						setGameState(STATE_JOIN_ROOM);
						break;

					case TEXT.MENU_GET_COINS:
						break;

					case TEXT.MENU_MESSAGE:
						setGameState(STATE_CHAT_SCREEN);
						break;

					case TEXT.MENU_FRIENDS:
						setGameState(STATE_FRIENDLIST_SCREEN);
						break;

					case TEXT.MENU_ABOUT:
						setGameState(STATE_ABOUT);
						break;

					case TEXT.MENU_HELP:
						setGameState(STATE_HELP);
						break;

					case TEXT.MENU_OPTIONS:
						setGameState(STATE_OPTIONS);
						break;

					case TEXT.MENU_EXIT:
						setGameState(STATE_EXIT_CONFIRM);
						break;

					/// Options
					case TEXT.MENU_OPTION_LANGUAGE_EN:
					case TEXT.MENU_OPTION_LANGUAGE_VI:
						int selectedLanguage = (Settings.getSelectedLanguage() == 0) ? 1 : 0;
						if (Settings.getSelectedLanguage() == 0) {
							Package.open(IData.TEXT_EN_PACKAGE);
							Package.loadTextPackage(IData.COMMON_EN_ID);
						} else {
							Package.open(IData.TEXT_VI_PACKAGE);
							Package.loadTextPackage(IData.COMMON_VI_ID);
						}
						Package.close();

						Settings.setSelectedLanguage(selectedLanguage);
						Settings.save();

						this.loadMenuItems(menuId);	// Reload items string
						this.menu.setMenuItems(this.menuItems);
						break;

					case TEXT.MENU_OPTION_SOUND_ON:
					case TEXT.MENU_OPTION_SOUND_OFF:
						if (soundPlayer.getEnableSound()) {
							soundPlayer.stop();
							soundPlayer.setEnableSound(false);
							soundPlayer.unload();
						} else {
							soundPlayer.setEnableSound(true);
							soundPlayer.loadSoundPack(SOUND_PACKAGE);

							if (menuId == MENU_OPTIONS) {
								soundPlayer.play(M_TITLE_ID);
							} else {
								soundPlayer.play(SFX_MOVE_CARD_ID);
							}
						}

						loadMenuItems(menuId);	// Reload items string
						menu.setMenuItems(this.menuItems);
						break;

					/// In-game menu
					case TEXT.MENU_RESUME:
						setGameState(STATE_GAME_PLAY);
						break;

					case TEXT.MENU_EXIT_TO_MAIN_MENU:
						setGameState(STATE_EXIT_TO_MAIN_MENU);
						break;
				}	// switch (selectedItem)

				// Go to previous state when press right soft key
				updateInputMenu(menuId);
				break;	//case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				g.drawImage(backgroundImage, 0, 0, 0);
				this.menu.paintMenu(g);
				break;

			case MESSAGE_DESTRUCTOR:
				if (nextState < STATE_MAIN_MENU_POPUP_MENU
					|| nextState > STATE_LOGIN_POPUP_MENU) {
					this.menuSprite = null;
					this.menuItems = null;
					this.menu = null;
				}
				break;
		}	//switch (message) {
	}

	public void updateInputMenu(byte menuId)
	{
		switch(menuId) {
			case MENU_MAIN:
				// if (RIGHT_SOFT_KEY) {
					// setGameState(STATE_LOGIN_SCREEN);
				// } else if (LEFT_SOFT_KEY) {
					// setGameState(STATE_MAIN_MENU_POPUP_MENU);
				// }
				break;

			default:
				if (RIGHT_SOFT_KEY) {
					if (nextState > lastState) {
						setGameState(STATE_MAIN_MENU);
					} else {
						setGameState(lastState);
					}
				}
		}
	}
