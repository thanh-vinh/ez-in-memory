
import javax.microedition.lcdui.Graphics;

/**
 * A popup screen object for input text.
 * Last updated: February 19, 2011
 */
 
public class InputBox extends TextField {

	private static final int SPACE_HEIGHT = 4;

	private String title;
	private FontSprite titleFont;
	private int backgroundColor, titleColor;
	private int left, top, width, height, titlePosY;
	
	private void calculateItemsPosY() {
	
		// Calculate item positions
		this.titlePosY = this.top + SPACE_HEIGHT;
		super.top = (this.title != null) ? this.titlePosY + this.titleFont.getHeight() + (SPACE_HEIGHT << 1) : this.titlePosY;
		this.height = super.getBottomPositionY() - this.top; 
	}
	
	public InputBox(String title, String label, String text, byte constraints, int left, int top, int width, int textFieldHeight) {

		super(label, text, constraints, left + SPACE_HEIGHT, top, width - (SPACE_HEIGHT << 1), textFieldHeight);
		
		this.title = title;
		this.left = left;
		this.top = top;
		this.width = width;
	}
	
	public InputBox(String label, String text, byte constraints, int left, int top, int width, int textFieldHeight) {

		this(null, label, text, constraints, left, top, width, textFieldHeight);
	}
	
	public void setFonts(FontSprite titleFont, FontSprite font) {
	
		this.titleFont = titleFont;
		super.setFont(font);
		
		this.calculateItemsPosY();
	}
	
	public void setColors(int backgroundColor, int titleColor, int textFieldBackgroundColor, int focusBackgroundColor, int borderColor) {
		
		this.backgroundColor = backgroundColor;
		this.titleColor = titleColor;
		super.setColors(textFieldBackgroundColor, focusBackgroundColor, borderColor);
	}	
	
	public void updateInputBox(int keyCode) {
	
		super.updateTextField(keyCode);
	}
	
	public void paintInputBox(Graphics g) {

		// Background
		GameLib.fillRect(this.backgroundColor, super.borderColor, this.left, this.top, this.width, this.height, g);
		// Title
		if (this.title != null) {
			GameLib.fillRect(this.titleColor, this.left, this.top, this.width, this.titleFont.getHeight() + (SPACE_HEIGHT << 1), g);
			GameLib.drawRect(super.borderColor, this.left, this.top, this.width, this.height, g);
			this.titleFont.drawString(this.title, this.left + SPACE_HEIGHT, this.titlePosY, Graphics.LEFT | Graphics.TOP, g);
		}
		// TextField		
		super.paintTextField(g);
	}
}