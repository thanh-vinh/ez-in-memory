
	///*********************************************************************
	///* Game_FriendListScreen.h
	///*********************************************************************

	public static PeopleInfoList peopleInfoList = null;

	public void updateFriendListScreen(byte menuId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				loadAvatarSprite();
				if (peopleInfoList == null) {
					peopleInfoList = new PeopleInfoList(0, getScreenHeight()-50, getScreenWidth(), avatarSprite, fonts[FONT_NORMAL]);
					peopleInfoList.getPeopleInfoList();
				} else {
					peopleInfoList.refreshPeopleInfoList();
				}
				setSoftKeys(SOFTKEY_BACK, SOFTKEY_OK, SOFTKEY_DELETE);
				break;

			case MESSAGE_UPDATE:
				drawGradientBox(BACKGROUND_GRADIENT_COLOR1, BACKGROUND_GRADIENT_COLOR2, 0, 0,
						getScreenWidth(), getScreenHeight(), GRADIENT_VERTICAL, g);
				if (LEFT_SOFT_KEY) {
					setGameState(STATE_MAIN_MENU);
				}
				peopleInfoList.update(keyCode);
				break;

			case MESSAGE_PAINT:
				peopleInfoList.paint(g);
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}
	}
