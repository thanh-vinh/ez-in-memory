
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;

/**
 * TextField object for input string.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * Last updated: February 20, 2011
 */
public class TextField {

	public static final byte ANY					= 0;
	public static final byte DECIMAL				= 1;
	public static final byte INITIAL_CAPS_SENTENCE	= 2;
	public static final byte INITIAL_CAPS_WORD		= 3;
	public static final byte NUMERIC				= 4;
	public static final byte PASSWORD				= 5;
	public static final byte URL					= 6;

	private static final char[] KEY_NUM1_CHARS = {'.', '?', '!'};
	private static final char[] KEY_NUM2_CHARS = {'a', 'b', 'c'};
	private static final char[] KEY_NUM3_CHARS = {'d', 'e', 'f'};
	private static final char[] KEY_NUM4_CHARS = {'g', 'h', 'i'};
	private static final char[] KEY_NUM5_CHARS = {'j', 'k', 'l'};
	private static final char[] KEY_NUM6_CHARS = {'m', 'n', 'o'};
	private static final char[] KEY_NUM7_CHARS = {'p', 'q', 'r', 's'};
	private static final char[] KEY_NUM8_CHARS = {'t', 'u', 'v'};
	private static final char[] KEY_NUM9_CHARS = {'w', 'x', 'y', 'z'};
	private static final char[] KEY_NUM0_CHARS = {' '};

	protected String label, text;
	protected byte constraints;
	protected FontSprite font;
	protected int left, top, width, height;
	protected int backgroundColor, focusBackgroundColor, borderColor;
	private boolean isFocus;
	private long framesCount;

	//
	// For update the input text
	//
	private StringBuffer inputTextBuffer;
	private int lastKeyPressed;
	private long lastKeyPressedTime;
	private int charIndex;							// Index in character array
	// private int cursorAtCharIndex, cursorPosX;		// Current cursor position
	private boolean goToNextChar = true;			// Enter next char or override current char?

	public TextField(String label, String text, byte constraints, int left, int top, int width, int height) {

		this.label = label;
		this.setString(text);
		this.constraints = constraints;
		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
		this.inputTextBuffer = new StringBuffer();
	}

	public void setFont(FontSprite font) {

		this.font = font;
	}

	public void setColors(int backgroundColor, int focusBackgroundColor, int borderColor) {

		this.backgroundColor = backgroundColor;
		this.focusBackgroundColor = focusBackgroundColor;
		this.borderColor = borderColor;
	}

	/**
	 *  Sets the contents of the TextField as a string value, replacing the previous contents.
	 */
	public void setString(String text) {

		// TOTO		Implement constraints condition (check for constraints while set text value)
		this.text = text;
		this.inputTextBuffer = new StringBuffer();
		this.inputTextBuffer.append(text);
		// this.cursorAtCharIndex = text.length() - 1;
	}

	/**
	 * Gets the contents of the TextField as a string value.
	 */
	public String getString() {

		return this.text;
	}

	public int getTextLength() {

		return this.text.length();
	}

	public void setFocus(boolean isFocus) {

		this.isFocus = isFocus;
	}

	public boolean isFocus() {

		return this.isFocus;
	}

	/**
	 * The height of TextField, include label & input box.
	 */
	public int getHeight() {

		return (this.height << 1);
	}

	public int getBottomPositionY() {

		return (this.top + this.getHeight());
	}

	public char[] getChars(int key) {

		switch(key)	{
		case Canvas.KEY_NUM1: return KEY_NUM1_CHARS;
		case Canvas.KEY_NUM2: return KEY_NUM2_CHARS;
		case Canvas.KEY_NUM3: return KEY_NUM3_CHARS;
		case Canvas.KEY_NUM4: return KEY_NUM4_CHARS;
		case Canvas.KEY_NUM5: return KEY_NUM5_CHARS;
		case Canvas.KEY_NUM6: return KEY_NUM6_CHARS;
		case Canvas.KEY_NUM7: return KEY_NUM7_CHARS;
		case Canvas.KEY_NUM8: return KEY_NUM8_CHARS;
		case Canvas.KEY_NUM9: return KEY_NUM9_CHARS;
		case Canvas.KEY_NUM0: return KEY_NUM0_CHARS;
		}
		return null;
	}

	void clearChar() {

		DBG("Clear character...");
		if(this.inputTextBuffer.length() > 0) {
			// this.inputTextBuffer.deleteCharAt(this.cursorAtCharIndex);
			this.inputTextBuffer.deleteCharAt(this.inputTextBuffer.length() - 1);
			this.text = this.inputTextBuffer.toString();
		}
	}

	public void updateTextField(int keyCode) {

		if(keyCode >= Canvas.KEY_NUM0 && keyCode <= Canvas.KEY_NUM9) {			//	keyCode 0 - 9
			this.lastKeyPressedTime = System.currentTimeMillis();
			if(this.goToNextChar || keyCode != this.lastKeyPressed) {
				this.goToNextChar = true;
				this.lastKeyPressed = keyCode;
				this.charIndex = 0;
			} else {
				this.charIndex++;
			}

			// this.framesCount = 0;

			char[] chars = this.getChars(keyCode);

			if(chars != null) {
				if(this.charIndex >= chars.length) {
					this.charIndex -= chars.length;
				}

				if(this.goToNextChar) {
					// this.inputTextBuffer.insert(cursorAtCharIndex, chars[charIndex]);
					// this.cursorAtCharIndex++;
					this.inputTextBuffer.append(chars[charIndex]);
					this.text = this.inputTextBuffer.toString();
				} else {
					// this.inputTextBuffer.setCharAt(this.cursorAtCharIndex - 1, chars[this.charIndex]);
					this.inputTextBuffer.setCharAt(this.inputTextBuffer.length() - 1, chars[this.charIndex]);
					this.text = this.inputTextBuffer.toString();
				}
				this.goToNextChar = false;
				DBG("Enter text on TextField, value: " + text);
			}
		} else if(RIGHT_SOFT_KEY) {				// Clear key
			this.clearChar();
			this.goToNextChar = true;
		} /*else if(KEY_LEFT) {				// Move the cursor to left
			if(this.cursorAtCharIndex > 0) {
				this.cursorAtCharIndex--;
				this.goToNextChar = true;
			}
		} else if(KEY_RIGHT)	{				// Move the cursor to right
			if(this.cursorAtCharIndex < this.inputTextBuffer.length()) {
				this.cursorAtCharIndex++;
				this.goToNextChar = true;
			}
		}*/

		if (System.currentTimeMillis() - this.lastKeyPressedTime > 2000) {
			this.goToNextChar = true;
		}
	}

	public void paintTextField(Graphics g) {

		int y = top;
		// Label
		if (label != null) {
			this.font.drawString(this.label, this.left, y, 0, g);
			// Fill rect text field
			y += font.getHeight() + 2;
		}

		if (this.isFocus) {
			g.setColor(this.focusBackgroundColor);
			GameLib.fillRect(this.focusBackgroundColor, this.left, y, this.width, this.height, g);
		} else {
			GameLib.fillRect(this.backgroundColor, this.left, y, this.width, this.height, g);
		}

		// Border text field
		g.setColor(this.borderColor);
		g.drawRect(this.left, y, this.width, this.height);
		// Draw string in the center
		// TODO		Manualy set align for string (set anchor for drawString() method)
		// this.font.drawString(this.text, this.left, y, this.align, g);
		y += (this.height >> 1);
		this.font.drawString(this.text, this.width >> 1, y , Graphics.VCENTER | Graphics.HCENTER, g);

		// Draw cursor
		// this.cursorPosX = ((this.font.getWidth(this.text) + this.width) >> 1);
		if (++this.framesCount % 10 == 0 && this.isFocus && this.goToNextChar) {
			this.framesCount = 0;
			this.font.drawString("_", ((this.font.getWidth(this.text) + this.width) >> 1), y , Graphics.VCENTER | Graphics.HCENTER, g);
		}
	}
}
