
	///*********************************************************************
	///* Game_WattingPlayers.h
	///*********************************************************************

	public static final byte MAX_ROW_CHAT_LOG = 4;
	public static int POS_FRAME_CHAT_X = 20;
	public static int POS_FRAME_CHAT_Y =  166;
	public static int POS_FRAME_CHAT_WIDTH =  200;
	public static int POS_FRAME_CHAT_HEIGH =  50;
	public static int SPACE_BETWEEN_LINE = 10;
	public static int startDisplayMessageChat = -1;
	public static int currentPlayerHostRoom = -1;
	public static CSprite talkSprite;

	public static int getPositionWithMainPlayer(String username)
	{
		int posMainPlayer = Game.getPositionInTable();
		int posCurrentPlayer = Game.getPositionInTable(username);

		if(posCurrentPlayer == -1) return posCurrentPlayer;
		if(posCurrentPlayer > posMainPlayer)
			return posCurrentPlayer - posMainPlayer;
		else
			return 4 + (posCurrentPlayer - posMainPlayer);
	}

	public void paintChatLogMessage(FontSprite font, Graphics g)
	{
		if (InfoPlaying.currentChatMessageString.equals("") || InfoPlaying.currentPlayerTalkPos == -1)
			return;

		if ((startDisplayMessageChat >= 0) && (startDisplayMessageChat < 50)) {
			int posX = PLAYERS_POS[InfoPlaying.currentPlayerTalkPos][0];
			int posY = PLAYERS_POS[InfoPlaying.currentPlayerTalkPos][1];

			if (InfoPlaying.currentPlayerTalkPos == 1) {
				posX-=fonts[FONT_BLACK].getWidth(InfoPlaying.currentChatMessageString) + 10;
			} else if (InfoPlaying.currentPlayerTalkPos == 2) {
				posY += 50;
				posX += 20;
			}

			int numberFrameBetween = fonts[FONT_NORMAL].getWidth(InfoPlaying.currentChatMessageString) / talkSprite.getFrameWidth(1);
			talkSprite.paintFrame(0, posX, posY - 50,Graphics.LEFT | Graphics.TOP,g) ;
			for (int i = 0; i <numberFrameBetween + 1; i++)
				talkSprite.paintFrame(1, posX + i*talkSprite.getFrameWidth(1) + 10, posY - 50,Graphics.LEFT | Graphics.TOP,g) ;

			talkSprite.paintFrame(2, posX + numberFrameBetween*talkSprite.getFrameWidth(1) + 13, posY - 50,Graphics.LEFT | Graphics.TOP,g) ;

			if (startDisplayMessageChat < InfoPlaying.currentChatMessageString.length())
				font.drawString(InfoPlaying.currentChatMessageString.substring(0,startDisplayMessageChat), posX + 10 + fonts[FONT_BLACK].getWidth(InfoPlaying.currentChatMessageString.substring(startDisplayMessageChat))/2, posY - 38, Graphics.LEFT | Graphics.TOP, g);
			else
				font.drawString(InfoPlaying.currentChatMessageString, posX + 10, posY - 38, Graphics.LEFT | Graphics.TOP, g);

			startDisplayMessageChat++;
		} else {
			startDisplayMessageChat = -1;
		}
	}

	public void updateWattingPlayers(byte message, Graphics g)
	{
		switch (message) {
		case MESSAGE_CONSTRUCTOR:
			if(talkSprite == null){
				loadTalkSprite();
			}
			if (avatarSprite == null) {
				loadAvatarSprite();
			}
			if (inputBox == null) {
				inputBox = new InputBox(Package.getText(TEXT.INGAME_ENTER_TO_CHAT), "", TextField.ANY, POPUP_INPUT_BOX_OFFSET_X,
						POPUP_INPUT_BOX_OFFSET_Y, getScreenWidth() - (POPUP_INPUT_BOX_OFFSET_X << 1), fonts[FONT_NORMAL].getHeight() + 8);
				inputBox.setFonts(fonts[FONT_BOLD], fonts[FONT_NORMAL]);
				inputBox.setColors(POPUP_INPUT_BOX_BACKGROUND_COLOR, POPUP_INPUT_BOX_TITLE_COLOR, TEXTFIELD_BACKGROUND_COLOR, TEXTFIELD_FOCUS_BACKGROUND_COLOR, TEXTFIELD_BORDER_COLOR);
			}
			setSoftKeys(SOFTKEY_MENU, SOFTKEY_OK, SOFTKEY_BACK);
			break;

		case MESSAGE_UPDATE:
		#ifndef USE_REDWAF_SERVER
			String updateString = Game.connection.getUpdate();
			// DBG("updateString: " + updateString);
			if(updateString != null) {
		#ifndef USE_DEAL_CARD_FROM_SERVER
				Game.connection.processUpdateString(updateString);
		#else
				if(!updateString.startsWith(Game.connection.DEAL_CARD)) {
					Game.connection.processUpdateString(updateString);
				} else {
					byte[] listCardFromCLientOne = Game.connection.changeStringToListCard(updateString.substring(Game.connection.DEAL_CARD.length()));
					Vector cards  	= Deck.getCards(listCardFromCLientOne);
					GameBoard.mainPlayer 	=  new Player(cards);
					GameBoard.mainPlayer.sortByRank();
					setGameState(STATE_GAME_PLAY);
					beginCountTime = true;
				}
		#endif
			}
		#endif

			if(subState == -1) {
				//setSoftKeys(SOFTKEY_MENU, SOFTKEY_OK, SOFTKEY_BACK);
			if (LEFT_SOFT_KEY) {
				// setGameState(STATE_WAITING_PLAY_POPUP_MENU);
				// init when change to InGame Popup Menu
				subState = SUBSTATE_WAITING_PLAY_PP_MENU;
				loadMenuPopupItems(SUBSTATE_WAITING_PLAY_PP_MENU);
				if (this.menuPopup != null)
					this.menuPopup = null;
				this.menuPopup = new MenuPopup(this.menuPopupItems, fonts[FONT_NORMAL], getScreenHeight() - POPUP_MENU_OFFSET_Y,
						getScreenWidth(), getScreenHeight());
				setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_CLOSE);
				
			} else if (RIGHT_SOFT_KEY) {
				#ifdef USE_REDWAF_SERVER
					if(commmandClient != null){
					try{
						commmandClient.leaveTable(InfoPlaying.idRoom,itemSelection);
					}catch(Exception e) {e.printStackTrace();}
					}
					else {DBG("commmandClient null");}
				#else
					connection.outTable();
				#endif
				setGameState(STATE_TABLE_LIST);
			} else if (KEY_FIRE) {
				if((InfoPlaying.isHostRoom)&&(getNumberUserinRoom() > 1)) {
#ifdef USE_DEAL_CARD_FROM_SERVER
					byte[] listCardFromServer = Game.connection.sentStartGame();
					DBG("doc them 1 du lieu "+connection.getUpdate());
					Vector cards  	= Deck.getCards(listCardFromServer);
					gameBoard.mainPlayer =  new Player(cards);
					gameBoard.mainPlayer.sortByRank();
#endif
					setGameState(STATE_GAME_PLAY);
					}
				} else if (KEY_ENTER_MESSAGE_INGAME) {
					subState = STATE_INGAME_POPUP_ENTER_CHAT;
					#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
					offScreenTransparentImage = createTransparentImage(TRANSPARENT_IMAGE_WIDTH,
						TRANSPARENT_IMAGE_HEIGHT, TRANSPARENT_IMAGE_COLOR, TRANSPARENT_IMAGE_ALPHA);
					#endif
				}
			} else if (subState ==STATE_INGAME_POPUP_ENTER_CHAT) {
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_OK, SOFTKEY_NONE);
				if (ANY_KEY_PRESSED) {
					inputBox.updateInputBox(keyCode);
				}

				if (KEY_FIRE) {
					DBG("Input message: " + inputBox.getString());
					InfoPlaying.currentChatMessageString  = inputBox.getString();
					startDisplayMessageChat = 0;
					InfoPlaying.currentPlayerTalkPos = 0;
					Game.connection.sendChatMessage(userName +":"+inputBox.getString());
					inputBox.setString("");
					subState = -1;
				}
			} else if (subState==SUBSTATE_WAITING_PLAY_PP_MENU
						|| subState==SUBSTATE_SETTING_PP_MENU
				)  {
				// Update In Game Menu
				updateIngameMenu(MESSAGE_UPDATE, g);
			}
			break;	//case MESSAGE_UPDATE

		case MESSAGE_PAINT:
			paintWattingPlayersScreen(g);
			if (subState ==STATE_INGAME_POPUP_ENTER_CHAT) {
			#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
				fillRect(offScreenTransparentImage, 0, 0, getScreenWidth(), getScreenHeight(), g);
			#endif
				inputBox.paintInputBox(g);
			}
			if (subState == SUBSTATE_WAITING_PLAY_PP_MENU
					|| subState==SUBSTATE_SETTING_PP_MENU
			)
				this.menuPopup.paintMenu(g);
			break;

		case MESSAGE_DESTRUCTOR:
			inputBox = null;
			#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
			offScreenTransparentImage = null;
			#endif
			break;
		}	//switch (message)
	}

	public void drawAvatarPlayer(Graphics g)
	{
		int pos = Game.getPositionInTable();
		int x, y;
		// Draw friend avatars
		for (int i = 1; i < 4; i++) {
			int name = i + pos;
			if(name > 3) { name-=4;}
			x = PLAYERS_POS[i][0];
			y = PLAYERS_POS[i][1];

			if (!InfoPlaying.arrayNamePlayer[name].equals("None")) {
				avatarSprite.paintFrame(i, x, y, Graphics.HCENTER | Graphics.VCENTER, g);
				fonts[FONT_NORMAL].drawString(InfoPlaying.arrayNamePlayer[name], x, y + 30, Graphics.HCENTER | Graphics.HCENTER, g);
				if(name == currentPlayerHostRoom)
					fonts[FONT_BIG].drawString( "MAIN", x, y + 10, Graphics.HCENTER | Graphics.TOP, g);
			}
		}
		// draw main character
		try{
		fonts[FONT_NORMAL].drawString(InfoPlaying.arrayNamePlayer[pos], PLAYERS_POS[0][0], PLAYERS_POS[0][1] + 30, Graphics.HCENTER | Graphics.HCENTER, g);
		}catch(Exception e) {}
		avatarSprite.paintFrame(0, PLAYERS_POS[0][0], PLAYERS_POS[0][1], Graphics.HCENTER | Graphics.VCENTER, g);
		if (InfoPlaying.isHostRoom)
			fonts[FONT_BIG].drawString( "MAIN", PLAYERS_POS[0][0], PLAYERS_POS[0][1]+10, Graphics.HCENTER | Graphics.TOP, g);
	}

	public void paintWattingPlayersScreen(Graphics g)
	{
		drawGradientBackground(g);
		fonts[FONT_NORMAL].drawString("Room:"+InfoPlaying.idRoom+"  Table"+InfoPlaying.idTable, 120, 150, Graphics.HCENTER | Graphics.HCENTER, g);
		fonts[FONT_NORMAL].drawString("Gold put:"+InfoPlaying.gold, 120, 170, Graphics.HCENTER | Graphics.HCENTER, g);
		//#ifndef USE_REDWAF_SERVER
		drawAvatarPlayer(g);
		paintChatLogMessage(fonts[FONT_BLACK],g);
		//#endif
		//paint start button
		if (InfoPlaying.isHostRoom
		//not reset softkey in menu popup state
		&& (currentState < STATE_MAIN_MENU_POPUP_MENU || currentState >STATE_SETTING_POPUP_SETTING_GOLD)
		) {
			if(Game.getNumberUserinRoom() == 1)
				//fonts[FONT_NORMAL].drawString("Please waiting", 120, 220, Graphics.HCENTER | Graphics.HCENTER, g);
				setSoftKeys(SOFTKEY_MENU, SOFTKEY_NONE, SOFTKEY_BACK);
			else if(Game.getNumberUserinRoom() > 1)
				//fonts[FONT_NORMAL].drawString("START", 120, 220, Graphics.HCENTER | Graphics.HCENTER, g);
				setSoftKeys(SOFTKEY_MENU, SOFTKEY_OK, SOFTKEY_BACK);
		}
	}
