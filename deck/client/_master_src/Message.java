
import javax.microedition.lcdui.Graphics;

/**
 * A object save the information of message from friends and other players.
 * Created: March 05, 2011
 */
 
public class Message {

	public static final int MESSAGE_MAX_CHARACTER = 30;
	
	// define variable
	private String text;
	private String peopleName;
	
	// Constructer Function
	public Message (String peopleName, String text)
	{
		this.text = text;
		this.peopleName = peopleName;
	}
	
	public String getPeopleName()
	{
		return this.peopleName;
	}
	
	public String getMessage()
	{
		return this.text;
	}
}