
public interface IData
{
	// Fonts
	#include "font_normal.h"
	#include "font_bold.h"
	#include "font_black.h"
	#include "font_money.h"
	#include "font_big.h"

	// Common sprite
	#include "common_sprite.h"
	// Font sprite
	#include "font_sprite.h"
	// Menu sprite
	#include "menu_sprite.h"
	// Card sprite & Rank suite sprite
	#include "card_sprite.h"
	// Avatars sprite
	#include "avatars_sprite.h"
	// Menupopup icons sprite
	#include "menupopup_icons_sprite.h"
	// Sound
	#include "sound.h"
	// Text	
	#include "text_en.h"
	#include "text_vi.h"

	//
	// Common sprite
	//
	// Common item sprite
	public static final int COMMON_ITEM_ITEM_SPRITE_FRAMES_WIDTH		= 20;
	public static final int COMMON_ITEM_ITEM_SPRITE_FRAMES_HEIGHT		= 20;
	public static final int COMMON_ITEM_ITEM_SPRITE_FRAMES_COUNT		= 11;
	public static final int[] LOADING_ANIM_FRAMES 						= {1, 2, 3, 4, 5, 6};
	public static final int[] LOADING_ANIM_DELAYS 						= {2, 2, 2, 2, 2, 2};
	public static final int COMMON_ITEM_ITEM_SPRITE_FRAME_ROOM			= 7;
	public static final int COMMON_ITEM_ITEM_SPRITE_FRAME_STATUS		= 8;

	// Main menu icons sprite
	public static final byte MAIN_MENU_ICONS_FRAMES_WIDTH				= 32;
	public static final byte MAIN_MENU_ICONS_FRAMES_HEIGHT				= 32;
	public static final byte MAIN_MENU_ICONS_FRAMES_COUNT				= 18;

	// Card sprite
	public static final int CARD_SPRITE_FRAMES_COUNT 					= 2;
	public static final int CARD_SPRITE_FRAMES_WIDTH 					= 46;
	public static final int CARD_SPRITE_FRAMES_HEIGHT 					= 66;
	public static final int CARD_SPRITE_CLOSED_FRAME_ID					= 0;
	public static final int CARD_SPRITE_WHITE_FRAME_ID 					= 1;

	// Rank suite sprite
	public static final int[] RANK_SUIT_SPRITE_FRAME_OFFSETS_X = {
		0, 6, 12, 18, 24, 30, 36, 42, 50, 56, 62, 68, 74,
		80, 86, 92,  98, 104, 110, 116, 122, 130, 136, 142, 148, 154,
		160, 167, 174, 181, 188, 198, 210, 220
	};
	public static final int[] RANK_SUIT_SPRITE_FRAMES_HEIGHT = {
		9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
		9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
		8, 8, 8, 8, 12, 12, 12, 12
	};

	/* //----------------------------------------------------------------------------------------------------
	// Mapping rank suit on card (Left -> Right, Top -> Down)
	public static final int ONE_QUATERS_CARD_W	= CARD_SPRITE_FRAMES_WIDTH >> 2;	// One quarters of card width
	public static final int ONE_THIRD_CARD_W	= CARD_SPRITE_FRAMES_WIDTH / 3;		// One third of card width
	public static final int HALF_CARD_W			= CARD_SPRITE_FRAMES_WIDTH >> 1;		// One third of card width
	public static final int ONE_SIXTH_CARD_H	= CARD_SPRITE_FRAMES_HEIGHT / 6;	// One fifth of card height
	public static final int ONE_FIFTH_CARD_H	= CARD_SPRITE_FRAMES_HEIGHT / 5;	// One fifth of card height
	public static final int ONE_QUATERS_CARD_H	= CARD_SPRITE_FRAMES_HEIGHT >> 2;	// One quarters of card height
	public static final int ONE_THIRD_CARD_H	= CARD_SPRITE_FRAMES_HEIGHT / 3;	// One third of card height
	public static final int HALF_CARD_H	= CARD_SPRITE_FRAMES_HEIGHT / 3;	// One third of card height

	public static final byte[][] CARD_SUITE_2		= {
		{HALF_CARD_W, ONE_THIRD_CARD_H} ,
		{HALF_CARD_W, CARD_SPRITE_FRAMES_HEIGHT - ONE_THIRD_CARD_H}
	};
	public static final byte[][] CARD_SUITE_3		= {
		{HALF_CARD_W, ONE_SIXTH_CARD_H} , {HALF_CARD_W ,HALF_CARD_H},
		{HALF_CARD_W, CARD_SPRITE_FRAMES_HEIGHT - ONE_SIXTH_CARD_H},
		{HALF_CARD_W, ONE_SIXTH_CARD_H} , {HALF_CARD_W ,HALF_CARD_H}
	};
	public static final byte[][] CARD_SUITE_4		= {
		{ONE_QUATERS_CARD_W, ONE_SIXTH_CARD_H} ,
		{CARD_SPRITE_FRAMES_WIDTH - HALF_CARD_W, CARD_SPRITE_FRAMES_HEIGHT - ONE_SIXTH_CARD_H},
		{ONE_QUATERS_CARD_W, CARD_SPRITE_FRAMES_HEIGHT - ONE_SIXTH_CARD_H},
		{CARD_SPRITE_FRAMES_WIDTH - ONE_QUATERS_CARD_W, CARD_SPRITE_FRAMES_HEIGHT - ONE_SIXTH_CARD_H}
	};
	//---------------------------------------------------------------------------------------------------- */

	// Avatar sprite
	public static final int AVATAR_SPRITE_FRAMES_COUNT					= 9;
	public static final int AVATAR_SPRITE_FRAMES_WIDTH					= 24;

	// MenuPopup Icons sprite
	public static final int MENUPOPUP_ICONS_FRAMES_COUNT					= 6;
	public static final int MENUPOPUP_ICONS_FRAMES_WIDTH					= 50;

	// Items sprite
	public static final int[] ITEM_SPRITE_FRAMES_OFFSETS_X				= {0, 11};
	public static final int[] ITEM_SPRITE_FRAMES_HEIGHT					= {14, 12};

	// Talk sprite
	public static final int[] TALK_SPRITE_FRAMES_OFFSETS_X				= {0, 13, 17};
	public static final int[] TALK_SPRITE_FRAMES_HEIGHT					= {30, 30, 30};

	// Table sprite
	public static final int[] TABLE_LIST_SPRITE_FRAMES_OFFSETS_X		= {0};
	public static final int[] TABLE_LIST_SPRITE_FRAMES_HEIGHT			= {46};
}
