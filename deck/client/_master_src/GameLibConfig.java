
public interface GameLibConfig {

	// Touch screen
#ifndef USE_TOUCH_SCREEN
	public static boolean useTouchScreen			= false;
#else
	public static boolean useTouchScreen			= true;
#endif

	// Screen width & screen height
	public static boolean setScreenSize 			= false;	// Set true if want define screen size
	public static int screenWidth 					= 320;
	public static int screenHeight					= 240;

	public static boolean useSleepAfterEachFrame 	= true;
	public static int sleepTime 					= 20;

	// Sound
	public static boolean useCachedPlayers			= true;
	public static boolean usePrefetchedPlayers		= true;		// Set true when useCachedPlayers only (call realize() & prefetch() while cache)

	// Software double buffering
	public static boolean useDoubleBuffering		= !true;

	// Transition effects
	public static boolean useTransitionEffect		= true;		// Create a temporary buffering if not set useDoubleBuffering
	public static int transitionSpeed				= 60;		// Speed, should be a 2^n number

	public static boolean useServiceRepaints		= true;		// Call serviceRepaints() method after repaint() method
}