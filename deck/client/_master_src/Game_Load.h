
	///*********************************************************************
	///* Game_Load.h
	///*********************************************************************

	/**
	 * Load fonts & text resources before SOUND_CONFIRM_SCREEN.
	 */
	public static void loadFontSprites()
	{
		DBG("Load font sprites...");
		Package.open(FONT_PACKAGE);

		fonts = new FontSprite[FONT_MONEY + 1];

		fonts[FONT_NORMAL] = Package.loadFontSprite(FONT_NORMAL_ID, FONT_NORMAL_CHARS,
				FONT_NORMAL_CHAR_OFFSETS, FONT_NORMAL_SPACE_CHAR_WIDTH, true);
		fonts[FONT_NORMAL].setExpandedSpacing(FONT_EXPANDED_SPACING);

		fonts[FONT_BOLD] = Package.loadFontSprite(FONT_BOLD_ID, FONT_BOLD_CHARS,
				FONT_BOLD_CHAR_OFFSETS, FONT_BOLD_SPACE_CHAR_WIDTH, true);
		fonts[FONT_BOLD].setExpandedSpacing(FONT_EXPANDED_SPACING);

		fonts[FONT_BLACK] = Package.loadFontSprite(FONT_BLACK_ID, FONT_BLACK_CHARS,
				FONT_BLACK_CHAR_OFFSETS, FONT_BLACK_SPACE_CHAR_WIDTH, true);
		fonts[FONT_BLACK].setExpandedSpacing(FONT_EXPANDED_SPACING);

		fonts[FONT_BIG] = Package.loadFontSprite(FONT_BIG_ID, FONT_BIG_CHARS,
				FONT_BIG_CHAR_OFFSETS, FONT_BIG_SPACE_CHAR_WIDTH, true);
		fonts[FONT_BIG].setExpandedSpacing(FONT_EXPANDED_SPACING);

		fonts[FONT_MONEY] = Package.loadFontSprite(FONT_MONEY_ID, FONT_MONEY_CHARS,
				FONT_MONEY_CHAR_OFFSETS, FONT_MONEY_SPACE_CHAR_WIDTH, true);
		fonts[FONT_MONEY].setExpandedSpacing(FONT_EXPANDED_SPACING);

		Package.close();

		// Set font pallets
		GameLib.initializeFonts(fonts);
	}

	public static void loadText()
	{
		DBG("Load text...");
		Settings.load();	// Load settings from Record store
		if (Settings.getSelectedLanguage() == 0) {
			Package.open(IData.TEXT_EN_PACKAGE);
			Package.loadTextPackage(IData.COMMON_EN_ID);
		} else {
			Package.open(IData.TEXT_VI_PACKAGE);
			Package.loadTextPackage(IData.COMMON_VI_ID);
		}
		Package.close();
	}

	public static void loadLogoImage()
	{
		DBG("Load logo image...");
		Package.open(COMMON_SPRITE_PACKAGE);
		logoImage = Package.getImage(LOGO_IMAGE_ID);
		Package.close();
	}

	public static void loadCommonItemSprite()
	{
		Package.open(COMMON_SPRITE_PACKAGE);
		commonItemSprite = Package.loadSprite(COMMON_ITEM_SPRITE_ID);
		commonItemSprite.setFrameSize(COMMON_ITEM_ITEM_SPRITE_FRAMES_WIDTH,
				COMMON_ITEM_ITEM_SPRITE_FRAMES_HEIGHT, COMMON_ITEM_ITEM_SPRITE_FRAMES_COUNT);
		commonItemSprite.buildCache();

		backgroundImage = Package.getImage(BACKGROUND_IMAGE_ID);

		Package.close();

		// Set loading animation
		commonItemSprite.setAnimation(LOADING_ANIM_FRAMES, LOADING_ANIM_DELAYS);
	}

	public static void loadSplashImage()
	{
		DBG("Load splash image...");
		Package.open(COMMON_SPRITE_PACKAGE);
		splashImage = Package.getImage(SPLASH_IMAGE_ID);
		Package.close();
	}

	public void loadAvatarSprite()
	{
		// Avatar sprite
		Package.open(AVATAR_SPRITE_PACKAGE);
		avatarSprite = Package.loadSprite(AVATARS_SPRITE_ID);
		avatarSprite.setFrameSize(AVATAR_SPRITE_FRAMES_WIDTH, avatarSprite.getHeight(), AVATAR_SPRITE_FRAMES_COUNT);
		avatarSprite.buildCache();
		Package.close();
	}

	public void loadMenuPopupIconsSprite()
	{
		// Menupopup Icons sprite
		Package.open(MENUPOPUP_ICONS_PACKAGE);
		popupIconsSprite = Package.loadSprite(MENUPOPUP_ICONS_ID);
		popupIconsSprite.setFrameSize(MENUPOPUP_ICONS_FRAMES_WIDTH, popupIconsSprite.getHeight(), MENUPOPUP_ICONS_FRAMES_COUNT);
		popupIconsSprite.buildCache();
		Package.close();
	}

	public void loadCardSprite()
	{
		Package.open(CARD_SPRITE_PACKAGE);
		// Card sprite
		cardSprite = Package.loadSprite(CARD_SPRITE_ID);
		cardSprite.setFrameSize(CARD_SPRITE_FRAMES_WIDTH, CARD_SPRITE_FRAMES_HEIGHT, CARD_SPRITE_FRAMES_COUNT);
		cardSprite.buildCache();
		// Rank suite sprite
		rankSuitSprite = Package.loadSprite(RANK_SUIT_SPRITE_ID);
		rankSuitSprite.setFrameXOffsets(RANK_SUIT_SPRITE_FRAME_OFFSETS_X);
		rankSuitSprite.setFramesHeight(RANK_SUIT_SPRITE_FRAMES_HEIGHT);
		rankSuitSprite.buildCache();
		Package.close();
		// Item sprite
		// TODO: Move to common item sprite in later)
		Package.open(IData.COMMON_SPRITE_PACKAGE);
		itemSprite = Package.loadSprite(IData.ITEM_SPRITE_ID);
		itemSprite.setFrameXOffsets(ITEM_SPRITE_FRAMES_OFFSETS_X);
		itemSprite.setFramesHeight(ITEM_SPRITE_FRAMES_HEIGHT);
		itemSprite.buildCache();
		Package.close();
	}

	// load talk sprite
	public void loadTalkSprite()
	{
		Package.open(IData.COMMON_SPRITE_PACKAGE);
		talkSprite = Package.loadSprite(IData.TALK_ID);
		talkSprite.setFrameXOffsets(TALK_SPRITE_FRAMES_OFFSETS_X);
		talkSprite.setFramesHeight(TALK_SPRITE_FRAMES_HEIGHT);
		talkSprite.buildCache();
		Package.close();
	}
