
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordEnumeration;

/*
 * Load/Save settings from Record store.
 */
public class Settings {

	public static final String RECORD_STORE_NAME = "DECK";

	private static RecordStore recordStore;
	private static int selectedLanguage = 0;
	private static String userName = "", password = "";

	public static void load() {

		DBG("[Settings] Load saved settings...");
		try {						// Try to open
			recordStore = RecordStore.openRecordStore(RECORD_STORE_NAME, false);

			// Read the record
			byte[] data = recordStore.getRecord(1);
			ByteArrayInputStream bytes = new ByteArrayInputStream(data);
			DataInputStream input = new DataInputStream(bytes);

			selectedLanguage = input.readInt();
			userName = input.readUTF();
			input.readBoolean();
			password = input.readUTF();

			recordStore.closeRecordStore();

		} catch (Exception e) {		// If error, initialize record
			e.printStackTrace();
			save();
		}
	}

	public static void setSelectedLanguage(int _selectedLanguage) {

		DBG("[Settings] Set selectd language: " + _selectedLanguage);
		selectedLanguage = _selectedLanguage;
	}

	public static void setSettings(int _selectedLanguage, String _userName, String _password) {

		selectedLanguage = _selectedLanguage;
		userName = _userName;
		password = _password;
	}

	public static void save() {

		DBG("[Settings] Save settings...");
		try {
			// Generate data
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			DataOutputStream output = new DataOutputStream(bytes);

			output.writeInt(selectedLanguage);
			output.writeUTF(userName);		// Username
			output.writeBoolean(true);		// Split
			output.writeUTF(userName);		// Username
			output.flush();

			byte[] data = bytes.toByteArray();

			// Save into record store
			recordStore = RecordStore.openRecordStore(RECORD_STORE_NAME, true);
			RecordEnumeration records = recordStore.enumerateRecords(null, null, false);

    		if (records.numRecords() == 0)							// If the record store is emty
    		{
    			recordStore.addRecord(data, 0, data.length);	// Add new record
    		}
    		else													// Else, update the record
    		{
    			int recordId = records.nextRecordId();
    			recordStore.setRecord(recordId, data, 0, data.length);
    		}

			recordStore.closeRecordStore();

			output.close();
			bytes.close();
			output = null;
			bytes = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int getSelectedLanguage() {

		return selectedLanguage;
	}

	public static String getUserName() {

		return userName;
	}

	public static String getPassword() {

		return password;
	}
}