
import java.util.Vector;
import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;

public class Game extends GameLib implements IGameConfig, IData, IStates
{
	private static final int FONT_NORMAL			= 0;
	private static final int FONT_BOLD				= 1;
	private static final int FONT_BLACK				= 2;
	private static final int FONT_BIG				= 3;
	private static final int FONT_MONEY				= 4;

	public static SoundPlayer soundPlayer;
	#ifdef USE_REDWAF_SERVER
	public CommandClient commmandClient;
	#endif
	public static NetworkConnection connection;
	
	private static byte lastState, currentState, nextState, subState;
	private static boolean isExitCurrentState, isEnterNextState;
	private static int selectedItem;		// Selected text id for MessageBox, Menu,...
	private static FontSprite[] fonts;
	private static CSprite commonItemSprite;
	private static CSprite cardSprite, rankSuitSprite, avatarSprite, popupIconsSprite;
	private static Image backgroundImage;

	private static Player[] players;			// Keep all players information in game play
	private static MultiChatBox multiChatBox;

#ifdef USE_TRANSPARENT_EFFECT_WHILE_POPUP
	private static Image offScreenTransparentImage;
#endif
	private static MessageBox messageBox;
	private static InputBox inputBox;
	private static String[] messageText;		// Stores multiple lines text
	public static String userNameMainPlayer;  // bien tam, chua hieu vi sao userName bi null
	

	#include "Game_Load.h"
	#include "Game_Paint.h"
	#include "Game_LogoScreen.h"
	#include "Game_ConfirmScreen.h"
	#include "Game_Menu.h"
	#include "Game_MenuPopup.h"
	#include "Game_AboutScreen.h"
	#include "Game_LoginScreen.h"
	#include "Game_Ingame.h"
	#include "Game_IngameMenu.h"
	#include "Game_JoinRoom.h"
	#include "Game_TableList.h"
#ifdef ADD_WAIT_PLAYER_STATE
	#include "Game_WattingPlayers.h"
#endif
	#include "Game_ChatScreen.h"
	#include "Game_FriendListScreen.h"
	#include "Game_CommonFuctions.h"
#ifdef USE_TOUCH_SCREEN
	#include "Game_Touch.h"
#endif

	public Game(MIDlet midlet)
	{
		super(midlet);
		isExitCurrentState = false;
		isEnterNextState = true;
	}

	/**
	 * Create a game thread and start it.
	 */
	public void start()
	{
		Package.setMIDlet(midlet);
	#ifndef START_GAME_AT_A_STATE
		setGameState(STATE_LOGO_SCREEN);
	#else	////#ifndef GAME_START_AT_MAIN_MENU
		DBG("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		DBG("Start game at a manual state: START_STATE...");
		DBG("Undefine START_GAME_AT_A_STATE to start game at logo screen.");
		DBG("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		loadFontSprites();
		loadText();
		loadCommonItemSprite();
		setGameState(START_STATE);
	#endif	////#ifndef GAME_START_AT_MAIN_MENU

		// TODO @Hien: Connect at here make game is slowed while start, should move to login screen
		#ifdef USE_REDWAF_SERVER 		
		CommandClient commmandClient = new CommandClient();
		// try{
		// commmandClient.handleInputMessage("1");
		// }catch(Exception e) {System.out.println("loi ket noi server"+e);}
		#endif
		connection = new NetworkConnection();
		Thread gameThread = new Thread(this);
		gameThread.start();
	}

	public static void setGameState(byte state)
	{
		nextState = state;
		// TODO Review subState variable, still use or remove it
		subState = -1;
		isExitCurrentState = true;
		DBG("SET GAME STATE: " + currentState + " -> " + state);
	}

	public void sendGameMessage(byte message)
	{
		switch(currentState) {
			case STATE_LOGO_SCREEN:
				updateLogoScreen(message, graphics);
				break;
			case STATE_SOUND_CONFIRM:
				updateConfirmScreen(SOUND_CONFIRM_SCREEN, message, graphics);
				break;
			case STATE_SPLASH_SCREEN:
				updateSplashScreen(message, graphics);
				break;
			case STATE_MAIN_MENU:
				updateMenu(MENU_MAIN, message, graphics);
				break;
			case STATE_OPTIONS:
				updateMenu(MENU_OPTIONS, message, graphics);
				break;
			case STATE_HELP:
				updateHelpScreen(message, graphics);
				break;
			case STATE_ABOUT:
				updateAboutScreen(message, graphics);
				break;
			case STATE_LOGIN_SCREEN:
				updateAccountInputScreen(LOGIN_SCREEN, message, graphics);
				break;
			case STATE_LOGIN_CHECK_ACCOUNT:
				updateAccountCheckingScreen(CHECK_LOGIN_ACCOUNT_SCREEN, message, graphics);
				break;
			case STATE_LOGIN_INFO_MESSAGE:
				updateAccountCheckingInfoScreen(LOGIN_MESSAGE_INFO_SCREEN, message, graphics);
				break;
			case STATE_REGISTER_SCREEN:
				updateAccountInputScreen(REGISTER_SCREEN, message, graphics);
				break;
			case STATE_REGISTER_CHECK_ACCOUNT:
				updateAccountCheckingScreen(CHECK_REGISTER_ACCOUNT_SCREEN, message, graphics);
				break;
			case STATE_REGISTER_INFO_MESSAGE:
				updateAccountCheckingInfoScreen(REGISTER_MESSAGE_INFO_SCREEN, message, graphics);
				break;
			case STATE_GAME_PLAY:
				updateIngame(message, graphics);
				break;
			case STATE_INGAME_MENU:
				updateMenu(MENU_INGAME, message, graphics);
				break;
			case STATE_EXIT_CONFIRM:
				updateConfirmScreen(EXIT_GAME_CONFIRM_SCREEN, message, graphics);
				break;
			case STATE_EXIT_TO_MAIN_MENU:
				updateConfirmScreen(EXIT_TO_MAIN_MENU_CONFIRM_SCREEN, message, graphics);
				break;
			case STATE_JOIN_ROOM:
				updateJoinRoomScreen(message, graphics);
				break;
			case STATE_TABLE_LIST:
				updateTableListScreen(message, graphics);
				break;
		#ifdef ADD_WAIT_PLAYER_STATE
			case STATE_TABLE_WATTING_PLAYERS:
				updateWattingPlayers(message, graphics);
				break;
		#endif
			case STATE_MAIN_MENU_POPUP_MENU:
				updateMenuPopup(STATE_MAIN_MENU_POPUP_MENU, message, graphics);
				break;
			// case STATE_INGAME_POPUP_MENU:
				// updateMenuPopup(STATE_INGAME_POPUP_MENU, message, graphics);
				// break;
			// case STATE_WAITING_PLAY_POPUP_MENU:
				// updateMenuPopup(STATE_WAITING_PLAY_POPUP_MENU, message, graphics);
				// break;
			case STATE_LOGIN_POPUP_MENU:
				updateMenuPopup(STATE_LOGIN_POPUP_MENU, message, graphics);
				break;
			// case STATE_SETTING_POPUP_MENU:
				// updateMenuPopup(STATE_SETTING_POPUP_MENU, message, graphics);
				// break;
			case STATE_SETTING_POPUP_SETTING_GOLD:
				updateMenuPopup(STATE_SETTING_POPUP_SETTING_GOLD, message, graphics);
				break;
			case STATE_CHAT_SCREEN:
				updateChatScreen(STATE_CHAT_SCREEN, message, graphics);
				break;
			case STATE_FRIENDLIST_SCREEN:
				updateFriendListScreen(STATE_FRIENDLIST_SCREEN, message, graphics);
				break;
		}
	}

	// TODO	Optimize transition effect
#ifdef USE_TWO_BUFFERS_TRANSITION
	public void createLastScreenBuffer()
	{
		if (currentState > STATE_LOADING) {
			lastScreenBuffer = Image.createImage(getScreenWidth(), getScreenHeight());
			Graphics g = lastScreenBuffer.getGraphics();
			graphics = g;
			sendGameMessage(MESSAGE_PAINT);
			g = null;
			graphics = null;	// Set is null to reset graphics variable in GameLib
		}
	}
#endif

	// @Override
	public void gameUpdate() throws Exception
	{
		if (isExitCurrentState) {
		#ifdef USE_TWO_BUFFERS_TRANSITION
			createLastScreenBuffer();	// Save current screen buffer to improve transition effect
		#endif
			sendGameMessage(MESSAGE_DESTRUCTOR);

			isExitCurrentState = false;
			isEnterNextState = true;
			lastState = currentState;
			currentState = nextState;
		} else if (isEnterNextState) {
			sendGameMessage(MESSAGE_CONSTRUCTOR);
			isEnterNextState = false;
			frameCounter = 0;
		} else {
			// if (commmandClient!=null)
				// commmandClient.handleInputMessage("");
			sendGameMessage(MESSAGE_UPDATE);
			sendGameMessage(MESSAGE_PAINT);
			paintSoftKeys(graphics);
		}
	}
}
