
@echo off

echo ###
echo # SETTING SOME COMMON VARIABLES...
echo ###

call config.bat

echo.
echo Done...
echo.

set COMPILE_MODE=%1%
if "%COMPILE_MODE%"=="" (
	set COMPILE_MODE=debug
)

echo ###
echo # BUILDING %PROJECT_FULL_NAME%...
echo ###
echo Build mode = %COMPILE_MODE%
ant %COMPILE_MODE%
