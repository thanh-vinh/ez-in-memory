/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50508
Source Host           : localhost:3306
Source Database       : deskonline

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2011-02-21 00:02:02
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `tbl_account`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_account`;
CREATE TABLE `tbl_account` (
  `fld_Acount_ID` int(11) NOT NULL,
  `fld_UserName` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  `fld_PassWord` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  PRIMARY KEY (`fld_Acount_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_sinhala_ci;

-- ----------------------------
-- Records of tbl_account
-- ----------------------------
INSERT INTO `tbl_account` VALUES ('1', 'thao', 'abcd');
INSERT INTO `tbl_account` VALUES ('2', 'hien.ph', 'daicaca');
INSERT INTO `tbl_account` VALUES ('3', 'hoang.nv', '0101');
INSERT INTO `tbl_account` VALUES ('4', 'vinh.nt', '0202');

-- ----------------------------
-- Table structure for `tbl_joinroom`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_joinroom`;
CREATE TABLE `tbl_joinroom` (
  `ID` varchar(50) COLLATE utf8mb4_sinhala_ci NOT NULL DEFAULT '',
  `Account` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  `idRoom` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_sinhala_ci;

-- ----------------------------
-- Records of tbl_joinroom
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_playing`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_playing`;
CREATE TABLE `tbl_playing` (
  `id` int(11) NOT NULL DEFAULT '0',
  `Account1` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  `Account2` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  `curStep` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  `curPlayer` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_sinhala_ci;

-- ----------------------------
-- Records of tbl_playing
-- ----------------------------
INSERT INTO `tbl_playing` VALUES ('1', 'daicaca', 'lamgiahien', 'b2', '1');

-- ----------------------------
-- Table structure for `tbl_room`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_room`;
CREATE TABLE `tbl_room` (
  `idRoom` varchar(50) COLLATE utf8mb4_sinhala_ci NOT NULL,
  `roomName` varchar(50) COLLATE utf8mb4_sinhala_ci DEFAULT NULL,
  `maxPlayer` int(11) DEFAULT NULL,
  `curPlayer` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRoom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_sinhala_ci;

-- ----------------------------
-- Records of tbl_room
-- ----------------------------
INSERT INTO `tbl_room` VALUES ('1', 'Server1', '20', '0');

-- ----------------------------
-- Procedure structure for `pcd_login`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pcd_login`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pcd_login`(IN `_user` varchar(50), IN `_pass` varchar(50))
BEGIN
	#Routine body goes here...
	select * from tbl_account where fld_UserName = _user and fld_PassWord = _pass;
END
;;
DELIMITER ;
