
public class Room {
	private int idRoom;
	private int curMember;
	private int maxMember;
	
	public Room(int i){
		idRoom = i;
		curMember = 0;
		maxMember = 40;
	}

	public int getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(int idRoom) {
		this.idRoom = idRoom;
	}

	public int getCurMember() {
		return curMember;
	}

	public void setCurMember(int curMember) {
		this.curMember = curMember;
	}

	public int getMaxMember() {
		return maxMember;
	}

	public void setMaxMember(int maxMember) {
		this.maxMember = maxMember;
	}
	
	public void removeUser(){
		if(curMember > 0)curMember--;
	}
	
	public void addUser(){
		curMember++;
	}
	

}
