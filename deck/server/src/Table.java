
import java.util.HashMap;
import org.apache.log4j.Logger;

public class Table{
	private boolean isFull;
	private	int numberUser;
	private int idUserKey;
	private long money;
	private	int  arrayUser[];
	private Logger log = Logger.getLogger(Table.class);
	
	
	public Table(){
		isFull = false;
		numberUser = 0;
		idUserKey = -1;
		money = 0;
		arrayUser =new int[]{-1,-1,-1,-1};
	}
	
	public int getIdUserKey(){
		return this.idUserKey;
	}
	public void setIdUserKey(int idUserKey){
		this.idUserKey = idUserKey;
	}
	
	public void setMoney(long money){
		this.money = money;
	}
	public long getMoney(){
		return this.money;
	}
	
	/**
	 * Get User.
	 */
	public int getUser(int i){
		return arrayUser[i];
	}
	/**
	 * Get numberUser.
	 */
	public int getNumberUser(){
		return numberUser;
	}
	
	
	/**
	 * Get in from table (Name).
	 */
	
	public String getUserOfTable(){
		String result="";
		for(int i=0 ; i< 4; i++){
			if(arrayUser[i] != -1) result+=Main.getUser(arrayUser[i]).getUsername()+";";
			else result+="None;";
		}
		return result;
	}
	/**
	 * Get first empty position to add User.
	 */
	
	public int getFirstEmpty(){
		int i = 0;
		while(i < 4){
			if(arrayUser[i] == -1) break;
			i++;
		}
		if(i < 4) return i;
		else return -1;

	}
	
	public int getFirstNotEmpty(){
		int i = 0;
		while(i < 4){
			if(arrayUser[i] != -1) break;
			i++;
		}
		if(i < 4) return i;
		else return -1;
	}
	
	/**
	 * Add idUser to this table.
	 */
	public int addUser(int idUser){
		int position= -1;
		if(isFull) return -1;
		else
		{
			position = getFirstEmpty();
			arrayUser[position] = idUser;
			numberUser++;
			if(numberUser == 4) isFull = true;
		}
		return position;
	}
	
	/**
	 * Remove idUser from this table.
	 */
	
	public int removeUser(int idUser){
		int positionRemove = -1; 
		for(int i=0; i< 4; i++){
			if(arrayUser[i] == idUser) {arrayUser[i] = -1; positionRemove = i;}
		}
		if(numberUser >0)
			numberUser--;
		if(numberUser < 4) isFull = false;	
		return positionRemove;
	}
	
	/**
	 * isFull.
	 */
	
	public boolean isFull(){
		return isFull;
	}
	
	/**
	 * isEmpty.
	 */
	public boolean isEmpty(){
		if(numberUser == 0) return true;
		else return false;
	}
	
	public int getIdNextPlayer(int position)
	{
		int j = (position + 1)%4;
		while((this.getUser(j) == -1) && (j != position))
		{
			j++;
			j = j%4;
		}
		return j;
	}
}