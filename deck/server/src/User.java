
import java.util.Vector;
import java.net.*;
import java.sql.*;
import java.io.*;
import org.apache.log4j.Logger;

public class User implements Runnable
{
	// variable for connection	
	private NetworkConnection connection;
	private connnectDataBase connectionData;
	private String lineIn,lineOut;
	private static Logger log = Logger.getLogger(User.class);
	
	// variable for User
	private int idThread;
	private int status;
	private Table table;
	private int idRoom;	
	private String username;
	private Vector userCards;
	private static byte[] vectorSaveCardFromClient = null;
	public int lastPlayerAttack = -1;
	public int position = -1; // position in table (0,1,2,3)
	public boolean isAllowPlaying = false;


	//define parameter for functions
	public static String BEGIN_LOGIN_STRING			= "login,";
	public static String DEAL_CARD					= "deal_Card";
	public static String START_GAME					= "start_Game";
	public static String ATTACH_CARD				= "attach_Card";
	public static String ALLOW_NEXT_PLAYER			= "allow_next_player";
	public static String RESULT_ATTACH_TRUE			= "attach_True";
	public static String RESULT_ATTACH_FALSE		= "attach_False";
	public static String LAST_CARD					= "LAST_CARD";
	public static String IS_ALLOW_PLAY				= "allow_play";
	public static String IS_NOT_ALLOW_PLAY			= "not_allow_play";
	public static String IS_RE_ALLOW_PLAY			= "re_allow_play";
	public static String IS_COMPLETE_CIRCEL			= "IS_COMPLETE_CIRCEL";
	public static String IS_WIN						= "is_win";
	public static String IS_LOSE					= "is_lose";
	public static String RESULT_LOGIN_OK			= "login_Success";
	public static String RESULT_LOGIN_ERROR			= "login_Error";
	public static String MESSAGE_GET_ROOM			= "Get Room";
	public static String MESSAGE_GET_TABLE_IN_ROOM	= "Get Table in Room ";
	public static String MESSAGE_JOIN_TABLE			= "Join Table ";
	public static String MESSAGE_OUT_TABLE			= "Out Table ";
	public static String MESSAGE_FAIL				= "Fail";
	public static String MESSAGE_None				= "None";
	public static String CHAT_MESSAGE				= "Message:";
	public static String MESSAGE_SET_GOLD			= "Set gold:";
	


	/**
	* Contructor. No param and 2 param
	*
	* @param socket 	Socket connection.
	* @param idThread 	Id of User Thread.
	*/
	public User(){
		// connectionData = new connnectDataBase();
		// connectionData.prepareForConnect();
	}

	public User(Socket s, int idThread){
		connection = new NetworkConnection(s);
		this.idThread = idThread;
		this.status = 0;
		this.idRoom = -1;
		this.username="user"+idThread;
		// Create a new connection
		this.connectionData = new connnectDataBase();
		this.connectionData.prepareForConnect();
	}
	/** End Contructor **/
	
	
	/**
	* Run mothed. Will run during thread. (Main method)
	*
	* Function Execute request from client
	* Sequence . Get Request -> Execute -> Send Reponse
	*/
	public void run()
	{
		try
		{
			while(true) // repeat getRequest
			{
				lineIn = connection.getRequest();  // getRequest from client
				if(lineIn.equals("End")) 
					break;
				
				else if(lineIn.equals("")){
					//DBG("chuoi rong chuoi rong");
				}
				
				//Request get Room List
				else if(lineIn.equals(MESSAGE_GET_ROOM)) {
					connection.sendReponse(getConnection().getRoom());
					lineIn = "";
				}
				
				//Request get userName
				else if(lineIn.equals("Get userName")) {
					connection.sendReponse(getUsername());
					lineIn = "";
				}
				
				// Request get Table List in idRoom
				else if(lineIn.startsWith(MESSAGE_GET_TABLE_IN_ROOM)) {
					String table = getConnection().getTable(Integer.parseInt(lineIn.substring(lineIn.length()-1)));
					connection.sendReponse(table);
					lineIn = "";
				}
				
				// Request send message to others client in table
				else if(lineIn.startsWith(CHAT_MESSAGE)){
					connection.sendMessageToUsersInTable(lineIn, this.table, this.position);
					lineIn = "";
				}
				// Request login
				else if(lineIn.startsWith(BEGIN_LOGIN_STRING)) {
					String logined = "";
					logined = userLogin(lineIn);
					getConnection().sendReponse(logined);
					lineIn = "";
				}
				
				//Start Game by Deal Card
				else if(lineIn.equals(START_GAME)) {
					//DBG( ">>>>>>>>>>>>>>>>>>> lineIn.equals(START_GAME) " ) ;					
					sendCardStringToClient();
					//DBG( ">>>>>>>>>>>>>>>>>>> lineIn.equals(START_GAME) end" ) ;
					lineIn ="";
				}
			#ifdef USE_CHECK_CARDS_FROM_SERVER
				else if(lineIn.startsWith(ATTACH_CARD)){
					//DBG(">>>>>>>>>>>>>>>>>>> lineIn.equals(ATTACH_CARD) ");	
					String stringCardsListFromClient = lineIn.substring(ATTACH_CARD.length());
					//DBG("stringCardsListFromClient = " + stringCardsListFromClient);
					sentResultAttachCardsToClient(stringCardsListFromClient);					
					lineIn = "";
					//DBG(">>>>>>>>>>>>>>>>>>> lineIn.equals(ATTACH_CARD) End");					
				}
			#endif
		// bo luot trong inGame
				else if(lineIn.equals(ALLOW_NEXT_PLAYER))			
				{		
					DBG("ALLOW_NEXT_PLAYER");
					int idNextPlayer = this.table.getIdNextPlayer(this.position);				
					connection.sendReponse(ALLOW_NEXT_PLAYER);
					this.isAllowPlaying = false;
					User userNeedSendResult = Main.getUser(table.getUser(idNextPlayer));
					userNeedSendResult.isAllowPlaying = true;
					if(this.lastPlayerAttack == idNextPlayer)
					{
						//userNeedSendResult.getConnection().sendReponse(IS_RE_ALLOW_PLAY);
						connection.sendReponse(IS_COMPLETE_CIRCEL);						
						connection.sendMessageToUsersInTable(IS_COMPLETE_CIRCEL, this.table, this.position);																	
					}
					//else{
						//userNeedSendResult.getConnection().sendReponse(IS_ALLOW_PLAY);
						connection.sendReponse(IS_ALLOW_PLAY+Main.getUser(this.table.getUser(idNextPlayer)).getUsername());
						connection.sendMessageToUsersInTable(IS_ALLOW_PLAY+Main.getUser(this.table.getUser(idNextPlayer)).getUsername(),this.table, this.position);		
					//}
						
				}
				// Request Join to Table
				else if(lineIn.startsWith(MESSAGE_JOIN_TABLE)){
					int idTable = Integer.parseInt(lineIn.substring(11));
					joinTable(idTable);
					connection.newMemberEven(this.position, this.username, this.table);
					lineIn = "";
				}
				
				// Request Out Table
				else if(lineIn.equals(MESSAGE_OUT_TABLE)){
					testAllowPlayOutTable();
					outTable();					
					lineIn = "";
				}
				
				// Request Set gold in table
				else if(lineIn.startsWith(MESSAGE_SET_GOLD)){
					DBG("MESSAGE_SET_GOLD="+lineIn.substring(MESSAGE_SET_GOLD.length()));
					this.table.setMoney(Long.parseLong(lineIn.substring(MESSAGE_SET_GOLD.length())));
					lineIn = "";
				}
				Thread.sleep(100);

			} // end while

			this.connection.close();
		}
		catch(Exception e){
			if(status == 1){
				testAllowPlayOutTable();
				outTable(); // out User from table when have network problem 
			}
			DBG("Server: Loi trong khi goi tra loi...");
		}
		DBG( "Server:Ngat ket noi :" + connection.getSock().getInetAddress() + ":" + connection.getSock().getPort() ) ;
	}
	
	// get username
	public String getUsername(){
		return this.username;
	}

	// sendReponse to client
	public NetworkConnection getConnection(){
		return this.connection;
	}
	
	/**
	 * Methold joinTable.
	 * 
	 * @function 		Add user to table have id=idTable.
	 * @param idTable	idTable user join in				
	 * @return Fail		If table is full	
	 * 		  InfoUserFromTable If table is not full
	 */		  
	public void joinTable(int idTable){
		String reponseString=MESSAGE_FAIL;
		if( !Main.getTable(idTable).isFull() ){
			this.table = Main.getTable(idTable);
			this.position = this.table.addUser(idThread);
			this.status = 1;
			DBG("join table success");
			try
			{
				reponseString = this.table.getUserOfTable();
				connection.sendReponse(reponseString);
				if(this.table.getNumberUser() == 1) {
					this.isAllowPlaying = true;
					connection.sendReponse("Host"); 
					this.table.setIdUserKey(0);
				}
				else connection.sendReponse("Host:"+table.getIdUserKey()); 
			}
			catch(Exception e) {DBG(e);}
		}
		else{
			connection.sendReponse(reponseString);
		}
	}

	/**
	* Methold outTable.
	* 
	* @function 		out user from table
	*/
	public void outTable(){
		int positionRemove = table.removeUser(idThread);
		if(positionRemove != -1) 
			connection.sendMessageToUsersInTable("User Out Room "+positionRemove, this.table, this.position);
		if(!this.table.isEmpty() && (this.table.getIdUserKey() == positionRemove)) {
			//Main.getUser(this.table.getUser(this.table.getFirstNotEmpty())).getConnection().sendReponse("Host"); 
			connection.sendMessageToUsersInTable("Host:"+this.table.getFirstNotEmpty(), this.table, this.position);	
			table.setIdUserKey(this.table.getFirstNotEmpty());
			this.isAllowPlaying = false;
		}
		//if( && this.isAllowPlaying)
		if(this.table.isEmpty()) this.table.setIdUserKey(-1);
		this.position = -1;
	}
	
	public void testAllowPlayOutTable()
	{
		if(this.isAllowPlaying)
		{			
			int j = this.table.getIdNextPlayer(this.position);
			Main.getUser(table.getUser(j)).getConnection().sendReponse(IS_ALLOW_PLAY);
			Main.getUser(table.getUser(j)).isAllowPlaying = true;
		}		
	}

	public void getProfile(){
		//	  String result = execQuery("Select * from account where account ='daicaca'");
		//	  idUser = result
	}




	/**
	* Sent result ATTACH_CARD to Client.
	* 
	* @function 		
	*/
	
	public void sentResultAttachCardsToClient(String stringCardsListFromClient)
	{
		if(this.isAllowPlaying)
		{			
			String listCardToClient = null;
			DBG("sentResultAttachCardsToClient : stringCardsListFromClient = " + stringCardsListFromClient);
			byte[] byteCardsListFromClient = changeStringToListCard(stringCardsListFromClient);
			Vector CardsListFromClient = Deck.getCards(byteCardsListFromClient);							
			
		#ifdef _DEBUG
			DBG(">>>>>>>>>>>>>>>>>userCards<<<<<<<<<<<<<<<< ");
			for (int i = 0; i < userCards.size(); i++) {
				ICard card = (ICard) userCards.elementAt(i);
				card.print();
			}		
			DBG(">>>>>>>>>>>>>>>>>CardsListFromClient<<<<<<<<<<<<<<<< = " + CardsListFromClient.size());
			for (int i = 0; i < CardsListFromClient.size(); i++) {
			
				ICard card = (ICard) CardsListFromClient.elementAt(i);
				card.print();
			}
		#endif		
			
			if(testCompareCardsList(CardsListFromClient, userCards))
			{
				this.lastPlayerAttack = this.position;	
				for(int i =0;i < NetworkConnection.MAX_MEMBER; i++){
					if((i != this.position) && (table.getUser(i) != -1)) {						
						Main.getUser(table.getUser(i)).lastPlayerAttack = this.position;
					}	
				}
				//getConnection().sendReponse(LAST_CARD + stringCardsListFromClient);
				getConnection().sendMessageToUsersInTable(LAST_CARD + this.username + ":" +stringCardsListFromClient, this.table, this.position);				
				userCards = removeVector(CardsListFromClient , userCards);
				getConnection().sendReponse(RESULT_ATTACH_TRUE);
				
		//This Player is Win	
		
				if(userCards.size() == 0) 
				{
					table.setIdUserKey(position);
					getConnection().sendReponse(IS_WIN);
					getConnection().sendMessageToUsersInTable(IS_LOSE, this.table, this.position);	
					getConnection().sendMessageToUsersInTable("Host:"+this.position,this.table, this.position);				
					return;
				}			
				
		//Else : Allow Next Player Attack
		
				int idNextPlayer = this.table.getIdNextPlayer(this.position);
				//this.getConnection().sendReponse(IS_NOT_ALLOW_PLAY);
				this.isAllowPlaying = false;
				//Main.getUser(table.getUser(j)).getConnection().sendReponse(IS_ALLOW_PLAY);
				connection.sendReponse(IS_ALLOW_PLAY+Main.getUser(this.table.getUser(idNextPlayer)).getUsername());
				connection.sendMessageToUsersInTable(IS_ALLOW_PLAY+Main.getUser(this.table.getUser(idNextPlayer)).getUsername(),this.table, this.position);		
				Main.getUser(table.getUser(idNextPlayer)).isAllowPlaying = true;
				
		//End this Player is Win
			}
			else
			{				
				getConnection().sendReponse(RESULT_ATTACH_FALSE);				
			}	
		}
	}
	
	
	/**
	* userLogin.
	* 
	* @function 		
	*/
	public String userLogin(String inputUser)
	{
		//tach String
		String result = "";
		int indexBeginLogin = BEGIN_LOGIN_STRING.length();
		result = inputUser.substring(indexBeginLogin);
		int indexBetween = result.indexOf(',');
		String userName = result.substring(0, indexBetween);
		String passWord = result.substring(indexBetween + 1, result.length());
		//end tach String 
		
		//begin access database
		connectionData.startConnect();
		String[] listParameter = {userName, passWord};  
		DBG("userName = " + userName + "  passWord = " + passWord);	
		CallableStatement my_stmt = connectionData.createCallableStatement("pcd_login", listParameter);
		ResultSet resultQuery = connectionData.executeProcedureQuery(my_stmt);  		
		try
		{
			if(resultQuery.next()) result = RESULT_LOGIN_OK;
			else result = RESULT_LOGIN_ERROR;
		}  
		catch(Exception ex)	
		{
			DBG(ex.toString());
		}
		connectionData.disConnect();
		//disConnect after process.

		return result;
	}
	/**
	* Methold 
	* 
	* @function 		
	*/	
	
	public String sendCardStringToClient()
	{
#ifdef _DEBUG
		DBG("sendCardStringToClient begin");
#endif
		Deck deck = new Deck();
		deck.shuffle();
		String listCardToClient = "";
		Player players[]  = new Player[4];
		for (int i = 0; i < 4; i++) {
			if(table.getUser(i) != -1) 
			{
				Vector cards = deck.getCards(i);
				Main.getUser(table.getUser(i)).userCards = cards;
				// players[i] = new Player(cards);				
				// players[i].print();				
				// listCardToClient = players[i].changeCardListToString();				
				listCardToClient = changeCardListToString(cards);				
				if(listCardToClient != null)
				{
					DBG("public byte[] sendCardToClient( " + i + " )changeCardListToString = " + listCardToClient);					
				}
				else
					DBG("listCardToClient = null");
				listCardToClient = DEAL_CARD + listCardToClient;
				Main.getUser(table.getUser(i)).getConnection().sendReponse(listCardToClient);
			}
		}
#ifdef _DEBUG
		DBG("sendCardStringToClient end");
#endif
		return listCardToClient;
	}

	
////*********************************************************************************////
////*********************************************************************************////
////*********************************************************************************////
////************************Public Function Use For Server***************************////
	
	public static String changeCardListToString(Vector cards)
	{
		DBG("changeCardListToString begin");
		byte valueCard[] = new byte[cards.size()];
		String chuoiCardList = "";
		
		for (int i = 0; i < cards.size(); i++) {
			ICard card1 = (ICard) cards.elementAt(i);
			valueCard[i] = (byte)((card1.getRank())*4 + card1.getSuit());
			chuoiCardList += (char)(valueCard[i]); 
			DBG("" + valueCard[i]);
		
		}	
		DBG(chuoiCardList);
		DBG("public byte[] changeCardListToValue() end");
		
		return chuoiCardList;
	}
	
	public static byte[] changeStringToListCard(String listCardString)
	{
		byte[] result = new byte[listCardString.length()];
		for(int i = 0; i < listCardString.length(); i++)
		{
			result[i] = (byte)(listCardString.charAt(i));
		}
		return result;
	}
	
	public static boolean testCompareCardsList(Vector cardsListClient, Vector userCardList)
	{
		boolean testCard = false;
		vectorSaveCardFromClient = new byte[cardsListClient.size()];
		ICard cardClient, cardUser;
		for(int i = 0; i < cardsListClient.size(); i++)
		{
			cardClient = (ICard)(cardsListClient.elementAt(i));
			testCard = false;
			for(int j = 0; j < userCardList.size(); j++)
			{
				cardUser   = (ICard)(userCardList.elementAt(j));
				if(cardClient.getValue() == cardUser.getValue())
				{
					testCard = true;
					vectorSaveCardFromClient[i] = (byte)j; 
					break;
				}
			}			
			if(!testCard)
			{
				vectorSaveCardFromClient = null;
				break;
			}
		}
		return testCard;
	}

	public static Vector removeVector(Vector cardsListClient, Vector userCardList)
	{
		ICard cardClient, cardUser;
		for(int i = 0; i < cardsListClient.size(); i++)
		{
			cardClient = (ICard)(cardsListClient.elementAt(i));
			for(int j = 0; j < userCardList.size(); j++)
			{
				cardUser   = (ICard)(userCardList.elementAt(j));
				if(cardClient.getValue() == cardUser.getValue())
				{
					userCardList.removeElementAt(j);
					break;
				}
			}			
		}
		return userCardList;
	}

////************************Public Function Use For Server End***********************////	
////*********************************************************************************////
////*********************************************************************************////
////*********************************************************************************////

}
