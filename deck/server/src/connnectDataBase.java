/**
 * @(#)connnectDataBase.java
 *
 *
 * @author 
 * @version 1.00 2011/1/31
 */

import org.apache.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.CallableStatement;
import java.sql.Statement;
import java.sql.ResultSet;
//import java.sql.*;

public class connnectDataBase {
	public String driverName;
	public String url;
	public Connection con;
	public Statement stmt;
	public boolean statusConnect;
	public String defaultPass = "";	
	private static Logger log = Logger.getLogger(connnectDataBase.class);
	
	//Constructor for connectDatabase
	public connnectDataBase() {
		driverName 	= "";
		url 		= "";
		con			= null;
		stmt		= null;
		statusConnect = false;
    }
    
    //
    public Connection getConnection()
    {
    	return con;
    }
	
	// 
	public boolean getStatusConnect()
	{
		return statusConnect;
	}
	
	//
	public void prepareForConnect()
	{
		driverName 	= "com.mysql.jdbc.Driver";
		url 	   	= "jdbc:mysql://localhost/deskonline?user=root&password=" + defaultPass + "&useUnicode=true&characterEncoding=utf8";		
	}
	
	public void prepareForConnect(String driverNameForConnect, String server, String database, String userName, String pass)
	{
		driverName = driverNameForConnect;
		url = server + "/" + database + "?user=" + userName + "&password="+pass+"&useUnicode=true&characterEncoding=utf8";
	}
	
	//Start Connect
	public void startConnect()
	{
		if(statusConnect) return;
		try
		{
			Class.forName(driverName).newInstance();	
			con = DriverManager.getConnection(url);
			statusConnect = true;
		}
		catch(Exception ex)
		{
			DBG("startConnect Error : " + ex.toString());
		}
	}
	
	//Dis Connect
	public void disConnect()
	{
		try
		{
			if(statusConnect) con.close();
			statusConnect = false;
		}
		catch(Exception ex)
		{
			DBG("disConnect Error : " + ex.toString());
		}
	}
    
    // Create Statement For Procedure WithOut List Parameter
    public CallableStatement createCallableStatement(String procedureName)
    {
    	CallableStatement callStatement = null;
       	try
    	{
	    	String sqlQuery = "{call " + procedureName + "()}";
	    	
	    	callStatement = con.prepareCall(sqlQuery);
    	}
    	catch(Exception ex)
    	{
    		DBG("createCallableStatement WithOut Parameter Error : " + ex.toString());
    	}
    	return callStatement;
    }
        
    // Create Statement For Procedure With List in Parameter
    public CallableStatement createCallableStatement(String procedureName, String[] listParameter)
    {
    	CallableStatement callStatement = null;
       	try
    	{
	    	int numberParameter = listParameter.length;
	    	DBG(" numberParameter : " + numberParameter);
	    	String listForCallableStatement = "";
	    	String sqlQuery = "";
	    	for(int i = 0; i < numberParameter ; i++)
	    		if(i < numberParameter - 1)
	    			listForCallableStatement += "?,";
	    		else
	    			listForCallableStatement += "?";
	    	sqlQuery += "{call " + procedureName + "(" + listForCallableStatement + ")}";
	    	
	    	DBG("listForCallableStatement = " + listForCallableStatement);
	    	DBG("sqlQuery = " + sqlQuery);
	    	
	    	callStatement = con.prepareCall(sqlQuery);
	    	
	    	for(int i = 0; i < numberParameter; i++)    	
	    	{
	    		callStatement.setString(i + 1, listParameter[i]);
	    	}    	
    	}
    	catch(Exception ex)
    	{
    		DBG("createCallableStatement With List Parameter Error : " + ex.toString());
    	}
    	return callStatement;
    }
    
    // Create Statement For Procedure With List Out Parameter
    public CallableStatement createCallableStatement(String procedureName, String[] listParameter, int isOutParameter)
    {
    	CallableStatement callStatement = null;
    	return callStatement;
    }
    
    // Create Statement For Procedure With List in/out Parameter
    public CallableStatement createCallableStatement(String procedureName, String[] listInParameter, String[] listOutParameter)
    {
    	CallableStatement callStatement = null;
       	try
    	{
	    	int numberInParameter  = listInParameter.length;
	    	int numberOutParameter = listOutParameter.length;
	    	DBG(" numberInParameter : " + numberInParameter);
	    	String listForCallableStatement = "";
	    	String sqlQuery = "";
	    	for(int i = 0; i < numberInParameter + numberOutParameter ; i++)
	    		if(i < numberInParameter + numberOutParameter - 1)
	    			listForCallableStatement += "?,";
	    		else
	    			listForCallableStatement += "?";
	    	sqlQuery += "{call " + procedureName + "(" + listForCallableStatement + ")}";
	    	
	    	DBG("listForCallableStatement = " + listForCallableStatement);
	    	DBG("sqlQuery = " + sqlQuery);
	    	
	    	callStatement = con.prepareCall(sqlQuery);
	    	
	    	for(int i = 0; i < numberInParameter; i++)    	
	    	{
	    		callStatement.setString(i + 1, listInParameter[i]);
	    	}    	
    	}
    	catch(Exception ex)
    	{
    		DBG("createCallableStatement With List Parameter Error : " + ex.toString());
    	}
    	return callStatement;
    }
    
    //Execute Statement With Select Query
    public ResultSet executeQuery(String sqlQuery)
    {
    	ResultSet result = null;
    	Statement stmt;
    	try
    	{
    		if(statusConnect)
    		{
    			stmt = con.createStatement();
    			result = stmt.executeQuery(sqlQuery);
    		}
    	}
    	catch(Exception ex)
    	{
    		DBG("executeQuery : " + ex.toString());	
    	}
    	return result;	
    }
    
    //Execute Statement With Upate Query
    public boolean executeNonQuery(String sqlQuery)
    {
    	int result;
    	Statement stmt;
    	try
    	{
    		if(statusConnect)
    		{
    			stmt = con.createStatement();
    			result = stmt.executeUpdate(sqlQuery);
    			return true;
    		}
    		else
    			return false;    		
    	}
    	catch(Exception ex)
    	{
    		DBG("executeNonQuery : " + ex.toString());
    		return  false;
    	}
    }
    
    public ResultSet executeProcedureQuery(CallableStatement stmtFunction)
    {
    	ResultSet result = null;
    	if(stmtFunction != null)
    	{
    		try
    		{
    			result = stmtFunction.executeQuery();
    		}
    		catch(Exception ex)
    		{
    			DBG("executeProcedureQuery : " + ex.toString());
    		}
    	}
    	return result;
    }
    
    public boolean executeProcedureNonQuery(CallableStatement stmtFunction)
    {
    	int result;
    	if(stmtFunction != null)
    	{
    		try
    		{
    			result = stmtFunction.executeUpdate();
    			return true;
    		}   
    		catch(Exception ex) 	
    		{
    			DBG("executeProcedureNonQuery Error : " + ex.toString());
    			return false;
    		}
    	}
    	else
    		return false;
    }
}