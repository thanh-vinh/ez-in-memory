
import java.util.Vector;
import org.apache.log4j.Logger;


public class CardList {

	public static final int SORT_BY_RANK 		= 0;
	public static final int SORT_BY_SEQUENCER 	= 1;
	public static final int SORT_BY_PAIR 		= 2;
	private Logger log = Logger.getLogger(CardList.class);

	public Vector cards; 			// <ICard>
	public Vector selectedIndex;	// <Integer>

	public int sortType	= SORT_BY_RANK;

	public CardList() {

		this.cards = new Vector();
		this.selectedIndex = new Vector();
	}

	public CardList(Vector cards) {

		this.cards = cards;
		this.selectedIndex = new Vector();
	}

	public void setCards(Vector cards) {

		this.cards.removeAllElements();
		this.cards = null;
		this.cards = cards;
	}

	public void add(ICard card) {

		this.cards.addElement(card);
	}

	public Vector getCards() {

		Vector cards = new Vector(this.cards.size());
		for (int i = 0; i < this.cards.size(); i++) {
			cards.addElement(this.getCardAt(i));
		}

		return cards;
	}

	public int getCardsCount() {

		return this.cards.size();
	}

	public int getSelectedCardsCount() {

		return this.selectedIndex.size();
	}

	public CardList getSelectedCards() {

		int size = this.selectedIndex.size();
		Vector v = new Vector(size);

		for (int i = 0; i < size; i++) {
			int index = ((Integer) this.selectedIndex.elementAt(i)).intValue();
			ICard card = this.getCardAt(index);
			v.addElement(card);
		}

		// Remove selected & deselect all cards
		this.removeCards(v);
		this.deselectAll();

		CardList cards = new CardList(v);
		v = null; // Only set is null, don't call removeAllElements() because it cause cards size is 0 (!)

		return cards;
	}

	public void removeAt(int index) {

		this.cards.removeElementAt(index);
	}

	public void removeCard(ICard card) {

		for (int i = 0; i < this.cards.size(); i++) {
			ICard item = this.getCardAt(i);
			if (item.compareTo(card) == 0) {
				this.removeAt(i);
				break;
			}
		}
	}

	public void removeCards(Vector cards) {

		for (int i = 0; i < cards.size(); i++) {
			ICard card = (ICard) cards.elementAt(i);
			this.removeCard(card);
		}
	}

	public ICard getCardAt(int index) {

		return ((ICard) this.cards.elementAt(index));
	}

	public void sortByRank() {

		DBG("Sort by rank...");

		for (int i = 0; i < this.cards.size(); i++) {
			for (int j = i + 1; j < this.cards.size(); j++) {
				ICard card1 = (ICard) this.cards.elementAt(i);
				ICard card2 = (ICard) this.cards.elementAt(j);
				if (card1.compareTo(card2) > 0) {
					Card.swap(this.cards, i, j);
				}
			}
		}
	}

	public void sortBySequencer() {

		DBG("Sort by sequencer...");

		for (int i = 0; i < this.cards.size(); i++) {
			for (int j = i + 1; j < this.cards.size() - 1; j++) {
				ICard card1 = (ICard) this.cards.elementAt(i);
				ICard card2 = (ICard) this.cards.elementAt(j);
				if (card1.getRank() + 1 == card2.getRank()) {
					Card.swap(this.cards, i + 1, j);
				}
			}
		}
	}

	public void sortByPair() {

		DBG("Sort by pair...");

		for (int i = 0; i < this.cards.size(); i++) {
			for (int j = i + 1; j < this.cards.size() - 1; j++) {
				ICard card1 = (ICard) this.cards.elementAt(i);
				ICard card2 = (ICard) this.cards.elementAt(j);
				if (card1.getRank() == card2.getRank()) {
					Card.swap(this.cards, i + 1, j);
				}
			}
		}
	}

	public void sort() {

		switch (this.sortType) {
			case SORT_BY_RANK:
				this.sortByRank();
				break;

			case SORT_BY_SEQUENCER:
				this.sortBySequencer();
				break;

			case SORT_BY_PAIR:
				this.sortByRank();
				this.sortByPair();
				break;
		}

		this.sortType++;
		this.sortType %= 3;
	}

	public void print() {

		for (int i = 0; i < this.cards.size(); i++) {
			ICard card = (ICard) this.cards.elementAt(i);
			card.print();
		}
	}

	public boolean wasSelected(int index) {

		if (this.selectedIndex.indexOf(new Integer(index)) > -1) {
			return true;
		} else {
			return false;
		}
	}

	public void select(int index) {

		this.selectedIndex.addElement(new Integer(index));
	}

	public void deselect(int index) {

		index = this.selectedIndex.indexOf(new Integer(index));
		this.selectedIndex.removeElementAt(index);
	}

	public void deselectAll() {

		this.selectedIndex.removeAllElements();
		this.selectedIndex = null;
		this.selectedIndex = new Vector();
	}

	public int[] getSelected() {

		int length = this.selectedIndex.size();
		int[] result = new int[length];

		for (int i = 0; i < length; i++) {
			result[i] = ((Integer) this.selectedIndex.elementAt(i)).intValue();
		}

		return result;
	}

	public CardList getAllSelected() {

		CardList selectedCards = new CardList();

		for (int i = 0; i < this.selectedIndex.size(); i++) {
			int index = ((Integer) this.selectedIndex.elementAt(i)).intValue();
			ICard card = (ICard) this.cards.elementAt(index);
			selectedCards.add(card);
		}

		return selectedCards;
	}

	public void update() {

		for (int i = 0; i < this.selectedIndex.size(); i++) {
			int index = ((Integer) this.selectedIndex.elementAt(i)).intValue();
			this.removeAt(index);
		}

		this.deselectAll();
	}

	public boolean isPair() {

		if (this.cards.size() == 2) {
			if (this.getCardAt(0).isPairWith(this.getCardAt(1))) {
				return true;
			}
		}
		return false;
	}

	public boolean isSequencer() {

		for (int i = 1; i < this.cards.size(); i++) {
			if (!this.getCardAt(i - 1).isSequencerWith(this.getCardAt(i))) {
				return false;
			}
		}

		return true;
	}

	public int compareTo(CardList cards) {

		return 0;
	}	
	
}
