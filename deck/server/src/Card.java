
import java.util.Vector;
import org.apache.log4j.Logger;

public class Card implements ICard {

	public int rank;
	public int suit;
	public int value;
	private Logger log = Logger.getLogger(ICard.class);

	public Card(int rank, int suit) {
	
		this.rank = rank;
		this.suit = suit;
//	#ifndef RANK_ACE_IN_LAST  //  hoang.nv  : is right ???
		// If rank is ace or deuce, increase the value for comparing card
		if (rank < RANK_3) {
			rank += RANK_KING;
		}
//	#endif
		this.value = (rank << 4) | suit;
	}

	public static void swap(Vector cards, int i, int j) {

		ICard card1 = (ICard) cards.elementAt(i);
		ICard card2 = (ICard) cards.elementAt(j);
		cards.setElementAt(card2, i);
		cards.setElementAt(card1, j);
	}

	/**
	 * Spades, Hearts, Diamonds and Clubs.
	 */
	// @Override
	public int getSuit() {

		return this.suit;
	}

	// @Override
	public int getValue() {

		return this.value;
	}

	/*
	 * Thirteen ranks running from two (deuce) to ten, Jack, Queen, King, and Ace.
	 */
	// @Override 
	public int getRank() {

		return this.rank;
	}

	// @Override
	public boolean isPairWith(ICard card) {

		if (this.getRank() == card.getRank())
			return true;
		else
			return false;
	}

	/**
	 * Compare this card with another card.
	 * @param card
	 * 		Another card to compare.
	 * @return
	 * 		GREATER, EQUAL, LOWER.
	 */
	// @Override
	public int compareTo(ICard card) {

		if (this.getValue() > card.getValue()) {
			return ICard.GREATER;
		} else if (this.getValue() < card.getValue()) {
			return ICard.LOWER;
		} else {
			return ICard.EQUAL;
		}
	}

	// @Override
	public boolean isSequencerWith(ICard card) {

		if ((this.getRank() + 1) == (card.getRank())) {
			return true;
		} else {
			return false;
		}
	}
	// @Override
	public void print() {

		DBG(this.value + "\t:\t" + ICard.RANKS[this.rank] + "\t" + ICard.SUITS[this.suit]);
	}
}