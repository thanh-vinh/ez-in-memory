import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import org.apache.log4j.Logger;
import java.net.SocketTimeoutException;



public class NetworkConnection {
	//define constant
	private static final String ERROR_MESSAGE = "Error";
	public static String MESSAGE_FAIL				= "Fail";
	public static final int MAX_MEMBER = 4;
	public static final int MAX_TABLE_IN_ROOM = 20;
	private Socket sock;
	private BufferedInputStream bis;
	private DataInputStream inputStream;
	private DataOutputStream outputStream;
	private Logger log = Logger.getLogger(NetworkConnection.class);
	
	public Socket getSock() {
		return sock;
	}

	public void setSock(Socket sock) {
		this.sock = sock;
	}

	public NetworkConnection(Socket socket){
		try{
			this.sock = socket;
			this.sock.setSoTimeout(200);
			DBG( "Server:Da ket noi:" + sock.getInetAddress() + " : " + sock.getPort() ) ;
			// create input output variable
			this.bis=new BufferedInputStream(sock.getInputStream());
			this.inputStream=new DataInputStream(bis);
			this.outputStream=new DataOutputStream(sock.getOutputStream());
		}
		catch(Exception e) {DBG( ERROR_MESSAGE + e ) ;}
	}
	
	public void close() throws IOException{
		sock.close();
	}

	// getRequest from client
	public String getRequest() throws IOException
	{	String lineIn = "";
		try{
			lineIn = this.inputStream.readUTF();
		}
		catch(SocketTimeoutException e) {}
		return lineIn;
	}

	public void sendReponse(String message){
		try{
			this.outputStream.writeUTF(message);
		}
		catch(Exception e) {DBG( "Bi loi trong qua trinh sendReponse"+e ) ;}
	}
	
	/**
	* Methold newMemberEven.
	* 
	* @function 		Send a message to inform that user ready join to table.
	* 					Message include position and username of new user
	* 
	*/
	public void newMemberEven(int position, String username, Table table){
		sendMessageToUsersInTable(position+","+username, table, position);
	}
	
	/**
	* Methold sendMessageToUsersInTable.
	* 
	* @function 		Send a message to others in table
	* @param message 	Message will be sent.
	*/
	public void sendMessageToUsersInTable(String message, Table table, int position){
		for(int i =0;i < MAX_MEMBER; i++){
			if((i != position) && (table.getUser(i) != -1)) {
				DBG( "send to Thread="+table.getUser(i) ) ;
				Main.getUser(table.getUser(i)).getConnection().sendReponse(message);
			}	
		}
	}
	
	/**
	* Methold getRoom.
	* 
	* @function 		Get Room list
	*/
	public String getRoom(){
		String result ="";
		for(int i=0;i< Main.ROOM_NUM; i++){
			result+=i+","+Main.getRoom(i).getCurMember()+";";
		}
		return result;
	}
	
	/**
	* Methold getTable.
	* 
	* @function 		Get Table list from idRoom (Room have 10 table)
	*/
	public String getTable(int idRoom){
		String result ="";
		for(int i = idRoom * MAX_TABLE_IN_ROOM; i < (idRoom + 1) * MAX_TABLE_IN_ROOM; i++){
			result+=i+","+Main.getTable(i).getNumberUser()+","+Main.getTable(i).getMoney()+";";
		}
		return result;
	}
	
	

}
