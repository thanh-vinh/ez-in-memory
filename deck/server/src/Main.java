

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.apache.log4j.Logger;

public class Main {
	public static int DEFAULT_PORT = 3330;
	final static int MAX_CONN = 0x064;
	public final static int ROOM_NUM = 10;
	public final static int TABLE_NUM = 200;
	public final static int THREAD_USER_NUM = 10000;
	public static int id = 0; 
	private static Logger log = Logger.getLogger(Main.class);

	private ThreadPoolExecutor threadPool = null;
	private static User[] arrayUser;
	private static Room[] room;
	private static Table[] table;

	public Main() throws IOException {	
		init();
		final ServerSocket socket = new ServerSocket(DEFAULT_PORT);
		threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(MAX_CONN);

		for (;;) {
			Socket sock = socket.accept();
			arrayUser[id]=new User(sock, id);
			threadPool.execute(arrayUser[id]);
			id++;
			DBG("Thread pool sequen" + threadPool.getPoolSize());
		}
	}
	void init(){
		DBG("Khoi tao slot nguoi choi....");
		arrayUser = new User[THREAD_USER_NUM];
		DBG("Khoi tao room................");
		room = new Room[ROOM_NUM];
		for(int i=0; i<ROOM_NUM; i++)
		room[i]= new Room(i);
		DBG("Khoi tao table................");
		table = new Table[TABLE_NUM];
		for(int i = 0; i< TABLE_NUM; i++){
			table[i] = new Table();
		}
	
	}
	public static Table getTable(int i){
		return table[i];
	}
	public static User getUser(int i){
		return arrayUser[i];
	}
	public static Room getRoom(int i){
		return room[i];
	}
	

	
	
	
	// ------------------------------------
	// main
	// ------------------------------------
	public static void main(String argv[]) {
		try {
			new Main();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
