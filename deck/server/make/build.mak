#######################################
#set global properties for this build
#######################################

# set define file for CPP tool
DEFINES_FILE=_DEFINES.H

# set input/output paths
MASTER_SRC_PATH=src
BUILD_PATH=.version
SRC_OUT_PATH=${BUILD_PATH}/3-src
PREPROCESS_PATH=${BUILD_PATH}/4-preprocess
CLASSES_OUT_PATH=${BUILD_PATH}/5-classes

# set WTK properties
WTK_PATH=C:\WTK22
MIDP_LIB=${WTK_PATH}/lib/midpapi20.jar
CLDC_LIB=${WTK_PATH}/lib/cldcapi11.jar
WTK_CLDC_VERSION=1.1
WTK_MIDP_VERSION=2.0

# set for tools
TOOLS_PATH=_tools
ANTENNA_LIB=${TOOLS_PATH}/antenna-bin-1.2.1-beta.jar
TEXT_EXPORTER=${TOOLS_PATH}/text-exporter/text-exporter.jar
FONT_EXPORTER=${TOOLS_PATH}/font-exporter/font-exporter.jar
DATA-PACKAGE=${TOOLS_PATH}/data-package/data-package.jar
CPP_TOOL_PATH=${TOOLS_PATH}/cpp
PREPROCESS_SCRIPT=${TOOLS_PATH}/cpp/preprocess.bat
#PROGUARD_PATH=${TOOLS_PATH}/proguard.jar

# set antenna properties
wtk.home=${WTK_PATH}
wtk.cldc.version=${WTK_CLDC_VERSION}
wtk.midp.version=${WTK_MIDP_VERSION}
#wtk.proguard.home=${PROGUARD_PATH}
