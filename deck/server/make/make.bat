@echo off

echo BUILD %PROJECT_FULL_NAME% WITH ANT...

echo #############################################################
echo # SETTING SOME COMMON VARIABLES...
echo #############################################################

call config.bat

echo.
echo Done...
echo.

set COMPILE_MODE=%1%

echo.
echo Done...
echo.

echo #############################################################
echo # BUILDING...
echo #############################################################
echo Build mode = %COMPILE_MODE%
ant %COMPILE_MODE%

echo.
echo Done...
echo.
