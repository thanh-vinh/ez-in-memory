/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */



import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;			// !!!For testing only
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
//import javax.microedition.io.Connector;
//import javax.microedition.io.SocketConnection;

/**
 * Sockets based implementation of the RedDwarfClient interface.
 *
 * @author Karel Herink, Remy Beriot
 */
public class RedDwarfSocketClient implements RedDwarfClient {

    private boolean debug = true;

    private String host;
    private int port;
    private RedDwarfListener responseHandler;
    private InputReader reader;
    private OutputWriter writer;
    /* private SocketConnection socket; */
    private Socket socket;			// !!!For testing

    private byte[] reconnectKey;
    private boolean loggedIn;
    private Hashtable channels = new Hashtable();
    private Vector requests = new Vector();

    /** Added by Thanh Vinh to overload sendToChannel() method. */
    private String channelName;

    public RedDwarfSocketClient(String host, int port, RedDwarfListener responseHandler) {
        this.responseHandler = responseHandler;
        this.host = host;
        this.port = port;
    }

    public void printDebugInfo(boolean debug) {
        this.debug = debug;
    }

    /**
     * @see RedDwarfClient#connect()
     *
     * @throws java.io.IOException
     */
    public void connect() throws IOException {
    	/* socket = (SocketConnection) Connector.open("socket://" + this.host + ":" + this.port, Connector.READ_WRITE);
    	InputStream in = socket.openInputStream();
    	OutputStream out = socket.openOutputStream(); */

		/* !!!For testing only */
    	socket = new Socket(this.host, this.port );
    	InputStream in = socket.getInputStream();
    	OutputStream out = socket.getOutputStream();

        reader = new InputReader(this, in);
        writer = new OutputWriter(this, out);
        new Thread(reader).start();
        new Thread(writer).start();

        //give the threads some time to set-up (should be done in a better way)
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @see RedDwarfClient#disconnect()
     *
     * @throws java.io.IOException
     */
    public void disconnect() throws IOException {
        reader.setDisconnected(true);
        writer.setDisconnected(true);
        this.loggedIn = false;
        socket.close();
    }

    private class InputReader implements Runnable {
        private RedDwarfSocketClient main;
        private InputStream in;
        private boolean disconnected = false;

        public InputReader(RedDwarfSocketClient main, InputStream in) {
            this.main = main;
            this.in = in;
        }

        public void run() {
            if (debug) System.out.println("InputReader ready..");
            while (!disconnected) {
                try {
                    byte hi = (byte) in.read();
                    if (hi == -1) {
                        main.disconnect();
                        //post a dummy empty message to wake up the writer - it will realize that we;return disconnected
                        MessageBuffer dummy = new MessageBuffer(new byte[0]);
                        postRequest(dummy);
                        break;
                    }
                    byte lo = (byte) in.read();
                    int len = ((hi & 255) << 8) + (lo & 255) << 0;

                    if (debug) System.out.println("Received response lenght: " + len);

                    byte[] msgBytes = new byte[len];
                    in.read(msgBytes);
                    MessageBuffer buf = new MessageBuffer(msgBytes);
                    handleApplicationMessage(buf);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (debug) System.out.println("InputReader finished..");
        }

        public void setDisconnected(boolean disconnected) {
            this.disconnected = disconnected;
        }

        public boolean isDisconnected() {
            return disconnected;
        }
    }

    private class OutputWriter implements Runnable {
        private RedDwarfSocketClient main;
        private OutputStream out;
        private boolean disconnected = false;

        public OutputWriter(RedDwarfSocketClient main, OutputStream out) {
            this.main = main;
            this.out = out;
        }

        public void run() {
            if (debug) System.out.println("OutputWriter ready..");
            while (!disconnected) {
                synchronized (requests) {
                    while (requests.size() == 0) {
                        try {
                            requests.wait();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                    MessageBuffer request = (MessageBuffer) requests.elementAt(0);
                    requests.removeElementAt(0);
                    try {
                        //request with empty buffer means we have disconnected
                        if (disconnected) {
                            break;
                        }
                        out.write(request.getBuffer());
                        out.flush();
                        if (debug) System.out.println("Wrote request");
                    } catch (IOException ex) {
                        if (!disconnected) {
                            ex.printStackTrace();
                            try {
                                main.disconnect();
                            } catch (IOException ex1) {
                                //ignore
                            }
                        }
                    }
                }
            }
            if (debug) System.out.println("OutputWriter finished..");
        }

        public void setDisconnected(boolean disconnected) {
            this.disconnected = disconnected;
        }

        public boolean isDisconnected() {
            return disconnected;
        }
    }

    private void handleApplicationMessage(MessageBuffer msg) throws IOException {
        byte command = msg.getByte();
        switch (command) {
            case SimpleSgsProtocol.LOGIN_SUCCESS:
                if (debug) System.out.println("Logged in");
                reconnectKey = msg.getBytes(msg.limit() - msg.position());
                loggedIn = true;
                responseHandler.loggedIn();
                break;

            case SimpleSgsProtocol.LOGIN_FAILURE: {
                String reason = msg.getString();
                if (debug) System.out.println("Login failed: " + reason);
                responseHandler.loginFailed(reason);
                break;
            }

            case SimpleSgsProtocol.LOGIN_REDIRECT: {
                String host = msg.getString();
                int port = msg.getInt();
                if (debug) System.out.println("Login redirect: " + host + ":" + port);

                //TODO: Disconnect our current connection, and connect to the new host and port
               break;
            }

            case SimpleSgsProtocol.SESSION_MESSAGE: {
                if (debug) System.out.println("Session message");
                checkLoggedIn();
                byte[] msgBytes = msg.getBytes(msg.limit() - msg.position());
                responseHandler.receivedSessionMessage(msgBytes);
                break;
            }

            case SimpleSgsProtocol.RECONNECT_SUCCESS:
                if (debug) System.out.println("Reconnected success");
                loggedIn = true;
                reconnectKey = msg.getBytes(msg.limit() - msg.position());
                responseHandler.reconnected();
                break;

            case SimpleSgsProtocol.RECONNECT_FAILURE:
                String reason = msg.getString();
                if (debug) System.out.println("Reconnect failure: " + reason);
                this.disconnect();
                break;

            case SimpleSgsProtocol.LOGOUT_SUCCESS:
                if (debug) System.out.println("Logged out gracefully");
                loggedIn = false;
                break;

            case SimpleSgsProtocol.CHANNEL_JOIN: {
                if (debug) System.out.println("Channel join");
                checkLoggedIn();
//                String channelName = msg.getString();
                this.channelName = msg.getString();		// Modified by Thanh Vinh
                byte[] channelId = msg.getBytes(msg.limit() - msg.position());

                if (channels.get(channelName) == null) {
                    channels.put(channelName, channelId);
                }
                else {
                    if (debug) System.out.println("Cannot join channel " + channelName + ": already a member");
                }
                responseHandler.joinedChannel(channelName);

//                if (debug) {
//                	System.out.println("channelName = " + this.channelName);
//                	System.out.println("channelId leng = " + channelId.length);
//                	int id = (channelId[0] << 8) & channelId[1];
//                	System.out.println("channelId = " + id);
//                }

                break;
            }

            case SimpleSgsProtocol.CHANNEL_LEAVE: {
                if (debug) System.out.println("Channel leave");
                checkLoggedIn();
                byte[] channelId = msg.getBytes(msg.limit() - msg.position());
                String channelName = getChannelNameById(channelId);

                if (channelName != null) {
                    channels.remove(channelName);
                    responseHandler.leftChannel(channelName);
                } else {
                    if (debug) System.out.println("Cannot leave channel: not a member");
                }
                break;
            }

            case SimpleSgsProtocol.CHANNEL_MESSAGE:
                if (debug) System.out.println("Channel message");
                checkLoggedIn();
                byte[] channelId = msg.getBytes(msg.getShort());
                String channelName = getChannelNameById(channelId);
                if (channelName == null) {
                    if (debug) System.out.println("Ignore message on channel: not a member");
                    return;
                }
                byte[] msgBytes = msg.getBytes(msg.limit() - msg.position());
                responseHandler.receivedChannelMessage(channelName, msgBytes);
                break;

            default:
                throw new IOException("Unknown session opcode: " + command);
        }
    }

    // -------------------------------------------------------------------------
    // RedDwarfClient methods
    // -------------------------------------------------------------------------

    //methods dealing with server session

    /**
     * @see RedDwarfClient#login(java.lang.String, java.lang.String)
     *
     * @param userName
     * @param password
     * @throws java.io.IOException
     */
    public void login(String userName, String password) throws IOException {
        int len = 2 + MessageBuffer.getSize(userName) + MessageBuffer.getSize(password);
        MessageBuffer msg = new MessageBuffer(2 + len);
        msg.putShort(len).
                putByte(SimpleSgsProtocol.LOGIN_REQUEST).
                putByte(SimpleSgsProtocol.VERSION).
                putString(userName).
                putString(password);
        postRequest(msg);
    }

    /**
     * @see RedDwarfClient#isConnected()
     *
     * @return
     * @throws java.io.IOException
     */
    public boolean isConnected() throws IOException {
        return loggedIn;
    }

    /**
     * @see RedDwarfClient#logout(boolean)
     * @param force
     * @throws java.io.IOException
     */
    public void  logout(boolean force) throws IOException {
        int len = 1;
        MessageBuffer msg = new MessageBuffer(2 + len);
        msg.putShort(len).
                putByte(SimpleSgsProtocol.LOGOUT_REQUEST);
        postRequest(msg);
    }

    /**
     * @see RedDwarfClient#sendToSession(byte[])
     *
     * @param message
     * @throws java.io.IOException
     */
    public void  sendToSession(byte[] message) throws IOException {
        int len = 1 + message.length;
        MessageBuffer msg = new MessageBuffer(2 + len);
        msg.putShort(len).
                putByte(SimpleSgsProtocol.SESSION_MESSAGE).
                putBytes(message);
        postRequest(msg);
    }

    //methods dealing with client channels

    /**
     * @see RedDwarfClient#sendToChannel(java.lang.String, byte[])
     *
     * @param channelName
     * @param message
     * @throws java.io.IOException
     */
    public void sendToChannel(String channelName, byte[] message) throws IOException {
        byte[] channelId = (byte[]) channels.get(channelName);
        int len = 1 + 2 + channelId.length + message.length;
        MessageBuffer msg = new MessageBuffer(2 + len);
        msg.putShort(len).
                putByte(SimpleSgsProtocol.CHANNEL_MESSAGE).
                putByteArray(channelId).
                putBytes(message);
        postRequest(msg);
    }

    /**
     * Added by Thanh Vinh.<br>
     * @see RedDwarfSocketClient#sendToChannel(java.lang.String, byte[])
     */
    public void sendToChannel(byte[] message) throws IOException
    {
        byte[] channelId = (byte[]) channels.get(this.channelName);
        int len = 1 + 2 + channelId.length + message.length;
        MessageBuffer msg = new MessageBuffer(2 + len);
        msg.putShort(len).
                putByte(SimpleSgsProtocol.CHANNEL_MESSAGE).
                putByteArray(channelId).
                putBytes(message);
        postRequest(msg);
    }


    //utility methods

    private void checkLoggedIn() {
        if (!loggedIn) {
            throw new IllegalStateException("Client not logged in");
        }
    }

    private void postRequest(MessageBuffer msg) {
        synchronized(requests) {
            requests.addElement(msg);
            requests.notifyAll();
        }
    }

    private String getChannelNameById(byte[] channelId) {
        Enumeration keys = channels.keys();
        while (keys.hasMoreElements()) {
            String name = (String) keys.nextElement();
            byte[] id = (byte[]) channels.get(name);
            if (byteArraysEqual(channelId, id)) {
                return name;
            }
            continue;
        }
        return null;
    }

    private boolean byteArraysEqual(byte[] a, byte[] b) {
        if ((a == null && b != null) || (a != null && b == null)) {
            return false;
        }
        if (a == null && b == null) {
            return true;
        }
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
}
