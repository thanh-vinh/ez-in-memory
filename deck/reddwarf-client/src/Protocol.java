
import java.util.logging.Level;
import java.util.logging.Logger;

public class Protocol implements IProtocol
{
	private static final Logger logger = Logger.getLogger(Protocol.class.getName());

	private MessageBuffer input;
	private MessageBuffer output;

	/* public Protocol()
	{

	} */

	public static byte[] getLoginRequestMessage(String user, String password)
	{
		String s = user + "|" + password;	//Command, version, and user|password
		int capacity = BYTE_SIZE + BYTE_SIZE + MessageBuffer.getSize(s);

		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(LOGIN_REQUEST);
		message.putByte(VERSION);
		message.putString(s);

		return message.getBuffer();
	}

	public static byte[] getRoomListRequestMessage()
	{
		MessageBuffer message = new MessageBuffer(BYTE_SIZE);
		message.putByte(ROOM_LIST_REQUEST);

		return message.getBuffer();
	}

	public static byte[] getTableListRequestMessage(String roomName)
	{
		int capacity = BYTE_SIZE + MessageBuffer.getSize(roomName);

		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(TABLE_LIST_REQUEST);
		message.putString(roomName);

		return message.getBuffer();
	}

	public static byte[] getTableJoinRequestMessage(short roomIndex, short tableIndex)
	{
		int capacity = BYTE_SIZE + SHORT_SIZE * 2;

		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(TABLE_JOIN_REQUEST);
		message.putShort(roomIndex);
		message.putShort(tableIndex);

		return message.getBuffer();
	}

	/**
	 * Set the received message.
	 */
	public void setMessage(byte[] message)
	{
		logger.log(Level.INFO, "Set message {0} ", message);

		this.input = new MessageBuffer(message);
	}

	/**
	 * Get the response message.
	 */
	public byte[] getMessage()
	{
		// Handle the received message
		this.handleApplicationMessage();
		// Return ByteBuffer from MessageBuffer
		byte[] message = this.output.getBuffer();
		return message;
	}

	public void handleApplicationMessage()
	{
		String name;
		short length;
		short index;
		byte status;

		try {
			// Get the client's command
			byte command = this.input.getByte();
			logger.log(Level.INFO, "Handle command: {0}", command);

			switch (command) {
				case LOGIN_REQUEST:
					break;

				case ROOM_LIST:
					length = this.input.getShort();
					GameClient.initializeRoomList(length);
					for (short i = 0; i < length; i++) {
						status = this.input.getByte();
						name = this.input.getString();

						GameClient.putRoom(i, status, name);
					}
					break;

				case TABLE_LIST:
					index = this.input.getShort();
					length = this.input.getShort();

					GameClient.setCurrentRoomIndex(index);
					GameClient.initializeTableList(length);

					for (short i = 0; i < length; i++) {
						status = this.input.getByte();
						name = this.input.getString();

						GameClient.putTable(i, status, name);
					}
					break;

				case TABLE_JOIN:
					int newUserId = this.input.getInt();
					String newUserName = this.input.getString();

					System.out.println("New user has joined...");
					System.out.println("ID: " + newUserId);
					System.out.println("Name: " + newUserName);
					break;

				case TABLE_LEAVE:
					int userId = this.input.getInt();

					System.out.println("A user has leave...");
					System.out.println("ID: " + userId);
					break;

				default:
					throw new Exception("Unknown session opcode: " + command);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] getStartGameMessage()
	{
		int capacity = BYTE_SIZE;
		MessageBuffer message = new MessageBuffer(capacity);
		message.putByte(START_GAME_REQUEST);
		return message.getBuffer();
	}
}
