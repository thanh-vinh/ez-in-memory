
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.sgs.app.Channel;
import com.sun.sgs.app.ChannelListener;
import com.sun.sgs.app.ClientSession;
import java.nio.ByteBuffer;

class TableChannelListener implements Serializable, ChannelListener
{
	/** The version of the serialized form of this class. */
	private static final long serialVersionUID = 1L;

	/** The {@link Logger} for this class. */
	private static final Logger logger = Logger.getLogger(TableChannelListener.class.getName());
	private Table table;

	public TableChannelListener(Table table)
	{
		this.table = table;
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * Logs when data arrives on a channel. A typical listener would
	 * examine the message to decide whether it should be discarded,
	 * modified, or sent unchanged.
	 */
	public void receivedMessage(Channel channel, ClientSession session, ByteBuffer message)
	{
		if (logger.isLoggable(Level.INFO)) {
			logger.log(Level.INFO, "Channel message from {0} on channel {1}",
				new Object[] { session.getName(), channel.getName() });
		}

		Protocol protocol = new Protocol(channel, session);
		protocol.setMessage(message);
		ByteBuffer responseMessage = protocol.getMessage(true);

		if (responseMessage != null) {
			channel.send(responseMessage);
		}
	}
}
