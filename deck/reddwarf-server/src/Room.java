
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.sgs.app.AppContext;
import com.sun.sgs.app.ChannelManager;
import com.sun.sgs.app.Delivery;
import com.sun.sgs.app.NameExistsException;

public class Room implements Serializable
{
	/** The default name prefix of the channel */
	public static final String TABLE_NAME_PREFIX 	= "Table ";

	protected Table[] tables;

	/** The version of the serialized form of this class. */
	private static final long serialVersionUID 		= 1L;
	private static final Logger logger 				= Logger.getLogger(GameServer.class.getName());

	private short roomId;
	private String name;
	private short tablesPerRoom;

	public static String getTableName(int index)
	{
		String name = TABLE_NAME_PREFIX + index;
		return name;
	}

	public Room(short roomId, String name, short tablesPerRoom)
	{
		this.roomId = roomId;
		this.name = name;
		this.tablesPerRoom = tablesPerRoom;
	}

	public void initialize()
	{
		logger.log(Level.INFO, "Create channel list for {0}", this.name);

		ChannelManager channelManager = AppContext.getChannelManager();
		this.tables = new Table[this.tablesPerRoom];

		for (int i = 0; i < this.tablesPerRoom; i++) {
			String tableName = TABLE_NAME_PREFIX + String.valueOf(GameServer.TABLES_PER_ROOM * this.roomId + i);
			System.out.println("Create channel: " + tableName);
			try {
				this.tables[i] = new Table(tableName);
				TableChannelListener listener = new TableChannelListener(tables[i]);
				channelManager.createChannel(tableName, listener, Delivery.RELIABLE);
			} catch (NameExistsException e) {
				System.out.println("This channel name is exist...");
			}
		}
	}

	public String getName()
	{
		return this.name;
	}

	public byte getStatus()
	{
		return 0;
	}

	public short getTableCount()
	{
		return this.tablesPerRoom;
	}

	public Table getTableAt(short index)
	{
		return this.tables[index];
	}

	public String getTableNameAt(short index)
	{
		return this.tables[index].getName();
	}
}
