
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A buffer for composing messages.
 */
public class OutputMessageBuffer
{
	protected DataOutputStream output;

	private ByteArrayOutputStream byteOutputStream;

	/**
	 * Composing a response message use {@code DataOuputStream}.
	 * @see DataOutputStream
	 */
	public OutputMessageBuffer()
	{
		this.byteOutputStream = new ByteArrayOutputStream();
		this.output = new DataOutputStream(this.byteOutputStream);
	}

	public OutputMessageBuffer(ByteBuffer message) throws IOException
	{
		this.byteOutputStream = new ByteArrayOutputStream();
		this.output = new DataOutputStream(this.byteOutputStream);
		byte[] bytes = new byte[message.remaining()];
		message.get(bytes);

		this.output.write(bytes);
	}

	public byte[] getBytes()
	{
		return this.byteOutputStream.toByteArray();
	}

	public ByteBuffer getByteBuffer()
	{
		byte[] buf = this.byteOutputStream.toByteArray();
		return ByteBuffer.wrap(buf);
	}

	public final void putBoolean(boolean v) throws IOException
	{
		this.output.writeBoolean(v);
	}

	public final void putByte(byte v) throws IOException
	{
		this.output.writeByte(v);
	}

	public final void putShort(short v) throws IOException
	{
		this.output.writeShort(v);
	}

	public final void putChar(char v) throws IOException
	{
		this.output.writeChar(v);
	}

	public final void putInt(int v) throws IOException
	{
		this.output.writeInt(v);
	}

	public final void putLong(long v) throws IOException
	{
		this.output.writeLong(v);
	}

	public final void putUTF(String str) throws IOException
	{
		this.output.writeUTF(str);
	}

	public void close() throws IOException
	{
		if (this.output != null) {
			this.output.close();
			this.byteOutputStream.close();
		}
	}
}
