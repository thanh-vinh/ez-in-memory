
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A buffer for decomposing messages.
 */
public class InputMessageBuffer
{
	protected DataInputStream input;

	private byte[] buf;
	private ByteArrayInputStream byteInputStream;

	/**
	 * Decomposing a received message use {@code DataInputStream}.
	 * @see DataInputStream
	 */
	public InputMessageBuffer()
	{

	}

	public InputMessageBuffer(ByteBuffer message) throws IOException
	{
		this.setMessage(message);
	}

	public void setMessage(ByteBuffer message) throws IOException
	{
		this.close();

		// Convert ByteBuffer to DataInputStream
		this.buf = new byte[message.remaining()];
		message.get(buf);
		this.byteInputStream = new ByteArrayInputStream(buf);
		this.input = new DataInputStream(this.byteInputStream);
	}

	public final boolean getBoolean() throws IOException
	{
		return this.input.readBoolean();
	}

	public final byte getByte() throws IOException
	{
		return this.input.readByte();
	}

	public final short getShort() throws IOException
	{
		return this.input.readShort();
	}

	public final char getChar() throws IOException
	{
		return this.input.readChar();
	}

	public final int getInt() throws IOException
	{
		return this.input.readInt();
	}

	public final long getLong() throws IOException
	{
		return this.input.readLong();
	}

	public final String getUTF() throws IOException
	{
		return this.input.readUTF();
	}

	public void close() throws IOException
	{
		if (this.input != null) {
			this.input.close();
			this.byteInputStream.close();
			this.buf = null;
		}
	}
}
