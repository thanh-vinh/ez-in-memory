
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.sgs.app.AppListener;
import com.sun.sgs.app.ClientSession;
import com.sun.sgs.app.ClientSessionListener;

public class GameServer implements Serializable, AppListener
{
	public static final short MAX_ROOMS				= 10;
	public static final short TABLES_PER_ROOM		= 20;

	/** The default name prefix of the room */
	public static final String ROOM_NAME_PREFIX 	= "Room ";

	/** The name of properties file */
	public static final String PROPERTIES_FILE 		= "META-INF/config.properties";

	/** The version of the serialized form of this class. */
	private static final long serialVersionUID 		= 1L;
	private static final Logger logger 				= Logger.getLogger(GameServer.class.getName());

	private static Room[] rooms;
	private static Hashtable<String, Player> players;

	// private Properties props;

	public static Room getRoomAt(int index)
	{
		return rooms[index];
	}

	public static short getRoomCount()
	{
		return (short) rooms.length;
	}

	public static String getRoomNameAt(int index)
	{
		Room room = rooms[index];
		String name = room.getName();

		return name;
	}

	public static byte getRoomStatusAt(int index)
	{
		Room room = rooms[index];
		byte status = room.getStatus();

		return status;
	}

	public static Table getTableAt(short roomId, short tableId)
	{
		Room room = rooms[roomId];
		Table table = room.getTableAt(tableId);

		return table;
	}

	/**
	 * Add a player into Hashtable.
	 */
	public static void addPlayer(Player player)
	{
		String key = player.getUserName();
		players.put(key, player);
		logger.log(Level.INFO, "Added player \"{0}\" into Hashtable", key);
	}

	public static Player getPlayer(String key)
	{
		return players.get(key);
	}

	public static void removePlayer(String key)
	{
		players.remove(key);
		logger.log(Level.INFO, "Removed player \"{0}\" from Hashtable", key);
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * Creates the channels.  Channels persist across server restarts,
	 * so they only need to be created here in {@code initialize}.
	 */
	@Override
	public void initialize(Properties props)
	{
		logger.log(Level.INFO, "Create {0} rooms for GameServer", MAX_ROOMS);

		rooms = new Room[MAX_ROOMS];
		for (short i = 0; i < rooms.length; i++) {
			String name = ROOM_NAME_PREFIX + String.valueOf(i);
			rooms[i] = new Room(i, name, TABLES_PER_ROOM);
			rooms[i].initialize();
		}

		players = new Hashtable();
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * Returns a {@link PlayerSessionListener} for the logged-in session.
	 */
	@Override
	public ClientSessionListener loggedIn(ClientSession session)
	{
		logger.log(Level.INFO, "User {0} has logged in", session.getName());
		return new PlayerSessionListener(session);
	}
}
