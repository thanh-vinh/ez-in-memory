﻿/*
 * Created by SharpDevelop.
 * User: Thanh Vinh
 * Date: 11/8/2011
 * Time: 10:05 PM
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace ImageProcessor.Core
{
	public static class ImageLib
	{
		public static Bitmap GetBitmap(string fileName)
		{
			Bitmap bitmap = null;

			if (fileName != null) {
				try {
					bitmap = (Bitmap)Bitmap.FromFile(fileName);
				} catch (IOException e) {
					System.Diagnostics.Debug.WriteLine(e.StackTrace);
				}
			}

			return bitmap;
		}

		public static Bitmap Crop(string fileName, Rectangle rect)
		{
			Bitmap bitmap = null;

			try {
				Bitmap source = (Bitmap)Bitmap.FromFile(fileName);
				bitmap = source.Clone(rect, source.PixelFormat);
				source.Dispose();
			} catch (Exception e) {
				System.Diagnostics.Debug.WriteLine(e.StackTrace);
			}

			return bitmap;
		}

		public static void Save(Bitmap bitmap, string fileName)
		{
			bitmap.Save(fileName);
		}

		public static Rectangle BestFix(Rectangle selectedRect, Bitmap bitmap)
		{
			int TRANSPARENT_COLOR = Color.Magenta.ToArgb();
			Rectangle rect = selectedRect;

			if (selectedRect != null && bitmap != null) {
				//
				// Quick fix invalid rectangle
				//
				if (rect.X < 0) {
					rect.X = 0;
				}
				if (rect.Y < 0) {
					rect.Y = 0;
				}
				if (rect.X + rect.Width > bitmap.Width) {
					rect.Width = bitmap.Width - rect.X;
				}
				if (rect.Y + rect.Height > bitmap.Height) {
					rect.Height = bitmap.Height - rect.Y;
				}

				//
				// Ignore transparent pixels
				//
				int x, y;
				bool isSkip = false;

				// Scan left
				for (x = rect.X; x < rect.Right; x++) {
					isSkip = true;
					for (y = rect.Y; y < rect.Bottom; y++) {
						if (bitmap.GetPixel(x, y).ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Width -= (x - rect.X);
						rect.X = x;
					} else {
						break;
					}
				}

				// Scan top
				for (y = rect.Y; y < rect.Bottom; y++) {
					isSkip = true;
					for (x = rect.X; x < rect.Right; x++) {
						if (bitmap.GetPixel(x, y).ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Height -= (y - rect.Y);
						rect.Y = y;
					} else {
						break;
					}
				}

				// Scan right
				for (x = rect.Right - 1; x > rect.X; x--) {
					isSkip = true;
					for (y = rect.Y; y < rect.Bottom; y++) {
						if (bitmap.GetPixel(x, y).ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Width = x - rect.X;
					} else {
						break;
					}
				}

				// Scan bottom
				for (y = rect.Bottom - 1; y > rect.Y; y--) {
					isSkip = true;
					for (x = rect.X; x < rect.Right; x++) {
						if (bitmap.GetPixel(x, y).ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Height = y - rect.Y;
					} else {
						break;
					}
				}
			}

			return rect;
		}

		public static Rectangle BestFix(Rectangle selectedRect, string fileName)
		{
			Rectangle rect = selectedRect;

			if (fileName != null) {
				try {
					Bitmap bitmap = (Bitmap)Bitmap.FromFile(fileName);
					rect = BestFix(selectedRect, bitmap);
					bitmap.Dispose();
				} catch (IOException e) {
					System.Diagnostics.Debug.WriteLine(e.StackTrace);
				}
			}

			return rect;
		}

		public static void BestFixImage(string source, string dest)
		{
			Bitmap bitmap = GetBitmap(source);

			Rectangle sourceRect = new Rectangle();
			sourceRect.Width = bitmap.Width;
			sourceRect.Height = bitmap.Height;

			Rectangle destRect = BestFix(sourceRect, bitmap);
			bitmap.Dispose();
			bitmap = null;

			bitmap = Crop(source, destRect);
			Save(bitmap, dest);

			bitmap.Dispose();
			bitmap = null;
		}

		public static void MergeImages(string source, string dest, int patternSize, int maxColumn)
		{
			DirectoryInfo directory = new DirectoryInfo(source);
			FileInfo[] files = directory.GetFiles();
			Console.WriteLine("LENGTH: " + files.Length);

			Image[] images = new Image[files.Length];

			for (int i = 0; i < files.Length; i++) {
				string file = files[i].FullName;
				images[i] = Image.FromFile(file);
			}

			Bitmap destBitmap = new Bitmap((maxColumn + 1) * patternSize, (maxColumn * patternSize) << 1);
			Graphics g = Graphics.FromImage(destBitmap);
			Rectangle rect = new Rectangle(0, 0, destBitmap.Width, destBitmap.Height);
			g.SetClip(rect);
			g.FillRectangle(Brushes.Magenta, rect);

			int x = 0, y = 0;
			for (int i = 0; i < images.Length; i++) {
				x = (i % maxColumn) * patternSize;
				if (i % maxColumn == 0) {
					y += patternSize;
				}

				// Console.WriteLine(i + ": " + x + ", " + y);
				g.DrawImageUnscaled(images[i], x, y);
			}

			string fileName = dest + Path.DirectorySeparatorChar + "merged.bmp";
			destBitmap.Save(fileName);
			g.Dispose();
			destBitmap.Dispose();
			destBitmap = null;
		}
	}
}
