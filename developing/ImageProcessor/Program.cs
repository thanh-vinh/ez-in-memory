﻿/*
 * Created by SharpDevelop.
 * User: Thanh Vinh
 * Date: 11/8/2011
 * Time: 10:00 PM
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;

using ImageProcessor.Core;

namespace ImageProcesser
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static int Main(string[] args)
		{
			Console.WriteLine("ImageProcessor version 0.0.1");
			Console.WriteLine("Copyright 2011 EZ Team <ezgroup@groups.live.com>");

			if (args.Length > 2) {
				string command = args[0];
				string source = args[1];
				string dest = args[2];

				if (command.Equals("-bestfix")) {
					Console.WriteLine("Bestfix image...");

					try {
						DirectoryInfo directory = new DirectoryInfo(source);
						FileInfo[] files = directory.GetFiles();

						for (int i = 0; i < files.Length; i++) {
							Console.WriteLine("Processing file: " + files[i].Name);

							string input = files[i].FullName;
							string output = dest + Path.DirectorySeparatorChar + files[i].Name;

							ImageLib.BestFixImage(input, output);
						}

						Console.WriteLine("Completed...");
						return 0;
					} catch (Exception e) {
						Console.WriteLine(e.StackTrace);
					}
				} else if (command.Equals("-merge")) {
					try {
						Console.WriteLine("Merge images...");

			      		int patternSize = int.Parse(args[3]);
		      			int maxColumn = int.Parse(args[4]);

		      			ImageLib.MergeImages(source, dest, patternSize, maxColumn);

		      			Console.WriteLine("Completed...");
		      			return 0;
		      		} catch (Exception e) {
		      			Console.WriteLine(e.StackTrace);
		      		}
				} else {
					Console.WriteLine("Invalid command!");
				}
			} else {
				Console.WriteLine("Invalid parameters, please see the readme file for details!");
			}

			return 1;
		}

	}
}
