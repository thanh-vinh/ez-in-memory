
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Export text from a data source and generate source code.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @date August 14, 2011
 */

public class TextPackage
{
	private static final String TEXT_ENCODING			= "UTF-8";
	private static final String OUT_FILE_EXT			= ".bin";

	private String name;
	private String platform = TextExporter.PLATFORM_J2ME;
	private ArrayList<String> indexes;
	private ArrayList<String> values;

	public TextPackage(String name)
	{
		this.name = name;
		this.indexes = new ArrayList<String>();
		this.values = new ArrayList<String>();
	}

	public void setPlatform(String platform)
	{
		this.platform = platform;
	}

	public void add(String index, String value)
	{
		this.indexes.add(index);
		this.values.add(value);
	}

	/**
	 * Save to a binary file.
	 */
	public void exportData(String outputFile) throws IOException
	{
		String fileName = outputFile + "_" + this.name + OUT_FILE_EXT;
		DataOutputStreamEx output = new DataOutputStreamEx(new FileOutputStream(fileName, false));

		// Use Little Endian for XNA platform
		boolean isReverseBytes = (this.platform.equals(TextExporter.PLATFORM_XNA) ? true : false);

		// Elements size
		output.writeInt(this.values.size(), isReverseBytes);

		for (int i = 0; i < this.values.size(); i++) {
			String value = this.values.get(i);
			byte[] data = value.getBytes(TEXT_ENCODING);
			int length = data.length;

			// Write binary data
			output.writeInt(length, isReverseBytes);	// Byte length (value string length)
			output.write(data, 0, length);				// Byte (value string)
		}

		output.close();
	}

	/**
	 * Get source code, include text id.
	 */
	public String getSource()
	{
		StringBuffer buffer = new StringBuffer();

		// Use Java source for J2ME platform, else C++/C# source
		boolean isJavaSource = (this.platform.equals(TextExporter.PLATFORM_XNA) ? false : true);

		for (int i = 0; i < this.indexes.size(); i++) {
			if (isJavaSource) {
				buffer.append("\n\tpublic static final int ");
			} else {
				buffer.append("\n\tpublic const int ");
			}
			buffer.append(this.indexes.get(i));
			buffer.append(" = ");
			buffer.append(i);
			buffer.append(";");
		}

		String source = buffer.toString();
		return source;
	}
}
