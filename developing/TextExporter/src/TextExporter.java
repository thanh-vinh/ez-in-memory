
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

/**
 * Export text from a data source and generate source code.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @date December 24, 2011
 */

public class TextExporter
{
	public static final String PLATFORM_J2ME			= "J2ME";
	public static final String PLATFORM_XNA				= "XNA";

	private static final int TEXT_ID_COLUMN				= 0;
	private static final int TEXT_HEADER_ROW			= 0;

	private static final String TEXT_ENCODING			= "UTF-8";
	private static final String SOURCE_FILE_NAME		= "TEXT";
	private static final String JAVA_EXT				= ".java";
	private static final String CPP_EXT					= ".cpp";
	private static final String CS_EXT					= ".cs";

	private String platform;
	private String dataOutPath;
	private String sourceOutPath;
	private Workbook workbook;

	public TextExporter(String platform, String inputFile, String dataOutputPath, String sourceOutPath) throws Exception
	{
		this.platform = platform;
		this.dataOutPath = dataOutputPath;
		this.sourceOutPath = sourceOutPath;

		try {
			// WorkbookSettings ws = new WorkbookSettings();
			// ws.setEncoding(TEXT_ENCODING);
			// this.workbook = Workbook.getWorkbook(new File(inputFile), ws);
			this.workbook = Workbook.getWorkbook(new File(inputFile));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<TextPackage> readSheet(Sheet sheet)
	{
		ArrayList<TextPackage> texts = new ArrayList<TextPackage>();
		int columnCount = sheet.getColumns();
		int rowCount = sheet.getRows();
		// System.out.println("columnCount: " + columnCount);
		// System.out.println("rowCount: " + rowCount);

		// Loop all columns in current sheet
		for (int column = TEXT_ID_COLUMN + 1; column < columnCount; column++) {
			// Ignore empty column - column has not a header
			Cell cell = sheet.getCell(column, TEXT_HEADER_ROW);
			String language = cell.getContents();
			if (language.length() == 0)
				break;

			TextPackage text = new TextPackage(language);
			text.setPlatform(this.platform);

			// Loop all cells in current sheet
			for (int row = TEXT_HEADER_ROW + 1; row < rowCount; row++) {
				// Ignore emty row - row has not an a text id
				cell = sheet.getCell(TEXT_ID_COLUMN, row);
				String index = cell.getContents();

				if (index.length() == 0)
					break;

				cell = sheet.getCell(column, row);
				String value = cell.getContents();
				text.add(index, value);
			}

			texts.add(text);
		}

		return texts;
	}

	public void exportData() throws IOException
	{
		System.out.println("Export data...");

		for (int i = 0; i < this.workbook.getNumberOfSheets(); i++) {
			Sheet sheet = this.workbook.getSheet(i);
			String sheetName = sheet.getName().toUpperCase();
			System.out.println("Processing sheet: " + sheetName);
			ArrayList<TextPackage> texts = this.readSheet(sheet);

			String fileName = this.dataOutPath + File.separator + sheetName;
			for (int j = 0; j < texts.size(); j++) {
				TextPackage text = texts.get(j);
				text.exportData(fileName);
			}
		}
	}

	public void exportSource() throws IOException
	{
		System.out.println("Export source...");
		StringBuffer buffer = new StringBuffer();

		// Use Java source for J2ME platform, else C++/C# source
		boolean isJavaSource = (this.platform.equals(PLATFORM_XNA) ? false : true);
		if (isJavaSource) {
			buffer.append("\npublic interface ");
		} else {
			buffer.append("\npublic static class ");
		}
		buffer.append(SOURCE_FILE_NAME);
		buffer.append("\n{");

		for (int i = 0; i < this.workbook.getNumberOfSheets(); i++) {
			Sheet sheet = this.workbook.getSheet(i);
			String sheetName = sheet.getName().toUpperCase();
			ArrayList<TextPackage> texts = this.readSheet(sheet);

			buffer.append("\n\t//");
			buffer.append("\n\t// Sheet: " + sheetName);
			buffer.append("\n\t//");

			if (texts.size() > 0) {
				TextPackage text = texts.get(TEXT_ID_COLUMN);	// Choise a language only
				String source = text.getSource();
				buffer.append(source);
			}
		}

		buffer.append("\n}\n");

		// Write to source file, default: Java language
		// TODO Implement others languages
		String fileName = this.sourceOutPath + File.separator + SOURCE_FILE_NAME;
		String extension = (this.platform.equals(PLATFORM_XNA) ? CS_EXT : JAVA_EXT);
		fileName += extension;

		BufferedWriter writer = new BufferedWriter(
			new OutputStreamWriter(new FileOutputStream(fileName, false), TEXT_ENCODING));
		writer.write(buffer.toString());
		writer.close();
	}

	public static void main(String[] args)
	{
		System.out.println("TextExporter v0.0.4");
		System.out.println("EZ Team, 2011");
		System.out.println("Contact: Thanh Vinh <thanh.vinh@hotmail.com>");
		System.out.println("Third party application/library: JXL library");

		if (args.length > 3) {
			String platform = args[0];
			String inputFile = args[1];
			String dataOutPath = args[2];
			String sourceOutPath = args[3];

			try {
				TextExporter text = new TextExporter(platform, inputFile, dataOutPath, sourceOutPath);

				System.out.println(" - Platform: " + platform);
				System.out.println(" - Input file in: " + inputFile);
				System.out.println(" - Save package files in: " + dataOutPath);
				System.out.println(" - Save source file in: " + sourceOutPath);

				text.exportData();
				text.exportSource();

				System.out.println("Export complete...");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Export fail, please read the help file to format your XLS/ODS file correct!");
			}
		} else {
			System.out.println("Command: TextExporter platform input-file data-out-path source-out-path");
			System.out.println("Example: TextExporter XNA Z:/data/text.xls Z:/packages Z:/source");
		}
	}
}
