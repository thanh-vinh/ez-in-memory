
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.IOException;

public class DataOutputStreamEx extends DataOutputStream
{
	public static short reverseBytes(short value)
	{
		return (short)((value & 0xFF) << 8 | (value & 0xFF00) >> 8);
	}

	public static int reverseBytes(int value)
	{
		return (value & 0x000000FF) << 24 | (value & 0x0000FF00) << 8 |
			(value & 0x00FF0000) >> 8 | (value & 0xFF000000) >> 24;
	}

	/*
	public static long reverseBytes(long value)
	{
		return (value & 0x00000000000000FF) << 56 | (value & 0x000000000000FF00) << 40 |
			(value & 0x0000000000FF0000) << 24 | (value & 0x00000000FF000000) << 8 |
			(value & 0x000000FF00000000) >> 8 | (value & 0x0000FF0000000000) >> 24 |
			(value & 0x00FF000000000000) >> 40 | (value & 0xFF00000000000000) >> 56;
	}
	*/

	public DataOutputStreamEx(OutputStream out)
	{
		super(out);
	}

	public void writeShort(short value, boolean reverse) throws IOException
	{
		if (reverse) {
			value = reverseBytes(value);
		}
		super.writeShort(value);
	}

	public void writeInt(int value, boolean reverse) throws IOException
	{
		if (reverse) {
			value = reverseBytes(value);
		}
		super.writeInt(value);
	}
}
