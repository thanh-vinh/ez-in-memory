﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using SpriteEditor.Core;

namespace SpriteEditor.UI
{
	public partial class SpriteEditorToolStrip : UserControl
	{
		private SpriteEditorForm form;

		public SpriteEditorToolStrip()
		{
			InitializeComponent();
		}

		#region Events
		private void newToolStripButton_Click(object sender, EventArgs e)
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Filter = "Sprite file (*.sprite)|*.sprite";
			dlg.ShowDialog();

			String fileName = dlg.FileName;
			if (fileName.Length > 0) {
				SpriteFile.New(fileName);
				SpriteFile sprite = new SpriteFile(fileName);
				this.form.SpriteFile = sprite;
			}
		}

		private void openToolStripButton_Click(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "Sprite file (*.sprite)|*.sprite";
			dlg.ShowDialog();

			if (dlg.FileName != "") {
				this.form.SpriteFile = new SpriteFile(dlg.FileName);
			}
		}

		private void saveToolStripButton_Click(object sender, EventArgs e)
		{
			if (this.form.SpriteFile != null) {
				if (!this.form.SpriteFile.IsNewSprite) {
					this.form.SpriteFile.Save();
				} else {
					SaveFileDialog dlg = new SaveFileDialog();
					dlg.Filter = "Sprite file (*.sprite)|*.sprite";
					dlg.ShowDialog();

					String fileName = dlg.FileName;
					if (fileName.Length > 0) {
						this.form.SpriteFile.FileName = fileName;
						this.form.SpriteFile.Save();
					}
				}
			}
		}

		private void zoomInToolStripButton_Click(object sender, EventArgs e)
		{
			this.form.ImageViewer.Zoom += 100;
		}

		private void zoomOutToolStripButton_Click(object sender, EventArgs e)
		{
			if (this.form.ImageViewer.Zoom > 100) {
				this.form.ImageViewer.Zoom -= 100;
			}
		}

		private void zoomToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.zoomToolStripComboBox.SelectedIndex > -1) {
				String zoomValue = this.zoomToolStripComboBox.Items[this.zoomToolStripComboBox.SelectedIndex].ToString();
				zoomValue = zoomValue.TrimEnd('%');
				this.form.ImageViewer.Zoom = Int32.Parse(zoomValue);
			}
		}

		//
		// Set background color
		//
		void WhiteToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.form.ImageViewer.BackColor = Color.White;
		}

		void MagentaToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.form.ImageViewer.BackColor = Color.Magenta;
		}
		#endregion

		#region Properties
		public int ZoomValue
		{
			set {
				this.zoomToolStripComboBox.Text = value + "%";
			}
		}
		#endregion

		public void SetParentForm(SpriteEditorForm form)
		{
			this.form = form;
		}
	}
}
