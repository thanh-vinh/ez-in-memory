﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using SpriteEditor.Core;
using SpriteEditor.Components;

namespace SpriteEditor.UI
{
	public partial class SpriteEditorForm : Form
	{
		public const String FORM_TEXT       = "Sprite Editor";
		public const int TAB_PAGE_MODULE    = 0;
		public const int TAB_PAGE_FRAME     = 1;
		public const int TAB_PAGE_ANIM      = 2;

		//
		// Properties
		//
		private int currentTabPage;
		private SpriteFile spriteFile;

		public SpriteEditorForm()
		{
			InitializeComponent();
		}

		#region Handle Events
		private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.currentTabPage = this.tabControl.SelectedIndex;

            if (this.spriteFile != null)
            {
                if (this.currentTabPage == TAB_PAGE_MODULE)
                {
                    this.ImageViewer.Mode = ImageViewerMode.Module;
                }
                else if (this.tabControl.SelectedIndex == TAB_PAGE_FRAME)
                {
                    this.modulePicker.Items = this.spriteFile.Modules;
                    this.modulePicker.Images = this.spriteFile.GetAllModuleImages();
                    this.ImageViewer.Mode = ImageViewerMode.Frame;
                }
                else if (this.tabControl.SelectedIndex == TAB_PAGE_ANIM)
                {
                    this.framePicker.Items = this.spriteFile.Frames;
                    this.framePicker.Images = this.spriteFile.GetAllFrameImages();
                    this.ImageViewer.Mode = ImageViewerMode.Animation;
                }
            }
		}

		//
		// Tool strip
		//
		private void saveToolStripButton_Click(object sender, EventArgs e)
		{
			this.spriteFile.Save();
		}

		//
		// ListView
		//
		private void listViewItemsChanged(object sender, EventArgs e)
		{
			this.moduleImageViewer.SelectedIndex = this.moduleListView.GetSelectedIndex();
		}

		private void moduleListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.moduleImageViewer.SelectedIndex = this.moduleListView.GetSelectedIndex();
		}

		private void frameListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = this.frameListView.GetSelectedIndex();

			if (index > -1) {
				this.ImageViewer.Mode = ImageViewerMode.Frame;
				this.ImageViewer.SelectedIndex = index;
				this.fmoduleListView.Items = ((Frame)this.spriteFile.Frames[index]).FModules;
			}
		}

		private void fmoduleListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = this.fmoduleListView.GetSelectedIndex();

			if (index > -1) {
				this.ImageViewer.Mode = ImageViewerMode.FModule;
				this.ImageViewer.SubItemSelectedIndex = index;
			}
		}

		private void animationListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = this.animationListView.GetSelectedIndex();

			if (index > -1) {
				this.ImageViewer.Mode = ImageViewerMode.Animation;
				this.ImageViewer.SelectedIndex = index;
				this.aframeListView.Items = ((Animation)this.spriteFile.Animations[index]).AFrames;
			}
		}

		private void aframeListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = this.aframeListView.GetSelectedIndex();

			if (index > -1) {
				this.ImageViewer.Mode = ImageViewerMode.AFrame;
				this.ImageViewer.SubItemSelectedIndex = index;
			}
		}

		private void paletteListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.spriteFile != null) {
				Palette palette = (Palette)this.paletteListView.GetSelected();
				if (palette != null) {
					this.ImageViewer.Palette = palette;
				}
			}
		}

		//
		// SpriteItemPicker
		//
		private void modulePicker_DoubleClick(object sender, EventArgs e)
		{
			Frame frame = (Frame)this.frameListView.GetSelected();
			if (frame != null) {
			   	int index = this.modulePicker.SelectedIndex;
			    Module module = (Module)this.modulePicker.SelectedItem;
			    FModule fmodule = new FModule(module);
			    frame.FModules.Add(fmodule);
			    this.fmoduleListView.Refresh();

			}
		}

		private void framePicker_DoubleClick(object sender, EventArgs e)
		{
			Animation animation = (Animation)this.animationListView.GetSelected();
			if (animation != null) {
			    int index = this.framePicker.SelectedIndex;
			    Frame frame = (Frame)this.framePicker.SelectedItem;
			    AFrame aFrame = new AFrame(frame);
				animation.AFrames.Add(aFrame);
			    this.aframeListView.Refresh();
			}
		}

		//
		// ImageViewer
		//
		private void imageViewer_DrawedRect(object sender, EventArgs e)
		{
			this.ListViewSpriteItem.Refresh();
		}

		private void imageViewer_MovedRect(object sender, EventArgs e)
		{
			this.ListViewSpriteItem.Refresh();
		}

		private void imageViewer_ResizedRect(object sender, EventArgs e)
		{
			this.ListViewSpriteItem.Refresh();
		}

		private void moduleImageViewer_MouseMove(object sender, MouseEventArgs e)
		{
			this.mousePositionStatusLabel.Text = "X, Y: " + this.frameImageViewer.GetMouseX() + ", " + this.frameImageViewer.GetMouseY();
		}

		private void moduleImageViewer_ZoomInOut(object sender, EventArgs e)
		{
			this.toolStrip.ZoomValue = this.frameImageViewer.Zoom;
		}

		//
		// Forms
		//
		private void SpriteEditorForm_Load(object sender, EventArgs e)
		{
			this.toolStrip.SetParentForm(this);
		}

		private void SpriteEditorForm_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
				e.Effect = DragDropEffects.All;
			} else {
				e.Effect = DragDropEffects.None;
			}
		}

		private void SpriteEditorForm_DragDrop(object sender, DragEventArgs e)
		{
			String fileName = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
			if (System.IO.Path.GetExtension(fileName).Equals(".sprite")) {
				this.SpriteFile = new SpriteFile(fileName);
			}
		}

		//
		// LinkLabel
		//
		private void changeSpriteImageLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "Sprite file (*.png)|*.png";
			dlg.ShowDialog();

			String fileName = dlg.FileName;

			if (fileName.Length > 0) {
				this.spriteFile.ImageFileName = fileName;
				this.ImageViewer.Sprite = this.spriteFile;
				this.ImageViewer.Mode = ImageViewerMode.Module;

				if (this.spriteFile.Palettes.Count == 0) {
					Color[] colors = ImageLib.GetColorPalette(fileName);
					Palette palette = new Palette(0, colors);
					this.spriteFile.Palettes.Add(palette);
				} else {
					((Palette)this.spriteFile.Palettes[0]).Colors = ImageLib.GetColorPalette(fileName); //this.ImageViewer.Image.Palette.Entries;
				}

				this.paletteListView.Refresh();
			}
		}
		#endregion

		#region Properties
		public SpriteFile SpriteFile
		{
			get
			{
				return spriteFile;
			}

			set
			{
				this.spriteFile = value;
				this.moduleImageViewer.Sprite = this.spriteFile;
				this.frameImageViewer.Sprite = this.spriteFile;
				this.animationImageViewer.Sprite = this.spriteFile;

				this.ImageViewer.Mode = ImageViewerMode.Module;

				this.moduleListView.Items = this.spriteFile.Modules;
				this.frameListView.Items = this.spriteFile.Frames;
				this.paletteListView.Items = this.spriteFile.Palettes;
				this.animationListView.Items = this.spriteFile.Animations;

				// Show sprite details
				base.Text = FORM_TEXT + " - " + Path.GetFileName(this.spriteFile.FileName);
				this.spriteNameLabel.Text = Path.GetFileName(this.spriteFile.FileName);
				this.spriteImageLabel.Text = Path.GetFileName(this.spriteFile.ImageFileName);
			}
		}

		public ImageViewer ImageViewer
		{
			get
			{
				if (this.currentTabPage == TAB_PAGE_MODULE)
					return this.moduleImageViewer;
				else if (this.currentTabPage == TAB_PAGE_FRAME)
					return this.frameImageViewer;
				else if (this.currentTabPage == TAB_PAGE_ANIM)
					return this.animationImageViewer;
				else
					return null;
			}
		}

		public ListViewSpriteItem ListViewSpriteItem
		{
			get
			{
				if (this.ImageViewer.Mode == ImageViewerMode.Module) {
					return this.moduleListView;
				} else if (this.ImageViewer.Mode == ImageViewerMode.FModule) {
					return this.fmoduleListView;
				} else if (this.ImageViewer.Mode == ImageViewerMode.Frame) {
					return this.frameListView;
				} else if (this.ImageViewer.Mode == ImageViewerMode.AFrame) {
					return this.aframeListView;
				} else if (this.ImageViewer.Mode == ImageViewerMode.Animation) {
					return this.animationListView;
				} else {
					return null;
				}
			}
		}

		#endregion
	}
}
