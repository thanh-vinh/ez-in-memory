﻿namespace SpriteEditor.UI
{
	partial class SpriteEditorToolStrip
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpriteEditorToolStrip));
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.zoomInToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.zoomOutToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.zoomToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.backgroundToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
			this.whiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.magentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip.SuspendLayout();
			this.SuspendLayout();
			//
			// toolStrip
			//
			this.toolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.newToolStripButton,
									this.openToolStripButton,
									this.saveToolStripButton,
									this.toolStripSeparator1,
									this.zoomInToolStripButton,
									this.zoomOutToolStripButton,
									this.zoomToolStripComboBox,
									this.toolStripSeparator2,
									this.backgroundToolStripDropDownButton});
			this.toolStrip.Location = new System.Drawing.Point(0, 0);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(873, 31);
			this.toolStrip.TabIndex = 2;
			this.toolStrip.Text = "toolStrip";
			//
			// newToolStripButton
			//
			this.newToolStripButton.Image = global::SpriteEditor.Properties.Resources.new_sprite;
			this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.newToolStripButton.Name = "newToolStripButton";
			this.newToolStripButton.Size = new System.Drawing.Size(59, 28);
			this.newToolStripButton.Text = "New";
			this.newToolStripButton.Click += new System.EventHandler(this.newToolStripButton_Click);
			//
			// openToolStripButton
			//
			this.openToolStripButton.Image = global::SpriteEditor.Properties.Resources.open_sprite;
			this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.openToolStripButton.Name = "openToolStripButton";
			this.openToolStripButton.Size = new System.Drawing.Size(64, 28);
			this.openToolStripButton.Text = "Open";
			this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
			//
			// saveToolStripButton
			//
			this.saveToolStripButton.Image = global::SpriteEditor.Properties.Resources.save_sprite;
			this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.saveToolStripButton.Name = "saveToolStripButton";
			this.saveToolStripButton.Size = new System.Drawing.Size(59, 28);
			this.saveToolStripButton.Text = "Save";
			this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
			//
			// toolStripSeparator1
			//
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
			//
			// zoomInToolStripButton
			//
			this.zoomInToolStripButton.Image = global::SpriteEditor.Properties.Resources.zoom_in;
			this.zoomInToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.zoomInToolStripButton.Name = "zoomInToolStripButton";
			this.zoomInToolStripButton.Size = new System.Drawing.Size(80, 28);
			this.zoomInToolStripButton.Text = "Zoom in";
			this.zoomInToolStripButton.Click += new System.EventHandler(this.zoomInToolStripButton_Click);
			//
			// zoomOutToolStripButton
			//
			this.zoomOutToolStripButton.Image = global::SpriteEditor.Properties.Resources.zoom_out;
			this.zoomOutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.zoomOutToolStripButton.Name = "zoomOutToolStripButton";
			this.zoomOutToolStripButton.Size = new System.Drawing.Size(88, 28);
			this.zoomOutToolStripButton.Text = "Zoom out";
			this.zoomOutToolStripButton.Click += new System.EventHandler(this.zoomOutToolStripButton_Click);
			//
			// zoomToolStripComboBox
			//
			this.zoomToolStripComboBox.Items.AddRange(new object[] {
									"100%",
									"200%",
									"300%",
									"400%",
									"500%",
									"600%",
									"700%",
									"800%",
									"900%"});
			this.zoomToolStripComboBox.Name = "zoomToolStripComboBox";
			this.zoomToolStripComboBox.Size = new System.Drawing.Size(121, 31);
			this.zoomToolStripComboBox.Text = "100%";
			this.zoomToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.zoomToolStripComboBox_SelectedIndexChanged);
			//
			// toolStripSeparator2
			//
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
			//
			// backgroundToolStripDropDownButton
			//
			this.backgroundToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.backgroundToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.whiteToolStripMenuItem,
									this.magentaToolStripMenuItem});
			this.backgroundToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("backgroundToolStripDropDownButton.Image")));
			this.backgroundToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.backgroundToolStripDropDownButton.Name = "backgroundToolStripDropDownButton";
			this.backgroundToolStripDropDownButton.Size = new System.Drawing.Size(84, 28);
			this.backgroundToolStripDropDownButton.Text = "Background";
			//
			// whiteToolStripMenuItem
			//
			this.whiteToolStripMenuItem.Name = "whiteToolStripMenuItem";
			this.whiteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.whiteToolStripMenuItem.Text = "White";
			this.whiteToolStripMenuItem.Click += new System.EventHandler(this.WhiteToolStripMenuItemClick);
			//
			// magentaToolStripMenuItem
			//
			this.magentaToolStripMenuItem.Name = "magentaToolStripMenuItem";
			this.magentaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.magentaToolStripMenuItem.Text = "Magenta";
			this.magentaToolStripMenuItem.Click += new System.EventHandler(this.MagentaToolStripMenuItemClick);
			//
			// SpriteEditorToolStrip
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.toolStrip);
			this.Name = "SpriteEditorToolStrip";
			this.Size = new System.Drawing.Size(873, 40);
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.ToolStripMenuItem whiteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem magentaToolStripMenuItem;
		private System.Windows.Forms.ToolStripDropDownButton backgroundToolStripDropDownButton;

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton openToolStripButton;
		private System.Windows.Forms.ToolStripButton saveToolStripButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton zoomInToolStripButton;
		private System.Windows.Forms.ToolStripButton zoomOutToolStripButton;
		private System.Windows.Forms.ToolStripComboBox zoomToolStripComboBox;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton newToolStripButton;
	}
}
