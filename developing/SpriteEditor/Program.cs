﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

using SpriteEditor.Core;
using SpriteEditor.UI;

namespace SpriteEditor
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static int Main(String[] args)
		{
			if (args.Length == 0) {
				// Use GUI
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Application.Run(new SpriteEditorForm());

				return 0;
			} else {
				// No use GUI
				if (args.Length > 3) {
					// Get the parameters
					String platform = args[0];
					String inputFile = args[1];
					String outputDataPath = args[2];
					String outputSourcePath = args[3];

					SpriteFile spriteFile = new SpriteFile(inputFile);
					if (platform.Equals("J2ME", StringComparison.CurrentCultureIgnoreCase)) {
						spriteFile.Platform = Platform.J2ME;
					} else if (platform.Equals("XNA", StringComparison.CurrentCultureIgnoreCase)) {
						spriteFile.Platform = Platform.WindowsPhone;
					}

					//
					// Binary data
					//
					// Binary output file with the name same as input file
					String outputFile = outputDataPath + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(inputFile);
					// with extension is .bsprite
					outputFile += ".bsprite";
					spriteFile.SaveBinary(outputFile);

					//
					// Source code
					//
					// Source output file with the name same as input file
					outputFile = outputSourcePath + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(inputFile);
					// with extension is .h
					outputFile += ".h";

					// Save source file
					spriteFile.SaveSource(outputFile);
					Application.Exit();

					return 0;
				} else {
					return 1;
				}
			}
		}
	}
}
