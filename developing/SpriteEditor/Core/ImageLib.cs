﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

#if USE_FREE_IMAGE_LIB
using FreeImageAPI;
#endif

namespace SpriteEditor.Core
{
	public static class ImageLib
	{
		public static Image GetImage(string fileName)
		{
			Image image = null;

			if (fileName != null) {
				try {
					image = Image.FromFile(fileName);
				} catch (IOException e) {
					System.Diagnostics.Debug.WriteLine(e.StackTrace);
				}
			}

			return image;
		}

		public static byte GetPixelLength(string fileName)
		{
			Image image = Image.FromFile(fileName);
			byte length;

			switch (image.PixelFormat) {
				case PixelFormat.Format8bppIndexed:
					length = 1;
					break;

				case PixelFormat.Format24bppRgb:
					length = 3;
					break;

				//case PixelFormat.Format32bppArgb:
				//    return 4;
				default:
					length = 4;
					break;
			}

			image.Dispose();
			return length;
		}

		/// <summary>
		/// Get palette from an image file.
		/// </summary>
		public static Color[] GetColorPalette(string fileName)
		{
			Color[] colors = null;
			if (fileName != null) {
				try {
					Image image = Image.FromFile(fileName);
					colors = image.Palette.Entries;
					image.Dispose();
				} catch (IOException e) {
					System.Diagnostics.Debug.WriteLine(e.StackTrace);
				}
			}

			return colors;
		}

		/// <summary>
		/// Get palette from a palette file.
		/// </summary>
		public static Color[] GetColorPalette(string fileName, int size)
		{
			FileStream f = File.OpenRead(fileName);
			byte[] data = new byte[f.Length];
			f.Read(data, 0, data.Length);
			Color[] paletteColor = new Color[size];
			for (int i = 0; i < size; i++) {
				paletteColor[i] = Color.FromArgb(0xff, data[i * 3] & 0xff, data[i * 3 + 1] & 0xff, data[i * 3 + 2] & 0xff);
			}

			f.Close();
			f.Dispose();
			return paletteColor;
		}

		public static byte[] GetPalette(string fileName)
		{
			Image image = Bitmap.FromFile(fileName);
			Color[] colors = image.Palette.Entries;
			int length = colors.Length;
			byte[] palette = new byte[length * 3];
			for (int i = 0; i < length; i++) {
				palette[i * 3] = colors[i].R;
				palette[i * 3 + 1] = colors[i].G;
				palette[i * 3 + 2] = colors[i].B;
			}

			return palette;
		}

		public static byte[] LockBits(string fileName, Rectangle rect)
		{
			Bitmap bitmap = (Bitmap)Bitmap.FromFile(fileName);
			BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.ReadOnly, bitmap.PixelFormat);

			int pixelLength = GetPixelLength(fileName);

			byte[] data = new byte[bitmapData.Height * bitmapData.Width * pixelLength];
			System.Runtime.InteropServices.Marshal.Copy(bitmapData.Scan0, data, 0, data.Length);
			bitmap.UnlockBits(bitmapData);
			bitmap.Dispose();

			return data;
		}

		/// <summary>
		/// Get RGBA values of a part of an image, using LockBits method.
		/// </summary>
		public static byte[] GetPixelData(string fileName, Rectangle rect)
		{
			Bitmap bitmap = (Bitmap)Bitmap.FromFile(fileName);
			BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.ReadOnly, bitmap.PixelFormat);

			//
			// Compute pixel range
			//
			int width = rect.Width * GetPixelLength(fileName);
			int height = rect.Height;
			int y = rect.Y;
			byte[] data = new byte[width * height];

			//
			// Scan each row for removing padding data
			//
			IntPtr ptr = bitmapData.Scan0;
			int offset = 0;
			for (int row = y; row < (y + height); row++) {
				System.Runtime.InteropServices.Marshal.Copy(ptr, data, offset, width);
				ptr = new IntPtr(ptr.ToInt32() + bitmapData.Stride);
				offset += width;
			}
			bitmap.UnlockBits(bitmapData);
			bitmap.Dispose();

			return data;
		}

		public static Bitmap Crop(string fileName, Rectangle rect)
		{
			Bitmap bitmap = null;

			try {
				Bitmap source = (Bitmap)Bitmap.FromFile(fileName);
				bitmap = source.Clone(rect, source.PixelFormat);
				source.Dispose();
			} catch (Exception e) {
				System.Diagnostics.Debug.WriteLine(e.StackTrace);
			}

			return bitmap;
		}

		/// <summary>
		/// Zoom in / zoom out an image with a scale - faster than using FreeImage.Rescale method.
		/// </summary>
		/// <param name="image">Original image.</param>
		/// <param name="scale">Scale value, example: Zoom in 200% -> scale = 2.</param>
		public static Image Rescale(Image image, int scale)
		{
			if (image != null) {
				int sourceWidth = image.Width;
				int sourceHeight = image.Height;

				int destWidth = sourceWidth * scale;
				int destHeight = sourceHeight * scale;

				Bitmap bitmap = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

				bitmap.MakeTransparent();
				bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

				Graphics graphics = Graphics.FromImage(bitmap);

				graphics.CompositingMode = CompositingMode.SourceCopy;
				graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
				graphics.DrawImage(image, new Rectangle(0, 0, destWidth, destHeight));//, GraphicsUnit.Pixel);

				graphics.Dispose();
				return bitmap;
			} else {
				return null;
			}
		}

		public static Size GetSize(string fileName)
		{
			Size size = new Size(0, 0);
			if (fileName != null) {
				try {
					Image image = Image.FromFile(fileName);
					size = image.Size;
				} catch (IOException e) {
					System.Diagnostics.Debug.WriteLine(e.StackTrace);
				}
			}

			return size;
		}

		public static Rectangle BestFix(Rectangle selectedRect, Bitmap bitmap)
		{
			int TRANSPARENT_COLOR = Color.Magenta.ToArgb();
			Rectangle rect = selectedRect;

			if (selectedRect != null && bitmap != null) {
				//
				// Quick fix invalid rectangle
				//
				if (rect.X < 0) {
					rect.X = 0;
				}
				if (rect.Y < 0) {
					rect.Y = 0;
				}
				if (rect.X + rect.Width > bitmap.Width) {
					rect.Width = bitmap.Width - rect.X;
				}
				if (rect.Y + rect.Height > bitmap.Height) {
					rect.Height = bitmap.Height - rect.Y;
				}

				//
				// Ignore transparent pixels
				//
				int x, y;
				bool isSkip = false;

				// Scan left
				for (x = rect.X; x < rect.Right; x++) {
					isSkip = true;
					for (y = rect.Y; y < rect.Bottom; y++) {
						Color color = bitmap.GetPixel(x, y);
						if (color.A != 0x0 && color.ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Width -= (x - rect.X);
						rect.X = x;
					} else {
						break;
					}
				}

				// Scan top
				for (y = rect.Y; y < rect.Bottom; y++) {
					isSkip = true;
					for (x = rect.X; x < rect.Right; x++) {
						Color color = bitmap.GetPixel(x, y);
						if (color.A != 0x0 && color.ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Height -= (y - rect.Y);
						rect.Y = y;
					} else {
						break;
					}
				}

				// Scan right
				for (x = rect.Right; x > rect.X; x--) {
					isSkip = true;
					for (y = rect.Y; y < rect.Bottom; y++) {
						Color color = bitmap.GetPixel(x, y);
						if (color.A != 0x0 && color.ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Width = x - rect.X;
					} else {
						break;
					}
				}

				// Scan bottom
				for (y = rect.Bottom; y > rect.Y; y--) {
					isSkip = true;
					for (x = rect.X; x < rect.Right; x++) {
						Color color = bitmap.GetPixel(x, y);
						if (color.A != 0x0 && color.ToArgb() != TRANSPARENT_COLOR) {
							isSkip = false;
							break;
						}
					}

					if (isSkip) {
						rect.Height = y - rect.Y;
					} else {
						break;
					}
				}
			}

			return rect;
		}

		public static Rectangle BestFix(Rectangle selectedRect, string fileName)
		{
			Rectangle rect = selectedRect;

			if (fileName != null) {
				try {
					Bitmap bitmap = (Bitmap)Bitmap.FromFile(fileName);
					rect = BestFix(selectedRect, bitmap);
					bitmap.Dispose();
				} catch (IOException e) {
					System.Diagnostics.Debug.WriteLine(e.StackTrace);
				}
			}

			return rect;
		}

		public static void RotateFlip(Image image, byte flag)
		{
			if (image != null) {
				if (flag == (byte)SpriteItemFlag.FlipY) {
					image.RotateFlip(RotateFlipType.RotateNoneFlipY);
				} else if (flag == (byte)SpriteItemFlag.FlipX) {
					image.RotateFlip(RotateFlipType.RotateNoneFlipX);
				} else if (flag == (byte)SpriteItemFlag.Rotate180) {
					image.RotateFlip(RotateFlipType.Rotate180FlipNone);
				} else if (flag == (byte)SpriteItemFlag.Rotate270FlipY) {
					image.RotateFlip(RotateFlipType.Rotate270FlipY);
				} else if (flag == (byte)SpriteItemFlag.Rotate90) {
					image.RotateFlip(RotateFlipType.Rotate90FlipNone);
				} else if (flag == (byte)SpriteItemFlag.Rotate270) {
					image.RotateFlip(RotateFlipType.Rotate270FlipNone);
				} else if (flag == (byte)SpriteItemFlag.Rotate90FlipY) {
					image.RotateFlip(RotateFlipType.Rotate90FlipY);
				}
			}
		}

		#region Palette Functions
		#region Functions Removed
		//static int GetPaletteOffsetInImage(byte[] imageData)
		//{
		//    int offset = 0;
		//    for (; offset < imageData.Length - 4; )
		//    {
		//        if (imageData[offset++] == 0x50)
		//            if (imageData[offset++] == 0x4c)
		//                if (imageData[offset++] == 0x54)
		//                    if (imageData[offset++] == 0x45)
		//                        break;
		//    }
		//    return offset;
		//}

		//public static int[] GetPaletteFromImage(string imagePath)
		//{
		//    FileStream f = File.OpenRead(imagePath);
		//    byte[] imageData = new byte[f.Length];
		//    f.Read(imageData, 0, imageData.Length);
		//    f.Close();
		//    f.Dispose();

		//    int offset = GetPaletteOffsetInImage(imageData);
		//    int[] palette = GetPaletteFromByteData(imageData, offset);
		//    return palette;
		//}


		//public static ColorPalette GetPaletteFromByteData(byte[] data, int offset)
		//{
		//    ColorPalette colorPalette;
		//    int[] palette = new int[NUM_COLOR_OF_PALETTE];
		//    for (int i = 0; i < NUM_COLOR_OF_PALETTE; i++)
		//    {
		//        //palette[i] = (palette[i] << 8) | (f.ReadByte() & 0xff);
		//        //palette[i] = (palette[i] << 8) | (f.ReadByte() & 0xff);
		//        //palette[i] = (palette[i] << 8) | (f.ReadByte() & 0xff);
		//        palette[i] = (palette[i] << 8) | (data[offset + 2] & 0xff);
		//        palette[i] = (palette[i] << 8) | (data[offset + 1] & 0xff);
		//        palette[i] = (palette[i] << 8) | (data[offset] & 0xff);
		//        offset += 3;
		//    }
		//    return palette;
		//}

		//private static int IsColorInPalette(int color, int[] palette)
		//{
		//    for (int i = 0; i < NUM_COLOR_OF_PALETTE; i++)
		//        if (palette[i] == color)
		//            return i;
		//    return NUM_COLOR_OF_PALETTE;
		//}

		//public static byte[] ConvertARGBToPaletteColor(int[] ARGBArray, int[] palette)
		//{
		//    byte[] paletteColor = new byte[ARGBArray.Length / 4];
		//    for (int i = 0; i * 4 < ARGBArray.Length - 4; i++)
		//    {
		//        int color = 0;
		//        color = (color << 8) | (ARGBArray[i * 4] & 0xff);
		//        color = (color << 8) | (ARGBArray[i * 4 + 1] & 0xff);
		//        color = (color << 8) | (ARGBArray[i * 4 + 2] & 0xff);

		//        int colorIndex = IsColorInPalette(color, palette);
		//        if (colorIndex >= NUM_COLOR_OF_PALETTE)
		//            paletteColor[i] = 0;
		//        else
		//            paletteColor[i] = (byte)colorIndex;
		//    }
		//    return paletteColor;
		//}
		#endregion

		//public static Color[] GetPaletteFromPaletteFile(string palettePath)
		//{
		//    const int NUM_COLOR_OF_PALETTE	= 256;

		//    FileStream f = File.OpenRead(palettePath);
		//    byte[] paletteData = new byte[f.Length];
		//    f.Read(paletteData, 0, paletteData.Length);
		//    Color[] paletteColor = new Color[NUM_COLOR_OF_PALETTE];
		//    for (int i = 0; i < NUM_COLOR_OF_PALETTE; i++)
		//    {
		//        //palette[i] = (palette[i] << 8) | (f.ReadByte() & 0xff);
		//        //palette[i] = (palette[i] << 8) | (f.ReadByte() & 0xff);
		//        //palette[i] = (palette[i] << 8) | (f.ReadByte() & 0xff);
		//        paletteColor[i] = Color.FromArgb(0xff, paletteData[i * 3] & 0xff, paletteData[i * 3 + 1] & 0xff, paletteData[i * 3 + 2] & 0xff);
		//    }
		//    f.Close();
		//    f.Dispose();

		//    //int[] palette = GetPaletteFromByteData(paletteData, 0);
		//    return paletteColor;
		//}

		//private static void SavePalette(int[] palette, FileStream f)
		//{
		//    const int NUM_COLOR_OF_PALETTE = 256;

		//    for (int i = 0; i < NUM_COLOR_OF_PALETTE; i++)
		//    {
		//        f.WriteByte((byte)palette[i]);
		//        f.WriteByte((byte)(palette[i] >> 8));
		//        f.WriteByte((byte)(palette[i] >> 16));
		//        //f.WriteByte((byte)(palette[i] >> 24));
		//    }
		//}
		#endregion

		#region FreeImage wrapper
		#if USE_FREE_IMAGE_LIB
		public static ImageFormat GetImageFormat(String fileName)
		{
			FREE_IMAGE_FORMAT fif = FreeImage.GetFIFFromFilename(fileName);

			switch (fif) {
				case FREE_IMAGE_FORMAT.FIF_PNG:
					return ImageFormat.Png;
				case FREE_IMAGE_FORMAT.FIF_BMP:
					return ImageFormat.Bmp;
				case FREE_IMAGE_FORMAT.FIF_JPEG:
					return ImageFormat.Jpeg;
				case FREE_IMAGE_FORMAT.FIF_GIF:
					return ImageFormat.Gif;
				default:
					throw new InvalidCastException();
			}
		}

		public static PixelFormat GetPixelFormat(String fileName)
		{
			FIBITMAP dib = FreeImage.LoadEx(fileName);
			FREE_IMAGE_COLOR_TYPE type = FreeImage.GetColorType(dib);
			FreeImage.Unload(dib);
			dib.SetNull();

			switch (type) {
				case FREE_IMAGE_COLOR_TYPE.FIC_PALETTE:
					return PixelFormat.Format8bppIndexed;
				case FREE_IMAGE_COLOR_TYPE.FIC_RGBALPHA:
					return PixelFormat.Format32bppArgb;
				case FREE_IMAGE_COLOR_TYPE.FIC_RGB:
					return PixelFormat.Format24bppRgb;
				default:
					return PixelFormat.Undefined;
			}
		}


		public static Color[] GetPaletteColors(String fileName, int size)
		{
			FreeImageAPI.Palette palette = new FreeImageAPI.Palette(size);
			palette.Load(fileName);
			RGBQUAD[] rgb = palette.ToArray();

			Color[] colors = new Color[size];
			for (int i = 0; i < size; i++) {
				// Ignore alpha channel
				colors[i] = Color.FromArgb(rgb[i].rgbRed, rgb[i].rgbGreen, rgb[i].rgbBlue);
			}

			return colors;
		}

		public static void LoadPalette(ColorPalette palette, String fileName)
		{
			Color[] colors = GetPaletteColors(fileName, palette.Entries.Length);
			for (int i = 0; i < palette.Entries.Length; i++) {
				palette.Entries[i] = colors[i];
			}
		}

		/// <summary>
		/// Too slow!!!!
		/// </summary>
		public static Bitmap Rescale(Bitmap source, int scale)
		{
			int width = source.Width * scale;
			int height = source.Height * scale;
			FIBITMAP dib = FreeImage.CreateFromBitmap(source);
			FIBITMAP newDib = FreeImage.Rescale(dib, width, height, FREE_IMAGE_FILTER.FILTER_BOX);

			Bitmap bitmap = FreeImage.GetBitmap(newDib);
			FreeImage.Unload(dib);
			dib.SetNull();
			FreeImage.Unload(newDib);
			newDib.SetNull();

			return bitmap;
		}

		public static Color[] GetPixelColor(String fileName)
		{
			FIBITMAP dib = FreeImage.LoadEx(fileName);
			uint width = FreeImage.GetWidth(dib);
			uint height = FreeImage.GetHeight(dib);

			RGBQUAD[] rgb = new RGBQUAD[width * height];
			Color[] colors = new Color[width * height];
			for (uint y = 0; y < height; y++) {
				for (uint x = 0; x < width; x++) {
					FreeImage.GetPixelColor(dib, x, y, out rgb[x + (y * width)]);
					colors[x + (y * width)] = rgb[x + (y * width)].Color;
					System.Diagnostics.Debug.WriteLine((x + (y * width)) + ". " + colors[x + (y * width)]);
				}
			}

			return colors;
		}

		public static byte[] GetPixelIndexed(String fileName)
		{
			FIBITMAP dib = FreeImage.LoadEx(fileName);
			uint width = FreeImage.GetWidth(dib);
			uint height = FreeImage.GetHeight(dib);
			byte[] data = new byte[width * height];

			FreeImage.FlipVertical(dib);

			for (uint y = 0; y < height; y++) {
				for (uint x = 0; x < width; x++) {
					byte value;
					FreeImage.GetPixelIndex(dib, x, y, out value);
					data[x + (y * width)] = value;
					//System.Diagnostics.Debug.WriteLine(x + ", " + y + ": " + value);
				}
			}

			return data;
		}
		#endif
		#endregion
	}
}
