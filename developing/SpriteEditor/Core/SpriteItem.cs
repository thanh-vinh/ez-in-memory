﻿using System;
//using System.Collections;
using System.Drawing;
//using System.Reflection;

namespace SpriteEditor.Core
{
	public abstract class SpriteItem
	{
		public char[] SPECICAL_CHARS = { '.', ',', ';' , '-', ':', '!', '\'', '?', '/', '\\' };
		
		private int id;
		private string name;

        //only use when edit item, not save it
        private Image spriteItemImage;

		#region Constructors
		public SpriteItem()
		{
			this.id = -1;
			this.name = null;
		}

		public SpriteItem(int id, string name)
		{
			this.id = id;
			this.name = name;
		}

		public SpriteItem(int id)
			: this(id, "")
		{
		}
		#endregion

		#region Static		
		public static Rectangle AdditionRectangle(Rectangle rect1, Rectangle rect2)
		{
			Rectangle rect = new Rectangle();

			rect.X = Math.Min(rect1.X, rect2.X);
			rect.Y = Math.Min(rect1.Y, rect2.Y);
			rect.Width = Math.Max((rect1.X + rect1.Width) - rect.X, (rect2.X + rect2.Width) - rect.X);
			rect.Height = Math.Max((rect1.Y + rect1.Height) - rect.Y, (rect2.Y + rect2.Height) - rect.Y);

			return rect;
		}
		#endregion

		#region Properties
		public int ID
		{
			get
			{
				return this.id;
			}

			set
			{
				this.id = value;
			}
		}

		public String Name
		{
			get
			{
				return this.name;
			}

			set
			{
				this.name = value;
			}
		}

        public Image SpriteItemImage
        {
            get { return spriteItemImage; }
            set { spriteItemImage = value; }
        }

		#endregion

//		#region Implements
//		public object Clone()
//		{
//			return this.MemberwiseClone();
//		}
//
//		/// <summary>
//		/// Clone the object, and returning a reference to a cloned object.
//		/// </summary>
//		/// <returns>Reference to the new cloned
//		/// object.</returns>
//		public object Clone()
//		{
//			//First we create an instance of this specific type.
//			System.Diagnostics.Debug.WriteLine("type: " + this.GetType());
//			object[] args = {this.id, this.name};
//			object newObject  = Activator.CreateInstance(this.GetType(), args);
//			//We get the array of fields for the new type instance.
//			FieldInfo[] fields = newObject.GetType().GetFields();
//			int i = 0;
//			foreach (FieldInfo fi in this.GetType().GetFields()) {
//				//We query if the fiels support the ICloneable interface.
//				Type ICloneType = fi.FieldType.GetInterface("ICloneable", true);
//				if (ICloneType != null) {
//					//Getting the ICloneable interface from the object.
//					ICloneable IClone = (ICloneable)fi.GetValue(this);
//					//We use the clone method to set the new value to the field.
//					fields[i].SetValue(newObject, IClone.Clone());
//				} else {
//					// If the field doesn't support the ICloneable
//					// interface then just set it.
//					fields[i].SetValue(newObject, fi.GetValue(this));
//				}
//
//				//Now we check if the object support the
//				//IEnumerable interface, so if it does
//				//we need to enumerate all its items and check if
//				//they support the ICloneable interface.
//				Type IEnumerableType = fi.FieldType.GetInterface("IEnumerable", true);
//				if (IEnumerableType != null) {
//					//Get the IEnumerable interface from the field.
//					IEnumerable IEnum = (IEnumerable)fi.GetValue(this);
//					//This version support the IList and the
//					//IDictionary interfaces to iterate on collections.
//					Type IListType = fields[i].FieldType.GetInterface("IList", true);
//					Type IDicType = fields[i].FieldType.GetInterface("IDictionary", true);
//
//					int j = 0;
//					if (IListType != null) {
//						//Getting the IList interface.
//						IList list = (IList)fields[i].GetValue(newObject);
//						foreach (object obj in IEnum) {
//							//Checking to see if the current item support the ICloneable interface.
//							ICloneType = obj.GetType().GetInterface("ICloneable", true);
//
//							if (ICloneType != null) {
//								//If it does support the ICloneable interface, we use it to set the clone of
//								//the object in the list.
//								ICloneable clone = (ICloneable)obj;
//								list[j] = clone.Clone();
//							}
//
//							//NOTE: If the item in the list is not
//							//support the ICloneable interface then in the
//							//cloned list this item will be the same
//							//item as in the original list
//							//(as long as this type is a reference type).
//							j++;
//						}
//					} else if (IDicType != null) {
//						//Getting the dictionary interface.
//						IDictionary dic = (IDictionary)fields[i].GetValue(newObject);
//						j = 0;
//						foreach (DictionaryEntry de in IEnum) {
//							//Checking to see if the item support the ICloneable interface.
//							ICloneType = de.Value.GetType().GetInterface("ICloneable", true);
//							if (ICloneType != null) {
//								ICloneable clone = (ICloneable)de.Value;
//								dic[de.Key] = clone.Clone();
//							}
//							j++;
//						}
//					}
//				}
//				i++;
//			}
//			return newObject;
//		}
//
//		#endregion

		#region Public
		public string GetFlagName(byte flag)
		{
			return flag.ToString();
		}
		
		public bool IsValidName()
		{
			if (this.name.Length > 0 && this.name.IndexOfAny(SPECICAL_CHARS) == -1) {
				return true;
			}
			
			return false;
		}
		#endregion

		#region Abstract
		/// <summary>
		/// Clone this object. Cannot use MemberwiseClone().
		/// </summary>
		public abstract SpriteItem Clone();

		/// <summary>
		/// Check for a valid item.
		/// </summary>
		public abstract bool IsValidItem();

		/// <summary>
		/// Get string array of all field values.
		/// </summary>
		public abstract string[] GetValues();

		/// <summary>
		/// Remove this object.
		/// </summary>
		public abstract void Dispose();
		#endregion
	}
}
