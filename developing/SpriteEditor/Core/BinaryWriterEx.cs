﻿using System;
using System.IO;

namespace SpriteEditor.Core
{
	public class BinaryWriterEx : BinaryWriter
	{
		private ByteOrder byteOrder;

		public BinaryWriterEx(Stream output)
			:base(output)
		{
		}

		public BinaryWriterEx(Stream output, ByteOrder format)
			:base(output)
		{
			this.byteOrder = format;
		}

		public ByteOrder ByteOrder
		{
			set
			{
				this.byteOrder = value;
			}
		}

		public static short ReverseBytes(short value)
		{
			return (short)((value & 0xFFU) << 8 | (value & 0xFF00U) >> 8);
		}

		public static uint ReverseBytes(uint value)
		{
			return (value & 0x000000FF) << 24 | (value & 0x0000FF00) << 8 |
				(value & 0x00FF0000) >> 8 | (value & 0xFF000000) >> 24;
		}

		public static UInt64 ReverseBytes(UInt64 value)
		{
			return (value & 0x00000000000000FFUL) << 56 | (value & 0x000000000000FF00UL) << 40 |
				(value & 0x0000000000FF0000UL) << 24 | (value & 0x00000000FF000000UL) << 8 |
				(value & 0x000000FF00000000UL) >> 8 | (value & 0x0000FF0000000000UL) >> 24 |
				(value & 0x00FF000000000000UL) >> 40 | (value & 0xFF00000000000000UL) >> 56;
		}

		#region Override
		public override void Write(short value)
		{
			if (this.byteOrder == ByteOrder.BigEndian) {
				value = ReverseBytes(value);
			}

			base.Write(value);
		}

		public override void Write(uint value)
		{
			if (this.byteOrder == ByteOrder.BigEndian) {
				value = ReverseBytes(value);
			}

			base.Write(value);
		}
		#endregion
	}
}
