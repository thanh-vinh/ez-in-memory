﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpriteEditor.Core
{
	public class ListAnimation : ListSpriteItem
	{
		public const int ANIMATION_ID_START = 3000; //0x3000;

		#region Override
		public override ListSpriteItem Clone()
		{
			return null;
		}

		public override int GetNextId()
		{
			return base.GetNextId(ANIMATION_ID_START);
		}

		public override SpriteItem CreateDefaultItem(int id)
		{
			Animation animation = new Animation(id);
			return animation;
		}

		public override string[] GetHeaders()
		{
			string[] headers = { "", "#", "ID", "Name" };
			return headers;
		}

		public override int[] GetHeadersWidth()
		{
			int[] widths = { 0, 40, 60, 100 };
			return widths;
		}
		#endregion

		#region Public
		public int GetAFrameCount()
		{
			int length = 0;
			for (int i = 0; i < base.Count; i++)
			{
				Animation animation = (Animation)base[i];
				length += animation.AFrames.Count;
			}

			return length;
		}
		#endregion
	}
}
