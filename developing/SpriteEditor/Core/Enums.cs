﻿using System;

namespace SpriteEditor.Core
{
	public enum Platform : int
	{
		J2ME,
		WindowsPhone,
	}

	public enum ByteOrder : int
	{
		LittleEndian, BigEndian
	}

	public enum ImageViewerMode : int
	{
		Module, FModule, Frame, AFrame, Animation
	}

	public enum SpriteItemFlag : byte
	{
		None			= 0,
		FlipY 			= 1,
		FlipX 			= 2,
		Rotate180 		= 3,
		Rotate270FlipY	= 4,
		Rotate90 		= 5,
		Rotate270 		= 6,
		Rotate90FlipY	= 7,
	}

	public enum SpriteItemsFlagsName : byte
	{

	}
}
