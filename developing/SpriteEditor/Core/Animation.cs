﻿using System;
using System.Drawing;

namespace SpriteEditor.Core
{
	public class Animation : SpriteItem
	{
		private ListAFrame aframes;

		#region Constructors
		public Animation(int id, string name)
			: base(id, name)
		{
			this.aframes = new ListAFrame();
		}

		public Animation(int id)
			: this(id, "")
		{
		}
		#endregion

		#region Override
		public override SpriteItem Clone()
		{
			Animation animation = new Animation(this.ID, this.Name);
			ListAFrame aframes = (ListAFrame)this.aframes.Clone();
			animation.AFrames = aframes;

			return animation;
		}

		public override bool IsValidItem()
		{
			return (this.aframes != null && this.aframes.Count > 0);
		}

		public override string[] GetValues()
		{
			string[] values = { this.ID.ToString(), this.Name };
			return values;
		}

		public override void Dispose()
		{
			this.aframes.Clear();
			this.aframes = null;
		}
		#endregion

		#region Properties
		public ListAFrame AFrames
		{
			get
			{
				return this.aframes;
			}

			set
			{
				this.aframes = value;
			}
		}
		#endregion

		#region Public
		public Rectangle GetRectangle()
		{
			Rectangle rect = new Rectangle();
			for (int i = 0; i < this.AFrames.Count; i++)
			{
				if (this.AFrames[i].IsValidItem())
				{
					rect = SpriteItem.AdditionRectangle(rect, ((AFrame)this.AFrames[i]).GetRectangle());
				}
			}

			return rect;
		}
		#endregion
	}
}
