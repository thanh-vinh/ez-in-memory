﻿using System;
using System.Drawing;
using System.Windows.Forms;

using SpriteEditor.Core;

namespace SpriteEditor.Components
{
	public partial class ListViewSpriteItem : UserControl
	{
		public const int LIST_VIEW_LEFT = 2;
		public const int LIST_VIEW_TOP = 40;

		public event EventHandler ItemsChanged;
		public event EventHandler SelectedIndexChanged;

		private ListSpriteItem items;

		public ListViewSpriteItem()
		{
			InitializeComponent();
		}

		private void FillListView()
		{
			this.listViewEx.Items.Clear();
			ListViewItem[] listViewItems = this.items.ToListViewItem();
			this.listViewEx.Items.AddRange(listViewItems);

			for (int i = 0; i < this.listViewEx.Items.Count; i++) {
				if (this.items[i].IsValidItem()) {
					this.listViewEx.Items[i].BackColor = Color.Transparent;
				} else {
					this.listViewEx.Items[i].BackColor = Color.Brown;
				}
			}
		}

		#region Handle Events
		private void ListViewSpriteItem_Resize(object sender, EventArgs e)
		{
			this.listViewEx.Left = LIST_VIEW_LEFT;
			this.listViewEx.Top = LIST_VIEW_TOP;
			this.listViewEx.Width = base.Width - (LIST_VIEW_LEFT << 1);
			this.listViewEx.Height = base.Height - LIST_VIEW_TOP - (LIST_VIEW_LEFT << 1);
		}

		private void listViewEx_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.OnSelectedIndexChanged(sender, e);
		}

		private void listViewEx_SubItemChanged(object sender, EventArgs e)
		{
			if (this.listViewEx.SelectedItems.Count > 0) {
				ListViewItem item = this.listViewEx.SelectedItems[0];
				int index = item.Index;
				Type type = this.items.GetType();

				if (type == typeof(ListModule)) {
					int x = int.Parse(item.SubItems[ListModule.HEADER_X].Text);
					int y = int.Parse(item.SubItems[ListModule.HEADER_Y].Text);
					int width = int.Parse(item.SubItems[ListModule.HEADER_WIDTH].Text);
					int height = int.Parse(item.SubItems[ListModule.HEADER_HEIGHT].Text);
					String name = item.SubItems[ListModule.HEADER_NAME].Text;

					Module module = (Module)this.items[index];
					module.X = x;
					module.Y = y;
					module.Width = width;
					module.Height = height;
					module.Name = name;

					this.OnItemsChanged(sender, e);
				} else if (type == typeof(ListFModule)) {
					int x = int.Parse(item.SubItems[ListFModule.HEADER_X].Text);
					int y = int.Parse(item.SubItems[ListFModule.HEADER_Y].Text);
					byte flags = byte.Parse(item.SubItems[ListFModule.HEADER_FLAGS].Text);

					FModule fmodule = (FModule)this.items[index];
					fmodule.X = x;
					fmodule.Y = y;
					fmodule.Flags = flags;

					this.OnItemsChanged(sender, e);
				} else if (type == typeof(ListAFrame)) {
					int x = int.Parse(item.SubItems[ListAFrame.HEADER_X].Text);
					int y = int.Parse(item.SubItems[ListAFrame.HEADER_Y].Text);
					byte time = byte.Parse(item.SubItems[ListAFrame.HEADER_TIME].Text);
					byte flags = byte.Parse(item.SubItems[ListAFrame.HEADER_FLAGS].Text);

					AFrame aframe = (AFrame)this.items[index];
					aframe.X = x;
					aframe.Y = y;
					aframe.Time = time;
					aframe.Flag = flags;

					this.OnItemsChanged(sender, e);
				} else if (type == typeof(ListFrame) || type == typeof(ListAnimation)) {
					String name = item.SubItems[ListFrame.HEADER_NAME].Text;
					this.items[index].Name = name;
				}

				this.Refresh();
			}
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			Type type = this.items.GetType();
			if (type != typeof(ListPalette))
			{
				this.items.Add();
			}
			else
			{
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					Palette palette = new Palette(this.items.GetNextId(), openFileDialog.FileName);
					this.items.Add(palette);
				}
			}
			this.Refresh();
			this.listViewEx.Items[this.listViewEx.Items.Count - 1].Selected = true;
			this.OnItemsChanged(sender, e);
		}

		private void insertButton_Click(object sender, EventArgs e)
		{
			int index = this.GetSelectedIndex();
			if (index > -1) {
				this.items.Insert(index);
				this.Refresh();

				this.listViewEx.Items[index + 1].Selected = true;
				this.OnItemsChanged(sender, e);
			}
		}

		private void deleteButton_Click(object sender, EventArgs e)
		{
			int index = this.GetSelectedIndex();
			if (index > -1) {
				this.items[index].Dispose();
				this.items.RemoveAt(index);
				this.Refresh();

				index = (index > 0) ? index - 1 : 0;
				if (this.listViewEx.Items.Count > 0) {
					this.listViewEx.Items[index].Selected = true;
				}

				this.OnItemsChanged(sender, e);
			}
		}

		private void cloneButton_Click(object sender, EventArgs e)
		{
			int index = this.GetSelectedIndex();
			if (index > -1) {
				this.items.CloneItem(index);
				this.Refresh();

				this.listViewEx.Items[index + 1].Selected = true;
				this.OnItemsChanged(sender, e);
			}
		}

		private void upButton_Click(object sender, EventArgs e)
		{
			int index = this.GetSelectedIndex();
			if (index > -1) {
				this.items.MoveUp(index);
				this.Refresh();

				index = (index > 0) ? index - 1 : 0;
				this.listViewEx.Items[index].Selected = true;
			}
		}

		private void downButton_Click(object sender, EventArgs e)
		{
			int index = this.GetSelectedIndex();
			if (index > -1) {
				this.items.MoveDown(index);
				this.Refresh();

				index = (index < this.items.Count - 1) ? index + 1 : this.items.Count - 1;
				this.listViewEx.Items[index].Selected = true;
			}
		}

		private void topButton_Click(object sender, EventArgs e)
		{
			int index = this.GetSelectedIndex();
			if (index > -1) {
				this.items.MoveTop(index);
				this.Refresh();

				this.listViewEx.Items[0].Selected = true;
			}
		}

		private void bottomButton_Click(object sender, EventArgs e)
		{
			int index = this.GetSelectedIndex();
			if (index > -1) {
				this.items.MoveBottom(index);
				this.Refresh();

				this.listViewEx.Items[this.listViewEx.Items.Count - 1].Selected = true;
			}
		}

		private void listViewEx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete) {
				this.deleteButton_Click(sender, e);
			}
		}
		#endregion

		#region Events
		protected virtual void OnItemsChanged(Object sender, EventArgs e)
		{
			if (this.ItemsChanged != null) {
				this.ItemsChanged(sender, e);
			}
		}

		protected virtual void OnSelectedIndexChanged(Object sender, EventArgs e)
		{
			if (this.SelectedIndexChanged != null) {
				this.SelectedIndexChanged(sender, e);
			}
		}
		#endregion

		#region Properties
		public ListSpriteItem Items
		{
			get
			{
				return this.items;
			}

			set
			{
				if (value != null) {
					this.items = value;
					ColumnHeader[] columns = this.items.ToColumnHeaders();
					this.listViewEx.Columns.Clear();
					this.listViewEx.Columns.AddRange(columns);
					this.FillListView();
					this.Refresh();			// Redraw ListView
					this.OnItemsChanged(this, null);
				}
			}
		}
		#endregion

		#region Overrides
		public override void Refresh()
		{
			int index = this.GetSelectedIndex();
			this.FillListView();
			if (index > - 1 && index < this.items.Count) {
				this.listViewEx.Items[index].Selected = true;		// Set selected item
				this.listViewEx.EnsureVisible(index);				// Scroll to selected item
			}

			base.Refresh();
		}
		#endregion

		#region Public methods
		public int GetSelectedIndex()
		{
			int index = -1;

			if (this.listViewEx.SelectedItems.Count > 0) {
				index = this.listViewEx.SelectedItems[0].Index;
			}

			return index;
		}

		public SpriteItem GetSelected()
		{
			SpriteItem item = null;

			if (this.listViewEx.SelectedItems.Count > 0) {
				int index = this.listViewEx.SelectedItems[0].Index;
				item = this.items[index];
			}

			return item;
		}
		#endregion
	}
}
