﻿namespace SpriteEditor.Components {

    partial class ImageViewer {

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)	{

			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			//
			// timer
			//
			this.timer.Interval = 500;
			this.timer.Tick += new System.EventHandler(this.timer_Tick);
			//
			// ImageViewer
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.DoubleBuffered = true;
			this.Name = "ImageViewer";
			this.Size = new System.Drawing.Size(386, 284);
			this.SizeChanged += new System.EventHandler(this.ImageViewer_SizeChanged);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ImageViewer_KeyDown);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ImageViewer_MouseDown);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ImageViewer_MouseMove);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ImageViewer_MouseUp);
			this.ResumeLayout(false);

		}
		#endregion

		private System.Windows.Forms.Timer timer;
	}
}
