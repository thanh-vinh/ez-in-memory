
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;

public class DataInputStreamEx extends DataInputStream
{
	public static short reverseBytes(short value)
	{
		return (short)((value & 0xFF) << 8 | (value & 0xFF00) >> 8);
	}

	public static int reverseBytes(int value)
	{
		return (value & 0x000000FF) << 24 | (value & 0x0000FF00) << 8 |
			(value & 0x00FF0000) >> 8 | (value & 0xFF000000) >> 24;
	}

	/*
	public static long reverseBytes(long value)
	{
		return (value & 0x00000000000000FF) << 56 | (value & 0x000000000000FF00) << 40 |
			(value & 0x0000000000FF0000) << 24 | (value & 0x00000000FF000000) << 8 |
			(value & 0x000000FF00000000) >> 8 | (value & 0x0000FF0000000000) >> 24 |
			(value & 0x00FF000000000000) >> 40 | (value & 0xFF00000000000000) >> 56;
	}
	*/

	public DataInputStreamEx(InputStream in)
	{
		super(in);
	}

	public short readShort(boolean reverse) throws IOException
	{
		short value = super.readShort();
		if (reverse) {
			value = reverseBytes(value);
		}
		return value;
	}

	public int readInt(boolean reverse) throws IOException
	{
		int value = super.readInt();
		if (reverse) {
			value = reverseBytes(value);
		}
		return value;
	}
}
