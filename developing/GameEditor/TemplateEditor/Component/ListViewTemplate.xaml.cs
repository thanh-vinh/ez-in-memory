﻿using System.Windows.Controls;
using Core.Templates;

namespace TemplateEditor.Component
{
	public partial class ListViewTemplate : UserControl
	{
		private ListTemplate Templates;

		public ListViewTemplate()
		{
			InitializeComponent();

			this.Templates = new ListTemplate();

			Template t = new Template(1, "Temp1");
			Template t2 = new Template(2, "Temp2");

			this.Templates.Items.Add(t);
			this.Templates.Items.Add(t2);

			this.FillListView();
		}

		private void FillListView()
		{
			GridView viewLayout = this.Templates.GetGridViewLayout();
			this.listView.View = viewLayout;
			
			this.listView.Items.Add(this.Templates[0]);
			this.listView.Items.Add(this.Templates[1]);
		}
	}
}
