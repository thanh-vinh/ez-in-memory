﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using Core.Collection;
using Core.Level;
using Core.Templates;
using Core.Util;

namespace LevelEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

		private void OnWindowLoaded(object sender, RoutedEventArgs e)
		{
		}

		private void OnTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.wpfPropertyGrid.SelectedObject = this.treeViewLayer.SelectedItem;

			if (this.treeViewLayer.SelectedItem != null)
			{
				this.canvasLayer.SelectChildUid(this.treeViewLayer.SelectedItem.UID);
			}
		}

		private void OnTreeViewVisibleChanged(object sender, EventArgs e)
		{
			Debug.WriteLine("OnTreeViewVisibleChanged...");
			CheckBox checkBox = sender as CheckBox;
			this.canvasLayer.SetVisible(this.treeViewLayer.SelectedItem, checkBox.IsChecked.Value);
		}

		private void OnCanvasLayerSelectedChild(object sender, EventArgs e)
		{
			Debug.WriteLine("OnCanvasLayerSelectedChild...");

			Item item = this.canvasLayer.SelectedItem;
			this.treeViewLayer.SelectedItem = item;
			this.wpfPropertyGrid.SelectedObject = null;
			this.wpfPropertyGrid.SelectedObject = item;
		}

		private void OnCanvasLayerMovedChild(object sender, EventArgs e)
		{
			Debug.WriteLine("OnCanvasLayerMovedChild...");
			// Only use this way to refresh
			this.wpfPropertyGrid.SelectedObject = this.treeViewLayer.SelectedItem;
		}

		private void OnCanvasLayerUnselectedChild(object sender, EventArgs e)
		{
			Debug.WriteLine("OnCanvasLayerUnselectedChild...");
		}

		private void OnClickNewMenuItem(object sender, RoutedEventArgs e)
		{
			string fileName = FileDialog.SaveFileDialog();
			MessageBox.Show(fileName);
		}

        private void OnClickOpenMenuItem(object sender, RoutedEventArgs e)
        {		
			string fileName = FileDialog.OpenFileDialog();//dlg.FileName;
			MessageBox.Show(fileName);

			LayerFile file = new LayerFile(fileName);
			file.Read();

			this.treeViewLayer.Layers = file.Layers;
            this.canvasLayer.LoadLayers(this.treeViewLayer.Layers);
        }
    }
}
