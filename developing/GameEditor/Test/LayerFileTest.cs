﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using Core.Level;
using Core.Templates;

namespace Test
{
	public class LayerFileTest
	{
		private const string LAYER_FILE = "E:\\game-file.game";
		private const string TEMPLATE_FILE = "E:\\template-file.gts";

		public static void Test()
		{
			New();
			Save();
			//Read();

			Console.ReadLine();
		}

		static void New()
		{
			LayerFile.New(LAYER_FILE, TEMPLATE_FILE);
		}

		static void Save()
		{
			TemplateFileTest.Save();

			ListLayer layers = ListLayer.CreateExampleListLayer();
			LayerFile file = new LayerFile(LAYER_FILE);
			file.Layers = layers;
			file.Save();
		}

		static void Read()
		{
			LayerFile file = new LayerFile(LAYER_FILE);

			for (int i = 0; i < file.Layers.Count; i++)
			{
				Layer layer = file.Layers[i];
				Console.WriteLine("Layer: {0}, {1}", layer.ID, layer.Name);
				for (int j = 0; j < layer.Count; j++)
				{
					Actor actor = layer[j];
					Console.WriteLine(" -> {0}, {1}, {2}, {3}", actor.Name, actor.ID, actor.Parameters);
				}
			}
		}
	}
}
