﻿using System;
using Core.Templates;

namespace Test
{
	class TemplateFileTest
	{
		private const string TEMPLATE_FILE = "E:\\template-file.gts";

		public static void Test()
		{
			//New();
			Save();
			//Read();

			Console.ReadLine();
		}

		static void New()
		{
			TemplateFile.New(TEMPLATE_FILE);
		}

		public static void Save()
		{
			TemplateFile file = new TemplateFile(TEMPLATE_FILE);
			file.Templates = ListTemplate.CreateExampleListTemplate();
			file.Save();
		}

		static void Read()
		{
			TemplateFile file = new TemplateFile(TEMPLATE_FILE);

			for (int i = 0; i < file.Templates.Count; i++)
			{
				Template template = file.Templates[i];
				Console.WriteLine("Template: {0}, {1}", template.ID, template.Name);
				for (int j = 0; j < template.Properties.Count; j++)
				{
					Property property = template.Properties[j];
					Console.WriteLine(" -> {0}, {1}, {2}, {3}", property.Name, property.Value, property.DefaultValue, property.Description);
				}
			}
		}
	}
}
