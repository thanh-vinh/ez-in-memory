﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Util
{
	public class FileDialog
	{
		public static string OpenFileDialog()
		{
			Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
			dlg.Filter = "Game File (*.game)|*.game|All Files (*.*)|*.*";
			dlg.ShowDialog();

			return dlg.FileName;
		}

		public static string SaveFileDialog()
		{
			Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
			dlg.Title = "Create new game file";
			dlg.Filter = "Game File (*.game)|*.game|All Files (*.*)|*.*";
			dlg.ShowDialog();

			return dlg.FileName;
		}
	}
}
