﻿
namespace Core
{
    public enum Platform : int	//TODO remove this, use param instead: -byte:big-endian/little-endian (default)
    {
        J2ME, WindowsPhone,
    }

	public enum ByteOrder : int
	{
		LittleEndian, BigEndian
	}

	public enum Language : int	//TODO use param: -language:cs/cpp/java
	{
		CS, CPP, Java,
	}

	public enum ImageViewerMode : int
	{
		Module, FModule, Frame, AFrame, Animation
	}

	public enum SpriteItemFlag : byte
	{
		None = 0,
		FlipY = 1,
		FlipX = 2,
		Rotate180 = 3,
		Rotate270FlipY = 4,
		Rotate90 = 5,
		Rotate270 = 6,
		Rotate90FlipY = 7,
	}

	public enum SpriteItemsFlagsName : byte
	{

	}
}
