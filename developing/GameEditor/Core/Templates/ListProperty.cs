﻿using Core.Collection;

namespace Core.Templates
{
	public class ListProperty : ListViewItem<Property>
	{
		public const string PROPERTY_NAME_DEFAULT	= "Property";

		public override void AddDefaultItem()
		{
			int id = Item.DEFAULT_ID;
			if (base.Count > 0)
			{
				id = base[base.Count - 1].ID + 1;
			}

			string name = PROPERTY_NAME_DEFAULT + " - " + id;

			Template template = new Template(id, name);
		}

		public override string[] GetColumnHeaders()
		{
			string[] headers = 
			{
				//"#",
				"ID",
				"Name"
			};

			return headers;
		}

		public override int[] GetColumnWidths()
		{
			int[] widths =
			{
				//20,
				30,
				80,
			};

			return widths;
		}

		public override string[] GetColumnBindings()
		{
			string[] bindings = 
			{
				//"#",
				"ID",
				"Name",
			};

			return bindings;
		}
	}
}

