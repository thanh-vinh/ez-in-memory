﻿using Core.Collection;
using Core.Sprite;

namespace Core.Templates
{
	public class Template : Item
	{
		#region Properties
		private string spriteName;
		public SpriteFile Sprite { get; private set; }
		public int FrameProperty { get; set; }
		public int AnimationProperty { get; set; }
		public ListProperty Properties { get; set; }
		#endregion

		public Template(int id, string name)
			: base(id, name)
		{
			this.Properties = new ListProperty();
			this.spriteName = null;
			this.Sprite = null;
			this.FrameProperty = -1;
			this.AnimationProperty = -1;
		}

		#region Properties
		public string SpriteName
		{
			get
			{
				return this.spriteName;
			}   

			set
			{
				this.spriteName = value;
				//this.Sprite = new SpriteFile(this.spriteName);
			}
		}
		#endregion

		public void AddProperty(Property property)
		{
			this.Properties.Items.Add(property);
		}
	}
}
