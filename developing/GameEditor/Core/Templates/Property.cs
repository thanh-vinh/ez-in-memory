﻿using Core.Collection;

namespace Core.Templates
{
	public class Property : Item
	{
		private const short DEFAULT_VALUE = -1;
		private const string DEFAULT_DESCRIPTION = null;

		public int Value { get; set; }
		public int DefaultValue { get; set; }
		public string Description { get; set; }

		public Property(string name, int value, int defaultValue, string description)
		{
			this.Name = name;
			this.Value = value;
			this.DefaultValue = defaultValue;
			this.Description = description;
		}

		public Property(string name)
			: this(name, DEFAULT_VALUE, DEFAULT_VALUE, DEFAULT_DESCRIPTION)
		{
		}
	}
}
