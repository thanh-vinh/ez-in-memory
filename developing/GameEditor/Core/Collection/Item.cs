﻿using System.ComponentModel;

namespace Core.Collection
{
	public class Item : INotifyPropertyChanged
	{
		public const short DEFAULT_ID					= 0;
		public const string DEFAULT_NAME				= null;

		public const string PROPERTY_CHANGE_INIT		= "Initialize";
		public const string PROPERTY_CHANGE_UID			= "UID";
		public const string PROPERTY_CHANGE_ID			= "ID";
		public const string PROPERTY_CHANGE_NAME		= "Name";
		public const string PROPERTY_CHANGE_VISIBLE		= "Visible";

		public event PropertyChangedEventHandler PropertyChanged;

		public int ID { get; set; }
		public int UID { get; set; }
		private string name;
		private bool visible;

		public Item(int id, string name)
		{
			this.ID = id;
			this.name = name;
			this.visible = true;
			this.UID = base.GetHashCode();

			this.OnPropertyChanged(PROPERTY_CHANGE_INIT);
		}

		public Item(string name)
			: this(DEFAULT_ID, name)
		{
		}

		public Item()
			: this(0, DEFAULT_NAME)
		{
		}

		#region Properties
		public string Name
		{
			get
			{
				return this.name;
			}

			set
			{
				this.name = value;
				this.OnPropertyChanged(PROPERTY_CHANGE_NAME);
			}
		}

		public bool Visible
		{
			get { return this.visible; }
			set { this.visible = value; this.OnPropertyChanged(PROPERTY_CHANGE_VISIBLE); }
		}
		#endregion

		protected virtual void OnPropertyChanged(string property)
		{
			System.Diagnostics.Debug.WriteLine("Item->OnPropertyChanged: {0} -> {1}", property, this);
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(property));
		}
	}
}
