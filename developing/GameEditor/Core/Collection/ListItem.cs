﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Core.Collection
{
	public abstract class ListItem<T> : Item
	{
		public const int INVALID_INDEX = -1;

		public ObservableCollection<T> Items { get; set; }

		public abstract void AddDefaultItem();

		public ListItem(int id, string name)
			: base(id, name)
		{
			this.Items = new ObservableCollection<T>();
		}

		public ListItem(string name)
			: this(0, name)
		{
		}

		public ListItem()
			: this(0, null)
		{
		}

		#region Properties
		public T this[int index]
		{
			get
			{
				return this.Items[index];
			}

			set
			{
				this.Items[index] = value;
			}
		}

		public int Count
		{
			get
			{
				return this.Items.Count;
			}
		}
		#endregion

		public int FindIndex(object item)
		{
			for (int i = 0; i < this.Count; i++)
			{
				if (this[i].Equals(item))
				{
					return i;
				}
			}

			return INVALID_INDEX;
		}

		public T Find(int uid)
		{
			for (int i = 0; i < this.Count; i++)
			{
				if (this[i].GetHashCode() == uid)
				{
					return this[i];
				}
			}

			return default(T);
		}
	}
}
