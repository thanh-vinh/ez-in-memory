﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;

namespace Core.Collection
{
	public abstract class ListViewItem<T> : ListItem<T>
	{
		public abstract string[] GetColumnHeaders();
		public abstract int[] GetColumnWidths();
		public abstract string[] GetColumnBindings();

		public GridView GetGridViewLayout()
		{
			string[] headers = this.GetColumnHeaders();
			int[] widths = this.GetColumnWidths();
			string[] bindings = this.GetColumnBindings();

			GridView gridview = new GridView();
			for (int i = 0; i < headers.Length; i++)
			{
				GridViewColumn column = new GridViewColumn();
				column.Header = headers[i];
				column.Width = widths[i];
				column.DisplayMemberBinding = new Binding(bindings[i]);

				gridview.Columns.Add(column);
			}

			return gridview;
		}
	}
}
