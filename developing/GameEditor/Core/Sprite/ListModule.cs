﻿using System;

namespace Core.Sprite
{
	public class ListModule : ListSpriteItem
	{
		public const int MODULE_ID_START = 1000; //0x1000;

		public const int HEADER_X 		= 3;
		public const int HEADER_Y 		= HEADER_X + 1;
		public const int HEADER_WIDTH 	= HEADER_Y + 1;
		public const int HEADER_HEIGHT 	= HEADER_WIDTH + 1;
		public const int HEADER_NAME 	= HEADER_HEIGHT + 1;


		#region Override
		public override ListSpriteItem Clone()
		{
			ListModule modules = new ListModule();
			for (int i = 0; i < base.Count; i++)
			{
				Module module = (Module)base[i];
				Module newModule = new Module(module.ID, module.X, module.Y, module.Width, module.Height, module.Name);
				modules.Add(newModule);
			}

			return modules;
		}

		public override int GetNextId()
		{
			return base.GetNextId(MODULE_ID_START);
		}

		public override SpriteItem CreateDefaultItem(int id)
		{
			SpriteItem module = new Module(id, 0, 0, 10, 10);
			return module;
		}

		public override string[] GetHeaders()
		{
			string[] headers = { "", "#", "ID", "X", "Y", "W", "H", "Name" };
			return headers;
		}

		public override int[] GetHeadersWidth()
		{
			int[] widths = { 0, 40, 60, 40, 40, 40, 40, 100};
			return widths;
		}
		#endregion
	}
}
