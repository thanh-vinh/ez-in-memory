﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace Core.Sprite
{
	public abstract class ListSpriteItem : List<SpriteItem>
	{
		private void Swap(int index1, int index2)
		{
			SpriteItem item1 = base[index1];
			SpriteItem item2 = base[index2];

			base[index1] = item2;
			base[index2] = item1;
		}

		#region Overload
		/// <summary>
		/// Add a default item.
		/// Overload List.Add() method.
		/// </summary>
		public void Add()
		{
			SpriteItem item = this.CreateDefaultItem(this.GetNextId());
			base.Add(item);
		}

		/// <summary>
		/// Insert a default item.
		/// Overload List.Insert() method.
		/// </summary>
		public void Insert(int index)
		{
			SpriteItem item = this.CreateDefaultItem(this.GetNextId());
			base.Insert(index, item);
		}
		#endregion

		#region Public
		public SpriteItem GetItemById(int id)
		{
			SpriteItem item = null;

			for (int i = 0; i < base.Count; i++)
			{
				if (base[i].ID == id)
				{
					item = base[i];
					break;
				}
			}

			return item;
		}

		public int GetNextId(int start)
		{
			int id = start;

			// Has at least one module
			if (base.Count > 0)
			{
				// Get the largest id of all modules
				for (int i = base.Count; i > 0; i--)
				{
					int currentId = base[base.Count - 1].ID;
					id = Math.Max(id, currentId);
				}

				id++;
			}

			return id;
		}

		/// <summary>
		/// Add new item with properties as same as an exists item.
		/// </summary>
		/// <param name="index">Base on a item in this list.</param>
		public void CloneItem(int index)
		{
			SpriteItem item = (SpriteItem)base[index].Clone();
			item.ID = this.GetNextId();
			base.Add(item);
		}

		public void MoveUp(int index)
		{
			if (index > 0)
			{
				this.Swap(index, index - 1);
			}
		}

		public void MoveDown(int index)
		{
			if (index < base.Count - 1)
			{
				this.Swap(index, index + 1);
			}
		}

		public void MoveTop(int index)
		{
			if (index > 0)
			{
				this.Swap(index, 0);
			}
		}

		public void MoveBottom(int index)
		{
			if (index > -1 && index < base.Count - 1)
			{
				this.Swap(index, base.Count - 1);
			}
		}

		//public ColumnHeader[] ToColumnHeaders()
		//{
		//    string[] headers = this.GetHeaders();
		//    int[] widths = this.GetHeadersWidth();
		//    ColumnHeader[] columnHeaders = new ColumnHeader[headers.Length];

		//    for (int i = 0; i < columnHeaders.Length; i++)
		//    {
		//        columnHeaders[i] = new ColumnHeader();
		//        columnHeaders[i].Text = headers[i];
		//        columnHeaders[i].Width = widths[i];
		//    }

		//    return columnHeaders;
		//}

		//public ListViewItem[] ToListViewItem()
		//{
		//    ListViewItem[] listViewItems = new ListViewItem[base.Count];
		//    for (int i = 0; i < base.Count; i++)
		//    {
		//        listViewItems[i] = new ListViewItem();
		//        listViewItems[i].SubItems.Add(i.ToString());
		//        string[] items = base[i].GetValues();
		//        listViewItems[i].SubItems.AddRange(items);
		//    }

		//    return listViewItems;
		//}
		#endregion

		#region Abstract
		/// <summary>
		/// Clone this list items.
		/// </summary>
		public abstract ListSpriteItem Clone();

		public abstract int GetNextId();

		/// <summary>
		/// Create a default item.
		/// </summary>
		public abstract SpriteItem CreateDefaultItem(int id);

		/// <summary>
		/// Get headers of a item.
		/// </summary>
		/// <returns></returns>
		public abstract string[] GetHeaders();

		public abstract int[] GetHeadersWidth();
		#endregion
	}
}
