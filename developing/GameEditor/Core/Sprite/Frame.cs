﻿using System;
using System.Drawing;

namespace Core.Sprite
{
	public class Frame : SpriteItem
	{
		private ListFModule fmodules;

		#region Constructors
		public Frame(int id, string name)
			: base(id, name)
		{
			this.fmodules = new ListFModule();
		}

		public Frame(int id)
			: this(id, "")
		{
		}
		#endregion

		#region Override
		public override SpriteItem Clone()
		{
			Frame frame = new Frame(this.ID, this.Name);
			ListFModule fmodules = (ListFModule)this.fmodules.Clone();
			frame.FModules = fmodules;
			return frame;
		}

		public override bool IsValidItem()
		{
			return (this.fmodules != null && this.fmodules.Count > 0);
		}

		public override string[] GetValues()
		{
			string[] values = { this.ID.ToString(), this.Name };
			return values;
		}

		public override void Dispose()
		{
			this.fmodules.Clear();
			this.fmodules = null;
		}
		#endregion

		#region Properties
		public ListFModule FModules
		{
			get
			{
				return this.fmodules;
			}

			set
			{
				this.fmodules = value;
			}
		}
		#endregion

		#region Public
		/// <summary>
		/// Get the largest rectangle include all the modules in this frame.
		/// </summary>
		public Rectangle GetRectangle()
		{
			Rectangle rect = new Rectangle();
			for (int i = 0; i < this.fmodules.Count; i++)
			{
				if (this.fmodules[i].IsValidItem())
				{
					rect = SpriteItem.AdditionRectangle(rect, ((FModule)this.fmodules[i]).GetRectangle());
				}
			}

			return rect;
		}
		#endregion
	}
}
