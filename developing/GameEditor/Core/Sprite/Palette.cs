﻿using System;
using System.Drawing;
using Core.Util;

namespace Core.Sprite
{
    public class Palette : SpriteItem
    {
		private static int size;

		private Color[] colors;

		public Palette(int id, string fileName)
			: base(id)
		{
			this.colors = ImageLib.GetColorPalette(fileName, size);
			base.Name = System.IO.Path.GetFileName(fileName);
		}

		public Palette(int id, String name, Color[] colors)
			: base(id, name)
		{
			this.colors = colors;
		}

		public Palette(int id, Color[] colors)
			: this(id, "IMAGE", colors)
		{
		}

		public Color this[int i]
		{
			get
			{
				if (i >= colors.Length || i < 0)
					throw new Exception("Array out of bound!");
				else
					return colors[i];
			}
		}

		#region Properties
		public static int Size
		{
			get
			{
				return size;
			}
			set
			{
				size = value;
			}
		}

		public Color[] Colors
		{
			get
			{
				return this.colors;
			}

			set
			{
				this.colors = value;
			}
		}
		#endregion

		#region Public
		public byte[] GetPalette()
		{
			byte[] data = new byte[size * 3];

			for (int i = 0; i < size; i++)
			{
				Color color = this.colors[i];
				data[i * 3] = colors[i].R;
				data[i * 3 + 1] = colors[i].G;
				data[i * 3 + 2] = colors[i].B;
			}

			return data;
		}
		#endregion

		#region Override
		public override SpriteItem Clone()
		{
			return null;
		}

		public override bool IsValidItem()
		{
			return (this.colors != null);
		}

		public override string[] GetValues()
		{
			string[] values = { this.Name };
			return values;
		}

		public override void Dispose()
		{
			this.colors = null;
		}
		#endregion
	}
}
