﻿using System;
using System.Drawing;

namespace Core.Sprite
{
	public class ListFModule : ListSpriteItem
	{
		public const int HEADER_X 		= 3;
		public const int HEADER_Y 		= HEADER_X + 1;
		public const int HEADER_NAME	= HEADER_Y + 1;
		public const int HEADER_FLAGS 	= HEADER_NAME + 1;

		#region Override
		public override ListSpriteItem Clone()
		{
			ListFModule fmodules = new ListFModule();
			for (int i = 0; i < base.Count; i++)
			{
				FModule fmodule = (FModule)base[i];
				FModule newFModule = new FModule(fmodule.Module, fmodule.X, fmodule.Y);
				fmodules.Add(newFModule);
			}

			return fmodules;
		}

		public override int GetNextId()
		{
			return 0;
		}

		public override SpriteItem CreateDefaultItem(int id)
		{
			return null;
		}

		public override string[] GetHeaders()
		{
			string[] headers = { "", "#", "ModuleID", "X", "Y", "ModuleName", "Flags" };
			return headers;
		}

		public override int[] GetHeadersWidth()
		{
			int[] widths = { 0, 30, 60, 40, 40, 80, 80 };
			return widths;
		}
		#endregion
	}
}
