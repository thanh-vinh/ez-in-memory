﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;
using Core.Controls;
using Core.Util;

namespace Core.Sprite
{
	public class SpriteFile
	{
		#region XML node, attribute
		public const String NODE_SPRITE					= "sprite";
		public const String NODE_IMAGE					= "image";
		public const String NODE_PALETTES				= "palettes";
		public const String NODE_PALETTE				= "palette";
		public const String NODE_SUMMARY				= "sumarry";
		public const String NODE_MODULES				= "modules";
		public const String NODE_MODULE					= "module";
		public const String NODE_FMODULES				= "fmodules";
		public const String NODE_FMODULE				= "fmodule";
		public const String NODE_FRAMES					= "frames";
		public const String NODE_FRAME					= "frame";
		public const String NODE_AFRAMES				= "aframes";
		public const String NODE_AFRAME					= "aframe";
		public const String NODE_ANIMATIONS				= "animations";
		public const String NODE_ANIMATION				= "animation";

		public const String ATTRIBUTE_MODULE_COUNT		= "module-count";
		public const String ATTRIBUTE_FMODULE_COUNT		= "fmodule-count";
		public const String ATTRIBUTE_FRAME_COUNT		= "frame-count";
		public const String ATTRIBUTE_AFRAME_COUNT		= "aframe-count";
		public const String ATTRIBUTE_ANIMATION_COUNT	= "animation-count";

		public const String ATTRIBUTE_ID				= "id";
		public const String ATTRIBUTE_X					= "x";
		public const String ATTRIBUTE_Y					= "y";
		public const String ATTRIBUTE_WIDTH				= "width";
		public const String ATTRIBUTE_HEIGHT			= "height";
		public const String ATTRIBUTE_NAME				= "name";
		public const String ATTRIBUTE_TIME				= "time";
		public const String ATTRIBUTE_FLAGS				= "flags";
		#endregion
		
		//
		// Properties
		//
		public String FileName { get; set; }
		public ListModule Modules { get; set; }
		public ListFrame Frames { get; set; }
		public bool IsNewSprite { get; set; }
		public ListPalette Palettes { get; set; }
		public ListAnimation Animations { get; set; }

		private Platform platform;
		private String imageFileName;

		#region Static
		public static void New(String fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);
			writer.WriteStartElement(NODE_SPRITE);			            // Sprite (root)
			writer.WriteEndElement();
			writer.Close();
		}
		#endregion

		public SpriteFile()
		{
			this.FileName = "";
			this.Modules = new ListModule();
			this.Frames = new ListFrame();
			this.Palettes = new ListPalette();
			this.Animations = new ListAnimation();
			this.IsNewSprite = true;
		}

		public SpriteFile(String name, String imageFileName)
		{
			this.FileName = name;
			this.imageFileName = imageFileName;
			this.Modules = new ListModule();
			this.Frames = new ListFrame();
			this.Palettes = new ListPalette();
			this.Animations = new ListAnimation();
			this.IsNewSprite = true;
			Module.MaxSize = new Size(0, 0);
		}

		public SpriteFile(String inputFile)
		{
			this.FileName = inputFile;
			this.Modules = new ListModule();
			this.Frames = new ListFrame();
			this.Palettes = new ListPalette();
			this.Animations = new ListAnimation();
			this.IsNewSprite = false;

			this.Read();

            for (int i = 0; i < this.Modules.Count; i++)
                this.Modules[i].SpriteItemImage = GetModuleImage((Module)this.Modules[i], true);
            for (int i = 0; i < this.Frames.Count; i++)
                this.Frames[i].SpriteItemImage = GetFrameImage((Frame)this.Frames[i], true);
		}

		//public Size GetMaxModuleSize()
		//{
		//    Size size = new Size();

		//    for (int i = 0; i < this.Modules.Count - 1; i++)
		//    {
		//        size.Width = Math.Max(((Module) this.Modules[i]).Width, ((Module) this.Modules[i + 1]).Width);
		//        size.Height = Math.Max(((Module) this.Modules[i]).Height, ((Module) this.Modules[i + 1]).Height);
		//    }

		//    return size;
		//}

		private int GetAllModuleSize()
		{
			int length = 0;
			for (int i = 0; i < this.Modules.Count; i++)
			{
				Module module = (Module)this.Modules[i];
				length += (module.Width * module.Height);
			}

			return length;
		}
				
		#region Properties
		public Platform Platform
		{
			set
			{
				this.platform = value;
			}
		}

		public string ImageFileName
		{
			get
			{
				return this.imageFileName;
			}
			set
			{
				this.imageFileName = value;
				Module.MaxSize = ImageLib.GetSize(value);
				Color[] colors = ImageLib.GetColorPalette(value);
				Palette.Size = colors.Length;
				if (this.Palettes.Count == 0)
				{
					Palette palette = new Palette(0, "IMAGE", colors);
					this.Palettes.Add(palette);
				}
				else
				{
					((Palette)this.Palettes[0]).Colors = colors;
				}
			}
		}
		#endregion

		#region Image
		public Image GetSpriteImage()
		{
			return ImageLib.GetImage(this.imageFileName);
		}

		public Image GetModuleImage(Module module, bool createNew)
		{
			if (module != null)
			{
                if (createNew)
                {
                    Bitmap bitmap = null;
                    bitmap = ImageLib.Crop(this.imageFileName, module.GetRectangle());
                    if (bitmap != null)
                        bitmap.MakeTransparent(Color.Magenta);
                    return bitmap;
                }
                else
                {
                    return module.SpriteItemImage;
                }
			}
            return null;
		}

		public Image[] GetAllModuleImages()
		{
			int length = this.Modules.Count;
			Image[] images = new Image[length];
			for (int i = 0; i < length; i++)
			{
				Module module = (Module)this.Modules[i];
				images[i] = this.GetModuleImage(module, false);
			}

			return images;
		}

		public Image GetFModuleImage(FModule fmodule)
		{
            Image image;
            if (fmodule.Module.SpriteItemImage != null)
                image = fmodule.Module.SpriteItemImage;
            else
			    image = this.GetModuleImage(fmodule.Module, true);
			ImageLib.RotateFlip(image, fmodule.Flags);

			return image;
		}

		public Image GetFrameImage(Frame frame, bool createNew)
		{
            if (createNew)
            {
                Rectangle rect = frame.GetRectangle();
                int baseX = -rect.X;
                int baseY = -rect.Y;
                int width = rect.Width;
                int height = rect.Height;

                Bitmap bitmap = null;

                if (width > 0 && height > 0)
                {
                    bitmap = new Bitmap(width, height);
                    Graphics g = Graphics.FromImage(bitmap);

                    for (int i = 0; i < frame.FModules.Count; i++)
                    {
                        FModule fmodule = (FModule)frame.FModules[i];
                        Image image = this.GetFModuleImage(fmodule);

                        if (image != null)
                        {
                            int x = fmodule.X;
                            int y = fmodule.Y;
                            g.DrawImage(image, baseX + x, baseY + y);
                        }
                    }
                    g.Dispose();
                }

                return bitmap;
            }
            else
            {
                return frame.SpriteItemImage;
            }
		}

		public Image[] GetAllFrameImages()
		{
			int length = this.Frames.Count;
			Image[] images = new Image[length];
			for (int i = 0; i < length; i++)
			{
				Frame frame = (Frame)this.Frames[i];
				images[i] = this.GetFrameImage(frame, false);
			}

			return images;
		}

		public Image GetAFrameImage(AFrame aframe)
		{
            Image image;
            if (aframe.Frame.SpriteItemImage != null)
                image = aframe.Frame.SpriteItemImage;
            else
			    image = this.GetFrameImage(aframe.Frame,true);
			ImageLib.RotateFlip(image, aframe.Flag);

			return image;
		}
		#endregion

		#region Read/write
		public void Read()
		{
			XmlReader reader = XmlReader.Create(this.FileName);

			while (reader.Read())
			{
				if (reader.IsStartElement())
				{			// Only dectect start elements
					if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_IMAGE)
					{
						reader.Read();

						if (reader.Value.Trim().Length > 0)
						{
							this.ImageFileName = Path.GetDirectoryName(this.FileName) + Path.DirectorySeparatorChar + reader.Value.Trim();
						}
						else
						{
							this.ImageFileName = "null";
						}
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_PALETTE)
					{
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						String name = Path.GetDirectoryName(this.FileName) + Path.DirectorySeparatorChar + reader.GetAttribute(ATTRIBUTE_NAME);
						Palette palette = new Palette(this.Palettes.GetNextId(), name);
						this.Palettes.Add(palette);
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_MODULE)
					{
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						int x = Int16.Parse(reader.GetAttribute(ATTRIBUTE_X));
						int y = Int16.Parse(reader.GetAttribute(ATTRIBUTE_Y));
						int width = Int16.Parse(reader.GetAttribute(ATTRIBUTE_WIDTH));
						int height = Int16.Parse(reader.GetAttribute(ATTRIBUTE_HEIGHT));
						String name = reader.GetAttribute(ATTRIBUTE_NAME);

						Module module = new Module(id, x, y, width, height, name);

						this.Modules.Add(module);
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_FRAME)
					{
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						String name = reader.GetAttribute(ATTRIBUTE_NAME);

						Frame frame = new Frame(id, name);

						this.Frames.Add(frame);
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_FMODULE)
					{
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						int x = Int16.Parse(reader.GetAttribute(ATTRIBUTE_X));
						int y = Int16.Parse(reader.GetAttribute(ATTRIBUTE_Y));
						byte flags = byte.Parse(reader.GetAttribute(ATTRIBUTE_FLAGS));

						Module module = (Module)this.Modules.GetItemById(id);
						if (module != null) {
							FModule fmodule = new FModule(module, x, y);
							fmodule.Flags = flags;
							Frame frame = (Frame)this.Frames[this.Frames.Count - 1];
							frame.FModules.Add(fmodule);
						}
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_ANIMATION)
					{
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						String name = reader.GetAttribute(ATTRIBUTE_NAME);

						Animation animation = new Animation(id, name);
						this.Animations.Add(animation);
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_AFRAME)
					{
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						int x = Int16.Parse(reader.GetAttribute(ATTRIBUTE_X));
						int y = Int16.Parse(reader.GetAttribute(ATTRIBUTE_Y));
						byte time = byte.Parse(reader.GetAttribute(ATTRIBUTE_TIME));
						byte flags = byte.Parse(reader.GetAttribute(ATTRIBUTE_FLAGS));

						Frame frame = (Frame)this.Frames.GetItemById(id);
						if (frame != null)
						{
							AFrame aframe = new AFrame(frame, x, y);
							aframe.Time = time;
							aframe.Flag = flags;
							Animation animation = (Animation)this.Animations[this.Animations.Count - 1];
							animation.AFrames.Add(aframe);
						}
					}
				}
			}
			reader.Close();
		}

		public void Save(String fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);

			writer.WriteStartElement(NODE_SPRITE);						// Sprite (root)
			writer.WriteElementString(NODE_IMAGE,
				Path.GetFileName(this.imageFileName));      			// Image

			writer.WriteStartElement(NODE_PALETTES);					// Palettes
			for (int i = 1; i < this.Palettes.Count; i++)
			{
				writer.WriteStartElement(NODE_PALETTE);					// Palette

				Palette palette = (Palette)this.Palettes[i];
				writer.WriteAttributeString(ATTRIBUTE_ID, palette.ID.ToString());
				writer.WriteAttributeString(ATTRIBUTE_NAME, palette.Name);

				writer.WriteEndElement();								// End palette
			}
			writer.WriteEndElement();									// End palettes

			writer.WriteStartElement(NODE_SUMMARY);						// Summary
			writer.WriteAttributeString(ATTRIBUTE_MODULE_COUNT, this.Modules.Count.ToString());
			writer.WriteAttributeString(ATTRIBUTE_FMODULE_COUNT, this.Frames.GetFModulesCount().ToString());
			writer.WriteAttributeString(ATTRIBUTE_FRAME_COUNT, this.Frames.Count.ToString());
			writer.WriteAttributeString(ATTRIBUTE_AFRAME_COUNT, this.Animations.GetAFrameCount().ToString());
			writer.WriteAttributeString(ATTRIBUTE_ANIMATION_COUNT, this.Animations.Count.ToString());
			writer.WriteEndElement();									// End summary

			writer.WriteStartElement(NODE_MODULES);						// Modules
			for (int i = 0; i < this.Modules.Count; i++)
			{
				writer.WriteStartElement(NODE_MODULE);					// Module

				Module module = (Module)this.Modules[i];
				writer.WriteAttributeString(ATTRIBUTE_ID, module.ID.ToString());
				writer.WriteAttributeString(ATTRIBUTE_X, module.X.ToString());
				writer.WriteAttributeString(ATTRIBUTE_Y, module.Y.ToString());
				writer.WriteAttributeString(ATTRIBUTE_WIDTH, module.Width.ToString());
				writer.WriteAttributeString(ATTRIBUTE_HEIGHT, module.Height.ToString());
				writer.WriteAttributeString(ATTRIBUTE_NAME, module.Name);

				writer.WriteEndElement();								// End module
			}
			writer.WriteEndElement();									// End modules

			writer.WriteStartElement(NODE_FRAMES);						// Frames
			for (int i = 0; i < this.Frames.Count; i++)
			{
				writer.WriteStartElement(NODE_FRAME);					// Frame

				Frame frame = (Frame)this.Frames[i];
				writer.WriteAttributeString(ATTRIBUTE_ID, frame.ID.ToString());
				writer.WriteAttributeString(ATTRIBUTE_NAME, frame.Name);

				writer.WriteStartElement(NODE_FMODULES);				// FModules
				for (int j = 0; j < frame.FModules.Count; j++)
				{
					FModule fmodule = (FModule)frame.FModules[j];
					writer.WriteStartElement(NODE_FMODULE);				// FModule

					writer.WriteAttributeString(ATTRIBUTE_ID, fmodule.ID.ToString());
					writer.WriteAttributeString(ATTRIBUTE_X, fmodule.X.ToString());
					writer.WriteAttributeString(ATTRIBUTE_Y, fmodule.Y.ToString());
					writer.WriteAttributeString(ATTRIBUTE_FLAGS, fmodule.Flags.ToString());

					writer.WriteEndElement();							// End fmodule
				}
				writer.WriteEndElement();								// End fmodules
				writer.WriteEndElement();								// End frame
			}
			writer.WriteEndElement();									// End frames

			writer.WriteStartElement(NODE_ANIMATIONS);					// Animations
			for (int i = 0; i < this.Animations.Count; i++)
			{
				writer.WriteStartElement(NODE_ANIMATION);				// Animation

				Animation animation = (Animation)this.Animations[i];
				writer.WriteAttributeString(ATTRIBUTE_ID, animation.ID.ToString());
				writer.WriteAttributeString(ATTRIBUTE_NAME, animation.Name);

				writer.WriteStartElement(NODE_AFRAMES);					// AFrames
				for (int j = 0; j < animation.AFrames.Count; j++)
				{
					AFrame aframe = (AFrame)animation.AFrames[j];
					writer.WriteStartElement(NODE_AFRAME);				// AFrame

					writer.WriteAttributeString(ATTRIBUTE_ID, aframe.ID.ToString());
					writer.WriteAttributeString(ATTRIBUTE_X, aframe.X.ToString());
					writer.WriteAttributeString(ATTRIBUTE_Y, aframe.Y.ToString());
					writer.WriteAttributeString(ATTRIBUTE_TIME, aframe.Time.ToString());
					writer.WriteAttributeString(ATTRIBUTE_FLAGS, aframe.Flag.ToString());

					writer.WriteEndElement();							// End AFrame
				}
				writer.WriteEndElement();								// End AFrames
				writer.WriteEndElement();								// End Animtion
			}
			writer.WriteEndElement();									// End Animtions

			writer.WriteEndElement();									// End Sprite (root)

			writer.Close();
		}

		public void Save()
		{
			this.Save(this.FileName);
		}
		#endregion

		#region Export
		/// <summary>
		/// Save the binary file.
		/// </summary>
		public void SaveBinary(String fileName)
		{
			// Use Big Endian instead Little Endian (Little Endian is default in C#)
			FileStream stream = new FileStream(fileName, FileMode.Create);
			BinaryWriterEx writer = new BinaryWriterEx(stream);
			if (this.platform == Platform.J2ME)
			{
				writer.ByteOrder = ByteOrder.BigEndian;
			}
			else if (this.platform == Platform.WindowsPhone)
			{
				writer.ByteOrder = ByteOrder.LittleEndian;
			}

			//
			// Palettes, using on J2ME only
			//
			if (this.platform == Platform.J2ME)
			{
				writer.Write((byte) this.Palettes.Count);					// Palette count
				writer.Write((short) Palette.Size);							// Color count in a palette

				for (int i = 0; i < this.Palettes.Count; i++)
				{
					Palette palette = (Palette)this.Palettes[i];
					byte[] data = palette.GetPalette();
					writer.Write(data);
				}
			}

			//
			// Modules
			//
			writer.Write(ImageLib.GetPixelLength(this.imageFileName));		// Format Pixel (length)
			if (this.platform == Platform.J2ME)								// J2ME only
			{
				writer.Write((uint) this.GetAllModuleSize());				// All module image buffer size
			}
			writer.Write((short) Modules.Count);							// Modules count
			for (int i = 0; i < this.Modules.Count; i++)					// Modules details
			{
				Module module = (Module) this.Modules[i];
				writer.Write((short) module.Width);							// Width
				writer.Write((short) module.Height);						// Height

				byte[] data = ImageLib.GetPixelData(this.imageFileName, module.GetRectangle());
				writer.Write(data);											// Image
			}

			//
			// Frames
			//
			writer.Write((short) this.Frames.Count);						// Frames count
			writer.Write((short) this.Frames.GetFModulesCount());			// FModules count in all frames
			for (int i = 0; i < this.Frames.Count; i++)
			{			// Frames details
				Frame frame = (Frame) this.Frames[i];
				writer.Write((short) frame.FModules.Count);					// FModules count in current frame
				for (int j = 0; j < frame.FModules.Count; j++)
				{	// FModule details
					FModule fmodule = (FModule) frame.FModules[j];
					Module module = (Module) this.Modules.GetItemById(fmodule.ID);
					int index = this.Modules.IndexOf(module);
					writer.Write((short) index);							// Module index
					writer.Write((short) fmodule.X);						// X
					writer.Write((short) fmodule.Y);						// Y
					writer.Write(fmodule.Flags);							// Flags
				}

				Rectangle rect = frame.GetRectangle();						// Frame rect
				writer.Write((short) rect.X);								// X
				writer.Write((short) rect.Y);								// Y
				writer.Write((short) rect.Width);							// Width
				writer.Write((short) rect.Height);							// Height
			}

			//
			// Animations
			//
			writer.Write((short) this.Animations.Count);					// Animations count
			writer.Write((short) this.Animations.GetAFrameCount());			// AFrames count in all animations
			for (int i = 0; i < this.Animations.Count; i++) {				// Animations details
				Animation animation = (Animation) this.Animations[i];
				writer.Write((short) animation.AFrames.Count);				// AFrames count in current animation
				for (int j = 0; j < animation.AFrames.Count; j++)
				{	// AFrame details
					AFrame aframe = (AFrame) animation.AFrames[j];
					Frame frame = (Frame) this.Frames.GetItemById(aframe.ID);
					int index = this.Frames.IndexOf(frame);
					writer.Write((short) index);							// Frame index
					writer.Write((short) aframe.X);							// X
					writer.Write((short) aframe.Y);							// Y
					writer.Write((byte) aframe.Time);						// Time
					writer.Write(aframe.Flag);								// Flags
				}
			}

			writer.Close();
			stream.Close();
		}

		public void SaveSource(String fileName)
		{
			String spriteName = Path.GetFileNameWithoutExtension(fileName).ToUpper();

			//
			// StringBuilder
			//
			StringBuilder builder = new StringBuilder();
			builder.Append("\n\t// Generate by Sprite Editor");
			builder.Append("\n\t//\n\t// ");
			builder.Append(spriteName);
			builder.Append("\n\t//\n");

			//
			// Modules ID
			//
			
			for (int i = 0; i < this.Modules.Count; i++)
			{
				SpriteItem module = this.Modules[i];
				if (module.IsValidName()) {
					if (this.platform == Platform.J2ME)
					{
						builder.Append("\tpublic static final int ");
					}
					else if (this.platform == Platform.WindowsPhone)
					{
						builder.Append("\tpublic const int ");
					}
					builder.Append(spriteName);
					builder.Append("_MODULE_");
					builder.Append(module.Name.ToUpper());
					builder.Append(" = ");
					builder.Append(i);
					builder.Append(";\n");
				}
			}

			//
			// Frames ID
			//
			for (int i = 0; i < this.Frames.Count; i++)
			{
				SpriteItem frame = this.Frames[i];
				if (frame.IsValidName()) {
					if (this.platform == Platform.J2ME)
					{
						builder.Append("\tpublic static final int ");
					}
					else if (this.platform == Platform.WindowsPhone)
					{
						builder.Append("\tpublic const int ");
					}
					builder.Append(spriteName);
					builder.Append("_FRAME_");
					builder.Append(frame.Name.ToUpper());
					builder.Append(" = ");
					builder.Append(i);
					builder.Append(";\n");
				}
			}

			//
			// Animations ID
			//
			for (int i = 0; i < this.Animations.Count; i++)
			{
				SpriteItem animation = this.Animations[i];
				if (animation.IsValidName())
				{
					if (this.platform == Platform.J2ME)
					{
						builder.Append("\tpublic static final int ");
					}
					else if (this.platform == Platform.WindowsPhone)
					{
						builder.Append("\tpublic const int ");
					}
					builder.Append(spriteName);
					builder.Append("_ANIMATION_");
					builder.Append(animation.Name.ToUpper());
					builder.Append(" = ");
					builder.Append(i);
					builder.Append(";\n");
				}
			}

			//
			// Save
			//
			StreamWriter writer = new StreamWriter(fileName, false);	// Always overwriter
			writer.Write(builder.ToString());
			writer.Close();
		}
		#endregion
	}
}
