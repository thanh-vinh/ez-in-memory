﻿using Core.Collection;
using Core.Templates;

namespace Core.Level
{
	public class Actor : Item
	{
		public const string PROPERTY_CHANGE_POSITION			= "Position";
		public const char WHITE_SPACE = ' ';

		public Template Template { get; set; }
		public int X { get; set; }
		public int Y { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public int Frame { get; set; }
		public int Animation { get; set; }
		public int[] Parameters { get; set; }

		public Actor(int id, Template template, int x, int y, int width, int height)
		{
			this.ID = id;
			this.Template = template;
			this.Name = this.Template.Name + "-" + id.ToString();

			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;

			this.InitializeParameters(template);
		}

		public Actor(int id, Template template)
			: this(id, template, 0, 0, 0, 0)
		{
		}

		public override string ToString()
		{
			string info = base.Name + ": x = " + this.X + ", y = " + this.Y + ", width = " + this.Width + ", height = " + this.Height;
			return info;
		}

		private void InitializeParameters(Template template)
		{
			ListProperty properties = template.Properties;
			this.Parameters = new int[properties.Count];

			for (int i = 0; i < this.Parameters.Length; i++)
			{
				Property property = properties[i];
				this.Parameters[i] = property.DefaultValue;
			}
		}

		//public void SetParameters(string values)
		//{
		//    string[] s = values.Split(WHITE_SPACE);
		//    for (int i = 0; i < this.Parameters.Length; i++)
		//    {
		//        this.Parameters[i] = System.Int16.Parse(s[i]);
		//    }
		//}

		public string GetParameters()
		{
			string values = "";
			for (int i = 0; i < this.Parameters.Length; i++)
			{
				values += this.Parameters[i].ToString() + WHITE_SPACE;
			}

			values = values.Trim();
			return values;
		}
	}
}
