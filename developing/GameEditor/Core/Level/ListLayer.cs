﻿using System;
using System.Windows;
using Core.Collection;
using Core.Templates;

namespace Core.Level
{
	public class ListLayer : ListItem<Layer>
	{
		public Actor Camera { get; set; }

		public override void AddDefaultItem()
		{
			throw new System.NotImplementedException();
		}

		//public void SetVisible(bool value)
		//{
		//    base.Visible = value;
		//    for (int i = 0; i < base.Count; i++)
		//    {
		//        Layer layer = base[i];
		//        layer.Visible = value;
		//    }
		//}

#if DEBUG
		public static ListLayer CreateExampleListLayer()
		{
			const int MAX_RANGE = 200;

			ListLayer layers = new ListLayer();
			ListTemplate templates = ListTemplate.CreateExampleListTemplate();

			for (int i = 0; i < 1; i++)
			{
				Layer layer = new Layer(i, "Layer-" + i);
				for (int j = 0; j < 2; j++)
				{
					Random random = new Random();
					int x = random.Next(MAX_RANGE) + (i * j * 10);
					int y = random.Next(MAX_RANGE) + (i * j * 20);
					int width = random.Next(MAX_RANGE);
					int height = random.Next(MAX_RANGE);

					Actor actor = new Actor(j, templates[0], x, y, width, height);
					layer.Items.Add(actor);

					System.Diagnostics.Debug.WriteLine(actor.ToString());
				}

				layers.Items.Add(layer);
			}

			return layers;
		}
#endif
	}
}
