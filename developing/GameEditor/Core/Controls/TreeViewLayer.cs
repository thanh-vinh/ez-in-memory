﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Core.Collection;
using Core.Level;
using Core.Templates;

namespace Core.Controls
{
	public class TreeViewLayer : TreeView
	{
		public event EventHandler EventVisibleChanged;

		private ListLayer m_layers;
		public new Item SelectedItem { get; set; }
		public DataTemplate DataTemplate { get; private set; }

		public TreeViewLayer()
			: base()
		{
			this.DataTemplate = this.GetDataTemplate();

			////
			//this.Layers = ListLayer.CreateExampleListLayer();
			//this.AppendItems();
			//
		}

		private DataTemplate GetDataTemplate()
		{
			FrameworkElementFactory visibleElement = new FrameworkElementFactory(typeof(CheckBox));
			visibleElement.SetBinding(CheckBox.IsCheckedProperty, new Binding("Visible"));
			visibleElement.SetBinding(CheckBox.UidProperty, new Binding("UID"));
			visibleElement.AddHandler(CheckBox.ClickEvent, new RoutedEventHandler(OnClickVisibleElement));	//handler click event

			FrameworkElementFactory nameElement = new FrameworkElementFactory(typeof(TextBlock));
			nameElement.SetBinding(TextBlock.TextProperty, new Binding("Name"));
			nameElement.AddHandler(TextBlock.MouseLeftButtonDownEvent, new MouseButtonEventHandler(OnDoubleClickNameElement));

			FrameworkElementFactory elements = new FrameworkElementFactory(typeof(StackPanel));
			elements.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
			elements.AppendChild(visibleElement);
			elements.AppendChild(nameElement);

			DataTemplate itemTemplate = new DataTemplate(typeof(StackPanel));
			itemTemplate.VisualTree = elements;

			return itemTemplate;
		}

		private void AppendItems()
		{
			base.Items.Clear();
			base.ItemTemplate = null;

			TreeViewItem layersTreeViewItem = new TreeViewItem();
			layersTreeViewItem.HeaderTemplate = this.DataTemplate;
			layersTreeViewItem.ItemTemplate = this.DataTemplate;
			layersTreeViewItem.Header = this.Layers;
			layersTreeViewItem.IsExpanded = true;

			layersTreeViewItem.Items.Add(this.Layers.Camera);					// Camera

			for (int i = 0; i < this.Layers.Count; i++)							// Layers
			{
				Layer layer = this.Layers[i];

				TreeViewItem actorTreeViewItem = new TreeViewItem();			// Layer
				actorTreeViewItem.HeaderTemplate = this.DataTemplate;
				actorTreeViewItem.ItemTemplate = this.DataTemplate;
				actorTreeViewItem.Header = layer;

				for (int j = 0; j < layer.Items.Count; j++)
				{
					Actor actor = layer.Items[j];								// Actor
					actorTreeViewItem.Items.Add(actor);
				}

				layersTreeViewItem.Items.Add(actorTreeViewItem);
			}

			base.ItemTemplate = this.DataTemplate;
			base.AddChild(layersTreeViewItem);
		}

		private Item GetItemByUid(int uid)
		{
			Item item;

			if (uid == this.Layers.UID)							// Root
			{
				item = this.Layers;
			}
			else
			{
				item = this.Layers.Find(uid);					// Layer
				if (item == null)
				{
					for (int i = 0; i < this.Layers.Count; i++)	// Actor
					{
						item = this.Layers[i].Find(uid);
						if (item != null)
						{
							break;
						}
					}
				}
			}

			return item;
		}

		#region Properties
		public ListLayer Layers
		{
			get { return this.m_layers; }
			set
			{
				this.m_layers = value;
				this.AppendItems();
			}
		}
		#endregion

		#region Override
		protected override void OnSelectedItemChanged(RoutedPropertyChangedEventArgs<object> e)
		{
			this.SelectedItem = base.SelectedItem as Item;
			base.OnSelectedItemChanged(e);

			if (base.SelectedItem == null)
			{
				return;
			}
		}
		#endregion

		#region Events
		protected void OnClickVisibleElement(object sender, RoutedEventArgs e)
		{
			CheckBox checkBox = sender as CheckBox;
			bool visible = checkBox.IsChecked.Value;
			int uid = Int32.Parse(checkBox.Uid);
			Item item = this.GetItemByUid(uid);
			item.Visible = visible;

			this.SelectedItem = item;
			Debug.WriteLine("Select check box, UID: {0}, item: {1}", checkBox.Uid, item);

			this.OnVisibleChanged(sender, e);
		}

		protected void OnVisibleChanged(object sender, RoutedEventArgs e)
		{
			if (this.EventVisibleChanged != null)
			{
				this.EventVisibleChanged(sender, e);
			}
		}

		protected void OnDoubleClickNameElement(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount == 2)
			{
				Debug.WriteLine("OnDoubleClickNameElement");
				//TextBox textBox = new TextBox();
				//TextBlock textBlock = sender as TextBlock;
				//textBlock.Visibility = System.Windows.Visibility.Hidden;

				//textBox.Margin = textBlock.Margin;
				//textBox.Text = textBlock.Text;
			}
		}
		#endregion
	}
}
