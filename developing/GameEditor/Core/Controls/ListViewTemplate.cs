﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Core.Templates;

namespace Core.Controls
{
	public class ListViewTemplate : ListView
	{
		private ListTemplate m_templates;
		//private DataTemplate m_dataTemplate;

		//private DataTemplate GetDataTemplate()
		//{
		//    FrameworkElementFactory nameElement = new FrameworkElementFactory(typeof(TextBlock));
		//    nameElement.SetBinding(TextBlock.TextProperty, new Binding("Name"));
		//    //nameElement.AddHandler(TextBlock.MouseLeftButtonDownEvent, new MouseButtonEventHandler(OnDoubleClickNameElement));

		//    FrameworkElementFactory elements = new FrameworkElementFactory(typeof(StackPanel));
		//    elements.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
		//    elements.AppendChild(nameElement);

		//    DataTemplate itemTemplate = new DataTemplate(typeof(StackPanel));
		//    itemTemplate.VisualTree = elements;

		//    return itemTemplate;
		//}

		private void AppendItems()
		{
			base.Items.Clear();

			//base.ItemTemplate = this.GetDataTemplate();
			//base.ItemsSource = this.m_templates.Items;

			for (int i = 0; i < this.m_templates.Count; i++)
			{
				base.Items.Add(this.m_templates[i]);
			}
		}

		public ListTemplate Templates
		{
			get { return this.m_templates; }
			set
			{
				this.m_templates = value;
				this.AppendItems();
			}
		}
	}
}
