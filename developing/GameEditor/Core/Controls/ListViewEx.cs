﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using Core.Collection;

namespace Core.Controls
{
	public class ListViewEx : ListView
	{
		public event EventHandler SubItemChanged;

		//private ListViewItem.ListViewSubItem subItem;
		private TextBox itemTextBox;

		//private void ItemTextBox_Leave(object sender, EventArgs e)
		//{
		//    subItem.Text = this.itemTextBox.Text;
		//    this.itemTextBox.Leave -= new EventHandler(ItemTextBox_Leave);
		//    this.itemTextBox.Visible = false;

		//    this.OnSubItemChanged(sender, e);
		//}

		//private void ItemTextBox_KeyPress(object sender, KeyEventArgs e)
		//{
		//    if (e.KeyCode == Keys.Enter) {
		//        subItem.Text = this.itemTextBox.Text;
		//    }

		//    if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape) {
		//        this.itemTextBox.KeyUp -= new KeyEventHandler(ItemTextBox_Leave);
		//        this.itemTextBox.Visible = false;

		//        this.OnSubItemChanged(sender, EventArgs.Empty);
		//    }
		//}

		public ListViewEx()
			: base()
		{
			////
			//// TextBox
			////
			this.itemTextBox = new TextBox();
			this.itemTextBox.Name = "itemTextBox";
			//this.itemTextBox.Visibility = System.Windows.Visibility.Collapsed;
			this.AddChild(this.itemTextBox);
			//this.itemTextBox.Leave += new EventHandler(ItemTextBox_Leave);

			//base.Controls.Add(this.itemTextBox);
		}

		//public void AddListViewItem(ListViewItem<T> items)
		//{
		//    base.View = items.GetGridViewLayout();

		//    for (int i = 0; i < items.Count; i++)
		//    {
		//        base.Items.Add(items[i]);
		//    }
		//}

		protected virtual void OnSubItemChanged(object sender, EventArgs e)
		{
			if (this.SubItemChanged != null) {
				this.SubItemChanged(sender, e);
			}
		}

		#region Overrides
		//protected override void OnDoubleClick(MouseButtonEventArgs e)
		//{
		//    base.OnMouseDoubleClick(e);

		//    //Point p = base.PointToClient(Cursor.Position);
		//    //int x = p.X;
		//    //int y = p.Y;

		//    //ListViewItem list = base.GetItemAt(x, y);
		//    //this.subItem = list.GetSubItemAt(x, y);

		//    //Rectangle rect = subItem.Bounds;

		//    //this.itemTextBox.Text = subItem.Text;
		//    //this.itemTextBox.Bounds = rect;
		//    //this.itemTextBox.Visible = true;
		//    //this.itemTextBox.BringToFront();
		//    //this.itemTextBox.Leave += new EventHandler(this.ItemTextBox_Leave);
		//    //this.itemTextBox.KeyUp += new KeyEventHandler(this.ItemTextBox_KeyPress);
		//    //this.itemTextBox.Focus();
		//}
		#endregion
	}
}
