﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.Windows.Shapes;
using Core.Collection;
using Core.Level;

namespace Core.Controls
{
	public class Canvas : System.Windows.Controls.Canvas
	{
		public const int INVALID_INDEX						= -1;
		public const int LINE_WIDHT							= 1;
		public const int LINE_WIDTH_RECTANGLE				= LINE_WIDHT;
		public const int LINE_WIDTH_SELECTED_RECTANGLE		= LINE_WIDTH_RECTANGLE << 1;

		public /*const*/ Brush LINE_COLOR_RECTANGLE				= Brushes.Blue;
		public /*const*/ Brush LINE_COLOR_SELECTED_RECTANGLE	= Brushes.Red;

		public event EventHandler EventSelectedChild, EventUnselectedChild, EventMovedChild;

		public int SelectedChildIndex { get; private set; }
		public UIElement SelectedChild { get; set; }
		private float scale;
		private ScaleTransform scaleTransform;

		public bool isSelectedChild;
		private Point lastMousePoint;

		public Canvas()
			: base()
		{
			base.Background = Brushes.Transparent;	// NULL -> MouseEvent doesn't work

			this.scaleTransform = new ScaleTransform();
			this.Scale = 1f;
			base.RenderTransform = this.scaleTransform;
		}

		public float Scale
		{
			get
			{
				return this.scale;
			}

			set
			{
				this.scale = value;
				this.scaleTransform.ScaleX = this.scaleTransform.ScaleY = this.scale;
			}
		}

		public static void SetPosition(UIElement element, double x, double y)
		{
			Canvas.SetLeft(element, x);
			Canvas.SetTop(element, y);
		}

		public static bool IsPointInRectangle(double x, double y, double x1, double y1, double x2, double y2)
		{
			if (x > x1 && x < x2 && y > y1 && y < y2)
			{
				return true;
			}

			return false;
		}

		public static bool IsPointUIElement(Point point, UIElement element)
		{
			double x = Canvas.GetLeft(element);
			double y = Canvas.GetTop(element);

			return IsPointInRectangle(point.X, point.Y, x, y, x + element.DesiredSize.Width, y + element.DesiredSize.Height);
		}

		//public static bool IsPointInShape(Point point, Shape shape)
		//{
		//	double x = Canvas.GetLeft(shape);
		//	double y = Canvas.GetTop(shape);

		//	return IsPointInRectangle(point.X, point.Y, x, y, x + shape.Width, y + shape.Height);
		//}

		#region Override
		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			//Debug.WriteLine("OnMouseMove...");

			Point point = e.GetPosition(this);

			if (e.LeftButton == MouseButtonState.Pressed && this.isSelectedChild)
			{
				double dx = point.X - this.lastMousePoint.X;
				double dy = point.Y - this.lastMousePoint.Y;

				Debug.WriteLine("Mouse move\n->dx: {0}, dy: {1}", dx, dy);

				this.MoveChild(this.SelectedChildIndex, dx, dy);
			}

			this.lastMousePoint = point;			
		}

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonDown(e);
			Point point = e.GetPosition(this);
			this.lastMousePoint = point;	// Remember last mouse point

			int index = base.Children.Count - 1;
			while (index > INVALID_INDEX)
			{
				UIElement element = VisualTreeHelper.GetChild(this, index) as UIElement;
				if (element.Visibility == System.Windows.Visibility.Visible && IsPointUIElement(point, element))
				{
					Debug.WriteLine("OnMouseLeftButtonDown->IsPointUIElement{0}", index);
					this.isSelectedChild = true;
					this.SelectChild(index);
					break;
				}

				index--;
			}

			if (index == INVALID_INDEX)
			{
				this.isSelectedChild = false;
				this.UnselectChild(this.SelectedChildIndex);
				this.SelectedChildIndex = INVALID_INDEX;
			}
		}

		protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonUp(e);
			//this.IsSelectedChild = false;
		}
		#endregion

		public void AppendLine(int x1, int y1, int x2, int y2)
		{
			Line line = new Line();
			line.Stroke = System.Windows.Media.Brushes.LightSteelBlue;
			line.X1 = x1;
			line.X2 = x2;
			line.Y1 = y1;
			line.Y2 = y2;
			line.StrokeThickness = LINE_WIDTH_RECTANGLE;

			base.Children.Add(line);
		}

		public void AppendRectangle(int uid, int x, int y, int width, int height)
		{
			Rectangle rect = new Rectangle();
			rect.Uid = uid.ToString();
			rect.Width = width;
			rect.Height = height;
			//rect.Fill = Brushes.Black;
			rect.Stroke = LINE_COLOR_RECTANGLE;
			rect.StrokeThickness = LINE_WIDTH_RECTANGLE;

			SetPosition(rect, x, y);
			base.Children.Add(rect);
		}

		public void AppendImage(Image image, int uid, int x, int y)
		{
			image.Uid = uid.ToString();
			SetPosition(image, x, y);			

			base.Children.Add(image);
		}

		public void SetVisible(int index, bool visible)
		{
			if (index > INVALID_INDEX)
			{
				if (visible)
				{
					base.Children[index].Visibility = Visibility.Visible;
				}
				else
				{
					base.Children[index].Visibility = Visibility.Hidden;
				}
			}
		}

		public void SelectChild(int index)
		{
			if (index > INVALID_INDEX && index != this.SelectedChildIndex)	// Effect on the first time only
			{
				Debug.WriteLine("Select child {0}", index);
				UIElement element = VisualTreeHelper.GetChild(this, index) as UIElement;

				if (element != null)
				{
					element.Opacity = 0.5f;
					//element.Stroke = LINE_COLOR_SELECTED_RECTANGLE;
					//element.StrokeThickness = LINE_WIDTH_SELECTED_RECTANGLE;
				}

				this.UnselectChild(this.SelectedChildIndex);
				this.isSelectedChild = true;
				this.SelectedChildIndex = index;
				this.SelectedChild = element;

				MouseEventArgs e = new MouseEventArgs(Mouse.PrimaryDevice, 0);
				this.OnSelectedChild(this, e);
			}
		}

		public void UnselectChild(int index)
		{
			if (index > INVALID_INDEX)
			{
				Debug.WriteLine("Unselect child {0}", index);
				UIElement element = VisualTreeHelper.GetChild(this, index) as UIElement;

				if (element != null)
				{
					element.Opacity = 1f;
					//element.Stroke = LINE_COLOR_RECTANGLE;
					//element.StrokeThickness = LINE_WIDTH_RECTANGLE;
				}

				this.isSelectedChild = false;
				this.SelectedChildIndex = INVALID_INDEX;

				MouseEventArgs e = new MouseEventArgs(Mouse.PrimaryDevice, 0);
				this.OnUnselectedChild(this, e);
			}
		}

		public void MoveChild(int index, double dx, double dy)
		{
			UIElement element = VisualTreeHelper.GetChild(this, index) as UIElement;
			double x = Canvas.GetLeft(element) + dx;
			double y = Canvas.GetTop(element) + dy;
			Debug.WriteLine("Move child from {0}, {1} => {2}, {3}", Canvas.GetLeft(element), Canvas.GetTop(element), x, y);

			SetPosition(element, x, y);

			MouseEventArgs e = new MouseEventArgs(Mouse.PrimaryDevice, 0);
			this.OnMovedChild(this, e);
		}

		#region Events
		protected virtual void OnSelectedChild(object sender, MouseEventArgs e)
		{
			if (this.EventSelectedChild != null)
			{
				this.EventSelectedChild(sender, e);
			}
		}

		protected virtual void OnUnselectedChild(object sender, MouseEventArgs e)
		{
			if (this.EventUnselectedChild != null)
			{
				this.EventUnselectedChild(sender, e);
			}
		}

		protected virtual void OnMovedChild(object sender, MouseEventArgs e)
		{
			if (this.EventMovedChild != null)
			{
				this.EventMovedChild(sender, e);
			}
		}
		#endregion
	}
}
