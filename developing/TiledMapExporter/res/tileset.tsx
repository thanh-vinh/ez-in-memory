<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE tileset SYSTEM "http://mapeditor.org/dtd/1.0/map.dtd">
<tileset name="tileset" tilewidth="32" tileheight="32">
 <image source="tileset.png" width="96" height="96"/>
 <tile id="0">
  <properties>
   <property name="name" value="hero"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="name" value="enemy"/>
  </properties>
 </tile>
</tileset>
