
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Tiled Map Editor tile layer information.
 * @author Thanh Vinh
 */
public class TileLayer
{
	public static final int GID_VALUE				= 0xffff;		// Max short: 0x7fff
	public static final int TILED_MAP_FLAG_FLIPX	= 0x80000000;
	public static final int TILED_MAP_FLAG_FLIPY	= 0x40000000;
	public static final int TILED_MAP_FLAG_FLIPXY	= 0xC0000000;

	public static final byte FLAG_NONE				= 0;
	public static final byte FLAG_FLIPY				= 1;
	public static final byte FLAG_FLIPX				= 1 << 1;

	private String name;
	private short width, height;
	private short[] firstGids;										// All tileset (layers) start gid
	private long[] data;

	public TileLayer(String name, short width, short height, short[] firstGids)
	{
		this.name = name;
		this.width = width;
		this.height = height;
		this.firstGids = firstGids;

		this.data = new long[width * height];
	}

	public String getName()
	{
		return this.name;
	}

	public void setTileAt(int pos, long gid)
	{
		this.data[pos] = gid;
	}

	public byte getTileFlags(long gid)
	{
		byte flags = FLAG_NONE;
		if ((gid & TILED_MAP_FLAG_FLIPX) != 0) {
			flags |= FLAG_FLIPX;
		}
		if ((gid & TILED_MAP_FLAG_FLIPY) != 0) {
			flags |= FLAG_FLIPY;
		}

		return flags;
	}

	public short getTile(long gid)
	{
		short tile = (short)(gid & GID_VALUE);

		if (tile > 0) {
			for (int i = this.firstGids.length - 1; i > -1; i--) {
				short firstGid = this.firstGids[i];
				if (tile >= firstGid) {
					tile -= firstGid;
					break;
				}
			}
		} else {
			tile = -1;
		}

		return tile;
	}

	/**
	 * Binary file format:
	 * - Tile id = gid - TILE_FIRST_GID instead of gid in XML file.
	 * - Tile none is 0xff (0 - 1 = -1) instead of 0 in XML file.
	 */
	public void saveBinary(String binaryFile)
	{
		try {
			DataOutputStream output = new DataOutputStream(new FileOutputStream(binaryFile, false));
			// Layer size
			output.writeShort(this.width);
			output.writeShort(this.height);

			//
			// Layer data
			//
			for (int i = 0; i < this.data.length; i++) {
				long gid = this.data[i];
				byte flags = this.getTileFlags(gid);
				short tile = this.getTile(gid);
				output.writeByte(flags);
				output.writeShort(tile);
				/* System.out.println("--- " + i);
				System.out.println("flag: " + flags);
				System.out.println("tile: " + tile); */
			}

			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
