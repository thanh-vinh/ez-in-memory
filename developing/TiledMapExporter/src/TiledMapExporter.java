
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Export Tiled Map Editor file <*.tmx>.
 * File format at: http://mapeditor.org/dtd/1.0/map.dtd
 * @author Thanh Vinh
 */
public class TiledMapExporter
{
	private static final String INTRO		 			= "TiledMapExporter version 0.0.2";
	private static final String COPYRIGHT	 			= "Copyright 2011 EZ Team <ezgroup@groups.live.com>";
	private static final String BINARY_EXTENSION		= ".bin";

	private static final String NODE_TILESET 			= "tileset";
	private static final String NODE_FILE	 			= "image";
	private static final String NODE_TILE	 			= "tile";
	private static final String NODE_PROPERTY	 		= "property";
	private static final String NODE_LAYER	 			= "layer";
	private static final String NODE_DATA	 			= "data";
	private static final String ATTRIB_ORIENTATION 		= "orientation";
	private static final String ATTRIB_SOURCE 			= "source";				// Tileset source <*.tsx>
	private static final String ATTRIB_WIDTH 			= "width";
	private static final String ATTRIB_HEIGHT 			= "height";
	private static final String ATTRIB_TILEWIDTH		= "tilewidth";
	private static final String ATTRIB_TILEHEIGHT 		= "tileheight";
	private static final String ATTRIB_ID	 			= "id";
	private static final String ATTRIB_NAME	 			= "name";
	private static final String ATTRIB_VALUE 			= "value";
	private static final String ATTRIB_FIRSTGID			= "firstgid";
	private static final String ATTRIB_GID 				= "gid";

	private String mapName;
	private String inputFile;
	private String outputPath;
	private short[] firstGids;
	private TileLayer[] tileLayers;

	public TiledMapExporter(String inputFile, String outputPath)
	{
		this.inputFile = inputFile;
		this.outputPath = outputPath;

		// Get map name
		File file = new File(inputFile);
		this.mapName = this.getFileNameWithoutExtension(inputFile);

		// Parse XML
		this.parseXML(this.inputFile);
	}

	private String getFileNameWithoutExtension(String pathname)
	{
		File file = new File(pathname);
		String fileName = file.getName();
		int index = fileName.lastIndexOf('.');
		fileName = fileName.substring(0, index);

		return fileName;
	}

	/**
	 * Parse Tiled Map Editor - TMX file.
	 * Note: Must save TMX file as XML.
	 */
	private void parseXML(String inputFile)
	{
		try {
			File file = new File(inputFile);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();

			//
			// Tileset firstgid
			//
			NodeList tilesetNodes = doc.getElementsByTagName(NODE_TILESET);
			this.firstGids = new short[tilesetNodes.getLength()];

			for (int i = 0; i < tilesetNodes.getLength(); i++) {
				Node tilesetNode = tilesetNodes.item(i);
				Element element = (Element) tilesetNode;
				short firstGid = Short.parseShort(element.getAttribute(ATTRIB_FIRSTGID));
				this.firstGids[i] = firstGid;
			}

			//
			// Layer data
			//
			NodeList layerNodes = doc.getElementsByTagName(NODE_LAYER);
			this.tileLayers = new TileLayer[layerNodes.getLength()];
			for (int i = 0; i < layerNodes.getLength(); i++) {
				Node layerNode = layerNodes.item(i);
				Element element = (Element) layerNode;
				/* System.out.println("name : " + element.getAttribute(ATTRIB_NAME));
				System.out.println("width : " + element.getAttribute(ATTRIB_WIDTH));
				System.out.println("height : " + element.getAttribute(ATTRIB_HEIGHT)); */

				String name = element.getAttribute(ATTRIB_NAME);
				short width = Short.parseShort(element.getAttribute(ATTRIB_WIDTH));
				short height = Short.parseShort(element.getAttribute(ATTRIB_HEIGHT));

				TileLayer tileLayer = new TileLayer(name, width, height, this.firstGids);

				if (layerNode.getNodeType() == Node.ELEMENT_NODE) {
					element = (Element) layerNode;
					NodeList tileNodes = element.getElementsByTagName(NODE_TILE);
					for (int j = 0; j < tileNodes.getLength(); j++) {
						Node tileNode = tileNodes.item(j);
						element = (Element)tileNode;
						/* System.out.println("grid: " + j);
						System.out.println("value : " + element.getAttribute(ATTRIB_GID)); */

						long gid = Long.parseLong(element.getAttribute(ATTRIB_GID));
						tileLayer.setTileAt(j, gid);
					}
				}

				this.tileLayers[i] = tileLayer;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveMapLayer()
	{
		System.out.println("Export all tileLayers in " + this.mapName);
		for (int i = 0; i < this.tileLayers.length; i++) {
			TileLayer tileLayer = this.tileLayers[i];
			String binaryFile = outputPath + File.separatorChar +
				this.mapName + "_" + tileLayer.getName() + BINARY_EXTENSION;

			System.out.println(" - Export tileLayer: " + tileLayer.getName());
			tileLayer.saveBinary(binaryFile);
		}
	}

	public static void main(String args[])
	{
		System.out.println(INTRO);
		System.out.println(COPYRIGHT);

		if (args.length > 1) {
			String inputFile = args[0];
			String outputPath = args[1];

			TiledMapExporter tiledMapExporter = new TiledMapExporter(inputFile, outputPath);

			tiledMapExporter.saveMapLayer();
			System.out.println("Export game map complete...");
		} else {
			System.out.println("Command: TiledMapExporter map-file binary-file-path");
			System.out.println("Example: TiledMapExporter tmx-file.xml Z:\\map-binary");
		}
	}
}
