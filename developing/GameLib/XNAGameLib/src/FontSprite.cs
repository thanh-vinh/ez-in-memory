using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

/**
 * A very simple font sprite.
 * @date Jan 03, 2012
 */

namespace GameLib
{
	public class FontSprite : Sprite
	{
		private const int INVALID_INDEX 	= -1; 			// A character is not exist in this font
		private const char SPACE_CHAR 		= (char) 0x20; 	// Space character

		private Dictionary<char, int> characterTable;		// Dictionary to look up a character in the module list
		private int spacing;
		private int height;
		private int expandedSpacing = 1;
		private int lineSpacing;

		public FontSprite()
			: base()
		{
		}

		public FontSprite(GraphicsDevice graphicsDevice, byte[] buf)
			: base(graphicsDevice, buf)
		{;
		}

		public void SetFontMapping(byte[] buf)
		{
			Stream stream = new MemoryStream(buf);
			BinaryReader reader = new BinaryReader(stream, System.Text.Encoding.UTF8);

			int length = reader.ReadInt16();

			// Initialize the Dictionary characterTable
			this.characterTable = new Dictionary<char, int>(length);
			for (int i = 0; i < length; i++) {
				int value = reader.ReadInt16();
				this.characterTable.Add((char) value, i);
			}

			this.spacing = reader.ReadInt16();		// Space char length
			this.height = reader.ReadInt16();		// Font height
			reader.Dispose();
			stream.Dispose();
		}

		private int GetCharIndex(char c)
		{
			int value = INVALID_INDEX;

			try {
				value = this.characterTable[c];	// Find value by key - character c
			} catch (KeyNotFoundException e) {
				// Ignore if error
			}

			return value;
		}

		public int GetCharWidth(char c)
		{
			int index = this.GetCharIndex(c);
			if (index != INVALID_INDEX) {
				return base.GetModuleWidth(index);
			} else {
				return this.spacing;
			}
		}

		public int GetCharHeight()
		{
			return this.height;
		}

		public int GetWidth(string text)
		{
			int width = 0;

			if (text != null) {
				for (int i = 0; i < text.Length; i++) {
					char c = text[i];
					int charWidth = this.GetCharWidth(c);
					width += charWidth + this.expandedSpacing;
				}
			}

			return width;
		}

		public void DrawCharAt(SpriteBatch spriteBatch, char c, int x, int y, int anchor)
		{
			if (c != SPACE_CHAR) {
				int index = this.GetCharIndex(c);
				if (index != INVALID_INDEX) {
					base.DrawModule(spriteBatch, index, x, y, anchor);
				}
			}
		}

		public void DrawString(SpriteBatch spriteBatch, string text, int x, int y, int anchor)
		{
			if (text == null) return;

			/*if ((anchor & Graphics.LEFT) != 0) {				// LEFT
				Ignore, default is TOP | LEFT
			} else */
			if ((anchor & SpriteAnchor.HCENTER) != 0) {			// HCENTER
				x -= this.GetWidth(text) >> 1;
			} else if ((anchor & SpriteAnchor.RIGHT) != 0) {	// RIGHT
				x -= this.GetWidth(text);
			}

			/*if ((anchor & Graphics.TOP) != 0) {				// TOP
				Ignore, default is TOP | LEFT
			} else */
			if ((anchor & SpriteAnchor.VCENTER) != 0) {			// VCENTER
				y -= this.GetCharHeight() >> 1;
			} else if ((anchor & SpriteAnchor.BOTTOM) != 0) {
				y -= this.GetCharHeight();
			}

			for (int i = 0; i < text.Length; i++) {
				char c = text[i];
				char c1 = c;
				if (i < text.Length - 1) {
					c1 = text[i + 1];
				}

				if (c == '\\' && Char.IsDigit(c1)) {
					//TODO Implement font palette here
					//int palette = int.Parse(c.ToString());
					//base.SetPalette(palette);
					i++;
				} else {
					this.DrawCharAt(spriteBatch, c, x, y, 0);
					int charWidth = this.GetCharWidth(c);
					x += charWidth + this.expandedSpacing;
				}
			}
		}

		// TODO use short[] instead String[]
		//public string[] WrapText(string text, int width)
		//{
		//    Vector abc = new Vector();
		//    String line = new String();
		//    String word = new String();
		//    char c;
		//    int cWidth = 0, index = 0, w;
		//    while(text.charAt(index)==' ')
		//        index++;
		//    while(index<text.length())
		//    {
		//        c = text.charAt(index);
		//        w = this.GetCharWidth(c);
		//        if (c==' ') {
		//            line += word;
		//            word = new String();
		//            if (cWidth + w > width) {
		//                abc.addElement(line);
		//                line = new String();
		//                cWidth = 0;
		//            } else {
		//                line += c;
		//            }
		//            cWidth += w;
		//        } else if (c=='\\' && !Character.isDigit(text.charAt(index+1))){
		//            line += c + text.charAt(index+1);
		//            index ++;
		//        } else {
		//            if (cWidth + w > width) {
		//                abc.addElement(line);
		//                line = new String();
		//                cWidth = 0;
		//            }
		//            word += c;
		//            cWidth += w;
		//        }
		//        index ++;
		//    }
		//    // the last char
		//    if (word.length()>0) {
		//        line += word;
		//        abc.addElement(line);
		//    }
		//    word = null;
		//    String wrapedText[] = new String[abc.size()];
		//    for (int i = 0; i < wrapedText.length; i++) {
		//        wrapedText[i] = (String)abc.elementAt(i);
		//    }
		//    abc = null;

		//    return wrapedText;
		//}

		public void DrawPage(SpriteBatch spriteBatch, String[] lines, int x, int y, int anchor)
		{
			if (lines != null) {
				for (int i = 0; i < lines.Length; i++) {
					String line = lines[i];
					this.DrawString(spriteBatch, line, x, y, anchor);
					y += (this.GetCharHeight() + this.lineSpacing);
				}
			}
		}
	}
}
