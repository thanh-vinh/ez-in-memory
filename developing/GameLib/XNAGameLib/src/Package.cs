﻿using System;
using System.IO;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#if WINDOWS || XBOX
using System.IO.Compression;
#else
using ICSharpCode.SharpZipLib.GZip;
#endif

namespace GameLib
{
	public static class Package
	{
		//
		// Point to GraphicsDevice
		//
		private static GraphicsDevice graphics;

		//
		// Package compress methodS
		//
		private const byte COMPRESS_TYPE_NONE 	= 0;
		private const byte COMPRESS_TYPE_GZIP 	= 1;

		private static string rootDirectory;	// Root directory

		//
		// For data package
		//
		private static string packageFile;		// File name
		private static bool useGzip;			// Compress
		private static int size;				// Package size
		private static int fileCount;			// Total files in this package
		private static int[] offsets;			// File offsets
		private static int[] lengths;			// File length

		private static string[] texts;			// Text cache

		public static void SetRootDirectory(string value)
		{
			rootDirectory = value;
		}

		public static void SetGraphics(GraphicsDevice g)
		{
			graphics = g;
		}


		public static int GetFileCount()
		{
			return fileCount;
		}

		public static int GetGzipDecompressSize(byte[] buf)
		{
			int length = BitConverter.ToInt32(buf, buf.Length - 4);	// Last 4 bytes of Gzip buffer
			return length;
		}

		public static void Open(String fileName)
		{
			packageFile = fileName;

			if (rootDirectory != null) {
				packageFile = rootDirectory + Path.DirectorySeparatorChar + fileName;
			}

			using (Stream stream = TitleContainer.OpenStream(packageFile)) {
				using (BinaryReader reader = new BinaryReader(stream)) {
					useGzip = (reader.ReadByte() == COMPRESS_TYPE_GZIP) ? true : false;	// Use Gzip
					size = reader.ReadInt32();											// Package size
					fileCount = reader.ReadInt16();										// File count

					// Load file offsets
					offsets = new int[fileCount];										// All file offsets
					for (int i = 0; i < fileCount; i++) {
						offsets[i] = reader.ReadInt32();								// File offset
					}

					// Calculate file lengths
					lengths = new int[fileCount];
					for (int i = 1; i < fileCount; i++) {						// From begin to end, except the last file
						lengths[i - 1] = offsets[i] - offsets[i - 1];
					}
					lengths[fileCount - 1] = size - offsets[fileCount - 1] - 1;	// The last file
				}
			}
		}

		public static void Close()
		{
			// Reset package properties
			packageFile = null;
			useGzip = false;
			size = 0;
			offsets = null;
			lengths = null;
		}

		/**
		 * Get data from a package which packed by data-package tool.
		 */
		public static byte[] GetBytes(int fileIndex)
		{
			int offset = offsets[fileIndex];
			int length = lengths[fileIndex];
			byte[] buf = new byte[length];

			using (Stream stream = TitleContainer.OpenStream(packageFile)) {	// Read data
				stream.Seek(offset, SeekOrigin.Begin);
				using (BinaryReader reader = new BinaryReader(stream)) {
					reader.Read(buf, 0, length);
				}
			}

			if (useGzip) {														// Decompress data
				using (MemoryStream stream = new MemoryStream(buf)) {
					#if WINDOWS || XBOX
					using (GZipStream reader = new GZipStream(stream, CompressionMode.Decompress)) {
					#else
					using (GZipInputStream reader = new GZipInputStream(stream)) {
					#endif
						length = GetGzipDecompressSize(buf);
						byte[] decompress = new byte[length];
						reader.Read(decompress, 0, length);
						buf = null;
						buf = decompress;
					}
				}
			}

			return buf;
		}

		/**
		 * Load a text package into a String array.
		 * @param fileName	Package file name which is defined properties file.
		 */
		public static void LoadText(int index)
		{
			byte[] buf = GetBytes(index);

			using (Stream stream = new MemoryStream(buf)) {
				using (BinaryReader reader = new BinaryReader(stream, Encoding.UTF8)) {
					int size = reader.ReadInt32();
					texts = new String[size];

					for (int i = 0; i < size; i++) {
						int length = reader.ReadInt32();
						byte[] data = new byte[length];
						reader.Read(data, 0, length);
						string value = Encoding.UTF8.GetString(data, 0, data.Length);
						texts[i] = value;
					}
				}
			}
		}

		/**
		 * Get a row from text package.
		 * @param textId	The index of this row which defined in Text class.
		 * @return			Value of this row in selected package.
		 */
		public static String GetText(int index)
		{
			if (index > -1)
				return texts[index];
			else
				return null;
		}

		public static Sprite LoadSprite(int fileIndex)
		{
			byte[] buf = GetBytes(fileIndex);
			Sprite sprite = new Sprite(graphics, buf);
			return sprite;
		}

		public static FontSprite LoadFontSprite(int fileIndex, int characterMapping)
		{
			byte[] buf = GetBytes(fileIndex);
			FontSprite font = new FontSprite(graphics, buf);
			buf = GetBytes(characterMapping);
			font.SetFontMapping(buf);
			return font;
		}
	}
}
