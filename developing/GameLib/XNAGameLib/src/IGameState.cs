﻿using System;
using Microsoft.Xna.Framework;

namespace GameLib
{
	public interface IGameState
	{
		void Initialize();
		void LoadContent();
		void Update(GameTime gameTime);
		void Draw(GameTime gameTime);
		void UnloadContent();
	}
}
