﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameLib
{
	public static class Message
	{
		public const int INITIALIZE				= 0;
		public const int UPDATE					= 1;
		public const int EXIT					= 2;
	}

	public static class SpriteAnchor
	{
		public const int NONE					= 0;
		public const int LEFT					= 0;
		public const int TOP					= 0;
		public const int RIGHT					= 1;
		public const int BOTTOM					= 1 << 1;
		public const int HCENTER				= 1 << 2;
		public const int VCENTER				= 1 << 3;
	}

	public static class SpriteFlag
	{
		public const int NONE					= 0;
		public const int FLIPY					= 1;
		public const int FLIPX					= 1 << 1;
		public const int ROTATE90				= 1 << 2;
	}
}
