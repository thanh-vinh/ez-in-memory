﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#if WINDOWS_PHONE		//Using multiple touch on WP
using Microsoft.Xna.Framework.Input.Touch;
#endif

namespace GameLib
{
	public partial class Game : Microsoft.Xna.Framework.Game
	{
		protected static long frameCounter;
		protected static GraphicsDeviceManager graphics;

		public Game()
			: base()
		{
			graphics = new GraphicsDeviceManager(this);
		}

		//
		// Override
		//
		/// <summary>
		/// Apply the game configuration in GameLibConfig class.
		/// </summary>
		protected override void Initialize()
		{
			// Graphics
			graphics.IsFullScreen = GameLibConfig.useFullScreen;
			if (GameLibConfig.setScreenSize) {
				graphics.PreferredBackBufferWidth = GameLibConfig.screenWidth;
				graphics.PreferredBackBufferHeight = GameLibConfig.screenHeight;
			}
			graphics.ApplyChanges();

			// Package
			Package.SetRootDirectory(GameLibConfig.contentRootDirectory);
			Package.SetGraphics(GetGraphics());

			// Others
			#if WINDOWS		//Using mouse/keyboard
			this.IsMouseVisible = GameLibConfig.visibleMouse;
			#endif

			// Multi touch
			#if WINDOWS_PHONE		//Using multiple touch on WP
			TouchPanel.EnabledGestures = GestureType.Tap | GestureType.DoubleTap | GestureType.Hold
				| GestureType.VerticalDrag | GestureType.HorizontalDrag | GestureType.FreeDrag;
			//TouchPanel.DisplayWidth = GameLibConfig.screenWidth;
			//TouchPanel.DisplayHeight = GameLibConfig.screenHeight;
			#endif	//WINDOWS_PHONE

			// Math
			InitializeRandom();

			base.Initialize();
		}

		protected override void Update(GameTime gameTime)
		{
			frameCounter++;

			#if WINDOWS		//Using mouse/keyboard
			prevKeyboard = keyboard;
			prevMouse = mouse;

			keyboard = Keyboard.GetState();
			mouse = Mouse.GetState();
			#endif	//WINDOWS

			#if WINDOWS_PHONE		//Using multiple touch on WP
			touchCollection = TouchPanel.GetState();
			#endif

			base.Update(gameTime);
		}

		//
		// Screen size
		//
		public static int GetScreenWidth()
		{
			return graphics.PreferredBackBufferWidth;
		}

		public static int GetScreenHeight()
		{
			return graphics.PreferredBackBufferHeight;
		}

		public static GraphicsDevice GetGraphics()
		{
			return graphics.GraphicsDevice;
		}

		public static long GetFrameCounter()
		{
			return frameCounter;
		}

		//
		// Debug to console/output screen
		//
		public static void Log(object message)
		{
			if (GameLibConfig.debug) {
				#if WINDOWS
				Console.WriteLine(message);						// Using console screen
				#else
				System.Diagnostics.Debug.WriteLine(message);	// Using output screen
				#endif
			}
		}
	}
}
