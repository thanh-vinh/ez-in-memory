
REM MsBuild to compile project
REM Using: make.bat project-file configuration
REM Example: make.bat Windows debug

@echo off

echo ##
echo # Setting some common variables...
echo ##
call ..\config.bat

REM Relative paths
set RELEASE_PATH=%PROJECT_DIR%\.release
set DOCS_PATH=%PROJECT_DIR%\.docs

REM Tools
set TOOLS_PATH=%PROJECT_DIR%\_tools
set CPP_TOOL=%TOOLS_PATH%\cpp\cpp.exe
set FONT_EXPORTER=%TOOLS_PATH%\font-exporter\font-exporter.jar
set TEXT_EXPORTER=%TOOLS_PATH%\text-exporter\text-exporter.jar
set SPRITE_EDITOR=%TOOLS_PATH%\SpriteEditor\SpriteEditor.exe
set TILED_MAP_EXPORTER=%TOOLS_PATH%\tiled-map-exporter\tiled-map-exporter.jar
set DATA_PACKAGE=%TOOLS_PATH%\data-package\data-package.jar
set CLASS_CREATER=%TOOLS_PATH%\Scripts\ClassCreater.bat

REM Windows studio project file extension
set PROJECT_FILE_EXT=csproj

set CONFIGURATION=%1%
set PROJECT_FILE=%2%

if "%CONFIGURATION%"=="" (
	set CONFIGURATION=debug
)
if "%PROJECT_FILE%"=="" (
	set PROJECT_FILE=GameLibWindows
)
set PROJECT_FILE=%PROJECT_FILE%.%PROJECT_FILE_EXT%

echo ##
echo # Building %PROJECT_NAME%...
echo ##
echo.

:init
cls
pushd %PROJECT_DIR%

if "%CONFIGURATION%"=="clean" (
	goto clean
) else (
	goto compile
)


:compile
echo.
echo ##
echo # Compile source...
echo ##
echo Project file: %PROJECT_FILE%
echo Configuration: %CONFIGURATION%

call MsBuild.exe %PROJECT_FILE% /t:Rebuild /p:Configuration=%CONFIGURATION%
if "%CONFIGURATION%"=="release" (
	echo Using Eazfuscator.NET to obfuscate...
	cd %RELEASE_PATH%
	call %EAZFUSCATOR_EXEC% %PROJECT_NAME%.dll
)
goto finish


:clean
echo.
echo ##
echo # Cleanup...
echo ##
rmdir %RELEASE_PATH% /s /q

goto finish


:error


:finish
popd
echo.
echo ##
echo # Compeleted!
echo ##
