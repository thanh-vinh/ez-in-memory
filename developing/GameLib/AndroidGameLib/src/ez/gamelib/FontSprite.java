package ez.gamelib;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import android.graphics.Canvas;

/**
 * A very simple font sprite.
 * @date Feb 12, 2012
 */

public class FontSprite extends Sprite
{
	private static final int INVALID_INDEX 	= -1; 			// A character is not exist in this font
	private static final char SPACE_CHAR 	= 0x20; 		// Space character

	private Hashtable<Character, Integer> characterTable;	// Dictionary to look up a character in the module list
	private int spacing;
	private int height;
	private int expandedSpacing = 1;
	private int lineSpacing;

	public FontSprite(byte[] buf) throws IOException
	{
		super(buf);
	}

	public void SetFontMapping(byte[] buf) throws IOException
	{
		ByteArrayInputStream stream = new ByteArrayInputStream(buf);
		DataInputStream reader = new DataInputStream(stream);
		int length = reader.readShort();

		// Initialize the Hashtable characterTable
		this.characterTable = new Hashtable<Character, Integer>(length);
		for (int i = 0; i < length; i++) {
			char c = reader.readChar();
			this.characterTable.put(Character.valueOf(c), new Integer(i));
		}

		this.spacing = reader.readShort();		// Space char length
		this.height = reader.readShort();		// Font height

		reader.close();
		stream.close();
	}

	private int GetCharIndex(char c)
	{
		Integer value = (Integer)this.characterTable.get(String.valueOf(c));	// Find value by key - character c

		if (value != null) {
			return value.intValue();
		} else {
			return INVALID_INDEX;
		}
	}

	public int GetCharWidth(char c)
	{
		int index = this.GetCharIndex(c);
		if (index != INVALID_INDEX) {
			return super.GetModuleWidth(index);
		} else {
			return this.spacing;
		}
	}

	public int GetCharHeight()
	{
		return this.height;
	}

	public int GetWidth(String text)
	{
		int width = 0;

		if (text != null) {
			for (int i = 0; i < text.length(); i++) {
				char c = text.charAt(i);
				int charWidth = this.GetCharWidth(c);
				width += charWidth + this.expandedSpacing;
			}
		}

		return width;
	}

	public void DrawCharAt(Canvas canvas, char c, int x, int y, int anchor)
	{
		if (c != SPACE_CHAR) {
			int index = this.GetCharIndex(c);
			if (index != INVALID_INDEX) {
				this.DrawModule(canvas, index, x, y, anchor);
			}
		}
	}

	public void DrawString(Canvas canvas, String text, int x, int y, int anchor)
	{
		if (text == null) return;

		/*if ((anchor & Graphics.LEFT) != 0) {				// LEFT
			Ignore, default is TOP | LEFT
		} else */
		if ((anchor & SpriteAnchor.HCENTER) != 0) {			// HCENTER
			x -= this.GetWidth(text) >> 1;
		} else if ((anchor & SpriteAnchor.RIGHT) != 0) {	// RIGHT
			x -= this.GetWidth(text);
		}

		/*if ((anchor & Graphics.TOP) != 0) {				// TOP
			Ignore, default is TOP | LEFT
		} else */
		if ((anchor & SpriteAnchor.VCENTER) != 0) {			// VCENTER
			y -= this.GetCharHeight() >> 1;
		} else if ((anchor & SpriteAnchor.BOTTOM) != 0) {
			y -= this.GetCharHeight();
		}

		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			char c1 = c;
			if (i < text.length() - 1) {
				c1 = text.charAt(i + 1);
			}

			if (c == '\\' && Character.isDigit(c1)) {
//				int palette = Character.digit(c1, 10);
//				super.SetPalette(palette);
				i++;
			} else {
				this.DrawCharAt(canvas, c, x, y, 0);
				int charWidth = this.GetCharWidth(c);
				x += charWidth + this.expandedSpacing;
			}
		}
	}

	// TODO use short[] instead String[]
	public String[] WrapText(String text, int width)
	{
		Vector<String> abc = new Vector<String>();
		String line = new String();
		String word = new String();
		char c;
		int cWidth = 0, index = 0, w;
		while(text.charAt(index)==' ')
			index++;
		while(index<text.length())
		{
			c = text.charAt(index);
			w = this.GetCharWidth(c);
			if (c==' ') {
				line += word;
				word = new String();
				if (cWidth + w > width) {
					abc.addElement(line);
					line = new String();
					cWidth = 0;
				} else {
					line += c;
				}
				cWidth += w;
			} else if (c=='\\' && !Character.isDigit(text.charAt(index+1))){
				line += c + text.charAt(index+1);
				index ++;
			} else {
				if (cWidth + w > width) {
					abc.addElement(line);
					line = new String();
					cWidth = 0;
				}
				word += c;
				cWidth += w;
			}
			index ++;
		}
		// the last char
		if (word.length()>0) {
			line += word;
			abc.addElement(line);
		}
		word = null;
		String wrapedText[] = new String[abc.size()];
		for (int i = 0; i < wrapedText.length; i++) {
			wrapedText[i] = (String)abc.elementAt(i);
		}
		abc = null;

		return wrapedText;
	}

	public void DrawPage(Canvas canvas, String[] lines, int x, int y, int anchor)
	{
		if (lines != null) {
			for (int i = 0; i < lines.length; i++) {
				String line = lines[i];
				this.DrawString(canvas, line, x, y, anchor);
				y += (this.GetCharHeight() + this.lineSpacing);
			}
		}
	}
}
