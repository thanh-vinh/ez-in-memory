package ez.gamelib;

public class SpriteFlag
{
	public static final int NONE					= 0;
	public static final int FLIPY					= 1;
	public static final int FLIPX					= 1 << 1;
	public static final int ROTATE90				= 1 << 2;
}
