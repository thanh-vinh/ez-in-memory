package ez.gamelib;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;

/**
 * A very simple sprite.
 * @date Feb 12, 2012
 */

public class Sprite
{
	//
	// Pixel format
	//
	public static final int PIXEL_FORMAT_8BPP_INDEXED	= 1;
	public static final int PIXEL_FORMAT_24BPP_RGB		= 3;
	public static final int PIXEL_FORMAT_32BPP_ARGB		= 4;

	//
	// Flags, same as same transform values in javax.microedition.lcdui.game.Sprite class
	//
	public static final int FLAG_NONE					= 0;
	public static final int FLAG_FLIPY					= 1;
	public static final int FLAG_FLIPX					= 1 << 1;
	public static final int FLAG_ROTATE90				= 1 << 2;

	//
	// Texture
	//
	private int pixelLength;			// Length of a pixel by bytes

	//
	// Module
	//
	private short[] modulesWidth;		// Module: width
	private short[] modulesHeight;		// Module: height
	private Bitmap[] textures;			// Module: image

	//
	// Frame
	//
	private short[] frames;				// Frames offset in fmodules
	private short[] framesLength;		// Frames length
	private short[] framesX;			// Frame X
	private short[] framesY;			// Frame Y
	private short[] framesWidth;		// Frame width
	private short[] framesHeight;		// Frame height

	//
	// FModule
	//
	private short[] fmodules;
	private short[] fmodulesX;
	private short[] fmodulesY;
	private byte[] fmodulesFlag;

	//
	// Animation
	//
	private short[] animations;			// Animations offset in aframes
	private short[] animationsLength;	// Animations length

	//
	// AFrame
	//
	private short[] aframes;			// AFrames offset in animations
	private short[] aframesX;			// AFrames x
	private short[] aframesY;			// AFrames y
	private byte[] aframesTime;			// AFrames time
	private byte[] aframesFlags;		// AFrames flags
	
	//
	// Sprite effect
	//
	private Matrix matrix;

	public Sprite(byte[] buf) throws IOException
	{
		this.Load(buf);
		this.matrix = new Matrix();
	}

	private int[] GetARGB(byte[] buf)
	{
		int length = (buf.length * PIXEL_FORMAT_32BPP_ARGB) / this.pixelLength;
		int[] argb = new int[length];

		int pos = 0;
		for (int i = 0; i < buf.length; i += this.pixelLength) {
			byte r = 0x0, g = 0x0, b = 0x0, a = (byte) 0xff;

			if (this.pixelLength == PIXEL_FORMAT_24BPP_RGB) {			// 24-bit image (RGB)
				r = buf[i];
				g = buf[i + 1];
				b = buf[i + 2];

				if (r == 0xff && g == 0x0 && b == 0xff) {				// Magenta -> Transparent
					a = 0x0;
				}
			} else if (this.pixelLength == PIXEL_FORMAT_32BPP_ARGB) {	// 32-bit image (RGBA)
				r = buf[i];
				g = buf[i + 1];
				b = buf[i + 2];
				a = buf[i + 3];
			}

			argb[pos] = a;
			argb[pos + 1] = r;
			argb[pos + 2] = g;
			argb[pos + 3] = b;

			pos += PIXEL_FORMAT_32BPP_ARGB;
		}

		return argb;
	}

	private int GetAFrameOffset(int animation)
	{
		return this.animations[animation];
	}

	private int GetFModuleOffset(int frame)
	{
		return this.frames[frame];
	}

	private int GetFModuleX(int frame, int fmodule)
	{
		int offset = this.GetFModuleOffset(frame) + fmodule;
		return this.fmodulesX[offset];
	}

	private int GetFModuleY(int frame, int fmodule)
	{
		int offset = this.GetFModuleOffset(frame) + fmodule;
		return this.fmodulesY[offset];
	}

	private int GetFModuleFlag(int frame, int fmodule)
	{
		int offset = this.GetFModuleOffset(frame) + fmodule;
		return this.fmodulesFlag[offset];
	}

	private int GetAFrameX(int animation, int aframe)
	{
		int offset = this.GetAFrameOffset(animation) + aframe;
		return this.aframesX[offset];
	}

	private int GetAFrameY(int animation, int aframe)
	{
		int offset = this.GetAFrameOffset(animation) + aframe;
		return this.aframesY[offset];
	}

	private int GetAFrameFlag(int animation, int aframe)
	{
		int offset = this.GetAFrameOffset(animation) + aframe;
		return this.aframesFlags[offset];
	}
	
	/**
	 * Destructor for CSprite.
	 * Sometimes, set CSprite = null but system still keeps image.
	 * Call this method to release them.
	 */
	protected void Dispose()
	{
		if (this.textures != null) {
			for (int i = 0; i < this.textures.length; i++) {
				if (this.textures[i] != null) {
					this.textures[i] = null;
				}
			}
		}
	}

	public void Load(byte[] buf) throws IOException
	{
		ByteArrayInputStream stream = new ByteArrayInputStream(buf);
		DataInputStream reader = new DataInputStream(stream);

		int length;
		byte[] data;

		//
		// Modules
		//
		this.pixelLength = reader.readByte();				// Pixel format (length)
		length = reader.readShort();						// Modules count
		this.textures = new Bitmap[length];
		this.modulesWidth = new short[length];
		this.modulesHeight = new short[length];

		for (int i = 0; i < modulesWidth.length; i++) {
			short width = reader.readShort();				// Width
			short height = reader.readShort();				// Height
			length = width * height * this.pixelLength;		// Length

			data = new byte[length];
			reader.read(data, 0, length);					// Image data (bytes)
			int[] agrb = this.GetARGB(data);
			this.textures[i] = Bitmap.createBitmap(agrb, width, height, Bitmap.Config.ALPHA_8);

			this.modulesWidth[i] = width;
			this.modulesHeight[i] = height;
		}

		//
		// Frames
		//
		length = reader.readShort();							// Frames count
		this.frames = new short[length];
		this.framesLength = new short[length];
		this.framesX = new short[length];
		this.framesY = new short[length];
		this.framesWidth = new short[length];
		this.framesHeight = new short[length];

		length = reader.readShort();							// FModules count
		this.fmodules = new short[length];
		this.fmodulesX = new short[length];
		this.fmodulesY = new short[length];
		this.fmodulesFlag = new byte[length];
		for (int i = 0; i < this.frames.length; i++) {
			length = reader.readShort();						// FModules count in the current frame
			this.framesLength[i] = (short)length;
			for (int j = 0; j < length; j++) {
				this.fmodules[this.frames[i] + j] = reader.readShort();		// Module index
				this.fmodulesX[this.frames[i] + j] = reader.readShort();	// X
				this.fmodulesY[this.frames[i] + j] = reader.readShort();	// Y
				this.fmodulesFlag[this.frames[i] + j] = reader.readByte();	// Flag
			}

			this.framesX[i] = reader.readShort();				// X
			this.framesY[i] = reader.readShort();				// Y
			this.framesWidth[i] = reader.readShort();			// Width
			this.framesHeight[i] = reader.readShort();			// Height

			if (i < this.frames.length - 1) {					// Frames offsets
				this.frames[i + 1] += (short)(this.frames[i] + length);
			}
		}

		//
		// Animations
		//
		length = reader.readShort();							// Animations count
		this.animations = new short[length];
		this.animationsLength = new short[length];

		length = reader.readShort();							// AFrames count
		this.aframes = new short[length];
		this.aframesX = new short[length];
		this.aframesY = new short[length];
		this.aframesTime = new byte[length];
		this.aframesFlags = new byte[length];
		for (int i = 0; i < this.animations.length; i++) {
			length = reader.readShort();						// AFrames count in the current animation
			this.animationsLength[i] = (short)length;
			for (int j = 0; j < length; j++) {
				this.aframes[this.animations[i] + j] = reader.readShort();		// Frame index
				this.aframesX[this.animations[i] + j] = reader.readShort();		// X
				this.aframesY[this.animations[i] + j] = reader.readShort();		// Y
				this.aframesTime[this.animations[i] + j] = reader.readByte();	// Time
				this.aframesFlags[this.animations[i] + j] = reader.readByte();	// Flag
			}

			if (i < this.animations.length - 1) {				// Frames offsets
				this.animations[i + 1] += (short)(this.animations[i] + length);
			}
		}

		reader.close();
		stream.close();
	}

	public Bitmap GetTexture(int module)
	{
		return this.textures[module];
	}

	public int GetModuleId(int frame, int fmodule)
	{
		int offset = this.frames[frame];
		int moduleId = this.fmodules[offset + fmodule];
		return moduleId;
	}

	public int GetFrameId(int animation, int aframe)
	{
		int offset = this.animations[animation];
		int frameId = this.aframes[offset + aframe];
		return frameId;
	}

	public int GetModuleWidth(int module)
	{
		return this.modulesWidth[module];
	}

	public int GetModuleHeight(int module)
	{
		return this.modulesHeight[module];
	}

	public int GetModuleCount()
	{
		return this.modulesWidth.length;	// Or, modulesHeight.length
	}

	public int GetFrameWidth(int frame)
	{
		return this.framesWidth[frame];
	}

	public int GetFrameHeight(int frame)
	{
		return this.framesHeight[frame];
	}

	public int GetFModuleCount(int frame)
	{
		return this.framesLength[frame];
	}

	public int GetAFrameCount(int animation)
	{
		return this.animationsLength[animation];
	}

	public int GetAFrameTime(int animation, int aframe)
	{
		int offset = this.GetAFrameOffset(animation) + aframe;
		return this.aframesTime[offset];
	}

	public int GetAnimationCount()
	{
		return this.animations.length;
	}

	public void GetFrameRect(int[] rect, int frame, int x, int y, int flags)
	{
		int frameX = this.framesX[frame];
		int frameY = this.framesY[frame];
		int frameWidth = this.framesWidth[frame];
		int frameHeight = this.framesHeight[frame];

		if ((flags & SpriteFlag.FLIPX) != 0) {
			frameX = -frameX - frameWidth;
		}

		if ((flags & SpriteFlag.FLIPY) != 0) {
			frameY = -frameY - frameHeight;
		}

		if ((flags & SpriteFlag.ROTATE90) != 0) {
			int temp = frameX;
			frameX = -frameY - frameHeight;
			frameY = temp;

			temp = frameWidth;
			frameWidth = frameHeight;
			frameHeight = temp;
		}

		rect[0] = x + frameX;
		rect[1] = y + frameY;
		rect[2] = rect[0] + frameWidth;
		rect[3] = rect[1] + frameHeight;
	}

	public void GetFrameRect(int[] rect, int frame, int x, int y)
	{
		this.GetFrameRect(rect, frame, x, y, SpriteFlag.NONE);
	}

	public void GetAFrameRect(int[] rect, int animation, int aframe, int x, int y)
	{
		int frame = this.GetFrameId(animation, aframe);
		int flags = this.GetFModuleFlag(animation, aframe);
		this.GetFrameRect(rect, frame, x, y, flags);
	}

	public void DrawModule(Canvas c, int module, int x, int y, int flags, int rotation, int anchor)
	{
		if ((anchor & SpriteAnchor.HCENTER) != 0) {				// HCENTER
			x -= this.GetModuleWidth(module) >> 1;
		} else if ((anchor & SpriteAnchor.RIGHT) != 0) {		// RIGHT
			x -= this.GetModuleWidth(module);
		}

		if ((anchor & SpriteAnchor.VCENTER) != 0) {				// VCENTER
			y -= this.GetModuleHeight(module) >> 1;
		} else if ((anchor & SpriteAnchor.BOTTOM) != 0) {		// BOTTOM
			y -= this.GetModuleHeight(module);
		}
		
		Bitmap texture = this.textures[module];

		// TODO Support more flags by implement rotation
//		if (flags == SpriteFlag.FLIPX) {
//			effect = SpriteEffects.FlipHorizontally;
//		} else if (flags == SpriteFlag.FLIPY) {
//			effect = SpriteEffects.FlipVertically;
//		}

		if (rotation == 0) {
			c.drawBitmap(texture, x, y, null);
		} else {
			this.matrix.postTranslate(x,  y);
			this.matrix.postRotate(rotation);
			c.drawBitmap(texture, this.matrix, null);
		}
	}
	
	public void DrawModule(Canvas c, int module, int x, int y, int flags, int anchor)
	{
		this.DrawModule(c, module, x, y, flags, 0, anchor);
	}

	public void DrawModule(Canvas c, int module, int x, int y, int anchor)
	{
		this.DrawModule(c, module, x, y, SpriteFlag.NONE, 0, anchor);
	}

	public void DrawFrame(Canvas c, int frame, int x, int y, int flags, int anchor)
	{
		if ((anchor & SpriteAnchor.HCENTER) != 0) {				// HCENTER
			x -= this.GetFrameWidth(frame) >> 1;
		} else if ((anchor & SpriteAnchor.RIGHT) != 0) {		// RIGHT
			x -= this.GetFrameWidth(frame);
		}

		if ((anchor & SpriteAnchor.VCENTER) != 0) {				// VCENTER
			y -= this.GetFrameHeight(frame) >> 1;
		} else if ((anchor & SpriteAnchor.BOTTOM) != 0) {		// BOTTOM
			y -= this.GetFrameHeight(frame);
		}

		for (int i = 0; i < this.framesLength[frame]; i++) {
			int module = this.GetModuleId(frame, i);
			int ox = this.GetFModuleX(frame, i) + x;
			int oy = this.GetFModuleY(frame, i) + y;

			int fmoduleFlags = this.GetFModuleFlag(frame, i);
			fmoduleFlags ^= flags;

			this.DrawModule(c, module, ox, oy, fmoduleFlags, 0);
		}
	}

	public void DrawFrame(Canvas c, int frame, int x, int y, int anchor)
	{
		this.DrawFrame(c, frame, x, y, SpriteFlag.NONE, anchor);
	}

	public void DrawAFrame(Canvas c, int animation, int aframe, int x, int y, int flags, int anchor)
	{
		int ox = this.GetAFrameX(animation, aframe);
		int oy = this.GetAFrameY(animation, aframe);
		int frame = this.GetFrameId(animation, aframe);
		flags ^= this.GetAFrameFlag(animation, aframe);
		this.DrawFrame(c, frame, x + ox, y + oy, flags, anchor);
	}

	public void DrawAFrame(Canvas c, int animation, int aframe, int x, int y, int anchor)
	{
		this.DrawAFrame(c, animation, aframe, x, y, SpriteFlag.NONE, anchor);
	}
}
