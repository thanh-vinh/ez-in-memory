package ez.gamelib;

public class SoundPlayer
{
//	private static int filesCount;
//	private static Stream[] inputStreams;
//	private static string[] contentTypes;
//	private static SoundEffectInstance[] players;
//	private static SoundEffectInstance player;

	/**
	 * Load the resources from a sound package.
	 * This package should store sound file only.
	 */
	public static void LoadSoundPack(String packageFile)
	{
//		Package.Open(packageFile);
//		filesCount = Package.GetFileCount();
//
//		// Load bytes data
//		inputStreams = new Stream[filesCount];				// InputStream
//		contentTypes = new string[filesCount];				// File content types
//		players = new SoundEffectInstance[filesCount];		// If not cache, others players is null except players[0]
//
//		for (int i = 0; i < filesCount; i++) {
//			byte[] buf = Package.GetBytes(i);
//			inputStreams[i] = new MemoryStream(buf);
//			buf = null;
//
//			// Cache players
//			if (GameLibConfig.useCachedPlayers) {
//				try {
//					SoundEffect soundEffect = SoundEffect.FromStream(inputStreams[i]);
//					players[i] = soundEffect.CreateInstance();
//				} catch (Exception e) {
//					System.Diagnostics.Debug.WriteLine(e.StackTrace);
//				}
//			}
//		}
//
//		Package.Close();
	}

	public static boolean IsPlaying(int index)
	{
//		if (GameLibConfig.useCachedPlayers) {
//			player = players[index];
//		}
//
//		if (player != null) {
//			if (player.State == SoundState.Playing) {
//				return true;
//			}
//		}

		return false;
	}

	/**
	 * Must be call this method when exit application to free memory.
	 */
	public static void Unload()
	{
//		for (int i = 0; i < filesCount; i++) {
//			try {
//				players[i] = null;
//				inputStreams[i].Dispose();
//				inputStreams[i] = null;
//			} catch (Exception e) {
//				System.Diagnostics.Debug.WriteLine(e.StackTrace);
//			}
//		}
//		inputStreams = null;
	}

	public static void Play(int index, boolean loop)
	{
//		try {
//			if (GameLibConfig.useCachedPlayers) {
//				player = players[index];
//				// Loop is set before call Play method only (once time)
//				if (loop && !player.IsLooped) {
//					player.IsLooped = loop;
//				}
//				player.Play();
//			} else {
//				player = SoundEffect.FromStream(inputStreams[index]).CreateInstance();
//				player.IsLooped = loop;
//				player.Play();
//			}
//		} catch (Exception e) {
//			System.Diagnostics.Debug.WriteLine(e.StackTrace);
//		}
	}

	public static void Play(int index)
	{
		Play(index, false);
	}

	public static void Stop(int index)
	{
//		try {
//			if (GameLibConfig.useCachedPlayers) {
//				player = players[index];
//			}
//			if (player != null) {
//				player.Stop();
//			}
//		} catch (Exception e) {
//			System.Diagnostics.Debug.WriteLine(e.StackTrace);
//		}
	}

	public static void StopAllSound()
	{
//		try {
//			if (GameLibConfig.useCachedPlayers) {
//				for (int i = 0; i < filesCount; i++) {
//					player = players[i];
//					if (player != null) {
//						player.Stop();
//					}
//				}
//			} else if (player != null) {
//				player.Stop();
//			}
//		} catch (Exception e) {
//			System.Diagnostics.Debug.WriteLine(e.StackTrace);
//		}
	}
}
