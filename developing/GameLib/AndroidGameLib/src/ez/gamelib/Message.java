package ez.gamelib;

public class Message
{
	public static final int INITIALIZE				= 0;
	public static final int UPDATE					= 1;
	public static final int EXIT					= 2;
}
