package ez.gamelib;

import java.util.Random;

import android.app.Activity;
import android.graphics.Canvas;
import android.view.View;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.util.Log;

/**
 * Last updated: Feb 12, 2012
 */

public abstract class Game extends View implements Runnable
{
	public static final float MATH_PI = (float) Math.PI;

	protected static Activity activity;
	protected static Canvas canvas;
	protected static long frameCounter;				// Frame counter
	protected static long lastTime, currentTime;		// Times
	protected static boolean isGamePause;
	protected static boolean isInterrupt;
	
	protected static int NUMBER_KEY_MAP = 50;
	protected static boolean keyMap[];
	protected static boolean keyMapHold[];
	
	protected static int NUMBER_TOUCH_POINT = 5;
	protected static int pointer_X[];
	protected static int pointer_Y[];
	
//	private static Bitmap circularBuffer;			// For circular buffer
//	private static Canvas circularGraphic;			// For circular buffer

	private static int screenWidth, screenHeight;	// Screen width, height

	private static Random random;					// Random function
	private boolean isGamePlaying;					// Game is running or not?

	protected abstract void Update() throws Exception;
	protected abstract void Update(long gameTime) throws Exception;

	public Game()
	{
		super(null);
	}

	public Game(Activity _activity)
	{
		super(_activity);
		InitializeRandom();

		// Package, set Activity to open asset files
		Package.SetActivity(_activity);

		// Set screen width & screen height
		screenWidth = super.getWidth();
		screenHeight = super.getHeight();		
		initKeyMap();
		initTouchMap();
	}
	
	protected void initKeyMap()
	{
		keyMap = new boolean[NUMBER_KEY_MAP];
		keyMapHold = new boolean[NUMBER_KEY_MAP];
		
		for(int i = 0; i < NUMBER_KEY_MAP; i++)
		{
			keyMap[i] = false;
			keyMapHold[i] = false;
		}
	}
	
	protected void initTouchMap()
	{
		pointer_X = new int[NUMBER_TOUCH_POINT];
		pointer_Y = new int[NUMBER_TOUCH_POINT];
		
		for(int i = 0; i < NUMBER_TOUCH_POINT; i++)
		{
			pointer_X[i] = -1;
			pointer_Y[i] = -1;			
		}
	}

	protected void Initialize()
	{

	}

	@Override
	protected void onDraw(Canvas c)
	{
		if (canvas == null) {
			canvas = c;
		}

		try {
			Update();
			frameCounter++;
		} catch (Exception e) {
			Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			Log("[GameLib] ERROR IN paint() method");
			e.printStackTrace();
		}

//		if (GameLibConfig.useTransitionEffect) {				// Transition effect
//			paintFillTransition(graphics);
//		}
	}

	/**
 	 * Game loop in a thread.
	 */
	public void run()
	{
		this.isGamePlaying = true;

		while (this.isGamePlaying) {
			if (!isGamePause) {
				super.invalidate();
				if (GameLibConfig.useSleepAfterEachFrame) {
					try {
						Thread.sleep(GameLibConfig.sleepTime);
					} catch (Exception e) {}
				}
			}

//			updateKey();
			isInterrupt = false;
		}

		// Exit game
		System.gc();
		activity.finish();
	}

	public static int GetScreenWidth()
	{
		return screenWidth;
	}

	public static int GetScreenHeight()
	{
		return screenHeight;
	}

	public static Canvas GetGraphics()
	{
		return canvas;
	}

	public static long GetFrameCounter()
	{
		return frameCounter;
	}

	public static void Sleep(long time)
	{
		try {
			Thread.sleep(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// TODO Implements these functions
	protected void Pause()
	{
		isGamePause = true;
	}

	protected void Resume()
	{
		if (isGamePause) {
			isGamePause = false;
			isInterrupt = true;
			super.invalidate();
		}
	}

	protected void Exit()
	{
		this.isGamePlaying = false;
	}

	///----------------------------------------------------------------------
	// Input
	///----------------------------------------------------------------------
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d("onKey", "______________________________ onKeyDown");
		pressKeyMap(keyCode);
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		Log.d("onKey", "______________________________ onKeyUp");
		releaseKeyMap(keyCode);
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		Log.d("onKey", "______________________________ onKeyLongPress");
		return super.onKeyLongPress(keyCode, event);
	}
	
	public void pressKeyMap(int key)
	{
		switch(key)
		{
			case KeyEvent.KEYCODE_0: 
				if(keyMap[0])
					keyMapHold[0] = true;
				keyMap[0] = true;
			
			case KeyEvent.KEYCODE_1: 
				if(keyMap[1])
					keyMapHold[1] = true;
				keyMap[1] = true;
				
			case KeyEvent.KEYCODE_2: 
				if(keyMap[2])
					keyMapHold[2] = true;
				keyMap[2] = true;
			
			case KeyEvent.KEYCODE_3: 
				if(keyMap[3])
					keyMapHold[3] = true;
				keyMap[3] = true;
			
			//..... will be implements
		}
	}
	
	public void releaseKeyMap(int key)
	{
		switch(key)
		{
			case KeyEvent.KEYCODE_0: 
				keyMapHold[0] = false;
				keyMap[0] = false;
			
			case KeyEvent.KEYCODE_1: 
				keyMapHold[1] = false;
				keyMap[1] = false;
				
			case KeyEvent.KEYCODE_2: 
				keyMapHold[2] = false;
				keyMap[2] = false;
			
			case KeyEvent.KEYCODE_3: 
				keyMapHold[3] = false;
				keyMap[3] = false;
			
			//..... will be implements
		}
	}
	
	public static int changeKeyMap(int key)
	{
		switch(key)
		{
			case KeyEvent.KEYCODE_0: 
				return 0;
			
			case KeyEvent.KEYCODE_1: 
				return 1;
				
			case KeyEvent.KEYCODE_2: 
				return 2;
			
			case KeyEvent.KEYCODE_3: 
				return 3;
			default: 
				return -1;
			//..... will be implements
		}
	}
	
	public static boolean IsKeyPressed(int key)
	{
		int keyCodeChange = -1;
		keyCodeChange = changeKeyMap(key);
		if(keyCodeChange != -1)
			return keyMap[keyCodeChange];
		return false;			
	}

	public static boolean IsKeyHold(int key)
	{
		int keyCodeChange = -1;
		keyCodeChange = changeKeyMap(key);
		if(keyCodeChange != -1)
			return keyMapHold[keyCodeChange];
		return false;
	}

	//
	// Mouse/Touch function
	//
	@Override
	public boolean onTouchEvent(final MotionEvent event) {
    	final int action	= event.getAction();
    	final int mask		= (action&MotionEvent.ACTION_MASK);
		final int count		= event.getPointerCount();
		final int pointerId = (action&MotionEvent.ACTION_POINTER_INDEX_MASK ) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;

		int a;

		if (action == MotionEvent.ACTION_DOWN) {
			Log.d("onTouchEvent", "------ MotionEvent.ACTION_DOWN--------- x = " + (int)event.getX(0) + "  y = " + (int)event.getY(0));
			pointer_X[0] = (int)event.getX(0);
			pointer_Y[0] = (int)event.getY(0);
		}


		if (mask == MotionEvent.ACTION_POINTER_DOWN) {
			Log.d("onTouchEvent", "------ MotionEvent.ACTION_POINTER_DOWN---------" + pointerId);
			Log.d("onTouchEvent", "------ MotionEvent.ACTION_POINTER_DOWN--------- X = " + (int)event.getX(pointerId) + "  Y = " + (int)event.getY(pointerId));
			if(pointerId < NUMBER_TOUCH_POINT)
			{
				pointer_X[pointerId] = (int)event.getX(pointerId);
				pointer_Y[pointerId] = (int)event.getY(pointerId);
			}
		}

		if (action == MotionEvent.ACTION_MOVE) {
			try
			{
				a = 2;
				for (int i=0; i<count; i++) {
				final int id = event.getPointerId(i);

					Log.d("onTouchEvent", "------MotionEvent.ACTION_MOVE for loop id---------"+id);
					Log.d("onTouchEvent", "------X = " + (int)event.getX(id)+"    Y = " + (int)event.getY(id));
					if(id < NUMBER_TOUCH_POINT)
					{
						pointer_X[id] = (int)event.getX(id);
						pointer_Y[id] = (int)event.getY(id);
					}
				}
			}
			catch(Exception e)
			{
				Log.d("onTouchEvent", e.getStackTrace().toString());
			}
		}

		if (mask == MotionEvent.ACTION_POINTER_UP) {
			Log.d("onTouchEvent", "------MotionEvent.ACTION_POINTER_UP pointerId---------"+pointerId);
			Log.d("onTouchEvent", "------MotionEvent.ACTION_POINTER_UP X = " + (int)event.getX(pointerId) + " Y= " + (int)event.getY(pointerId));
			if(pointerId < NUMBER_TOUCH_POINT)
			{
				pointer_X[pointerId] = -1;
				pointer_Y[pointerId] = -1;
			}
		}

		if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
			Log.d("onTouchEvent", "------MotionEvent.ACTION_UP--------- X = " + (int)event.getX(0) + "  Y= " + (int)event.getY(0));			
			pointer_X[0] = -1;
			pointer_Y[0] = -1;
		}
		return true;
    }
	
	public static boolean IsPointInRect(int x, int y, int x1, int y1, int x2, int y2)
	{
		return (x > x1 && y > y1 && x < x2 && y < y2);
	}

	public static boolean IsTap(int x1, int y1, int x2, int y2)
	{
		for(int i = 0; i < NUMBER_TOUCH_POINT; i++)
		{
			if(pointer_X[i] != -1 && pointer_Y[i] != -1 
				&& pointer_X[i] > x1 && pointer_Y[i] > y1
				&& pointer_X[i] < x2 && pointer_Y[i] < y2)
				return true;
		}
		return false;
	}

	public static boolean IsHold(int x1, int y1, int x2, int y2)
	{
		return false;
	}

	public static int GetPointerX()
	{
	        return 0;
	}

	public static int GetPointerY()
	{
	        return 0;
	}

	///----------------------------------------------------------------------
	// Math
	///----------------------------------------------------------------------
	public static void InitializeRandom()
	{
		random = new Random(System.currentTimeMillis());
	}

	public static int GetRandomInt()
	{
		return random.nextInt();
	}

	public static int GetRandomInt(int min, int max)
	{
		int number = random.nextInt(max);
		while (number < min) {
			number = random.nextInt(max);
		}

		return number;
	}

	public static int GetRandomInt(int min, int max, int exception)
	{
		int number = GetRandomInt(min, max);
		while (number == exception) {
			number = random.nextInt(max);
		}

		return number;
	}

	public static double GetRandomDouble()
	{
		return random.nextDouble();
	}

	///----------------------------------------------------------------------
	// Debug
	///----------------------------------------------------------------------
	public static void Log(Object o)
	{
		if (GameLibConfig.debug) {
			System.out.println(o);
		}
	}
}
