
/*
 * Package
 * @date June 17, 2012
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 */

function Package()
{
	//
	// Constants
	//
	var COMPRESS_TYPE_NONE 	    = 0;
	var COMPRESS_TYPE_GZIP 	    = 1;

	//
	// Variables
	//
	var rootDirectory 	        = "";					// Root directory
	var packageFile 			= "";					// File name
	var useGzip 				= COMPRESS_TYPE_NONE;	// Compress
	var size 					= 0;					// Package size
	var fileCount 				= 0;					// Total files in this package
	var offsets 				= new Array();			// File offsets (int)
	var lengths 				= new Array();			// File length (int)
	var texts 					= new Array();			// Text cache (string)
		
	this.Open = function(input)
	{
		packageFile = input;
		fileReader = new FileReader();
	}
	
	this.Close = function()
	{
		
	}
	
	this.GetFileCount = function()
	{
		return fileCount;
	}
	
	this.GetGzipDecompressSize = function(buffer)
	{
		return 0;
	}
	
	this.ReadBytes = function()
	{
		
	}
	
	this.ReadByte = function()
	{
		
	}
	
	this.ReadShort = function()
	{
		
	}
	
	this.ReadInteger = function()
	{
		
	}
	
	this.GetBytes = function(index)
	{
		return new Array();
	}
	
	this.LoadText = function(index)
	{
		
	}
	
	this.GetText = function(index)
	{
		
	}
	
	this.LoadSprite = function(index)
	{
		
	}
	
	this.LoadFontSprite = function(index)
	{
		
	}
}
