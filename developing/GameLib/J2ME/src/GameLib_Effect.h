
	///*********************************************************************
	///* GameLib_Effect.h
	///*********************************************************************

	//
	// Fill rect effect
	//
	public static final int TRANSITION_NONE								= 0;
	public static final int TRANSITION_FILL_LEFT_TO_RIGHT				= 1;
	public static final int TRANSITION_FILL_RIGHT_TO_LEFT				= 1 << 1;
	public static final int TRANSITION_FILL_TOP_TO_BOTTOM				= 1 << 2;
	public static final int TRANSITION_FILL_BOTTOM_TO_TOP				= 1 << 3;
	public static final int TRANSITION_FILL_CENTER_TO_LEFT_RIGHT		= 1 << 4;
	public static final int TRANSITION_FILL_CENTER_TO_TOP_BOTTOM		= 1 << 5;

	private static int transitionType;
	private static int transitionColor;
	private static boolean isPlayingTransitionEffect;
	private static int transitionX, transitionY;

	protected static void setTransition(int type, int color)
	{
		transitionType = type;
		transitionColor = color;
		isPlayingTransitionEffect = true;

		if (transitionType != TRANSITION_NONE) {
			transitionX = 0;
			transitionY = 0;

			/* if ((transitionType & TRANSITION_FILL_LEFT_TO_RIGHT) != 0
				|| (transitionType & TRANSITION_FILL_TOP_TO_BOTTOM) != 0) {
				transitionX = 0;
				transitionY = 0;
			} */

			if ((transitionType & TRANSITION_FILL_RIGHT_TO_LEFT) != 0) {
				transitionX = screenWidth;
			}

			if ((transitionType & TRANSITION_FILL_BOTTOM_TO_TOP) != 0) {
				transitionY = screenHeight;
			}

			if ((transitionType & TRANSITION_FILL_CENTER_TO_LEFT_RIGHT) != 0) {
				transitionX = screenWidth >> 1;
			}

			if ((transitionType & TRANSITION_FILL_CENTER_TO_TOP_BOTTOM) != 0) {
				transitionY = screenHeight >> 1;
			}
		}
	}

	private void paintFillTransition(Graphics g)
	{
		if (isPlayingTransitionEffect) {
			if ((transitionType & TRANSITION_FILL_LEFT_TO_RIGHT) != 0) {
				fillRect(g, transitionColor, transitionX, 0, screenWidth - transitionX, screenHeight);
				transitionX += GameLibConfig.transitionSpeed;
			} else if ((transitionType & TRANSITION_FILL_RIGHT_TO_LEFT) != 0) {
				fillRect(g, transitionColor, 0, 0, transitionX, screenHeight);
				transitionX -= GameLibConfig.transitionSpeed;
			} else if ((transitionType & TRANSITION_FILL_TOP_TO_BOTTOM) != 0) {
				fillRect(g, transitionColor, 0, transitionY, screenWidth, screenHeight - transitionY);
				transitionY += GameLibConfig.transitionSpeed;
			} else if ((transitionType & TRANSITION_FILL_BOTTOM_TO_TOP) != 0) {
				fillRect(g, transitionColor, 0, 0, screenWidth, transitionY);
				transitionY -= GameLibConfig.transitionSpeed;
			}

			if ((transitionType & TRANSITION_FILL_CENTER_TO_LEFT_RIGHT) != 0) {
				fillRect(g, transitionColor, 0, 0, transitionX, screenHeight);
				fillRect(g, transitionColor, screenWidth - transitionX, 0, transitionX, screenHeight);
				transitionX -= GameLibConfig.transitionSpeed;
			}

			if ((transitionType & TRANSITION_FILL_CENTER_TO_TOP_BOTTOM) != 0) {
				fillRect(g, transitionColor, 0, 0, screenWidth, transitionY);
				fillRect(g, transitionColor, 0, screenHeight - transitionY, screenWidth, transitionY);
				transitionY -= GameLibConfig.transitionSpeed;
			}

			if (transitionX > screenWidth || transitionX < 0
				|| transitionY > screenHeight || transitionY < 0) {
				isPlayingTransitionEffect = false;
			}
		}
	}

	/* private void paintBufferTransition()
	{
		if (GameLibConfig.useTransitionEffect && this.isPlayingTransitionEffect) {	// Use transition effects
			if (!GameLibConfig.useDoubleBuffering) {			// Not use double buffering
				if (offScreenBuffer == null) {					// Create buffer
					offScreenBuffer = Image.createImage(screenWidth, screenHeight);
					bufferGraphic = offScreenBuffer.getGraphics();
					graphics = bufferGraphic;
				}
			}

			if (offScreenBuffer != null) {					// Use buffer to show transition effects
				if (this.transitionType == TRANSITION_NEXT) {
					if (this.transitionX > GameLibConfig.transitionSpeed) {
						this.transitionX -= GameLibConfig.transitionSpeed;
					} else {
						this.transitionX -= GameLibConfig.transitionSpeed >> 3;
					}
					if (this.transitionX < 0) {
						this.transitionX = 0;
						isDisableKey = false;
						this.isPlayingTransitionEffect = false;
					}
				} else if (this.transitionType == TRANSITION_BACK) {
					if (this.transitionX < -GameLibConfig.transitionSpeed) {
						this.transitionX += GameLibConfig.transitionSpeed;
					} else {
						this.transitionX += GameLibConfig.transitionSpeed >> 3;
					}
					if (this.transitionX > 0) {
						this.transitionX = 0;
						isDisableKey = false;
						this.isPlayingTransitionEffect = false;
					}
				}
			#ifdef USE_TWO_BUFFERS_TRANSITION
				if (lastScreenBuffer != null) {
					if (this.transitionType == TRANSITION_NEXT)
						g.drawImage(lastScreenBuffer, transitionX - screenWidth, 0, 0);
					else
						g.drawImage(lastScreenBuffer, transitionX + screenWidth, 0, 0);
				}
			#endif
				g.drawImage(offScreenBuffer, transitionX, 0, 0);
			}
		} else {												// Not use transition effects
			if (!GameLibConfig.useDoubleBuffering) {			// Not use double buffering
				if (offScreenBuffer != null) {					// Clear buffer when finish transition
					bufferGraphic = null;
					graphics = g;
					offScreenBuffer = null;
					lastScreenBuffer = null;
				}
			} else {											// Use double buffering
				g.drawImage(offScreenBuffer, 0, 0, 0);			// Draw buffer normal
			}
		}
	} */
