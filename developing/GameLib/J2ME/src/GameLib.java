
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.Vector;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDlet;

/**
 * Last updated: October 08, 2011
 */

#if !USE_BLACKBERRY
public abstract class GameLib extends Canvas implements Runnable
#else
public abstract class GameLib extends BlackBerryMIDP implements Runnable
#endif
{
	public static MIDlet midlet;
	public static Graphics graphics;
	public static long frameCounter;				// Frame counter
	public static long lastTime, currentTime;		// Times
	public static boolean isGamePause;
	public static boolean isInterrupt;

	private static Image offScreenBuffer;			// For double buffering
	private static Graphics bufferGraphic;			// For double buffering

	private static Image circularBuffer;			// For circular buffer
	private static Graphics circularGraphic;		// For circular buffer

	private static int screenWidth, screenHeight;	// Screen width, height

	private static Random random;					// Random function
	private boolean isGamePlaying;					// Game is running or not?

	public abstract void gameUpdate() throws Exception;

	public GameLib(MIDlet _midlet)
	{
		super();
		super.setFullScreenMode(true);
		midlet = _midlet;
		initializeKeyAdapter();
		initRandom();

		// Set screen width & screen height
		if (GameLibConfig.setScreenSize) {
			screenWidth = GameLibConfig.screenWidth;
			screenHeight = GameLibConfig.screenHeight;
		} else {
			screenWidth = super.getWidth();
			screenHeight = super.getHeight();
		}

		// Double buffering
		if (GameLibConfig.useDoubleBuffering) {
			offScreenBuffer = Image.createImage(screenWidth, screenHeight);
			bufferGraphic = offScreenBuffer.getGraphics();
		}
	}

	public static int getScreenWidth()
	{
		return screenWidth;
	}

	public static int getScreenHeight()
	{
		return screenHeight;
	}

	public static void initRandom()
	{
		random = new Random(System.currentTimeMillis());
	}

	public static int getRandomNumber(int n)
	{
		return random.nextInt(n);
	}

	public static int getRandomNumber(int max, int exception)
	{
		int number = random.nextInt(max);
		while (number == exception) {
			number = random.nextInt(max);
		}

		return number;
	}

	/**
	* A method for splitting a string in J2ME.
	* @param input 		The string to split.
	* @param separator 	The characters to use as delimiters.
	* @return 			An array of strings.
	*/
	public static String[] splitString(String input, String separator)
	{
		Vector nodes = new Vector();

		// Parse nodes into vector
		int index = input.indexOf(separator);
		while(index >= 0) {
			nodes.addElement(input.substring(0, index));
			input = input.substring(index + separator.length());
			index = input.indexOf(separator);
		}
		// Get the last node, ignore "" string
		if (!input.equals(""))
			nodes.addElement(input);

		// Convert the vector into an array
		String[] splitArray = new String[nodes.size()];
		for (int i = 0; i < splitArray.length; i++) {
			splitArray[i] = (String) nodes.elementAt(i);
		}

		return splitArray;
	}

	public static String replaceString(String original, String oldString, String newString)
	{
		String[] arrayString = splitString(original, oldString);
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < arrayString.length; i++) {
			if (!arrayString[i].equals("")) {
				buffer.append(arrayString[i]);
				if (i < arrayString.length - 1) {
					buffer.append(newString);
				}
			}
		}

		String result = buffer.toString();
		buffer = null;
		return result;
	}

	public static void sleep(long time)
	{
		try {
			Thread.sleep(time);
		} catch (Exception e) {
			TRACE(e);
		}
	}

	// TODO Implements these functions
	protected void pause()
	{
		isGamePause = true;
	}

	protected void resume()
	{
		if (isGamePause) {
			isGamePause = false;
			isInterrupt = true;
			super.repaint();
		}
	}

	protected void exit()
	{
		this.isGamePlaying = false;
	}

	/**
	 * Call this method while exit game.
	 */
	protected void freeAllObject()
	{
		graphics = null;
		bufferGraphic = null;
		offScreenBuffer = null;
	}

	/**
 	 * Game loop in a thread.
	 */
	// @Override
	public void run()
	{
		this.isGamePlaying = true;

		while (this.isGamePlaying) {
			if (!isGamePause) {
				super.repaint();
				if (GameLibConfig.useServiceRepaints) {
					super.serviceRepaints();
				}
				if (GameLibConfig.useSleepAfterEachFrame) {
					try {
						Thread.sleep(GameLibConfig.sleepTime);
					} catch (Exception e) {}
				}
			}

			updateKey();
			isInterrupt = false;
		}

		// Exit game
		this.freeAllObject();
		System.gc();
		midlet.notifyDestroyed();
	}

	// @Override
	public void paint(Graphics g)
	{
		if (graphics == null) {
			if (!GameLibConfig.useDoubleBuffering) {			// Not use double buffering
				graphics = g;
			} else {											// Use double buffering
				graphics = bufferGraphic;
			}
		}

		try {
			gameUpdate();
			frameCounter++;
		} catch (Exception e) {
			DBG("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			DBG("[GameLib] ERROR IN paint() method");
			TRACE(e);
		}

		if (GameLibConfig.useDoubleBuffering) {					// Use double buffering
			if (offScreenBuffer != null) {
				g.drawImage(offScreenBuffer, 0, 0, 0);			// Draw buffer on screen
			}
		}

		if (GameLibConfig.useTransitionEffect) {				// Transition effect
			paintFillTransition(graphics);
		}
	}

	// @Override
	protected void hideNotify ()
	{
		pause();
	}

	// @Override
	protected void showNotify ()
	{
		resume();
	}

	#include "GameLib_Key.h"
	#include "GameLib_Touch.h"
	#include "GameLib_Draw.h"
	#include "GameLib_Math.h"
	#include "GameLib_Effect.h"
	#include "GameLib_Tile.h"
}
