
import java.io.IOException;
import java.io.ByteArrayInputStream;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;

public class SoundPlayer
{
	// Properties for watching
	private static int filesCount;
	private static int currentSoundIndex;

	private boolean enableSound;
	private ByteArrayInputStream[] inputStreams;
	private String[] contentTypes;
	private Player[] players;

	private String getContentType(byte[] data)
	{
		byte[] header = new byte[4];	// Read 4 bytes header

		for (int i = 0; i < 4; i++) {
			header[i] = data[i];
		}
		// Parse header string to content type
		String headerString = new String(header);
		if (headerString.equals("MThd"))
			return "audio/mid";
		else if (headerString.equals("RIFF"))
			return "audio/x-wav";
		else if (headerString.equals("#!AM"))
			return "audio/amr";
		else	// MP3
			return "audio/mpeg";
	}

	/**
	 * Load the resources from a sound package.
	 * This package should store sound file only.
	 */
	public void loadSoundPack(String packageFile)
	{
		DBG("[SoundPlayer] Loading sound pack...");
		Package.open(packageFile);
		filesCount = Package.getFileCount();

		// Load bytes data
		this.inputStreams = new ByteArrayInputStream[filesCount];	// InputStream
		this.contentTypes = new String[filesCount];					// File content types
		this.players = new Player[filesCount];						// If not cache, others players is null except players[0]

		for (int i = 0; i < filesCount; i++) {
			byte[] data = Package.getBytes(i);
			this.inputStreams[i] = new ByteArrayInputStream(data);
			this.contentTypes[i] = this.getContentType(data);
			data = null;

			// Cache players
			if (GameLibConfig.useCachedPlayers) {
				try {
					this.players[i] = Manager.createPlayer(this.inputStreams[i], this.contentTypes[i]);
					if (GameLibConfig.usePrefetchedPlayers) {
						this.players[i].realize();
						this.players[i].prefetch();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		Package.close();
	}

	public void setEnableSound(boolean value)
	{
		this.enableSound = value;
	}

	public boolean getEnableSound() {

		return this.enableSound;
	}

	public int getCurrentSound()
	{
		if (this.enableSound) {
			int index = GameLibConfig.useCachedPlayers ? currentSoundIndex : 0;
			if (this.players[index] != null) {
				if (this.players[index].getState() == Player.STARTED) {
					return currentSoundIndex;
				}
			}
		}

		return -1;
	}

	/**
	 * Must be call this method when exit application to free memory.
	 */
	public void unload()
	{
		for (int i = 0; i < filesCount; i++) {
			try {
				this.players[i] = null;
				this.inputStreams[i].close();
				this.inputStreams[i] = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.inputStreams = null;
	}

	public void play(int index, boolean loop)
	{
		if (this.enableSound) {
			if (this.getCurrentSound() != index) {

				currentSoundIndex = index;		// Remember file is playing

				DBG("[SoundPlayer] Playing sound, file index is: " + index);

				try {
					if (GameLibConfig.useCachedPlayers) {
						if (loop) {
							this.players[index].setLoopCount(-1);
						}
						this.players[index].start();
					} else {
						this.players[0] = Manager.createPlayer(this.inputStreams[index], this.contentTypes[index]);
						if (loop) {
							this.players[0].setLoopCount(-1);
						}
						this.players[0].realize();
						this.players[0].prefetch();
						this.players[0].start();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void play(int index)
	{
		this.play(index, false);
	}

	public void stop()
	{
		if (this.enableSound) {
			DBG("[SoundPlayer] Stop sound, file index is: " + currentSoundIndex);
			try {
				if (GameLibConfig.useCachedPlayers && this.players[currentSoundIndex] != null) {
					this.players[currentSoundIndex].stop();

					if (!GameLibConfig.usePrefetchedPlayers) {
						this.players[currentSoundIndex].deallocate();
					}
				} else if (this.players[0] != null) {
					this.players[0].stop();
					this.players[0].deallocate();
				}
			} catch (MediaException e) {
				e.printStackTrace();
			}
		}
	}

	public void stopAllSound()
	{
		if (this.enableSound) {
			DBG("[SoundPlayer] Stop all sound...");
			try {
				if (GameLibConfig.useCachedPlayers) {
					for (int i = 0; i < filesCount; i++) {
						this.players[i].stop();
						if (!GameLibConfig.usePrefetchedPlayers) {
							this.players[currentSoundIndex].deallocate();
						}
					}
				} else {
					this.players[0].stop();
					this.players[0].deallocate();
				}
			} catch (MediaException e) {
				e.printStackTrace();
			}
		}
	}
}
