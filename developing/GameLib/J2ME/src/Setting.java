
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordEnumeration;

/*
 * Load/Save settings from Record store.
 */
public class Setting
{
	private String recordName;
	private int size;
	private RecordStore recordStore;
	private byte[] buf;

	public Setting(String name, int size)
	{
		DBG("[Settings] Initialize...");
		this.recordName = name;
		this.size = size;

		try {										// Try to open
			this.recordStore = RecordStore.openRecordStore(this.recordName, false);
			this.buf = recordStore.getRecord(1);	// Read the record
			recordStore.closeRecordStore();
		} catch (Exception e) {						// If error, initialize record
			DBG("[Settings] Create new record...");
			this.save();
			e.printStackTrace();
		}
	}

	public void save()
	{
		DBG("[Settings] Save...");
		try {
			// Open record store/ create new if not exists
			this.recordStore = RecordStore.openRecordStore(this.recordName, true);
			RecordEnumeration records = recordStore.enumerateRecords(null, null, false);

    		if (records.numRecords() == 0) {							// If the record store is emty
				this.buf = new byte[this.size];							// Default values
    			this.recordStore.addRecord(this.buf, 0, this.buf.length);	// Add new record
    		} else {													// Else, update the record
    			int recordId = records.nextRecordId();
    			this.recordStore.setRecord(recordId, this.buf, 0, this.buf.length);
    		}

			this.recordStore.closeRecordStore();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public byte readByte(int pos)
	{
		return this.buf[pos];
	}

	public void writeByte(int pos, byte value)
	{
		this.buf[pos] = value;
	}

	public short readShort(int pos)
	{		
		short returnValue;
		
		short value = (short)this.buf[pos];
		returnValue = (short)(value & 0xff);
			 
		value = (short)(this.buf[pos + 1]); 
		returnValue = (short)((returnValue<<8) | (value & 0x00ff));
		
		return returnValue;
	}

	public void writeShort(int pos, short value)
	{
		this.buf[pos] = (byte)((value & 0xff00) >> 8);
		this.buf[pos + 1] = (byte)(value & 0x00ff);
	}
}
