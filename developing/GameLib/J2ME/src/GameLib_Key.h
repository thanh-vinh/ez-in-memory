
	///*********************************************************************
	///* GameLib_Key.h
	///*********************************************************************

	//
	// AdapterKey ----------------------------------------------------------
	//
	//
	// Number keys
	//
	public static final int DK_UNKNOW 						= 0;
	public static final int DK_NUM0							= 1;
	public static final int DK_NUM1 						= 1 << 1;
	public static final int DK_NUM2 						= 1 << 2;
	public static final int DK_NUM3 						= 1 << 3;
	public static final int DK_NUM4 						= 1 << 4;
	public static final int DK_NUM5 						= 1 << 5;
	public static final int DK_NUM6 						= 1 << 6;
	public static final int DK_NUM7						= 1 << 7;
	public static final int DK_NUM8 						= 1 << 8;
	public static final int DK_NUM9 						= 1 << 9;
	public static final int DK_STAR 						= 1 << 10;
	public static final int DK_POUND 						= 1 << 11;

	//
	// D-Pad
	//
	public static final int DK_PAD_UP 						= 1 << 12;
	public static final int DK_PAD_DOWN 					= 1 << 13;
	public static final int DK_PAD_LEFT 					= 1 << 14;
	public static final int DK_PAD_RIGHT					= 1 << 15;
	public static final int DK_PAD_MIDDLE 					= 1 << 16;

	//
	// Soft keys
	//
	public static final int DK_SOFT_LEFT 					= 1 << 17;
	public static final int DK_SOFT_RIGHT 					= 1 << 18;

	//
	// Volume keys
	//
	public static final int DK_VOLUME_UP 					= 1 << 19;
	public static final int DK_VOLUME_DOWN					= 1 << 20;

	//
	// Nokia S60
	//
	public static final int DK_PENCIL	 					= 1 << 21;
	public static final int DK_DELETE						= 1 << 22;
	public static final int DK_BACK 						= 1 << 23;

	//
	// Platform names
	//
	private static final String PLATFORM_MOTOROLA 			= "motorola";
	private static final String PLATFORM_NOKIA 				= "nokia";
	private static final String PLATFORM_SONY_ERICSSON 		= "SE";
	private static final String PLATFORM_SIEMENS 			= "siemens";
	private static final String PLATFORM_SAMSUNG 			= "samsung";
	private static final String PLATFORM_LG 				= "LG";
	private static final String PLATFORM_UNKNOW 			= "NA";

	//
	// Standard key values for differen platforms
	//
	private static final int SOFT_KEY_LEFT_SE 				= -6;
	private static final int SOFT_KEY_RIGHT_SE 				= -7;
	private static final int DELETE_KEY_SE 					= -8;
	private static final int INTERNET_KEY_SE 				= -10;
	private static final int BACK_KEY_SE 					= -11;
	private static final int SOFT_KEY_LEFT_SAMSUNG 			= -6;
	private static final int SOFT_KEY_RIGHT_SAMSUNG 		= -7;
	private static final int DELETE_KEY_SAMSUNG 			= -8;
	private static final int SOFT_KEY_LEFT_SIEMENS 			= -1;
	private static final int SOFT_KEY_RIGHT_SIEMENS 		= -4;
	private static final int SOFT_KEY_LEFT_NOKIA 			= -6;
	private static final int SOFT_KEY_RIGHT_NOKIA 			= -7;
	private static final int DELETE_KEY_NOKIA 				= -8;
	private static final int PENCIL_KEY_NOKIA 				= -50;
	private static final int SOFT_KEY_LEFT_MOTOROLA 		= -21;
	private static final int SOFT_KEY_RIGHT_MOTOROLA 		= -22;
	private static final int SOFT_KEY_LEFT_MOTOROLA2 		= -20;
	private static final int SOFT_KEY_LEFT_MOTOROLA1 		= 21;
	private static final int SOFT_KEY_RIGHT_MOTOROLA1		= 22;
	private static final int DPAD_KEY_MIDLE_MOTOROLA 		= -23;
	private static final int DPAD_KEY_MIDLE_NOKIA 			= -5;

	private static final String SOFT_WORD 					= "SOFT";

	//
	// Detect platform & key values
	//
	private String platformName;
	private int leftSoftKeyCode;
	private int rightSoftKeyCode;
	private int middleKeyCode;
	private int deleteKeyCode;
	private int backKeyCode;


	/**
	 * Initalize Key Adapter, must call before use.
	 */
	private void initializeKeyAdapter()
	{
		platformName = getPlatform();
		leftSoftKeyCode = getLeftSoftkeyCode();
		rightSoftKeyCode = getRightSoftkeyCode();
		middleKeyCode = getMidleKeyCode();
		deleteKeyCode = getDeleteKeyCode();
		backKeyCode = getBackKeyCode();

		DBG("[GameLib] Initalize Key adapter...");
		DBG("> platformName: " + platformName);
		DBG("> leftSoftKeyCode: " + leftSoftKeyCode);
		DBG("> rightSoftKeyCode: " + rightSoftKeyCode);
		DBG("> middleKeyCode: " + middleKeyCode);
	}

	/**
	* Get mobile phone platform.
	*/
	private String getPlatform()
	{
		// Detecting NOKIA or SonyEricsson
		try {
			final String currentPlatform = System.getProperty("microedition.platform");
			if (currentPlatform.indexOf("Nokia") != -1) {
				return PLATFORM_NOKIA;
			} else if (currentPlatform.indexOf("SonyEricsson") != -1) {
				return PLATFORM_SONY_ERICSSON;
			}
		} catch (Throwable ex) {
		}

		 // Detecting SAMSUNG
		 try {
			 Class.forName("com.samsung.util.Vibration");
			 return PLATFORM_SAMSUNG;
		 } catch (Throwable ex) {
		 }

		// Detecting MOTOROLA
		try {
			Class.forName("com.motorola.multimedia.Vibrator");
			return PLATFORM_MOTOROLA;
		} catch (Throwable ex) {
			try {
				Class.forName("com.motorola.graphics.j3d.Effect3D");
				return PLATFORM_MOTOROLA;
			} catch (Throwable ex2) {
				try {
					Class.forName("com.motorola.multimedia.Lighting");
					return PLATFORM_MOTOROLA;
				} catch (Throwable ex3) {
					try {
						Class.forName("com.motorola.multimedia.FunLight");
						return PLATFORM_MOTOROLA;
					} catch (Throwable ex4) {
					}
				}
			}
		}

		// Detecting SIEMENS
		try {
			Class.forName("com.siemens.mp.io.File");
			return PLATFORM_SIEMENS;
		} catch (Throwable ex) {
		}

		// Detecting LG
		try {
			Class.forName("mmpp.media.MediaPlayer");
			return PLATFORM_LG;
		} catch (Throwable ex) {
			try {
				Class.forName("mmpp.phone.Phone");
				return PLATFORM_LG;
			} catch (Throwable ex1) {
				try {
					Class.forName("mmpp.lang.MathFP");
					return PLATFORM_LG;
				} catch (Throwable ex2) {
					try {
						Class.forName("mmpp.media.BackLight");
						return PLATFORM_LG;
					} catch (Throwable ex3) {
					}
				}
			}
		}

		return PLATFORM_UNKNOW;
	}

	/**
	 * Get real left soft key code by platform.
	 */
	private int getLeftSoftkeyCode()
	{
		int keyCode = SOFT_KEY_LEFT_NOKIA;		// Default -6

		try {
			if (platformName.equals(PLATFORM_MOTOROLA)) {
				String softkeyLeftMoto = "";

				try {
					softkeyLeftMoto = super.getKeyName(SOFT_KEY_LEFT_MOTOROLA).toUpperCase();
				} catch (IllegalArgumentException ilae) {
				}

				String softkeyLeftMoto1 = "";
				try {
					softkeyLeftMoto1 = super.getKeyName(SOFT_KEY_LEFT_MOTOROLA1).toUpperCase();
				} catch (IllegalArgumentException ilae) {
				}

				String softkeyLeftMoto2 = "";
				try {
					softkeyLeftMoto2 = super.getKeyName(SOFT_KEY_LEFT_MOTOROLA2).toUpperCase();
				} catch (IllegalArgumentException ilae) {
				}

				if (softkeyLeftMoto.indexOf(SOFT_WORD) >= 0 && softkeyLeftMoto.indexOf("1") >= 0) {
					return SOFT_KEY_LEFT_MOTOROLA;
				} else if (softkeyLeftMoto1.indexOf(SOFT_WORD) >= 0 && softkeyLeftMoto1.indexOf("1") >= 0) {
					return SOFT_KEY_LEFT_MOTOROLA1;
				} else if (softkeyLeftMoto2.indexOf(SOFT_WORD) >= 0 && softkeyLeftMoto2.indexOf("1") >= 0) {
					return SOFT_KEY_LEFT_MOTOROLA2;
				} else if (softkeyLeftMoto.indexOf(SOFT_WORD) >= 0 && softkeyLeftMoto.indexOf("LEFT") >= 0) {
					return SOFT_KEY_LEFT_MOTOROLA;
				} else if (softkeyLeftMoto1.indexOf(SOFT_WORD) >= 0 && softkeyLeftMoto1.indexOf("LEFT") >= 0) {
					return SOFT_KEY_LEFT_MOTOROLA1;
				} else if (softkeyLeftMoto2.indexOf(SOFT_WORD) >= 0 && softkeyLeftMoto2.indexOf("LEFT") >= 0) {
					return SOFT_KEY_LEFT_MOTOROLA2;
				}
			} else if (platformName.equals(PLATFORM_NOKIA)) {
				return SOFT_KEY_LEFT_NOKIA;
			} else if (platformName.equals(PLATFORM_SAMSUNG)) {
				return SOFT_KEY_LEFT_SAMSUNG;
			} else if (platformName.equals(PLATFORM_SIEMENS)) {
				String leftKeySiemensName = super.getKeyName(SOFT_KEY_LEFT_SIEMENS).toUpperCase();
				if (leftKeySiemensName.indexOf(SOFT_WORD) >= 0) {
					if (leftKeySiemensName.indexOf("1") >= 0) {
						return SOFT_KEY_LEFT_SIEMENS;
					} else if (leftKeySiemensName.indexOf("LEFT") >= 0) {
						return SOFT_KEY_LEFT_SIEMENS;
					}
				}
			} else if (platformName.equals(PLATFORM_SONY_ERICSSON)) {
				return SOFT_KEY_LEFT_SE;
			} else if (platformName.equals(PLATFORM_UNKNOW)) {
				//
				for (int i = -125; i <= 125; i++) {
					if (i == 0) {
						i++;
					}

					final String s = super.getKeyName(i).toUpperCase();
					if (s.indexOf(SOFT_WORD) >= 0) {
						if (s.indexOf("1") >= 0) {
							keyCode = i;
							break;
						}
						if (s.indexOf("LEFT") >= 0) {
							keyCode = i;
							break;
						}
					}
				}
			}

			if (keyCode == 0) {
				return SOFT_KEY_LEFT_NOKIA;
			}
		} catch (Throwable iaEx) {
			return SOFT_KEY_LEFT_NOKIA;
		}

		return keyCode;
	}

	/**
	 * Get real right soft key code for current platform.
	 */
	private int getRightSoftkeyCode()
	{
		int keyCode = SOFT_KEY_RIGHT_NOKIA;			// Default: -7

		try {
			if (platformName.equals(PLATFORM_MOTOROLA)) {

				String rightSoftMoto1 = "";
				try {
					rightSoftMoto1 = super.getKeyName(SOFT_KEY_LEFT_MOTOROLA1).toUpperCase();
				} catch (IllegalArgumentException ilae) {
				}

				String rightSoftMoto = "";
				try {
					rightSoftMoto = super.getKeyName(SOFT_KEY_RIGHT_MOTOROLA).toUpperCase();
				} catch (IllegalArgumentException ilae) {
				}

				String rightSoftMoto2 = "";
				try {
					rightSoftMoto2 = super.getKeyName(SOFT_KEY_RIGHT_MOTOROLA1).toUpperCase();
				} catch (IllegalArgumentException ilae) {
				}

				if (rightSoftMoto.indexOf(SOFT_WORD) >= 0 && rightSoftMoto.indexOf("2") >= 0) {
					return SOFT_KEY_RIGHT_MOTOROLA;
				} else if (rightSoftMoto1.indexOf(SOFT_WORD) >= 0 && rightSoftMoto1.indexOf("2") >= 0) {
					return SOFT_KEY_RIGHT_MOTOROLA;
				} else if (rightSoftMoto2.indexOf(SOFT_WORD) >= 0 && rightSoftMoto2.indexOf("2") >= 0) {
					return SOFT_KEY_RIGHT_MOTOROLA1;
				} else if (rightSoftMoto.indexOf(SOFT_WORD) >= 0 && rightSoftMoto.indexOf("RIGHT") >= 0) {
					return SOFT_KEY_LEFT_MOTOROLA;
				} else if (rightSoftMoto1.indexOf(SOFT_WORD) >= 0 && rightSoftMoto1.indexOf("RIGHT") >= 0) {
					return SOFT_KEY_RIGHT_MOTOROLA1;
				} else if (rightSoftMoto2.indexOf(SOFT_WORD) >= 0 && rightSoftMoto2.indexOf("RIGHT") >= 0) {
					return SOFT_KEY_RIGHT_MOTOROLA;
				}

			} else if (platformName.equals(PLATFORM_NOKIA)) {
				return SOFT_KEY_RIGHT_NOKIA;
			} else if (platformName.equals(PLATFORM_SAMSUNG)) {
					return SOFT_KEY_RIGHT_SAMSUNG;
			} else if (platformName.equals(PLATFORM_SIEMENS)) {

				String rightSoftSiemens = super.getKeyName(SOFT_KEY_RIGHT_SIEMENS).toUpperCase();
				if (rightSoftSiemens.indexOf(SOFT_WORD) >= 0) {
					if (rightSoftSiemens.indexOf("4") >= 0) {
						return SOFT_KEY_RIGHT_SIEMENS;
					} else if (rightSoftSiemens.indexOf("RIGHT") >= 0) {
						return SOFT_KEY_RIGHT_SIEMENS;
					}
				}
			} else if (platformName.equals(PLATFORM_SONY_ERICSSON)) {
				return SOFT_KEY_RIGHT_SE;
			} else if (platformName.equals(PLATFORM_UNKNOW)) {
				for (int i = -125; i <= 125; i++) {
					if (i == 0) {
						i++;
					}

					String keyName = super.getKeyName(i).toUpperCase();
					if (keyName.indexOf(SOFT_WORD) >= 0) {
						if (keyName.indexOf("2") >= 0) {
							keyCode = i;
							break;
						} else if (keyName.indexOf("4") >= 0) {
							keyCode = i;
							break;
						} else if (keyName.indexOf("RIGHT") >= 0) {
							keyCode = i;
							break;
						}
					}
				}
			}
		} catch (Throwable iaEx) {
			return SOFT_KEY_RIGHT_NOKIA;
		}

		return keyCode;
	}

	/**
	 * Get real middle soft key code for current platform.
	 */
	private int getMidleKeyCode()
	{
		try {
			if (platformName.equals(PLATFORM_MOTOROLA)) {
				if (super.getKeyName(DPAD_KEY_MIDLE_MOTOROLA).toUpperCase().indexOf("SOFT") >= 0) {
					return DPAD_KEY_MIDLE_MOTOROLA;
				}
			} else if (platformName.equals(PLATFORM_NOKIA)) {
				if (super.getKeyName(DPAD_KEY_MIDLE_NOKIA).toUpperCase().indexOf("SOFT") >= 0) {
					return DPAD_KEY_MIDLE_NOKIA;
				}
			} else if (platformName.equals(PLATFORM_SAMSUNG)) {
			} else if (platformName.equals(PLATFORM_SIEMENS)) {
			} else if (platformName.equals(PLATFORM_SONY_ERICSSON)) {
				return INTERNET_KEY_SE;
			}
		} catch (Throwable e) {
		}

		return DPAD_KEY_MIDLE_NOKIA;			// Default: -5
	}

	/**
	 * Get real key's C or DELETE code for current platform.
	 */
	private int getDeleteKeyCode()
	{
		try {
			if (platformName.equals(PLATFORM_MOTOROLA)) {
			} else if (platformName.equals(PLATFORM_NOKIA)) {
				if (super.getKeyName(DELETE_KEY_SE).toUpperCase().indexOf("CLEAR") >= 0) {
					return DELETE_KEY_NOKIA;
				} else {
					return DELETE_KEY_NOKIA;
				}
			} else if (platformName.equals(PLATFORM_SAMSUNG)) {
				if (super.getKeyName(DELETE_KEY_SAMSUNG).toUpperCase().indexOf("CLEAR") >= 0) {
					return DELETE_KEY_SAMSUNG;
				}
			} else if (platformName.equals(PLATFORM_SIEMENS)) {
			} else if (platformName.equals(PLATFORM_SONY_ERICSSON)) {
				if (super.getKeyName(DELETE_KEY_SE).toUpperCase().indexOf("CLEAR") >= 0) {
					return DELETE_KEY_SE;
				} else if (super.getKeyName(DELETE_KEY_SE).toUpperCase().indexOf("C") >= 0) {
					return DELETE_KEY_SE;
				} else {
					return DELETE_KEY_SE;
				}
			}
		} catch (Throwable e) {
			return DELETE_KEY_SE;
		}

		return DK_UNKNOW;
	}

	/**
	 * Get real key's BACK code for current platform.
	 */
	private int getBackKeyCode()
	{
		try {
			if (platformName.equals(PLATFORM_MOTOROLA)) {
			} else if (platformName.equals(PLATFORM_NOKIA)) {
			} else if (platformName.equals(PLATFORM_SAMSUNG)) {
			} else if (platformName.equals(PLATFORM_SIEMENS)) {
			} else if (platformName.equals(PLATFORM_SONY_ERICSSON)) {
				return BACK_KEY_SE;
			}
		} catch (Throwable e) {
		}

		return DK_UNKNOW;
	}

	/**
	 * Used to adapter key code to predefined constances, which are platform independent.
	 * <p/>
	 * You can use this method in any kind of canvas, but better at first time to call
	 * <code>initializeKeyAdapter()</code> method at the beginning of midlet work, because initialisation takes time.
	 * <p/>
	 * @param keycode This code is sent by platform to canvas and redirected here
	 * @return this keycode is equal to one of our constants declared in this class
	*/
	public int getAdapterKeyCode(int keycode)
	{
		switch (keycode) {
			case Canvas.KEY_NUM0:
				return DK_NUM0;
			case Canvas.KEY_NUM1:
				return DK_NUM1;
			case Canvas.KEY_NUM2:
				return DK_NUM2;
			case Canvas.KEY_NUM3:
				return DK_NUM3;
			case Canvas.KEY_NUM4:
				return DK_NUM4;
			case Canvas.KEY_NUM5:
				return DK_NUM5;
			case Canvas.KEY_NUM6:
				return DK_NUM6;
			case Canvas.KEY_NUM7:
				return DK_NUM7;
			case Canvas.KEY_NUM8:
				return DK_NUM8;
			case Canvas.KEY_NUM9:
				return DK_NUM9;
			case Canvas.KEY_STAR:
				return DK_STAR;
			case Canvas.KEY_POUND:
				return DK_POUND;
			default:
				if (keycode == leftSoftKeyCode) {
					return DK_SOFT_LEFT;
				} else if (keycode == rightSoftKeyCode) {
					return DK_SOFT_RIGHT;
				} else if (keycode == deleteKeyCode) {
					return DK_DELETE;
				} else if (keycode == backKeyCode) {
					return DK_BACK;
				} else if (keycode == middleKeyCode) {
					return DK_PAD_MIDDLE;
				} else if (keycode == PENCIL_KEY_NOKIA) {
					return DK_PENCIL;
				} else {
					try {
						final int gameAction = super.getGameAction(keycode);
						if (gameAction == Canvas.UP) {
							return DK_PAD_UP;
						} else if (gameAction == Canvas.DOWN) {
							return DK_PAD_DOWN;
						} else if (gameAction == Canvas.LEFT) {
							return DK_PAD_LEFT;
						} else if (gameAction == Canvas.RIGHT) {
							return DK_PAD_RIGHT;
						} else if (gameAction == Canvas.FIRE) {
							return DK_PAD_MIDDLE;
						}
					} catch (IllegalArgumentException e) {
					}
				}
				break;
		}

		return DK_UNKNOW;
	}

	//
	// AdapterKey ---------------------------------------------------------- end
	//

	private static int keyPressed;
	private static int keyPressedRT;
	private static int keyCurrent;
	private static int keyRepeated;
	private static int keyReleased;
	private static int keyReleasedRT;

	public static boolean isAnyKeyPressed()
	{
		return keyPressed != 0;
	}

	public static boolean isAnyKeyRepeated(int key)
	{
		return (keyRepeated & 0x1FFFF) != 0;
	}

	public static boolean isKeyPressed(int key)
	{
		 return (keyPressed & key) != 0;
	}

	public static boolean isKeyReleased(int key)
	{
		return (keyReleased & key) != 0;
	}

	public static boolean isKeyRepeated(int key)
	{
		return (keyRepeated & key) != 0;
	}

	public static void clearKey()
	{
		keyCurrent 			= 0;
		keyRepeated 		= 0;
		keyPressed 			= 0;
		keyReleased 		= 0;
		keyPressedRT 		= 0;
		keyReleasedRT 		= 0;
	}

	public static void updateKey()
    {
		keyRepeated			= keyCurrent;
		keyPressed			= keyPressedRT;
		keyReleased 		= keyReleasedRT;
		keyPressedRT 		= 0;
		keyReleasedRT 		= 0;

    }

	// @Override
	protected void keyPressed(int keyCode)
	{
		keyPressedRT 		|= getAdapterKeyCode(keyCode);
		keyRepeated 		|= keyPressedRT;
		keyCurrent 			|= keyPressedRT;

		DBG("keyPressed (bit mask): " + keyPressedRT);
	}

	// @Override
	protected void keyReleased(int keyCode)
	{
		keyReleasedRT 		|= getAdapterKeyCode(keyCode);
		keyRepeated 		= 0;
		keyPressedRT 		= 0;
		keyCurrent  		&= ~keyReleasedRT;
	}
