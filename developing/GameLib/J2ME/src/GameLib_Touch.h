
	///*********************************************************************
	///* GameLib_Touch.h
	///*********************************************************************

	private static int pointerX, pointerY;
	private static boolean isPointerDragged, isPointerPressed, isPointerReleased;

	public static boolean isPointerInRect(int x1, int y1, int x2, int y2)
	{
		if (pointerX > x1 && pointerY > y1 && pointerX < x2 && pointerY < y2) {
			DBG("[POINTER] Pointer in (" + x1 + ", " + y1 + ", " + x2 + ", " + y2 + ")");
			return true;
		} else {
			return false;
		}
	}

	public static boolean wasPointerPressedIn(int x1, int y1, int x2, int y2)
	{
		if (isPointerReleased && isPointerInRect(x1, y1, x2, y2)) {
			isPointerReleased = false;
			return true;
		} else {
			return false;
		}
	}

	public static boolean wasPointerPressedInRect(int x, int y, int width, int height)
	{
		return wasPointerPressedIn(x, y, x + width, y + height);
	}

	// @Override
	protected void pointerDragged(int x, int y)
	{
		pointerX = x;
		pointerY = y;
		isPointerDragged = true;
		isPointerPressed = true;
	}

	// @Override
	protected void pointerPressed(int x, int y)
	{
		pointerX = x;
		pointerY = y;
		isPointerPressed = true;
	}

	// @Override
	protected void pointerReleased(int x, int y)
	{
		pointerX = x;
		pointerY = y;
		isPointerReleased = true;
		isPointerPressed = false;
		isPointerDragged = false;
	}

	public static void resetPointer()
	{
		isPointerDragged = false;
		isPointerPressed = false;
		isPointerReleased = false;
	}
