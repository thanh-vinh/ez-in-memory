
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.lcdui.Graphics;

/**
 * A very simple font sprite.
 * @date Aug 26, 2011
 */

public class FontSprite extends GameSprite
{
	private static final int INVALID_INDEX 	= -1; 		// A character is not exist in this font
	private static final char SPACE_CHAR 	= 0x20; 	// Space character

	private Hashtable characterTable;					// Hashtable to look up a character in the module list
	private int spacing;
	private int height;
	private int expandedSpacing = 1;
	private int lineSpacing;

	public FontSprite()
	{
		super();
	}

	public FontSprite(byte[] buf, boolean isCacheImages) throws IOException
	{
		super(buf, isCacheImages);
	}

	public void setFontMapping(byte[] buf) throws IOException
	{
		ByteArrayInputStream stream = new ByteArrayInputStream(buf);
		DataInputStream input = new DataInputStream(stream);
		int length = input.readShort();

		// Initialize the Hashtable characterTable
		this.characterTable = new Hashtable(length);
		for (int i = 0; i < length; i++) {
			char c = input.readChar();
			this.characterTable.put(String.valueOf(c), new Integer(i));
		}

		this.spacing = input.readShort();		// Space char length
		this.height = input.readShort();		// Font height
		input.close();
		stream.close();
	}

	private int getCharIndex(char c)
	{
		Integer value = (Integer)this.characterTable.get(String.valueOf(c));	// Find value by key - character c

		if (value != null) {
			return value.intValue();
		} else {
			return INVALID_INDEX;
		}
	}

	public int getCharWidth(char c)
	{
		int index = this.getCharIndex(c);
		if (index != INVALID_INDEX) {
			return super.getModuleWidth(index);
		} else {
			return this.spacing;
		}
	}

	public int getCharHeight()
	{
		return this.height;
	}

	public int getWidth(String text)
	{
		int width = 0;

		if (text != null) {
			for (int i = 0; i < text.length(); i++) {
				char c = text.charAt(i);
				int charWidth = this.getCharWidth(c);
				width += charWidth + this.expandedSpacing;
			}
		}

		return width;
	}

	public void drawCharAt(Graphics g, char c, int x, int y, int anchor)
	{
		if (c != SPACE_CHAR) {
			int index = this.getCharIndex(c);
			if (index != INVALID_INDEX) {
				this.paintModule(g, index, x, y, anchor);
			}
		}
	}

	public void drawString(Graphics g, String text, int x, int y, int anchor)
	{
		if (text == null) return;

		/*if ((anchor & Graphics.LEFT) != 0) {				// LEFT
			Ignore, default is TOP | LEFT
		} else */
		if ((anchor & Graphics.HCENTER) != 0) {				// HCENTER
			x -= this.getWidth(text) >> 1;
		} else if ((anchor & Graphics.RIGHT) != 0) {		// RIGHT
			x -= this.getWidth(text);
		}

		/*if ((anchor & Graphics.TOP) != 0) {				// TOP
			Ignore, default is TOP | LEFT
		} else */
		if ((anchor & Graphics.VCENTER) != 0) {				// VCENTER
			y -= this.getCharHeight() >> 1;
		} else if ((anchor & Graphics.BOTTOM) != 0) {
			y -= this.getCharHeight();
		}

		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			char c1 = c;
			if (i < text.length() - 1) {
				c1 = text.charAt(i + 1);
			}

			if (c == '\\' && Character.isDigit(c1)) {
				int palette = Character.digit(c1, 10);
				super.setPalette(palette);
				i++;
			} else {
				this.drawCharAt(g, c, x, y, 0);
				int charWidth = this.getCharWidth(c);
				x += charWidth + this.expandedSpacing;
			}
		}
	}

	// TODO use short[] instead String[]
	public String[] wrapText(String text, int width)
	{
		Vector abc = new Vector();
		String line = new String();
		String word = new String();
		char c;
		int cWidth = 0, index = 0, w;
		while(text.charAt(index)==' ')
			index++;
		while(index<text.length())
		{
			c = text.charAt(index);
			w = this.getCharWidth(c);
			if (c==' ') {
				line += word;
				word = new String();
				if (cWidth + w > width) {
					abc.addElement(line);
					line = new String();
					cWidth = 0;
				} else {
					line += c;
				}
				cWidth += w;
			} else if (c=='\\' && !Character.isDigit(text.charAt(index+1))){
				line += c + text.charAt(index+1);
				index ++;
			} else {
				if (cWidth + w > width) {
					abc.addElement(line);
					line = new String();
					cWidth = 0;
				}
				word += c;
				cWidth += w;
			}
			index ++;
		}
		// the last char
		if (word.length()>0) {
			line += word;
			abc.addElement(line);
		}
		word = null;
		String wrapedText[] = new String[abc.size()];
		for (int i = 0; i < wrapedText.length; i++) {
			wrapedText[i] = (String)abc.elementAt(i);
		}
		abc = null;

		return wrapedText;
	}

	public void drawPage(Graphics g, String[] lines, int x, int y, int anchor)
	{
		if (lines != null) {
			for (int i = 0; i < lines.length; i++) {
				String line = lines[i];
				this.drawString(g, line, x, y, anchor);
				y += (this.getCharHeight() + this.lineSpacing);
			}
		}
	}
}
