
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Export bitmap font image and generate source code.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * Last updated: October 29, 2011
 */

public class FontImage
{
	private static final Color TRANSPARENT_COLOR 	= Color.MAGENTA;
	private static final String FILE_FORMAT 		= "PNG";
	private static final char SPACE_CHAR 			= ' ';
	private static final int MAX_CHAR_PER_ROW		= 20;

	private Font font;
	private Color color;
	private String input;
	private int charactersCount;
	private Character[] characters;

	private BufferedImage bufferedImage;
	private BufferedImage[] images;

	public FontImage(Font font, Color color, String input)
	{
		this.font = font;
		this.color = color;
		this.input = input;

		this.charactersCount = this.input.length();
		this.characters = new Character[this.charactersCount];
		this.bufferedImage = this.getBufferedImage();
	}

	private BufferedImage getFixedSizeImage(BufferedImage image)
	{
		int width = 0;
		int transparentColor = TRANSPARENT_COLOR.getRGB() | 0xFF000000;	// Set alpha to 0xFF
		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				int color = image.getRGB(x, y) | 0xFF000000;			// Set alpha to 0xFF
				if (color != transparentColor) {
					width = x;
					break;
				}
			}
		}

		BufferedImage bufferedImage = image.getSubimage(0, 0, ++width, image.getHeight());

		return bufferedImage;
	}

	private BufferedImage getSingleCharImage(char c)
	{
		BufferedImage bufferedImage = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_INDEXED);
		Graphics2D g = bufferedImage.createGraphics();
		g.setFont(this.font);
		FontMetrics fm = g.getFontMetrics();

		int width = fm.charWidth(c);
		int height = fm.getHeight();

		fm = null;
		g = null;
		bufferedImage = null;

		bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_INDEXED);
		g = bufferedImage.createGraphics();
		g.setFont(this.font);
		fm = g.getFontMetrics();

		g.setColor(TRANSPARENT_COLOR);
		g.fillRect(0, 0, width, height);
		g.setColor(this.color);
		g.drawString(String.valueOf(c), 0, fm.getAscent());

		fm = null;
		g = null;

		BufferedImage fixedSizeImage = this.getFixedSizeImage(bufferedImage);
		bufferedImage = null;

		return fixedSizeImage;
	}

	private BufferedImage getBufferedImage()
	{
		// Create all character images
		this.images = new BufferedImage[this.charactersCount];
		for (int i = 0; i < this.charactersCount; i++) {
			this.images[i] = this.getSingleCharImage(this.input.charAt(i));
		}

		// Get the width & height of large image (merge all character images)
		int rowLength = (this.charactersCount / MAX_CHAR_PER_ROW) << 1;
		int width = 0, height = (this.images[0].getHeight() * rowLength);
		for (int i = 0; i < this.charactersCount; i++) {
			width += this.images[i].getWidth();
		}

		// Merge all images
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_INDEXED);
		Graphics2D g = bufferedImage.createGraphics();
		g.setColor(TRANSPARENT_COLOR);
		g.fillRect(0, 0, width, height);

		int x = 0, y = 0;
		for (int i = 0; i < this.charactersCount; i++) {
			this.characters[i] = new Character(x, y,
				this.images[i].getWidth(), this.images[i].getHeight(), this.input.charAt(i));

			g.drawImage(this.images[i], null, x, y);
			x += this.images[i].getWidth();

			if (i > 0 && i % MAX_CHAR_PER_ROW == 0) {
				x = 0;
				y += this.images[i].getHeight();
			}
		}

		return bufferedImage;
	}

	private void setTransparent(BufferedImage bufferedImage)
	{
		int transparentColor = TRANSPARENT_COLOR.getRGB() | 0xFF000000;	// Set alpha to 0xFF
		for (int x = 0; x < bufferedImage.getWidth(); x++) {
			for (int y = 0; y < bufferedImage.getHeight(); y++) {
				int color = bufferedImage.getRGB(x, y) | 0xFF000000;	// Set alpha to 0xFF
				if (color == transparentColor) {
					bufferedImage.setRGB(x, y, color & 0x00FFFFFF);		// Set alpha to 0x00
				}
			}
		}
	}

	public Character[] getCharacters()
	{
		return this.characters;
	}

	public int getSpaceCharWidth()
	{
		Graphics2D g = this.bufferedImage.createGraphics();
		g.setFont(this.font);
		FontMetrics fm = g.getFontMetrics();

		int width = fm.charWidth(SPACE_CHAR);
		fm = null;
		g = null;

		return width;
	}

	public int getCharHeight()
	{
		Graphics2D g = this.bufferedImage.createGraphics();
		g.setFont(this.font);
		FontMetrics fm = g.getFontMetrics();

		int height = fm.getHeight();
		return height;
	}

	/**
	 * Save output image.
	 * @since 0.0.2
	 */
	public void writeImage(String fileName, boolean transparent) throws IOException
	{
		if (transparent) {
			this.setTransparent(this.bufferedImage);
		}

		ImageIO.write(this.bufferedImage, FILE_FORMAT, new File(fileName));
	}
}
