
REM MsBuild to compile project
REM Using: make.bat project-file configuration
REM Example: make.bat Windows debug

@echo off

echo ##
echo # Setting some common variables...
echo ##
call ..\config.bat

REM Relative paths
set MASTER_SRC_PATH=%PROJECT_DIR%
set BUILD_PATH=%PROJECT_DIR%\.version
set RELEASE_PATH=%PROJECT_DIR%\.release
set DOCS_PATH=%PROJECT_DIR%\.docs

REM Windows studio project file extension
set PROJECT_FILE_EXT=csproj

set CONFIGURATION=%1%
set PROJECT_FILE=%2%

if "%CONFIGURATION%"=="" (
	set CONFIGURATION=debug
)
if "%PROJECT_FILE%"=="" (
	set PROJECT_FILE=Xna2Android
)
set PROJECT_FILE=%PROJECT_FILE%.%PROJECT_FILE_EXT%

echo ##
echo # Building %PROJECT_NAME%...
echo ##
echo.

:init
cls
pushd %PROJECT_DIR%

if "%CONFIGURATION%"=="clean" (
	goto clean
) else if "%CONFIGURATION%"=="run" (
	goto run
) else (
	goto compile
)

:compile
echo.
echo ##
echo # Compile source...
echo ##
echo Project file: %PROJECT_FILE%
echo Configuration: %CONFIGURATION%

call MsBuild.exe %MASTER_SRC_PATH%\%PROJECT_FILE% /t:Rebuild /p:Configuration=%CONFIGURATION%
if "%CONFIGURATION%"=="release" (
	echo Using Eazfuscator.NET to obfuscate...
	cd %RELEASE_PATH%
	if exist %PROJECT_NAME%.exe (
		call %EAZFUSCATOR_EXEC% %PROJECT_NAME%.exe
	)
)
goto finish

:run
echo.
echo ##
echo # Launch %PROJECT_NAME%.exe...
echo ##
REM call %RELEASE_PATH%\%PROJECT_NAME%.exe
cd %RELEASE_PATH%
call %PROJECT_NAME%.exe
goto finish

:clean
echo.
echo ##
echo # Cleanup...
echo ##
rmdir %BUILD_PATH% /s /q
rmdir %RELEASE_PATH% /s /q

goto finish

:error

:finish
popd
echo.
echo ##
echo # Compeleted!
echo ##
