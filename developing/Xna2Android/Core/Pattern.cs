﻿/*
 * Created by SharpDevelop.
 * User: Thanh Vinh
 * Date: 2/19/2012
 * Time: 11:18 AM
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;

namespace Xna2Android.Core
{
	/// <summary>
	/// Description of Pattern.
	/// </summary>
	public static class Pattern
	{
		private const string PATTERN_FILE 			= "pattern.xml";

		private const string PATTERN_NODE         	= "pattern";
		private const string PACKAGE_NODE			= "package";
        private const string REMOVE_NODE			= "remove";
        private const string REPLACE_NODE			= "replace";
        private const string XNA_ATTRIBUTE			= "xna";
        private const string ANDROID_ATTRIBUTE		= "android";

		private static List<string> invalid;	// Remove
		private static List<string> package;	// Remove
		private static List<string> xna;		// Replace, input
		private static List<string> android;	// Replace, output


		public static void Load()
		{
			invalid = new List<string>();
			package = new List<string>();
			xna = new List<string>();
			android = new List<string>();

			//
			// Parse XML
			//
			XmlReader reader = XmlReader.Create(PATTERN_FILE);

			while (reader.Read()) {
				if (reader.NodeType == XmlNodeType.Element && reader.Name == PACKAGE_NODE)	{
					package.Add(reader.GetAttribute(ANDROID_ATTRIBUTE));
				} else if (reader.NodeType == XmlNodeType.Element && reader.Name == REMOVE_NODE)	{
					invalid.Add(reader.GetAttribute(XNA_ATTRIBUTE));
				} else if (reader.NodeType == XmlNodeType.Element && reader.Name == REPLACE_NODE) {
					xna.Add(reader.GetAttribute(XNA_ATTRIBUTE));
					android.Add(reader.GetAttribute(ANDROID_ATTRIBUTE));
				}
			}
		}

		public static void AddPackage(ref string value)
		{
			for (int i = 0; i < package.Count; i++) {
				string pattern = package[i];
				value = pattern + "\n" + value;
			}
		}

		public static void Remove(ref string value)
		{
			for (int i = 0; i < invalid.Count; i++) {
				string pattern = invalid[i];
				string replacement = "";
				value = Regex.Replace(value, pattern, replacement);
			}
		}

		public static void Replace(ref string value)
		{
			for (int i = 0; i < android.Count; i++) {
				string pattern = xna[i];
				string replacement = android[i];
				value = Regex.Replace(value, pattern, replacement);
			}
		}
	}
}
